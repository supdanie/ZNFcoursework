<?php

namespace Test;

use App\Model\CompanyManager;
use App\Model\DeliveryNoteManager;
use App\Model\NoDataFoundException;
use App\Model\OrderItemManager;
use App\Model\OrderManager;
use App\Model\ProductTypeManager;
use App\Model\UserManager;
use Nette;
use Tester;
use Tester\Assert;
require __DIR__ . '/../bootstrap.php';
require_once __DIR__."/../../app/model/NoDataFoundException.php";
require_once __DIR__."/../../app/model/BaseManager.php";
require_once __DIR__."/../../app/model/UserManager.php";
require_once __DIR__."/../../app/model/CompanyManager.php";
require_once __DIR__."/../../app/model/OrderManager.php";
require_once __DIR__."/../../app/model/ProductTypeManager.php";
require_once __DIR__."/../../app/model/OrderItemManager.php";
require_once __DIR__."/../../app/model/DeliveryNoteManager.php";

/**
 * Class OrderTest
 * @package Test
 * @testCase
 */
class OrderTest extends Tester\TestCase
{
    private $maxUserID;
    private $maxID;
    private $maxOrderID;
    /** @var  DeliveryNoteManager */
    private $deliveryNoteManager;
    private static $dbContext = NULL;
    private static $cacheStorage = NULL;

    /** @return Nette\Caching\Storages\FileStorage */
    public static function getCacheStorage()
    {
        if (self::$cacheStorage === NULL) {
            self::$cacheStorage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp');
        }
        return self::$cacheStorage;
    }

    /** @return Nette\Database\Context */
    public static function getDbContext()
    {
        if (self::$dbContext === NULL) {
            $connection = new Nette\Database\Connection('pgsql:host=127.0.0.1;dbname=crm_supdanie', 'svm', 'svm');
            $structure = new Nette\Database\Structure($connection, self::getCacheStorage());
            $conventions = new Nette\Database\Conventions\DiscoveredConventions($structure);
            self::$dbContext = new Nette\Database\Context($connection, $structure, $conventions, self::getCacheStorage());
        }
        return self::$dbContext;
    }

    /** public function testAddOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');

    }

    public function testAcceptOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');

    }

    public function testExpediteOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
    }

    public function testPayOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
    }

    public function testCloseOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
    }

    public function testGiveToProduction(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
    }

    public function testProduce(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
    } */

    public function setUp()
    {
    }

    public function tearDown()
    {
        //self::$cacheStorage->clean([Nette\Caching\Cache::ALL => true]);
    }
}


$test = new OrderTest();
$test->run();
