--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--


CREATE TABLE IF NOT EXISTS companies (
    id integer DEFAULT nextval(('public."companies_id_seq"'::text)::regclass) NOT NULL,
    companyname character varying(50) NOT NULL,
    ico character varying(15) NOT NULL,
    adress1 character varying(50),
    adress2 character varying(50),
    zip text,
    url character varying(50),
    mail character varying(50),
    phone character varying(15),
    phonegsm character varying(15),
    phonefax character varying(15),
    dic character varying(25),
    bankacount character varying(25),
    bankcode character varying(4),
    provider boolean,
    deleted boolean DEFAULT false NOT NULL,
    customer boolean,
    adress12 text,
    adress22 text,
    zip2 text,
    contact text
);


ALTER TABLE public.companies OWNER TO svm;

--
-- Name: deliverynotes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS deliverynotes (
    id integer NOT NULL,
    productorderid integer,
    companyid integer NOT NULL,
    status integer,
    transactiondate timestamp without time zone DEFAULT now() NOT NULL,
    userid integer NOT NULL,
    transportoutcomeid integer,
    transportcode text,
    resolutiondate date,
    userid2 integer,
    transportnote text,
    invoiceid text,
    invoicetype integer,
    invoicedate date,
    invoiceuserid integer,
    paymentdate date,
    paid boolean DEFAULT false NOT NULL,
    paiddate date,
    paiduserid integer,
    paidnote text,
    deleted boolean DEFAULT false
);


ALTER TABLE public.deliverynotes OWNER TO svm;

--
-- Name: items; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS items (
    id integer,
    groupid integer,
    name text
);


ALTER TABLE public.items OWNER TO svm;

--
-- Name: locations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS locations (
    id integer NOT NULL,
    locationname character varying(100)
);


ALTER TABLE public.locations OWNER TO svm;

--
-- Name: productcategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS productcategory (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.productcategory OWNER TO svm;

SET default_with_oids = true;

--
-- Name: productorderitem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS productorderitem (
    id integer,
    name text,
    quantity double precision,
    productorderid integer,
    price double precision,
    currency character varying(20) DEFAULT 'CZK'::character varying NOT NULL,
    productid integer
);


ALTER TABLE public.productorderitem OWNER TO svm;

SET default_with_oids = false;

--
-- Name: productorders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS productorders (
    id integer NOT NULL,
    orderstatus integer,
    companyid integer,
    companyordernumber text,
    ordertype integer,
    recievedate date,
    recieverid integer,
    orderfile text,
    approved boolean,
    approveddate date,
    approveduserid integer,
    approvednote text,
    paidnote text,
    productiondeadline date,
    productionstatus integer,
    closed boolean DEFAULT false NOT NULL,
    expeditionuserid integer,
    expeditiondate date,
    expeditionreciever text,
    expeditiondate2 date,
    expeditionuserid2 integer,
    invoiceuserid integer,
    invoiceid text,
    invoiceprice double precision,
    invoicetype integer,
    invoicedate date,
    paid integer,
    paiddate date,
    paiduserid integer,
    paymentdate date,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.productorders OWNER TO svm;

SET default_with_oids = true;

--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS products (
    id integer NOT NULL,
    producttypeid integer NOT NULL,
    procuctcomment text,
    statusid integer,
    barcoderecipientid integer,
    created date,
    locationid integer,
    ownerid integer,
    counter integer
);


ALTER TABLE public.products OWNER TO svm;

--
-- Name: productsondeliverynote; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS productsondeliverynote (
    id integer NOT NULL,
    productid integer,
    deliverynoteid integer NOT NULL,
    productname text,
    productquantity integer,
    unitprice double precision,
    inevidence integer
);


ALTER TABLE public.productsondeliverynote OWNER TO svm;

--
-- Name: productstatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS  productstatus (
    id integer NOT NULL,
    statusname character varying(100)
);


ALTER TABLE public.productstatus OWNER TO svm;

--
-- Name: producttypes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS producttypes (
    id integer DEFAULT nextval(('public."material_products_seq"'::text)::regclass) NOT NULL,
    name character varying(100),
    serialnumber character varying(50),
    version character varying(15),
    productcomment character varying(100),
    barcodetitle character varying(20),
    price numeric,
    inevidence integer DEFAULT 1 NOT NULL,
    quantity integer DEFAULT 1 NOT NULL,
    units character varying(10),
    unit integer,
    authorized integer,
    status integer,
    deleted boolean DEFAULT false NOT NULL,
    servicegroupid integer,
    service boolean DEFAULT false NOT NULL,
    quantity2 numeric,
    price2 numeric,
    productcategoryid integer,
    pricelist boolean DEFAULT true NOT NULL,
    author integer
);


ALTER TABLE public.producttypes OWNER TO svm;

SET default_with_oids = false;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE IF NOT EXISTS users (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    surname character varying(50) NOT NULL,
    username character varying(20) NOT NULL,
    userpassword character varying(150),
    email text,
    deleted boolean DEFAULT false NOT NULL,
    role text DEFAULT 'customer'::text NOT NULL,
    companyid integer
);


ALTER TABLE public.users OWNER TO svm;

--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY companies (id, companyname, ico, adress1, adress2, zip, url, mail, phone, phonegsm, phonefax, dic, bankacount, bankcode, provider, deleted, customer, adress12, adress22, zip2, contact) FROM stdin;
\.


--
-- Data for Name: deliverynotes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY deliverynotes (id, productorderid, companyid, status, transactiondate, userid, transportoutcomeid, transportcode, resolutiondate, userid2, transportnote, invoiceid, invoicetype, invoicedate, invoiceuserid, paymentdate, paid, paiddate, paiduserid, paidnote, deleted) FROM stdin;
\.


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY items (id, groupid, name) FROM stdin;
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY locations (id, locationname) FROM stdin;
\.


--
-- Data for Name: productcategory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productcategory (id, name) FROM stdin;
\.


--
-- Data for Name: productorderitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productorderitem (id, name, quantity, productorderid, price, currency, productid) FROM stdin;
\.


--
-- Data for Name: productorders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productorders (id, orderstatus, companyid, companyordernumber, ordertype, recievedate, recieverid, orderfile, approved, approveddate, approveduserid, approvednote, paidnote, productiondeadline, productionstatus, closed, expeditionuserid, expeditiondate, expeditionreciever, expeditiondate2, expeditionuserid2, invoiceuserid, invoiceid, invoiceprice, invoicetype, invoicedate, paid, paiddate, paiduserid, paymentdate, deleted) FROM stdin;
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products (id, producttypeid, procuctcomment, statusid, barcoderecipientid, created, locationid, ownerid, counter) FROM stdin;
\.


--
-- Data for Name: productsondeliverynote; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productsondeliverynote (id, productid, deliverynoteid, productname, productquantity, unitprice, inevidence) FROM stdin;
\.


--
-- Data for Name: productstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productstatus (id, statusname) FROM stdin;
\.


--
-- Data for Name: producttypes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY producttypes (id, name, serialnumber, version, productcomment, barcodetitle, price, inevidence, quantity, units, unit, authorized, status, deleted, servicegroupid, service, quantity2, price2, productcategoryid, pricelist, author) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, name, surname, username, userpassword, email, deleted, role, companyid) FROM stdin;
\.


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: deliverynotes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_pkey PRIMARY KEY (id);


--
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: productcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productcategory
    ADD CONSTRAINT productcategory_pkey PRIMARY KEY (id);


--
-- Name: productorder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT productorder_pkey PRIMARY KEY (id);


--
-- Name: products_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey1 PRIMARY KEY (id);


--
-- Name: productsondeliverynote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productsondeliverynote
    ADD CONSTRAINT productsondeliverynote_pkey PRIMARY KEY (id);


--
-- Name: productstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productstatus
    ADD CONSTRAINT productstatus_pkey PRIMARY KEY (id);


--
-- Name: producttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY producttypes
    ADD CONSTRAINT producttypes_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: deliverynotes_approveduserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_approveduserid_fkey FOREIGN KEY (approveduserid) REFERENCES users(id);


--
-- Name: deliverynotes_companyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_companyid_fkey FOREIGN KEY (companyid) REFERENCES companies(id);


--
-- Name: deliverynotes_companyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_companyid_fkey FOREIGN KEY (companyid) REFERENCES companies(id);


--
-- Name: deliverynotes_expeditionuserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_expeditionuserid_fkey FOREIGN KEY (expeditionuserid) REFERENCES users(id);


--
-- Name: deliverynotes_invoiceuserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_invoiceuserid_fkey FOREIGN KEY (invoiceuserid) REFERENCES users(id);


--
-- Name: deliverynotes_paiduserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_paiduserid_fkey FOREIGN KEY (paiduserid) REFERENCES users(id);


--
-- Name: deliverynotes_paiduserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_paiduserid_fkey FOREIGN KEY (paiduserid) REFERENCES users(id);


--
-- Name: deliverynotes_productorderid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorderitem
    ADD CONSTRAINT deliverynotes_productorderid_fkey FOREIGN KEY (productorderid) REFERENCES productorders(id);


--
-- Name: deliverynotes_recieverid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_recieverid_fkey FOREIGN KEY (recieverid) REFERENCES users(id);


--
-- Name: deliverynotes_userid2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_userid2_fkey FOREIGN KEY (userid2) REFERENCES users(id);


--
-- Name: deliverynotes_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_userid_fkey FOREIGN KEY (userid) REFERENCES users(id);


--
-- Name: deliverynotesproductid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productorderitem
    ADD CONSTRAINT deliverynotesproductid_fkey FOREIGN KEY (productid) REFERENCES products(id);


--
-- Name: products_barcoderecipientid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_barcoderecipientid_fkey FOREIGN KEY (barcoderecipientid) REFERENCES users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: products_locationid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_locationid_fkey FOREIGN KEY (locationid) REFERENCES locations(id);


--
-- Name: products_ownerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_ownerid_fkey FOREIGN KEY (ownerid) REFERENCES users(id);


--
-- Name: products_producttypeid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_producttypeid_fkey FOREIGN KEY (producttypeid) REFERENCES producttypes(id);


--
-- Name: products_statusid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_statusid_fkey FOREIGN KEY (statusid) REFERENCES productstatus(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: productsondeliverynote_deliverynoteid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productsondeliverynote
    ADD CONSTRAINT productsondeliverynote_deliverynoteid_fkey FOREIGN KEY (deliverynoteid) REFERENCES deliverynotes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: productsondeliverynote_productid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productsondeliverynote
    ADD CONSTRAINT productsondeliverynote_productid_fkey FOREIGN KEY (productid) REFERENCES products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: producttypes_productcategoryid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producttypes
    ADD CONSTRAINT producttypes_productcategoryid_fkey FOREIGN KEY (productcategoryid) REFERENCES productcategory(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: users_companyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_companyid_fkey FOREIGN KEY (companyid) REFERENCES companies(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: svm
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM svm;
GRANT ALL ON SCHEMA public TO svm;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: companies; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE companies FROM PUBLIC;
REVOKE ALL ON TABLE companies FROM postgres;
GRANT ALL ON TABLE companies TO postgres;
GRANT ALL ON TABLE companies TO svm;


--
-- Name: deliverynotes; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE deliverynotes FROM PUBLIC;
REVOKE ALL ON TABLE deliverynotes FROM postgres;
GRANT ALL ON TABLE deliverynotes TO postgres;
GRANT ALL ON TABLE deliverynotes TO svm;


--
-- Name: items; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE items FROM PUBLIC;
REVOKE ALL ON TABLE items FROM postgres;
GRANT ALL ON TABLE items TO postgres;
GRANT ALL ON TABLE items TO svm;


--
-- Name: locations; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE locations FROM PUBLIC;
REVOKE ALL ON TABLE locations FROM postgres;
GRANT ALL ON TABLE locations TO postgres;
GRANT ALL ON TABLE locations TO svm;


--
-- Name: productcategory; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE productcategory FROM PUBLIC;
REVOKE ALL ON TABLE productcategory FROM postgres;
GRANT ALL ON TABLE productcategory TO postgres;
GRANT ALL ON TABLE productcategory TO svm;


--
-- Name: productorderitem; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE productorderitem FROM PUBLIC;
REVOKE ALL ON TABLE productorderitem FROM postgres;
GRANT ALL ON TABLE productorderitem TO postgres;
GRANT ALL ON TABLE productorderitem TO svm;


--
-- Name: productorders; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE productorders FROM PUBLIC;
REVOKE ALL ON TABLE productorders FROM postgres;
GRANT ALL ON TABLE productorders TO postgres;
GRANT ALL ON TABLE productorders TO svm;


--
-- Name: products; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE products FROM PUBLIC;
REVOKE ALL ON TABLE products FROM postgres;
GRANT ALL ON TABLE products TO postgres;
GRANT ALL ON TABLE products TO svm;


--
-- Name: productsondeliverynote; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE productsondeliverynote FROM PUBLIC;
REVOKE ALL ON TABLE productsondeliverynote FROM postgres;
GRANT ALL ON TABLE productsondeliverynote TO postgres;
GRANT ALL ON TABLE productsondeliverynote TO svm;


--
-- Name: productstatus; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE productstatus FROM PUBLIC;
REVOKE ALL ON TABLE productstatus FROM postgres;
GRANT ALL ON TABLE productstatus TO postgres;
GRANT ALL ON TABLE productstatus TO svm;


--
-- Name: producttypes; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE producttypes FROM PUBLIC;
REVOKE ALL ON TABLE producttypes FROM postgres;
GRANT ALL ON TABLE producttypes TO postgres;
GRANT ALL ON TABLE producttypes TO svm;


--
-- Name: users; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM postgres;
GRANT ALL ON TABLE users TO postgres;
GRANT ALL ON TABLE users TO svm;


--
-- PostgreSQL database dump complete
--

