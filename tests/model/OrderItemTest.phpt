<?php

namespace Test;

use App\Model\CompanyManager;
use App\Model\DeliveryNoteManager;
use App\Model\NoDataFoundException;
use App\Model\OrderItemManager;
use App\Model\OrderManager;
use App\Model\ProductTypeManager;
use App\Model\UserManager;
use Nette;
use Tester;
use Tester\Assert;
require __DIR__ . '/../bootstrap.php';
require_once __DIR__."/../../app/model/NoDataFoundException.php";
require_once __DIR__."/../../app/model/BaseManager.php";
require_once __DIR__."/../../app/model/UserManager.php";
require_once __DIR__."/../../app/model/CompanyManager.php";
require_once __DIR__."/../../app/model/OrderManager.php";
require_once __DIR__."/../../app/model/ProductTypeManager.php";
require_once __DIR__."/../../app/model/OrderItemManager.php";
require_once __DIR__."/../../app/model/DeliveryNoteManager.php";

/**
 * Class OrderItemTest
 * @package Test
 * @testCase
 */

class OrderItemTest extends Tester\TestCase
{
    private $maxID;
    private $maxProductID;
    private $maxOrderID;
    /** @var  OrderItemManager */
    private $orderItemManager;
    private static $dbContext = NULL;
    private static $cacheStorage = NULL;

    /** @return Nette\Caching\Storages\FileStorage */
    public static function getCacheStorage()
    {
        if (self::$cacheStorage === NULL) {
            self::$cacheStorage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp');
        }
        return self::$cacheStorage;
    }

    /** @return Nette\Database\Context */
    public static function getDbContext()
    {
        if (self::$dbContext === NULL) {
            $connection = new Nette\Database\Connection('pgsql:host=127.0.0.1;dbname=crm_supdanie', 'svm', 'svm');
            $structure = new Nette\Database\Structure($connection, self::getCacheStorage());
            $conventions = new Nette\Database\Conventions\DiscoveredConventions($structure);
            self::$dbContext = new Nette\Database\Context($connection, $structure, $conventions, self::getCacheStorage());
        }
        return self::$dbContext;
    }

    public function setUp()
    {

        $userManager = new UserManager(self::getDbContext());
        $companyManager = new CompanyManager(self::getDbContext());
        $orderManager = new OrderManager(self::getDbContext(), $companyManager, $userManager);
        $productTypeManager = new ProductTypeManager(self::getDbContext());
        $this->orderItemManager = new OrderItemManager(self::getDbContext(), $productTypeManager, $orderManager);
        $allItems = $this->orderItemManager->getAll();
        foreach($allItems as $item){
            if(!isset($this->maxID)){
                $this->maxID = $item[OrderItemManager::COLUMN_ID];
            } elseif($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                $this->maxID = $item[OrderItemManager::COLUMN_ID];
            }
        }
        $allOrders = $orderManager->getAll();
        foreach($allOrders as $order){
            if(!isset($this->maxOrderID)){
                $this->maxOrderID = $order[OrderManager::COLUMN_ID];
            } elseif($this->maxOrderID < $order[OrderManager::COLUMN_ID]){
                $this->maxOrderID = $order[OrderManager::COLUMN_ID];
            }
        }

        $allProducts = $productTypeManager->getAll();
        foreach($allProducts  as $product){
            if(!isset($this->maxProductID)){
                $this->maxProductID = $product[ProductTypeManager::COLUMN_ID];
            } elseif($this->maxProductID < $product[ProductTypeManager::COLUMN_ID]){
                $this->maxProductID = $product[ProductTypeManager::COLUMN_ID];
            }
        }
    }

    public function testAddItemOnOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder([], -1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID+1,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], -1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -1, "price" => -1, "currency" => 1], $this->maxOrderID);
        }, Nette\Neon\Exception::class);
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -1, "price" => 1, "currency" => 1], $this->maxOrderID);
        }, Nette\Neon\Exception::class);
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID+1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 8], $this->maxOrderID);
        }, Nette\Neon\Exception::class);
        Assert::noError(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
        });
    }

    public function testEditItemOnOrder(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID+1, ["product" => $this->maxProductID+1,
                "name" => "Product", "quantity" => 2, "price" => 1, "currency" => 1], $this->maxOrderID);
        }, NoDataFoundException::class);

        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID, ["product" => $this->maxProductID+1,
                "name" => "Product", "quantity" => -2, "price" => 1, "currency" => 1], $this->maxOrderID);
        }, NoDataFoundException::class);

        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -2, "price" => 1, "currency" => 9], $this->maxOrderID);
        }, Nette\Neon\Exception::class);

        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -2, "price" => -1, "currency" => 1], $this->maxOrderID);
        }, Nette\Neon\Exception::class);

        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 2, "price" => 1, "currency" => 1], $this->maxOrderID+1);
        }, NoDataFoundException::class);

        Assert::error(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -2, "price" => 1, "currency" => 1], $this->maxOrderID);
        }, Nette\Neon\Exception::class);

        Assert::noError(function(){
            $this->orderItemManager->addItemOnOrder(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1, "currency" => 1], $this->maxOrderID);
            $maxID = -1;
            $allItems = $this->orderItemManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[OrderItemManager::COLUMN_ID];
                }
            }
            $this->orderItemManager->editItemOnOrder($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 2, "price" => 1, "currency" => 1], $this->maxOrderID);
        });

    }

	public function tearDown()
	{
        $allItems = $this->orderItemManager->getAll();
        foreach($allItems as $item) {
            if ($item[OrderItemManager::COLUMN_ID] > $this->maxID) {
                $this->orderItemManager->remove($item[OrderItemManager::COLUMN_ID]);
            }
        }
        self::$cacheStorage->clean([Nette\Caching\Cache::ALL => true]);


	}
}


$test = new OrderItemTest();
$test->run();
