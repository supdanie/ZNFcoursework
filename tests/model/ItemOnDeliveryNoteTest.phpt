<?php

namespace Test;

use App\Model\CompanyManager;
use App\Model\DeliveryNoteManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\NoDataFoundException;
use App\Model\OrderItemManager;
use App\Model\OrderManager;
use App\Model\ProductTypeManager;
use App\Model\UserManager;
use Nette;
use Tester;
use Tester\Assert;
require __DIR__ . '/../bootstrap.php';
require_once __DIR__."/../../app/model/NoDataFoundException.php";
require_once __DIR__."/../../app/model/BaseManager.php";
require_once __DIR__."/../../app/model/UserManager.php";
require_once __DIR__."/../../app/model/CompanyManager.php";
require_once __DIR__."/../../app/model/OrderManager.php";
require_once __DIR__."/../../app/model/ProductTypeManager.php";
require_once __DIR__."/../../app/model/OrderItemManager.php";
require_once __DIR__."/../../app/model/DeliveryNoteManager.php";
require_once __DIR__."/../../app/model/ItemsOnDeliveryNoteManager.php";

/**
 * Class ItemOnDeliveryNoteTest
 * @package Test
 * @testCase
 */
class ItemOnDeliveryNoteTest extends Tester\TestCase
{
    private $maxID;
    private $maxProductID;
    private $maxDeliveryNoteID;
    /** @var  ItemsOnDeliveryNoteManager */
    private $itemsOnDeliveryNoteManager;
    private static $dbContext = NULL;
    private static $cacheStorage = NULL;

    /** @return Nette\Caching\Storages\FileStorage */
    public static function getCacheStorage()
    {
        if (self::$cacheStorage === NULL) {
            self::$cacheStorage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp');
        }
        return self::$cacheStorage;
    }

    /** @return Nette\Database\Context */
    public static function getDbContext()
    {
        if (self::$dbContext === NULL) {
            $connection = new Nette\Database\Connection('pgsql:host=127.0.0.1;dbname=crm_supdanie', 'svm', 'svm');
            $structure = new Nette\Database\Structure($connection, self::getCacheStorage());
            $conventions = new Nette\Database\Conventions\DiscoveredConventions($structure);
            self::$dbContext = new Nette\Database\Context($connection, $structure, $conventions, self::getCacheStorage());
        }
        return self::$dbContext;
    }

    public function setUp()
    {
        $userManager = new UserManager(self::getDbContext());
        $companyManager = new CompanyManager(self::getDbContext());
        $orderManager = new OrderManager(self::getDbContext(), $companyManager, $userManager);
        $productTypeManager = new ProductTypeManager(self::getDbContext());
        $orderItemManager = new OrderItemManager(self::getDbContext(), $productTypeManager, $orderManager);
        $deliveryNoteManager = new DeliveryNoteManager(self::getDbContext(),$orderManager, $orderItemManager, $userManager);
        $this->itemsOnDeliveryNoteManager = new ItemsOnDeliveryNoteManager(self::getDbContext(), $productTypeManager, $deliveryNoteManager);
        $allItems = $this->itemsOnDeliveryNoteManager->getAll();
        foreach($allItems as $item){
            if(!isset($this->maxID)){
                $this->maxID = $item[OrderItemManager::COLUMN_ID];
            } elseif($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                $this->maxID = $item[OrderItemManager::COLUMN_ID];
            }
        }
        $allDeliveryNotes = $deliveryNoteManager->getAll();
        foreach($allDeliveryNotes as $deliveryNote){
            if(!isset($this->maxDeliveryNoteID)){
                $this->maxDeliveryNoteID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
            } elseif($this->maxDeliveryNoteID < $deliveryNote[DeliveryNoteManager::COLUMN_ID]){
                $this->maxDeliveryNoteID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
            }
        }

        $allProducts = $productTypeManager->getAll();
        foreach($allProducts  as $product){
            if(!isset($this->maxProductID)){
                $this->maxProductID = $product[ProductTypeManager::COLUMN_ID];
            } elseif($this->maxProductID < $product[ProductTypeManager::COLUMN_ID]){
                $this->maxProductID = $product[ProductTypeManager::COLUMN_ID];
            }
        }
    }

    public function testAddItemOnDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote([], -1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID+1,
                "name" => "Product", "quantity" => 1, "price" => 1], -1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -1, "price" => -1], $this->maxDeliveryNoteID);
        }, Nette\Neon\Exception::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -1, "price" => 1], $this->maxDeliveryNoteID);
        }, Nette\Neon\Exception::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID+1);
        }, NoDataFoundException::class);
        Assert::noError(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID);
        });
    }

    public function testEditItemOnDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID);
            $maxID = -1;
            $allItems = $this->itemsOnDeliveryNoteManager->getAll();
            foreach($allItems as $item){
                if($item[OrderItemManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->itemsOnDeliveryNoteManager->editItemOnDeliveryNote($maxID+1, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 2, "price" => 1], $this->maxDeliveryNoteID);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID);
            $maxID = -1;
            $allItems = $this->itemsOnDeliveryNoteManager->getAll();
            foreach($allItems as $item){
                if($item[ItemsOnDeliveryNoteManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->itemsOnDeliveryNoteManager->editItemOnDeliveryNote($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => -2, "price" => 1], $this->maxDeliveryNoteID);
        }, Nette\Neon\Exception::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID);
            $maxID = -1;
            $allItems = $this->itemsOnDeliveryNoteManager->getAll();
            foreach($allItems as $item){
                if($item[ItemsOnDeliveryNoteManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->itemsOnDeliveryNoteManager->editItemOnDeliveryNote($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 2, "price" => -1], $this->maxDeliveryNoteID);
        }, Nette\Neon\Exception::class);
        Assert::error(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID);
            $maxID = -1;
            $allItems = $this->itemsOnDeliveryNoteManager->getAll();
            foreach($allItems as $item){
                if($item[ItemsOnDeliveryNoteManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->itemsOnDeliveryNoteManager->editItemOnDeliveryNote($maxID, ["product" => $this->maxProductID+1,
                "name" => "Product", "quantity" => 2, "price" => 1], $this->maxDeliveryNoteID);
        }, NoDataFoundException::class);
        Assert::noError(function(){
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote(["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 1, "price" => 1], $this->maxDeliveryNoteID);
            $maxID = -1;
            $allItems = $this->itemsOnDeliveryNoteManager->getAll();
            foreach($allItems as $item){
                if($item[ItemsOnDeliveryNoteManager::COLUMN_ID] > $this->maxID){
                    $maxID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->itemsOnDeliveryNoteManager->editItemOnDeliveryNote($maxID, ["product" => $this->maxProductID,
                "name" => "Product", "quantity" => 2, "price" => 1], $this->maxDeliveryNoteID);
        });
    }

    public function tearDown()
    {
        $allItems = $this->itemsOnDeliveryNoteManager->getAll();
        foreach($allItems as $item){
            if($item[ItemsOnDeliveryNoteManager::COLUMN_ID] > $this->maxID){
                $this->itemsOnDeliveryNoteManager->remove($item[ItemsOnDeliveryNoteManager::COLUMN_ID]);
            }
        }
        self::$cacheStorage->clean([Nette\Caching\Cache::ALL => true]);
    }
}


$test = new ItemOnDeliveryNoteTest();
$test->run();
