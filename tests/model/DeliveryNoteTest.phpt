<?php

namespace Test;

use App\Model\CompanyManager;
use App\Model\DeliveryNoteManager;
use App\Model\NoDataFoundException;
use App\Model\OrderItemManager;
use App\Model\OrderManager;
use App\Model\ProductTypeManager;
use App\Model\UserManager;
use Nette;
use Tester;
use Tester\Assert;
require __DIR__ . '/../bootstrap.php';
require_once __DIR__."/../../app/model/NoDataFoundException.php";
require_once __DIR__."/../../app/model/BaseManager.php";
require_once __DIR__."/../../app/model/UserManager.php";
require_once __DIR__."/../../app/model/CompanyManager.php";
require_once __DIR__."/../../app/model/OrderManager.php";
require_once __DIR__."/../../app/model/ProductTypeManager.php";
require_once __DIR__."/../../app/model/OrderItemManager.php";
require_once __DIR__."/../../app/model/DeliveryNoteManager.php";

/**
 * Class DeliveryNoteTest
 * @package Test
 * @testCase
 */
class DeliveryNoteTest extends Tester\TestCase
{
    private $maxUserID;
    private $maxID;
    private $maxOrderID;
    /** @var  DeliveryNoteManager */
    private $deliveryNoteManager;
    private static $dbContext = NULL;
    private static $cacheStorage = NULL;

    /** @return Nette\Caching\Storages\FileStorage */
    public static function getCacheStorage()
    {
        if (self::$cacheStorage === NULL) {
            self::$cacheStorage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp');
        }
        return self::$cacheStorage;
    }

    /** @return Nette\Database\Context */
    public static function getDbContext()
    {
        if (self::$dbContext === NULL) {
            $connection = new Nette\Database\Connection('pgsql:host=127.0.0.1;dbname=crm_supdanie', 'svm', 'svm');
            $structure = new Nette\Database\Structure($connection, self::getCacheStorage());
            $conventions = new Nette\Database\Conventions\DiscoveredConventions($structure);
            self::$dbContext = new Nette\Database\Context($connection, $structure, $conventions, self::getCacheStorage());
        }
        return self::$dbContext;
    }

	public function setUp()
	{
        $userManager = new UserManager(self::getDbContext());
        $companyManager = new CompanyManager(self::getDbContext());
        $orderManager = new OrderManager(self::getDbContext(), $companyManager, $userManager);
        $productTypeManager = new ProductTypeManager(self::getDbContext());
        $orderItemManager = new OrderItemManager(self::getDbContext(), $productTypeManager, $orderManager);
        $this->deliveryNoteManager = new DeliveryNoteManager(self::getDbContext(), $orderManager, $orderItemManager, $userManager);
        $deliveryNotes = $this->deliveryNoteManager->getAll();
        foreach($deliveryNotes as $deliveryNote){
            if(!isset($this->maxID)){
                $this->maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
            } else if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $this->maxID){
                $this->maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
            }
        }
        $allUsers = $userManager->getAll();
        foreach($allUsers as $user){
            if(!isset($this->maxUserID)){
                $this->maxUserID = $user[UserManager::COLUMN_ID];
            } elseif ($user[UserManager::COLUMN_ID] > $this->maxUserID){
                $this->maxUserID = $user[UserManager::COLUMN_ID];
            }
        }
        $orders = $orderManager->getAll();
        foreach($orders as $order){
            if(!isset($this->maxOrderID)){
                $this->maxOrderID = $order[OrderManager::COLUMN_ID];
            } elseif ($order[UserManager::COLUMN_ID] > $this->maxOrderID){
                $this->maxOrderID = $order[OrderManager::COLUMN_ID];
            }
        }
	}

    public function testAddDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::noError(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
        });
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote(-1, -1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote(1, -1);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID+1, $this->maxUserID+1);
        }, NoDataFoundException::class);
    }

    public function testInvoiceDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::noError(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            var_dump($maxID);
            $this->deliveryNoteManager->invoiceDeliveryNote($maxID, ["invoice" => "",
                DeliveryNoteManager::COLUMN_INVOICE_TYPE => 1,
                DeliveryNoteManager::COLUMN_INVOICE_DATE => new Nette\Utils\DateTime(),
                "paymentdate" => new Nette\Utils\DateTime()], $this->maxUserID);
        });
        Assert::error(function(){
            $this->deliveryNoteManager->invoiceDeliveryNote(-1, [], $this->maxUserID);
        },NoDataFoundException::class);
        Assert::error(function(){
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->invoiceDeliveryNote($maxID+1, [], $this->maxUserID);
        },NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->invoiceDeliveryNote($maxID, ["invoice" => "",
                DeliveryNoteManager::COLUMN_INVOICE_TYPE => 11,
                DeliveryNoteManager::COLUMN_INVOICE_DATE => new Nette\Utils\DateTime(),
                "paymentdate" => new Nette\Utils\DateTime()], $this->maxUserID);
        }, Nette\Neon\Exception::class);
    }

    public function testExpediteDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::noError(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->expediteDeliveryNote($maxID, ["transportoutcome" => 4,
                    DeliveryNoteManager::COLUMN_TRANSPORTCODE => "", DeliveryNoteManager::COLUMN_TRANSPORTNOTE => ""]
                , $this->maxUserID);
        });
        Assert::error(function(){
            $this->deliveryNoteManager->expediteDeliveryNote(-2, [], $this->maxUserID);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->expediteDeliveryNote($maxID, ["transportoutcome" => 2,
            DeliveryNoteManager::COLUMN_TRANSPORTCODE => "", DeliveryNoteManager::COLUMN_TRANSPORTNOTE => ""]
                , $this->maxUserID+1);
        },NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->expediteDeliveryNote($maxID, ["transportoutcome" => 113,
                    DeliveryNoteManager::COLUMN_TRANSPORTCODE => "", DeliveryNoteManager::COLUMN_TRANSPORTNOTE => ""]
                , $this->maxUserID);
        },Nette\Neon\Exception::class);
    }

    public function testPayDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::noError(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->invoiceDeliveryNote($maxID, ["invoice" => "",
                DeliveryNoteManager::COLUMN_INVOICE_TYPE => 1,
                DeliveryNoteManager::COLUMN_INVOICE_DATE => new Nette\Utils\DateTime(),
                "paymentdate" => new Nette\Utils\DateTime()], $this->maxUserID);
            $this->deliveryNoteManager->payDeliveryNote($maxID, ["paymentdate" => new Nette\Utils\DateTime(),
                "paiduser" => $this->maxUserID, "paidnote" => ""]);
        });
        Assert::error(function(){
            $this->deliveryNoteManager->payDeliveryNote(-2, ["paymentdate" => new Nette\Utils\DateTime(),
                "paiduser" => $this->maxUserID, "paidnote" => ""]);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->invoiceDeliveryNote($maxID, ["invoice" => "",
                DeliveryNoteManager::COLUMN_INVOICE_TYPE => 1,
                DeliveryNoteManager::COLUMN_INVOICE_DATE => new Nette\Utils\DateTime(),
                "paymentdate" => new Nette\Utils\DateTime()], $this->maxUserID);
            $this->deliveryNoteManager->payDeliveryNote($maxID+1, ["paymentdate" => new Nette\Utils\DateTime(),
                "paiduser" => $this->maxUserID, "paidnote" => ""]);
        }, NoDataFoundException::class);
        Assert::error(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            $this->deliveryNoteManager->invoiceDeliveryNote($maxID, ["invoice" => "",
                DeliveryNoteManager::COLUMN_INVOICE_TYPE => 1,
                DeliveryNoteManager::COLUMN_INVOICE_DATE => new Nette\Utils\DateTime(),
                "paymentdate" => new Nette\Utils\DateTime()], $this->maxUserID);
            $this->deliveryNoteManager->payDeliveryNote($maxID, ["paymentdate" => new Nette\Utils\DateTime(),
                "paiduser" => $this->maxUserID+1, "paidnote" => ""]);
        }, NoDataFoundException::class);
    }

    public function testRemoveDeliveryNote(){
        Tester\Environment::lock('database', __DIR__ . '/../../temp');
        Assert::noError(function(){
            $this->deliveryNoteManager->remove(-1);
        });
        Assert::noError(function(){
            $this->deliveryNoteManager->addDeliveryNote($this->maxOrderID, $this->maxUserID);
            $maxID = -1;
            $deliveryNotes = $this->deliveryNoteManager->getAll();
            foreach($deliveryNotes as $deliveryNote){
                if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $maxID){
                    $maxID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                }
            }
            Assert::true($maxID > $this->maxID);
            $this->deliveryNoteManager->remove($maxID);
        });
    }

	public function tearDown()
	{
	    $deliveryNotes = $this->deliveryNoteManager->getAll();
        foreach($deliveryNotes as $deliveryNote){
            if($deliveryNote[DeliveryNoteManager::COLUMN_ID] > $this->maxID){
                $this->deliveryNoteManager->remove($deliveryNote[DeliveryNoteManager::COLUMN_ID]);
            }
        }
        self::$cacheStorage->clean([Nette\Caching\Cache::ALL => true]);
	}
}


$test = new DeliveryNoteTest();
$test->run();
