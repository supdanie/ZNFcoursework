Instalační přiručka
=============

Požadavky: Verze PHP 7.0 a vyšší, databáze PostgreSQL

Pro úspěšnou instalaci stačí nahrát obsah složky s projektem na server a následně poupravit .htaccess. V souboru .htaccess se v závislosti na cílovém serveru přidá řádek: #RewriteBase /path/to/dir. Cesta k adresáři /path/to/dir je obvykle relativní vzhledem k adresáři, kam se mají nahrávat webové stránky.

Následně stačí si vytvořit databázi v PostgreSQL. Nad vytvořenou databází je potřeba spustit skript dump.sql. Spustit skript dump.sql z postgresu lze příkazem \i dump.sql.

Po vytvoření databáze je potřeba patřičně upravit údaje v souboru config.neon. Část konfiguračního souboru, kde se nastavují údaje do databáze vypadá takto:

database:

    dsn: 'pgsql:host=127.0.0.1;dbname=crm_supdanie'
    user: svm
    password: svm
    options:
         lazy: yes

Tuto část konfiguračního souboru config.neon je potřeba vhodně upravit.