<?php

namespace App\Presenters;


use App\Model\OrderManager;

class HomepagePresenter extends BasePresenter
{
    private $orderModel;
    public function __construct(OrderManager $orderModel){
        parent::__construct();
        $this->orderModel = $orderModel;
    }
	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
        $this->template->orders = $this->orderModel->getAll();
	}
}
