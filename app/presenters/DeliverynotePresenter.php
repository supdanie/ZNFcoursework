<?php

namespace App\Presenters;
use App\Forms\ItemsOnDeliveryNoteFormFactory;
use App\Model\CompanyManager;
use App\Model\DeliveryNoteManager;
use App\Model\ItemsOnDeliveryNoteManager;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 1.6.18
 * Time: 18:39
 */
class DeliverynotePresenter extends \App\Presenters\BasePresenter
{
    /** @var  \App\Model\DeliveryNoteManager instance třídy pro správu dodacích listů */
    private $deliveryNoteManager;
    /** @var  \App\Model\ItemsOnDeliveryNoteManager instance třídy pro správu položek na dodacích listech */
    private $itemsOnDeliveryNoteManager;
    /** @var  ItemsOnDeliveryNoteFormFactory továrnička na vytvoření formuláře pro správu položek na dodacím listu*/
    private $itemsOnDeliveryNoteFormFactory;
    /** @var  \App\Forms\DeliveryNoteFormFactory továrnička na formuláře pro správu dodacích listů */
    private $deliveryNoteFormFactory;
    /** @var  identifikátor objednávky */
    private $order;
    /** @var  \App\Controls\OrderControlFactory továrnička na komponenty pro správu objednávky, či dodacího listu */
    private $orderControlFactory;
    /** @var  identifikátor dodacího listu, se kterým se práve pracuje */
    private $managedDeliveryNote;
    /** @var  CompanyManager instance třídy pro získání informací o firmách */
    private $companyManager;

    /**
     * DeliverynotePresenter constructor.
     * @param DeliveryNoteManager $deliveryNoteManager instance třídy pro správu dodacích listů
     * @param \App\Model\ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager instance třídy pro správu položek na dodacích listech
     * @param \App\Forms\DeliveryNoteFormFactory $deliveryNoteFormFactory továrnička na vytvoření formuláře pro správu položek na dodacím listu
     * @param CompanyManager $companyManager instance třídy pro získání informací o firmách
     * @param ItemsOnDeliveryNoteFormFactory $itemsOnDeliveryNoteFormFactory továrnička na formuláře pro správu dodacích listů
     * @param \App\Controls\OrderControlFactory $orderControlFactory továrnička na komponenty pro správu objednávky, či dodacího listu
     */
    public function __construct(\App\Model\DeliveryNoteManager $deliveryNoteManager,
                                \App\Model\ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager,
                                \App\Forms\DeliveryNoteFormFactory $deliveryNoteFormFactory,
                                CompanyManager $companyManager,
                                ItemsOnDeliveryNoteFormFactory $itemsOnDeliveryNoteFormFactory,
                                \App\Controls\OrderControlFactory $orderControlFactory){
        $this->deliveryNoteManager = $deliveryNoteManager;
        $this->itemsOnDeliveryNoteManager = $itemsOnDeliveryNoteManager;
        $this->itemsOnDeliveryNoteFormFactory = $itemsOnDeliveryNoteFormFactory;
        $this->companyManager = $companyManager;
        $this->deliveryNoteFormFactory = $deliveryNoteFormFactory;
        $this->orderControlFactory = $orderControlFactory;
    }


    /**
     * Zamítne přístup, pokud uživatel není admin a dodací list není jeho
     */
    /**  protected function beforeRender()
    {
        parent::beforeRender();
        $order = $this->deliveryNoteManager->get(intval($this->managedDeliveryNote));
        $user = $order[DeliveryNoteManager::COLUMN_USER];
        if(isset($this->managedDeliveryNote) && $this->managedDeliveryNote != null) {
            if ($this->getUser()->isInRole("admin") && $this->getUser()->getId() != $user) {
                throw new AccessDeniedException("Přístup zamítnut");
            }
        }
    } */


    public function actionPdf($order, $id)
    {
        $template = $this->createTemplate();
        $template->setFile(__DIR__ . "/path/to/pdf.latte");
        $mpdf = new \mPDF();
        $deliveryNote = $this->deliveryNoteManager->get(intval($id));
        $company = $this->companyManager->get($deliveryNote[DeliveryNoteManager::COLUMN_COMPANY]);
        $mpdf->WriteHTML($company[CompanyManager::COLUMN_COMPANY_NAME]);
        $mpdf->WriteHTML($company[CompanyManager::COLUMN_ADDRESS1]." ".$company[CompanyManager::COLUMN_ADDRESS12]);
        $mpdf->WriteHTML($company[CompanyManager::COLUMN_ADDRESS2]." ".$company[CompanyManager::COLUMN_ADDRESS22]);
        $items = $this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote(intval($id));
        $mpdf->WriteHTML("<table>");
        foreach($items as $item){
            $mpdf->WriteHTML("<tr>
            <td>".$item[ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_NAME]."</td>
            <td>".$item[ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY]."</td>
            <td>".$item[ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE]."</td>
            <td>".($item[ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY]*
                    $item[ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE])."</td>
            </tr>");
        }
        $mpdf->WriteHTML("</table>");
        $mpdf->Output();
// Output a PDF file directly to the browser
        $mpdf->Output();
    }

    /**
     * Není-li uživatel přihlášen, bude odkázán na formulář pro přihlášení.
     */
    protected function startup(){
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    /**
     * Zde nastavujeme proměnnou, ve které jsou uchovány informace o objednávce
     * @param $id identifikátor objednávky
     */
    public function actionDefault($order){
        $this->order = $order;
    }

    /**
     * Zde se nastavuje identifikátor objednávky a identifikátor dodacího listu, se kterými se bude pracovat při vytváření komponent.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function actionView($order, $id){
        $this->order = $order;
        $this->managedDeliveryNote = $id;
    }

    /**
     * Zde se nastavuje identifikátor objednávky a identifikátor dodacího listu, se kterými se bude pracovat při vytváření komponent.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function actionExpedite($order, $id){
        $this->order = $order;
        $this->managedDeliveryNote = $id;
    }

    /**
     * Zde se nastavuje identifikátor objednávky a identifikátor dodacího listu, se kterými se bude pracovat při vytváření komponent.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function actionGivetoproduction($order, $id){
        $this->order = $order;
        $this->managedDeliveryNote = $id;
    }

    /**
     * Zde se nastavuje identifikátor objednávky a identifikátor dodacího listu, se kterými se bude pracovat při vytváření komponent.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function actionInvoice($order, $id){
        $this->order = $order;
        $this->managedDeliveryNote = $id;
    }

    /**
     * Zde se nastavuje identifikátor objednávky a identifikátor dodacího listu, se kterými se bude pracovat při vytváření komponent.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function actionPay($order, $id){
        $this->order = $order;
        $this->managedDeliveryNote = $id;
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro zadání údajů o expedici zboží
     */
    public function createComponentExpediteForm(){
        return $this->deliveryNoteFormFactory->expediteForm($this->managedDeliveryNote, $this->getUser()->getId());
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro zadání údajů o fakturaci dodacího listu
     */
    public function createComponentInvoiceForm(){
        return $this->deliveryNoteFormFactory->invoiceForm($this->managedDeliveryNote, $this->getUser()->getId());
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro zadání údajů o platbě
     */
    public function createComponentPayForm(){
        return $this->deliveryNoteFormFactory->payForm($this->managedDeliveryNote, $this->getUser()->getId());
    }


    /**
     * @return \App\Controls\DeliveryNoteControl komponenta pro správu dodacího listu a jeho položek
     */
    public function createComponentDeliveryNoteComponent(){
        return $this->orderControlFactory->createDeliveryNoteControl($this->managedDeliveryNote, $this->getUser()->getID());
    }
    /**
     * Zde se předává identifikátor objednávky a identifikátor dodacího listu příslušné šabloně.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function renderExpedite($order, $id){
        $this->template->order = $order;
        $this->template->id = $id;
    }

    /**
     * Zde se předává identifikátor objednávky a identifikátor dodacího listu příslušné šabloně.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function renderInvoice($order, $id){
        $this->template->order = $order;
        $this->template->id = $id;
    }

    /**
     * Zde se předává identifikátor objednávky a identifikátor dodacího listu příslušné šabloně.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function renderPay($order, $id){
        $this->template->order = $order;
        $this->template->id = $id;
    }

    /**
     * Zde se předává identifikátor objednávky a identifikátor dodacího listu příslušné šabloně.
     * @param $order identifikátor objednávky
     * @param $id identifikátor dodacího listu
     */
    public function renderView($order, $id){
        $this->template->order = $order;
        $this->template->id = $id;
        if(!isset($this->template->priceForAll)){
            $this->template->priceForAll = 0;
        }
    }

    /**
     * Změní proměnou v šabloně, ve které je uložená celková cena za všechny kusy daného produktu, který se má vložit.
     */
    public function handleChangetotalprice(){
        if($this->isAjax()){
            $quantity = intval($this->getParameter("quantity"));
            $price = floatval($this->getParameter("price"));
            $this->template->priceForAll = $quantity*$price;
            $this->redrawControl("totalprice");
        }
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro přidání nové položky
     */
    public function createComponentAddItemForm(){
        return $this->itemsOnDeliveryNoteFormFactory->addItemForm($this->managedDeliveryNote);
    }
}