<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.5.18
 * Time: 17:57
 */

namespace App\Presenters;


use App\Controls\OrderControlFactory;
use App\Forms\OrderFormFactory;
use App\Forms\OrderItemFormFactory;
use App\Model\CompanyManager;
use App\Model\DeliveryNoteManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\NoDataFoundException;
use App\Model\OrderItemManager;
use App\Model\OrderManager;
use App\Model\UserManager;
use Nette\Application\UI\Multiplier;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Ublaboo\DataGrid\DataGrid;


class OrderPresenter extends BasePresenter
{
    /** @var OrderManager instance třídy pro správu objednávek  */
    private $orderModel;
    /**
     * @var OrderFormFactory továrnička na formuláře pro správu objednávek
     */
    private $orderFormFactory;
    /** @var  OrderItemManager instance třídy pro správu položek na objednávce  */
    private $orderItemManager;
    /** @var  OrderItemManager továrnička na formuláře pro správu položek */
    private $orderItemFormFactory;
    /** @var  ItemsOnDeliveryNoteManager instance třídy pro správu položek na dodacích listech*/
    private $itemsOnDeliveryNoteManager;
    /** @var  OrderControlFactory továrnička na komponenty pro správu objednávky, či dodacího listu */
    private $orderControlFactory;
    /** @var  UserManager instance třídy pro správu uživatelů*/
    private $userManager;
    /** @var  CompanyManager instance třídy pro získání informací o firmách */
    private $companyManager;
    /** @var  DeliveryNoteManager instance třídy pro správu dodacích listů */
    private $deliveryNoteManager;
    /**
     * @var identifikátor objednávky, se kterou se pracuje
     */
    private $managedOrder;

    /**
     * OrderPresenter constructor.
     * @param OrderManager $orderModel instance třídy pro správu objednávek
     * @param OrderFormFactory $orderFormFactory továrnička na formuláře pro správu objednávek
     * @param OrderItemManager $orderItemManager instance třídy pro správu položek na objednávce
     * @param OrderItemFormFactory $orderItemFormFactory továrnička na formuláře pro správu položek
     * @param ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager instance třídy pro správu položek na dodacích listech
     * @param OrderControlFactory $orderControlFactory továrnička na komponenty pro správu objednávky, či dodacího listu
     * @param UserManager $userManager instance třídy pro správu uživatelů
     * @param CompanyManager $companyManager instance třídy pro získání informací o firmách
     * @param DeliveryNoteManager $deliveryNoteManager instance třídy pro správu dodacích listů
     */
    public function __construct(OrderManager $orderModel, OrderFormFactory $orderFormFactory,
                                OrderItemManager $orderItemManager, OrderItemFormFactory $orderItemFormFactory,
                                ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager,
                                OrderControlFactory $orderControlFactory, UserManager $userManager,
                                CompanyManager $companyManager,
                                DeliveryNoteManager $deliveryNoteManager){
        $this->orderModel = $orderModel;
        $this->orderFormFactory = $orderFormFactory;
        $this->orderItemManager = $orderItemManager;
        $this->orderItemFormFactory = $orderItemFormFactory;
        $this->itemsOnDeliveryNoteManager = $itemsOnDeliveryNoteManager;
        $this->orderControlFactory = $orderControlFactory;
        $this->userManager = $userManager;
        $this->companyManager = $companyManager;
        $this->deliveryNoteManager = $deliveryNoteManager;
    }

    /**
     * Není-li uživatel přihlášen, bude odkázán na formulář pro přihlášení.
     */
    protected function startup(){
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    /**
     * Zamítne přístup, pokud uživatel není admin a objednávka není jeho
     */
    /** protected function beforeRender()
    {
        parent::beforeRender();
        $order = $this->orderModel->get(intval($this->managedOrder));
        $user = $order[OrderManager::COLUMN_RECEIVER];
        if(isset($this->managedOrder) && $this->managedOrder != null) {
            if ($this->getUser()->isInRole("admin") && $this->getUser()->getId() != $user) {
                throw new AccessDeniedException("Přístup zamítnut");
            }
        }
    } */

    public function actionPdf($id)
    {
        $order = $this->orderModel->get(intval($id));
        $company = $this->companyManager->get($order[OrderManager::COLUMN_COMPANY]);
        $items = $this->orderItemManager->getItemsOfOrder(intval($id));
        $name = OrderItemManager::COLUMN_NAME;
        $quantity = OrderItemManager::COLUMN_QUANTITY;
        $price = OrderItemManager::COLUMN_PRICE;
        $currency = OrderItemManager::COLUMN_CURRENCY;
        $mpdf = new \mPDF();
        $mpdf->WriteHTML($company[CompanyManager::COLUMN_COMPANY_NAME]);
        $mpdf->WriteHTML($company[CompanyManager::COLUMN_ADDRESS1]." ".
            $company[CompanyManager::COLUMN_ADDRESS12]);
        $mpdf->WriteHTML($company[CompanyManager::COLUMN_ADDRESS2]." ".
            $company[CompanyManager::COLUMN_ADDRESS22]);
        $mpdf->WriteHTML("<table>");
        foreach($items as $item){
            $currencyText = "CZK";
            if($item[$currency] == 2){
                $currencyText = "EUR";
            }
            if($item[$currency] == 3){
                $currencyText = "USD";
            }
            if($item[$currency] == 4){
                $currencyText = "GPB";
            }
            $mpdf->WriteHTML("<tr>
            <td>".$item[$name]."</td>
            <td>".$item[$quantity]."</td>
            <td>".$item[$price]."</td>
            <td>".$currencyText."</td>
            <td>".($item[$quantity]*$item[$price])."</td>
            <td>".$currencyText."</td>
            </tr>");
        }
        $mpdf->WriteHTML("</table>");
        $mpdf->Output();
    }

    /**
     * V akci se nastaví identifikátor objednávky dle hodnoty v URL
     * @param $id identifikátor objednávky
     */
    public function actionView($id){
        $this->managedOrder = $id;
    }

    /**
     * V akci se nastaví identifikátor objednávky dle hodnoty v URL
     * @param $id identifikátor objednávky
     */
    public function actionAccept($id){
        $this->managedOrder = $id;
    }

    /**
     * V akci se nastaví identifikátor objednávky dle hodnoty v URL
     * @param $id identifikátor objednávky
     */
    public function actionExpedite($id){
        $this->managedOrder = $id;
    }

    /**
     * V akci se nastaví identifikátor objednávky dle hodnoty v URL
     * @param $id identifikátor objednávky
     */
    public function actionGivetoproduction($id){
        $this->managedOrder = $id;
    }

    /**
     * V akci se nastaví identifikátor objednávky dle hodnoty v URL
     * @param $id identifikátor objednávky
     */
    public function actionPay($id){
        $this->managedOrder = $id;
    }
    /**
     * Zde předáváme seznam objednávek do šablony.
     */
    public function renderDefault(){
        $this->template->orders = $this->orderModel->getAll();
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro přidání nové objednávky
     */
    public function createComponentAddForm(){
        return $this->orderFormFactory->createNewForm();
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro evidenci přijetí objednávky
     */
    public function createComponentAcceptForm(){
        $user = !empty($this->getUser()) ? $this->getUser()->getId() : null;
        return $this->orderFormFactory->acceptOrderForm($this->managedOrder, $user);
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro zadání informací o expedici objednávky
     */
    public function createComponentExpediteForm(){
        $user = !empty($this->getUser()) ? $this->getUser()->getId() : null;
        return $this->orderFormFactory->expediteOrderForm($this->managedOrder, $user);
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro evidenci informací o zadání objednávky do výroby
     */
    public function createComponentGiveToProductionForm(){
        return $this->orderFormFactory->giveOrderToProductionForm($this->managedOrder);
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro evidenci platby objednávky
     */
    public function createComponentPayForm(){
        return $this->orderFormFactory->payOrderForm($this->managedOrder);
    }

    /**
     * @return Multiplier komponenta pro zobrazení detailu objednávky
     */
    public function createComponentOrderComponent(){
        return new Multiplier(function($id){
            return $this->orderControlFactory->createOrderControl($id, $this->getUser()->getID());
        });
    }

    /**
     * @return DataGrid Mřížka, ve které jsou zobrazeny informace o objednávkách
     */
    public function createComponentOrderGrid(){
        $grid = new DataGrid();
        $grid->setDataSource($this->orderModel->getAll()->fetchAll());
        $grid->addColumnText(OrderManager::COLUMN_ID, "Číslo objednávky")->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_STATUS, "Stav objednávky")
            ->setRenderer(function($item){
                $status = OrderManager::COLUMN_STATUS;
                $statusNumber = $item->$status;
                return OrderManager::getStatusTitle($statusNumber);
            })->setSortable();

        $grid->addColumnText(OrderManager::COLUMN_COMPANY, "Firma")->
        setRenderer(function($item){
            $company = OrderManager::COLUMN_COMPANY;
            $companyID = $item->$company;
            if($companyID === null){
                return "";
            }
            return $this->companyManager->get($companyID)[CompanyManager::COLUMN_COMPANY_NAME];
        })->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_COMPANY_ORDER_NUMBER, "Číslo objednávky firmy")->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_ORDERTYPE, "Způsob objednání")->
        setRenderer(function($item){
            $orderType = OrderManager::COLUMN_ORDERTYPE;
            $orderTypeID = $item->$orderType;
            if(empty($orderTypeID)){
                return "";
            }
            if($orderTypeID == 1){
                return "Ústní";
            }
            if($orderTypeID == 2){
                return "Telefonická";
            }
            if($orderTypeID == 3){
                return "Osobní";
            }
            if($orderTypeID == 4){
                return "E-mailem";
            }
            return "";
        })->setSortable();

        $grid->addColumnDateTime(OrderManager::COLUMN_RECEIVE_DATE, "Datum přijetí")->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_RECEIVER, "Přijal")->
        setRenderer(function($item){
            $receiver = OrderManager::COLUMN_RECEIVER;
            $receiverID = $item->$receiver;
            if($receiverID == null){
                return "";
            }
            return $this->userManager->get($receiverID)[UserManager::COLUMN_REAL_NAME];
        })->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_ORDER_FILE, "Elektronická objednávka")->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_APPROVED, "Schváleno")->setRenderer(
            function($item){
                $approvedTitle = OrderManager::COLUMN_APPROVED;
                $approved = $item->$approvedTitle;
                if($approved === null){
                    return "Ne";
                } elseif($approved == true){
                    return "Ano";
                } else {
                    return "Ne";
                }
            }
        )->setSortable();
        $grid->addColumnDateTime(OrderManager::COLUMN_APPROVED_DATE, "Datum schválení")->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_APPROVER, "Schválil")
            ->setRenderer(function($item){
                $approver = OrderManager::COLUMN_APPROVER;
                $approverID = $item->$approver;
                if($approverID === null){
                    return "";
                }
                return $this->userManager->get($approverID)[UserManager::COLUMN_REAL_NAME];
            })->setSortable();
        $grid->addColumnNumber(OrderManager::COLUMN_APPROVED_PRICE, "Předpokládaná celková cena")->
        setRenderer(function($item){
            $order = $item->id;
            $itemsOnOrder = $this->orderItemManager->getItemsOfOrder($order);
            $price = 0;
            $currency = 1;
            foreach($itemsOnOrder as $itemOnOrder){
                $price += $itemOnOrder[OrderItemManager::COLUMN_QUANTITY]*$itemOnOrder[OrderItemManager::COLUMN_PRICE];
                $currency = $itemOnOrder[OrderItemManager::COLUMN_CURRENCY];
            }
            $currencyString = "CZK";
            if($currency == 2){
                $currencyString = "EUR";
            }
            if($currency == 3){
                $currencyString = "USD";
            }
            if($currency == 4){
                $currencyString = "GPB";
            }
            return $price." ".$currencyString;
        })->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_APPROVED_NOTE, "Poznámka ke schválení")->setSortable();
        $grid->addColumnDateTime(OrderManager::COLUMN_PRODUCTION_DEADLINE, "Do kdy vyrobit")->setSortable();
        $grid->addColumnText("realPrice", "Skutečná celková cena")->
        setRenderer(function($item){
            $order = $item->id;
            $itemsOnOrder = $this->orderItemManager->getItemsOfOrder($order);
            $totalPrice = 0;
            $currency = 1;
            foreach($itemsOnOrder as $itemOnOrder){
                $currency = $itemOnOrder[OrderItemManager::COLUMN_CURRENCY];
                break;
            }
            $deliveryNotes = $this->deliveryNoteManager->getDeliveryNotesByOrder($item->id);
            foreach($deliveryNotes as $deliveryNote){
                $deliveryNoteID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
                $itemsOnNote = $this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote($deliveryNoteID);
                foreach($itemsOnNote as $itemOnNote){
                    $quantity = $itemOnNote[ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY];
                    $price = $itemOnNote[ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE];
                    $totalPrice += $quantity*$price;
                }
            }
            $currencyString = "CZK";
            if($currency == 2){
                $currencyString = "EUR";
            }
            if($currency == 3){
                $currencyString = "USD";
            }
            if($currency == 4){
                $currencyString = "GPB";
            }
            return $totalPrice." ".$currencyString;
        })->setSortable();
        $grid->addColumnText(OrderManager::COLUMN_CLOSED, "Uzavřeno")->setRenderer(
            function($item){
                $closedTitle = OrderManager::COLUMN_CLOSED;
                $closed = $item->$closedTitle;
                if($closed === null){
                    return "Ne";
                } elseif($closed == true) {
                    return "Ano";
                } else {
                    return "Ne";
                }
            })->setSortable();
        $grid->addAction('expedite', 'Expedice', 'Order:expedite');
        $grid->addAction('detail', 'Detail objednávky', 'Order:view');
        $grid->addAction('giveToProduction', 'Dát do výroby', 'Order:givetoproduction');
        $grid->addAction('pay', 'Evidovat platbu', 'Order:pay');
        $grid->addAction('close', 'Uzavřít objednávku', 'close!');
        $grid->addAction("pdf", "Zobrazit v PDF", "Order:pdf");
        $grid->addAction('delete', 'Smazat', 'delete!');
        return $grid;
    }

    /**
     * Metoda zajistí uzavření objednávky.
     * @param $id identifikátor objednávky, která má být uzavřena.
     */
    public function handleClose($id){
         $this->orderModel->closeOrder($id);
    }

    /**
     * Akce, která zajistí smazání objednávky
     * @param $id identifikátor objednávky, která má být smazána
     */
    public function handleDelete($id){
        $items = $this->orderItemManager->getItemsOfOrder($id);
        foreach($items as $item){
            $itemID = $item[OrderItemManager::COLUMN_ID];
            $this->orderItemManager->remove($itemID);
        }
        $deliveryNotes = $this->deliveryNoteManager->getDeliveryNotesByOrder($id);
        foreach($deliveryNotes as $deliveryNote){
            $deliveryNoteID = $deliveryNote[DeliveryNoteManager::COLUMN_ID];
            $itemsOnDeliveryNote = $this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote($deliveryNoteID);
            foreach($itemsOnDeliveryNote as $item){
                $itemID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
                $this->itemsOnDeliveryNoteManager->remove($itemID);
            }
            $this->deliveryNoteManager->remove($deliveryNoteID);
        }
        $this->orderModel->remove($id);
    }

    /**
     * Zde je předán identifikátor objednávky do šablony.
     * @param $id identifikátor objednávky, jejiž detail má být zobrazen
     */
    public function renderView($id){
        $this->template->order = $id;
        if(!isset($this->template->priceForAll)) {
            $this->template->priceForAll = 0;
        }
    }


    /**
     * Přesměruje na detail dodacího listu.
     * @param $id identifikátor dodacího listu
     */
    public function handleDetail($id){
        $this->redirect("Deliverynote:view", ["order" => $this->orderID, "id" => $id]);
    }


    /**
     * @return DataGrid Mřížka se seznamem dodacích listů k dané objednávce.
     */
    public function createComponentGridOfDeliveryNotes(){
        $grid = new DataGrid();
        $grid->setDataSource($this->deliveryNoteManager->getDeliveryNotesByOrder($this->managedOrder)->fetchAll());
        $grid->setColumnsHideable();
        $grid->setMultiSortEnabled();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_STATUS, "Stav")
            ->setRenderer(function($item){
                $statusTitle = DeliveryNoteManager::COLUMN_STATUS;
                $status = $item->$statusTitle;
                return DeliveryNoteManager::getStatusTitle($status);
            })->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_ID, "Číslo dodacího listu")->setSortable();
        $grid->addColumnDateTime(DeliveryNoteManager::COLUMN_TRANSACTION_DATE, "Datum")->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_USER, "Vystavil")->
        setRenderer(function($item){
            $userTitle = DeliveryNoteManager::COLUMN_USER;
            $user = $item->$userTitle;
            if($user === null) {
                return "";
            }
            return $this->userManager->get($user)[UserManager::COLUMN_REAL_NAME];
        })->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_TRANSPORT_OUTCOME, "Způsob doručení")
            ->setRenderer(function($item){
                $outcomeTitle = DeliveryNoteManager::COLUMN_TRANSPORT_OUTCOME;
                $outcome = $item->$outcomeTitle;
                if($outcome === null){
                    return "";
                }
                $outcomes = [0 => "N", 1 => "Osobní", 2 => "Poštou", 3 => "Vlakem",
                    4 => "PPL", 5 => "Doprava", 6 => "Neznámý"];
                if($outcome >= 0 && $outcome <= 6){
                    return $outcomes[$outcome];
                }
                return "";
            })->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_TRANSPORTCODE, "Vyzvednul / transportní kód")->setSortable();
        $grid->addColumnDateTime(DeliveryNoteManager::COLUMN_RESOLUTION_DATE, "Datum vydání")->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_USER2, "Vydal")->setRenderer(function($item){
            $userTitle = DeliveryNoteManager::COLUMN_USER2;
            $user = $item->$userTitle;
            if($user === null) {
                return "";
            }
            return $this->userManager->get($user)[UserManager::COLUMN_REAL_NAME];
        })->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_TRANSPORTNOTE, "Poznámka")->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_INVOICE ,"Upload faktury")->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_INVOICE_TYPE, "Způsob úhrady")
            ->setRenderer(function($item){
                $array = [1 => "Nájem", 2 => "prodej - faktura", 3 => "prodej - zálohová faktura",
                    4 => "prodej - dobírka", 5 => "prodej - hotovost", 6 => "zápůjčka - 30 dní",
                    7 => "zápůjčka - dlouhodobě", 8 => "zápůjčka - paralelní spoj"];
                $invoiceTypeTitle = DeliveryNoteManager::COLUMN_INVOICE_TYPE;
                $invoiceType = $item->$invoiceTypeTitle;
                if($invoiceType >= 1 && $invoiceType <= 8){
                    return $array[$invoiceType];
                }
                return "";
            })->setSortable();
        $grid->addColumnDateTime(DeliveryNoteManager::COLUMN_INVOICE_DATE, "Datum fakturace")->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_INVOICE_USER, "Fakturu vystavil")->setRenderer(
            function($item){
                $invoiceUserTitle = DeliveryNoteManager::COLUMN_INVOICE_USER;
                $invoiceUser = $item->$invoiceUserTitle;
                if($invoiceUser === null){
                    return "";
                }
                try {
                    return $this->userManager->get($invoiceUser)[UserManager::COLUMN_REAL_NAME];
                } catch (NoDataFoundException $ex){
                    return "";
                }
            }
        )->setSortable();
        $grid->addColumnDateTime(DeliveryNoteManager::COLUMN_PAYMENT_DATE, "Datum splatnosti")->setSortable();
        $grid->addColumnText("totalPrice", "Celková cena")->setRenderer(function ($item){
            $itemsOnDeliveryNote = $this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote($item->id);
            $totalPrice = 0;
            foreach($itemsOnDeliveryNote as $itemOnDeliveryNote){
                $quantity = $itemOnDeliveryNote[ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY];
                $price = $itemOnDeliveryNote[ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE];
                $totalPrice+=$quantity*$price;
            }
            return $totalPrice;
        })->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_PAID, "Zaplaceno")
            ->setRenderer(function($item){
                $paidTitle = DeliveryNoteManager::COLUMN_PAID;
                $paid = $item->$paidTitle;
                if($paid === null){
                    return "Ne";
                }
                if($paid == true){
                    return "Ano";
                }
                return "Ne";
            })->setSortable();
        $grid->addColumnDateTime(DeliveryNoteManager::COLUMN_PAID_DATE, "Datum úhrady")->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_PAID_USER, "Platbu převzal")
            ->setRenderer(function($item){
                $paidUserTitle = DeliveryNoteManager::COLUMN_PAID_USER;
                $paidUser = $item->$paidUserTitle;
                if($paidUser === null){
                    return "";
                }
                try {
                    return $this->userManager->get($paidUser)[UserManager::COLUMN_REAL_NAME];
                } catch(NoDataFoundException $ex){
                    return "";
                }
            })->setSortable();
        $grid->addColumnText(DeliveryNoteManager::COLUMN_PAIDNOTE, "Poznámka k platbě")->setSortable();
        $grid->addAction("detail", "Zobrazit", "viewdeliverynote!");
        $grid->addAction("expedite", "Expedovat", "expeditedeliverynote!");
        $grid->addAction("invoice", "Fakturovat", "invoicedeliverynote!");
        $grid->addAction("pay", "Zaplatit", "paydeliverynote!");
        $grid->addAction("viewinpdf", "Zobrazit v PDF", "viewinpdf!");
        $grid->addAction("remove", "Smazat", "remove!");
        return $grid;
    }

    /**
     * Přesměruje uživatele na dodací list v PDF
     * @param $id identifikátor dodacího listu
     */
    public function handleViewinpdf($id){
        $order = $this->deliveryNoteManager->get($id)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
        $this->redirect(303, "Deliverynote:pdf", ["order" => $order, "id" => $id]);
    }

    /**
     * Přesměruje uživatele na stránku s detailem dodacího listu
     * @param $id identifikátor dodacího listu
     */
    public function handleViewdeliverynote($id){
        $order = $this->deliveryNoteManager->get($id)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
        $this->redirect(303, "Deliverynote:view", ["order" => $order, "id" => $id]);
    }

    /**
     * Přesměruje uživatele na stránku s formulářem pro evidenci informací o expedici zboží
     * @param $id identifikátor dodacího listu
     */
    public function handleExpeditedeliverynote($id){
        $order = $this->deliveryNoteManager->get($id)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
        $this->redirect(303, "Deliverynote:expedite", ["order" => $order, "id" => $id]);
    }

    /**
     * Přesměruje uživatele na stránku s formulářem pro evidenci informací o fakturaci
     * @param $id identifikátor dodacího listu
     */
    public function handleInvoicedeliverynote($id){
        $order = $this->deliveryNoteManager->get($id)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
        $this->redirect(303, "Deliverynote:invoice", ["order" => $order, "id" => $id]);
    }

    /**
     * Přesměruje uživatele na stránku s formulářem pro evidenci informací o platbě
     * @param $id identifikátor dodacího listu
     */
    public function handlePaydeliverynote($id){
        $order = $this->deliveryNoteManager->get($id)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
        $this->redirect(303, "Deliverynote:pay", ["order" => $order, "id" => $id]);
    }
    /**
     * @param $id identifikátor dodacího listu, který má být smazán.
     */
    public function handleRemove($id){
        $items = $this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote($id);
        foreach($items as $item){
            $itemID = $item[ItemsOnDeliveryNoteManager::COLUMN_ID];
            $this->itemsOnDeliveryNoteManager->remove($itemID);
        }
        $this->deliveryNoteManager->remove($id);
        $this->redirect(303, "Order:view", ["id" => $this->managedOrder]);
    }


    /**
     * Změní proměnou v šabloně, ve které je uložená celková cena za všechny kusy daného produktu, který se má vložit.
     */
    public function handleChangetotalprice(){
        if($this->isAjax()){
            $quantity = intval($this->getParameter("quantity"));
            $price = floatval($this->getParameter("price"));
            $this->template->priceForAll = $quantity*$price;
            $this->redrawControl("totalprice");
        }
    }


    /**
     * @return \Nette\Application\UI\Form formulář pro přidání nové položky do objednávky
     */
    public function createComponentAddItemForm(){
        return $this->orderItemFormFactory->addItemForm($this->managedOrder);
    }

}