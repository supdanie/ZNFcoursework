<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:50
 */

namespace App\Model;
use Nette\Neon\Exception;
use Nette\Utils\DateTime;
use Nette;

/**
 * Class OrderManager Třída pro správu objednávek. Umožňuje základní operace s objednávkami a získávání informací o nich. Slouží k evidenci informací o objednávkách.
 * @package App\Model
 */
class OrderManager extends BaseManager
{
    const TABLE_NAME = "productorders",
        COLUMN_ID = "id",
        COLUMN_STATUS = "orderstatus",
        COLUMN_COMPANY = "companyid",
        COLUMN_COMPANY_ORDER_NUMBER="companyordernumber",
        COLUMN_ORDERTYPE = "ordertype",
        COLUMN_RECEIVE_DATE = "recievedate",
        COLUMN_RECEIVER = "recieverid",
        COLUMN_ORDER_FILE = "orderfile",
        COLUMN_APPROVED = "approved",
        COLUMN_APPROVED_DATE = "approveddate",
        COLUMN_APPROVER = "approveduserid",
        COLUMN_APPROVED_PRICE = "approvedprice",
        COLUMN_APPROVED_NOTE = "approvednote",
        COLUMN_PAID_NOTE = "paidnote",
        COLUMN_PRODUCTION_DEADLINE = "productiondeadline",
        COLUMN_PRODUCTION_STATUS = "productionstatus",
        COLUMN_CLOSED = "closed",
        COLUMN_EXPEDITION_USER = "expeditionuserid",
        COLUMN_EXPEDITION_DATE = "expeditiondate",
        COLUMN_EXPEDITION_RECEIVER = "expeditionreciever",
        COLUMN_EXPEDITION_DATE2 = "expeditiondate2",
        COLUMN_EXPEDITION_USER2 = "expeditionuserid2",
        COLUMN_INVOICE_USER = "invoiceuserid",
        COLUMN_INVOICEID = "invoiceid",
        COLUMN_INVOICE_PRICE = "invoiceprice",
        COLUMN_INVOICE_TYPE = "invoicetype",
        COLUMN_INVOICE_DATE = "invoicedate",
        COLUMN_PAID = "paid",
        COLUMN_PAID_DATE = "paiddate",
        COLUMN_PAID_USER = "paiduserid",
        COLUMN_PAYMENT_DATE = "paymentdate",
        ORDER_NEW = 0,
        ORDER_ACCEPTED = 1,
        ORDER_INVOICED = 2,
        ORDER_PRODUCED = 3,
        ORDER_PARTIALLY_PAID = 4,
        ORDER_PAID = 5,
        ORDER_IN_PRODUCTION = 6,
        ORDER_PREPARED_TO_SEND = 7,
        ORDER_SENT = 8,
        ORDER_DELIVERED = 9,
        ORDER_CLOSED = 10,
        ORDER_CANCELED = 11,
        ORDER_PARTIALLY_PRODUCED = 12,
        ORDER_PARTIALLY_INVOICED = 13,
        ORDER_PARTIALLY_EXPEDITED = 14,
        ORDER_UNKNOWN = 15;

    /** @var CompanyManager  instance třídy pro získání informací o firmách*/
    private $companyManager;
    /** @var UserManager instance třídy pro správu uživatelů */
    private $userManager;

    /**
     * OrderManager constructor.
     * @param Nette\Database\Context $database objekt představující databázi
     * @param CompanyManager $companyManager instance třídy pro získání informací o firmách
     * @param UserManager $userManager instance třídy pro správu uživatelů
     */
    public function __construct(Nette\Database\Context $database, CompanyManager $companyManager, UserManager $userManager)
    {
        parent::__construct($database);
        $this->companyManager = $companyManager;
        $this->userManager = $userManager;
    }

    /**
     * @param $value číselná hodnota stavu objednávky
     * @return mixed|string název stavu objednávky
     */
    public static function getStatusTitle($value){
        if($value < 0 || $value > 15){
            return "";
        }
        $array = [0 => "Vytvořeno", 1 => "Přijato", 2 => "Fakturováno", 3 => "Vyrobeno", 4 => "Částečně zaplaceno",
        5 => "Zaplaceno", 6 => "Ve výrobě", 7 => "Připraveno k odeslání", 8 => "Odesláno", 9 => "Doručeno", 10 => "Uzavřeno",
        11 => "Zrušeno", 12 => "Částečně vyrobeno", 13 => "Částečně fakturováno", 14 => "Částečně fakturováno",
            15 => "Neznámý stav"];
        return $array[$value];
    }

    /**
     * Metoda zajistí přidání nové objednávky do databáze.
     * @param $values pole hodnot z formuláře
     */
    public function addNewOrder($values){
        $this->throwExceptionIfErrorAddNewOrder($values);
        $count = $this->database->table(self::TABLE_NAME)->count();
        $this->database->table(self::TABLE_NAME)->insert(
            [self::COLUMN_ID => $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1,
                self::COLUMN_STATUS => self::ORDER_NEW,
                self::COLUMN_COMPANY => $values[self::COLUMN_COMPANY],
                self::COLUMN_COMPANY_ORDER_NUMBER => $values[self::COLUMN_COMPANY_ORDER_NUMBER],
                self::COLUMN_ORDERTYPE => $values[self::COLUMN_ORDERTYPE],
                self::COLUMN_ORDER_FILE => $values[self::COLUMN_ORDER_FILE]]);
    }

    /**
     * Metoda kontroluje, zda jsou hodnoty z formuláře validní.
     * @param $values pole hodnot z formuláře
     * @throws Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní.
     */
    public function throwExceptionIfErrorAddNewOrder($values){
        $company = $this->companyManager->get($values[self::COLUMN_COMPANY]);
        if(empty($company)){
            throw new Exception("Firma s tímto ID neexistuje.");
        }
        $orderType = $values[self::COLUMN_ORDERTYPE];
        if(!is_integer($orderType)){
            throw new Exception("Typ objednávky musí být celé číslo.");
        }
        if($orderType < 1 && $orderType > 4){
            throw new Exception("Typ objednávky musí být celé číslo od 1 do 4.");
        }
    }

    /**
     * Metoda zajistí evidenci přijetí objednávky.
     * @param $id identifikátor objednávky, která má být přijata
     * @param $values pole hodnot z formuláře
     * @param $userID identifikátor uživatele, který má přijmout danou objednávku
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function acceptOrder($id, $values, $userID){
        $user = $this->userManager->get($userID);
        if(empty($user)){
            throw new Exception("Uživatel s tímto ID neexistuje.");
        }
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
           throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $prices = $this->database->query("SELECT sum(price*quantity) as supposedprice,i3.name FROM productorderitem  LEFT JOIN items as i3 ON currency=i3.id and i3.groupid=3 WHERE productorderid=%ID% group by i3.name");
        $supposedPrice = 0;
        foreach($prices as $price){
            $supposedPrice = $price["supposedprice"];
        }
        $order->update([
            self::COLUMN_STATUS => self::ORDER_ACCEPTED,
            self::COLUMN_APPROVED => true,
            self::COLUMN_APPROVED_DATE => new DateTime(),
            self::COLUMN_APPROVER => $userID,
            self::COLUMN_APPROVED_PRICE => $supposedPrice,
            self::COLUMN_APPROVED_NOTE => $values[self::COLUMN_APPROVED_NOTE]]);
    }

    /**
     * Metoda zajístí evidenci informací o expedici objednávky.
     * @param $id identifikátor objednávky, která má být expedována
     * @param $values pole hodnot z formuláře
     * @param $userID identifikátor aktuálně přihlášeného uživatele, který má zajistit expedici objednávky
     * @param bool $partially má být objednávka expedována jen částečně?
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function expediteOrder($id, $values, $userID, $partially = false){
        $user = $this->userManager->get($userID);
        if(empty($user)){
            throw new Exception("Uživatel s tímto ID neexistuje.");
        }
        $this->throwExceptionIfErrorExpediteOrder($id, $values);
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $this->throwExceptionIfErrorExpediteOrder($order, $values);
        $status = self::ORDER_PREPARED_TO_SEND;
        if($partially === true){
            $status = self::ORDER_PARTIALLY_EXPEDITED;
        }
        $order->update([
            self::COLUMN_STATUS => $status,
            self::COLUMN_EXPEDITION_USER => $values[self::COLUMN_EXPEDITION_USER],
            self::COLUMN_EXPEDITION_DATE => $values[self::COLUMN_EXPEDITION_DATE],
            self::COLUMN_EXPEDITION_RECEIVER => $userID,
            self::COLUMN_EXPEDITION_DATE2 => new DateTime(),
            self::COLUMN_EXPEDITION_USER2 => $userID
        ]);
    }

    /**
     * Metoda kontroluje, zda jsou hodnoty z formuláře pro expedici objednávky validní.
     * @param $order objekt představující objednávku, která má být expedována
     * @param $values pole hodnot z formuláře
     * @throws Exception Výjimka, která je vyhozena, pokud nějaká hodnota z formuláře není validní.
     */
    public function throwExceptionIfErrorExpediteOrder($order, $values){
        $expeditionDate = $values[self::COLUMN_EXPEDITION_DATE];
        if(!$expeditionDate instanceof DateTime){
            throw new Exception("Datum expedice musí být datumem.");
        }
        if($expeditionDate > new DateTime()){
            throw new Exception("Datum expedice nesmí být v budoucnosti.");
        }
        if($expeditionDate < $order[self::COLUMN_APPROVED_DATE]){
            throw new Exception("Datum expedice nesmí být dřivější než datum přijetí objednávky.");
        }
        $user = $this->userManager->get($values[self::COLUMN_EXPEDITION_USER]);
        if(empty($user)){
            throw new Exception("Uživatel s tímto ID nebyl nalezen.");
        }
    }

    /**
     * Metoda zajistí evidenci informací o fakturaci objednávky
     * @param $id identifikátor objednávky, která má být fakturována
     * @param $values pole hodnot z formuláře
     * @param bool $partially má být objednávka fakturována částečně?
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function invoiceOrder($id, $values, $partially = false){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $this->throwExceptionIfErrorInvoiceOrder($order, $values);
        $status = self::ORDER_INVOICED;
        if($partially === true){
            $status = self::ORDER_PARTIALLY_INVOICED;
        }
        $order->update([
            self::COLUMN_STATUS => $status,
            self::COLUMN_INVOICEID => $values[self::COLUMN_INVOICEID],
            self::COLUMN_INVOICE_PRICE => $values[self::COLUMN_INVOICE_PRICE],
            self::COLUMN_INVOICE_TYPE => $values[self::COLUMN_INVOICE_TYPE],
            self::COLUMN_INVOICE_DATE => $values[self::COLUMN_INVOICE_DATE],
        ]);
    }

    /**
     * @param $order objekt představující objednávku
     * @param $values pole hodnot z formuláře
     * @throws Exception Výjimka, která je vyhozena, pokud hodnoty z formuláře nejsou validní.
     */
    public function throwExceptionIfErrorInvoiceOrder($order, $values){
        $price = $values[self::COLUMN_INVOICE_PRICE];
        $date = $values[self::COLUMN_INVOICE_DATE];
        $type = $values[self::COLUMN_INVOICE_TYPE];
        if(!is_numeric($price)){
            throw new Exception("Cena musí být reálné číslo.");
        }
        if($price <= 0){
            throw new Exception("Cena musí být kladné číslo.");
        }
        if(!is_integer($type)){
            throw new Exception("Způsob úhrady musí být celé číslo.");
        }
    }

    /**
     * Metoda zajistí evidenci platby objednávky.
     * @param $id identifikátor objednávky
     * @param $values pole hodnot z formuláře
     * @param bool $partially je objednávka zaplacena pouze částečně?
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function payOrder($id, $values, $partially = false){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $status = self::ORDER_PAID;
        if($partially === true){
            $status = self::ORDER_PARTIALLY_PAID;
        }
        $order->update([
            self::COLUMN_STATUS => $status,
            self::COLUMN_PAID_NOTE => $values["paidnote"],
            self::COLUMN_PAID => true,
            self::COLUMN_PAID_DATE => new DateTime(),
            self::COLUMN_PAID_USER => $values["user"],
            self::COLUMN_PAYMENT_DATE => $values["paymentdate"]
        ]);
    }

    /**
     * Metoda validuje hodnoty z formuláře pro evidenci platby
     * @param $order objekt představující objednávku
     * @param $values pole hodnot z formuláře
     * @throws Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní.
     */
    public function throwExceptionIfErrorPayOrder($order, $values){
        $user = $values["user"];
        $paymentDate = $values["paymentdate"];
        $this->userManager->get($user);
        if($paymentDate > new DateTime()){
            throw new Exception("Datum platby nesmí být pozdější než dnešní datum.");
        }
        if($order[self::COLUMN_INVOICE_DATE] > $paymentDate){
            throw new Exception("Datum platby nesmí být dřivější než datum fakturace objednávky.");
        }
    }

    /**
     * Metoda zajistí evidenci informací o tom, že objednávka má být dána do výroby
     * @param $id identifikátor objednávky
     * @param $values pole hodnot z formuláře
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function giveOrderToProduction($id, $values){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $order->update([self::COLUMN_STATUS => self::ORDER_IN_PRODUCTION,
                        self::COLUMN_PRODUCTION_DEADLINE => $values["deadline"],
                         self::COLUMN_PRODUCTION_STATUS => 1
        ]);
    }

    /**
     * Metoda eviduje, že objednávka je vyrobena.
     * @param $id identifikátor objednávky, která má být vyrobena
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function produceOrder($id){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $order->update([self::COLUMN_STATUS => self::ORDER_PRODUCED,
            self::COLUMN_PRODUCTION_STATUS => 2
        ]);
    }

    /**
     * Metoda zajisstí změnu stavu objednávky
     * @param $id identifikátor objednávky, u níž má být změněn stav
     * @param $status stav, který objednávka má mít
     * @throws Exception Pokud stav s danou číselnou hodnotou neexistuje.
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function changeStatus($id, $status){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        if($status < 0 && $status > 11){
            throw new Exception("Takový stav neexistuje.");
        }
        $order->update([self::COLUMN_STATUS => $status]);
    }

    /**
     * @param $id identifikátor objednávky, která má být přijata
     * @param $userID identifikátor uživatele, který přijímá danou objednávku
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function receiveOrder($id, $userID){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $order->update([self::COLUMN_STATUS => self::ORDER_DELIVERED,
            self::COLUMN_RECEIVE_DATE => new DateTime(),
            self::COLUMN_RECEIVER => $userID]);
    }

    /**
     * Metoda zajistí uzavření objednávky
     * @param $id identifikátor objednávky, která má být uzavřena
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function closeOrder($id){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException("Nelze nalézt objednávku s daným ID.");
        }
        $order->update([self::COLUMN_STATUS => self::ORDER_CLOSED,
                        self::COLUMN_CLOSED => true]);
    }

}