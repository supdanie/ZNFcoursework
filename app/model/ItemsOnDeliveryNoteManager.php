<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.5.18
 * Time: 16:32
 */

namespace App\Model;


use Nette\Database\Context;
use Nette\Neon\Exception;

/**
 * Class ItemsOnDeliveryNoteManager třída pro správu položek na dodacích listech
 * @package App\Model
 */
class ItemsOnDeliveryNoteManager extends BaseManager
{
    const TABLE_NAME = "productsondeliverynote",
        COLUMN_ID = "id",
        COLUMN_PRODUCT = "productid",
        COLUMN_DELIVERY_NOTE = "deliverynoteid",
        COLUMN_PRODUCT_NAME = "productname",
        COLUMN_PRODUCT_QUANTITY = "productquantity",
        COLUMN_UNIT_PRICE = "unitprice",
        COLUMN_IN_EVIDENCE = "inevidence";

    /** @var ProductTypeManager  instance třídy ProductTypeManager, která slouží k získání informací o typech produktů*/
    private $productTypeManager;
    /** @var DeliveryNoteManager instance třídy pro správu dodacích listů */
    private $deliveryNoteManager;

    /**
     * ItemsOnDeliveryNoteManager constructor.
     * @param Context $database objekt přestavující databázi
     * @param ProductTypeManager $productTypeManager instance třídy ProductTypeManager, která slouží k získání informací o typech produktů
     * @param DeliveryNoteManager $deliveryNoteManager instance třídy pro správu dodacích listů
     */
    public function __construct(Context $database, ProductTypeManager $productTypeManager,
                                DeliveryNoteManager $deliveryNoteManager)
    {
        parent::__construct($database);
        $this->productTypeManager = $productTypeManager;
        $this->deliveryNoteManager = $deliveryNoteManager;
    }

    /**
     * Metoda zajistí přidání nové položky na dodací list.
     * @param $values pole hodnot z formuláře pro přidání položky na dodací list
     * @param $deliveryNoteID  identifikátor dodacího listu
     * @throws NoDataFoundException Výjimka je vyhozena, pokud záznam s daným ID nebyl nalezen.
     */
    public function addItemOnDeliveryNote($values, $deliveryNoteID){
        $deliveryNote = $this->deliveryNoteManager->get($deliveryNoteID);
        if(empty($deliveryNote)){
            throw new Exception("Dodací list s tímto ID nebyl nalezen.");
        }
        $this->throwExceptionIfErrorAdd($values);
        $count = $this->database->table(self::TABLE_NAME)->count();
        $this->database->table(self::TABLE_NAME)->insert([
            self::COLUMN_ID => $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1,
            self::COLUMN_PRODUCT => $values["product"],
            self::COLUMN_DELIVERY_NOTE => $deliveryNoteID,
            self::COLUMN_PRODUCT_NAME => $values["name"],
            self::COLUMN_PRODUCT_QUANTITY => $values["quantity"],
            self::COLUMN_UNIT_PRICE => $values["price"],
            self::COLUMN_IN_EVIDENCE => 1
        ]);
    }

    /**
     * @param $id identifikátor položky na dodacím listu
     * @param $values pole hodnot z formuláře
     * @param $deliveryNoteID identifikátor dodacího listu
     * @throws Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní.
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen, nebo daný produkt není nalezen
     */
    public function editItemOnDeliveryNote($id, $values, $deliveryNoteID){
        $deliveryNote = $this->deliveryNoteManager->get($deliveryNoteID);
        if(empty($deliveryNote)){
            throw new Exception("Dodací list s tímto ID nebyl nalezen.");
        }
        $this->throwExceptionIfErrorAdd($values);
        $item = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($item)){
            throw new NoDataFoundException("Položka s tímto ID nebyla nalezena");
        }
        $item->update([
            self::COLUMN_ID => $id,
            self::COLUMN_PRODUCT => $values["product"],
            self::COLUMN_DELIVERY_NOTE => $deliveryNoteID,
            self::COLUMN_PRODUCT_NAME => $values["name"],
            self::COLUMN_PRODUCT_QUANTITY => $values["quantity"],
            self::COLUMN_UNIT_PRICE => $values["price"],
            self::COLUMN_IN_EVIDENCE => 1
        ]);
    }

    /**
     * Metoda kontroluje validitu hodnot z formuláře
     * @param $values pole hodnot z formuláře pro přidání položky na dodací list
     * @throws Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní.
     */
    public function throwExceptionIfErrorAdd($values){
        $quantity = $values["quantity"];
        if(!is_integer($quantity)){
            throw new Exception("Množství musí být celé číslo.");
        }
        if($quantity <= 0){
            throw new Exception("Množství musí být kladné.");
        }
        $price = $values["price"];
        if(!is_numeric($price)){
            throw new Exception("Cena musí být číslo.");
        }
        if($price <= 0){
            throw new Exception("Cena musí být kladná.");
        }
        $product = $this->productTypeManager->get($values["product"]);
        if(empty($product)){
            throw new Exception("Produkt s tímto ID neexistuje.");
        }
    }

    /**
     * @param $deliveryNote identifikátor dodacího listu
     * @return static položky na dodacím listu
     */
    public function getItemsOnDeliveryNote($deliveryNote){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_DELIVERY_NOTE, $deliveryNote);
    }

    /**
     * Metoda smaže z dodacího listu všechny položky daného typu produktu
     * @param $product identifikátor produktu z tabulky typů produktů
     */
    public function removeItemsWithProduct($deliveryNote, $product){
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_DELIVERY_NOTE, $deliveryNote)->where(self::COLUMN_PRODUCT, $product)->delete();
    }
}