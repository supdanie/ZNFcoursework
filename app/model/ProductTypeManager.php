<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:50
 */

namespace App\Model;
use Nette\Neon\Exception;

/**
 * Class OrderManager Třída pro získání informací o produktech.
 * @package App\Model
 */
class ProductTypeManager extends BaseManager
{
    const TABLE_NAME = "producttypes",
        COLUMN_ID = "id",
        COLUMN_NAME = "name",
        COLUMN_SERIAL_NUMBER="serialnumber",
        COLUMN_VERSION="version",
        COLUMN_PRODUCT_COMMENT="productcomment",
        COLUMN_PRICE="price",
        COLUMN_IN_EVIDENCE="inevidence",
        COLUMN_QUANTITY="quantity",
        COLUMN_UNITS="units",
        COLUMN_UNIT="unit",
        COLUMN_AUTHORIZED="authorized",
        COLUMN_STATUS="status",
        COLUMN_SERVICE_GROUP="servicegroupid",
        COLUMN_SERVICE="service",
        COLUMN_QUANTITY2="quantity2",
        COLUMN_PRICE2="price2",
        COLUMN_PRODUCT_CATEGORY="productcategoryid",
        COLUMN_PRICELIST="pricelist",
        COLUMN_AUTHOR="author";

    /**
     * Metoda zajistí přidání nového produktu do databáze. Tato metoda není využita.
     * @param $values pole hodnot z formuláře
     */
    public function addProductType($values){
        $count = $this->database->table(self::TABLE_NAME)->count();
        if($this->validateProductType($values) == true) {
            $this->database->table(self::TABLE_NAME)->insert(
                [self::COLUMN_ID => $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1,
                    self::COLUMN_NAME => $values[self::COLUMN_NAME],
                    self::COLUMN_SERIAL_NUMBER => $values[self::COLUMN_SERIAL_NUMBER],
                    self::COLUMN_VERSION => $values[self::COLUMN_VERSION],
                    self::COLUMN_PRODUCT_COMMENT => $values[self::COLUMN_PRODUCT_COMMENT],
                    self::COLUMN_PRICE => $values[self::COLUMN_PRICE],
                    self::COLUMN_IN_EVIDENCE => $values[self::COLUMN_IN_EVIDENCE],
                    self::COLUMN_QUANTITY => $values[self::COLUMN_QUANTITY],
                    self::COLUMN_UNITS => $values[self::COLUMN_UNITS],
                    self::COLUMN_UNIT => $values[self::COLUMN_UNIT],
                    self::COLUMN_AUTHORIZED => $values[self::COLUMN_AUTHORIZED],
                    self::COLUMN_STATUS => 0,
                    self::COLUMN_SERVICE_GROUP => $values[self::COLUMN_SERVICE_GROUP],
                    self::COLUMN_SERVICE => $values[self::COLUMN_SERVICE],
                    self::COLUMN_QUANTITY2 => $values[self::COLUMN_QUANTITY2],
                    self::COLUMN_PRICE2 => $values[self::COLUMN_PRICE2],
                    self::COLUMN_PRODUCT_CATEGORY => $values[self::COLUMN_PRODUCT_CATEGORY],
                    self::COLUMN_PRICELIST => $values[self::COLUMN_PRICELIST],
                    self::COLUMN_AUTHOR => $values[self::COLUMN_AUTHOR]
                ]);
        }
    }

    /**
     * @param $values pole hodnot z formuláře
     * @return bool zda jsou hodnoty z formuláře validní
     * @throws \Exception Pokud nějaká hodnota z formuláře není validní (nesprávný datový typ, či nesprávná hodnota jako například záporná cena)
     */
    public function validateProductType($values){
        if(!is_float($values[self::COLUMN_PRICE]) || !is_float($values[self::COLUMN_PRICE2])){
            throw new \Exception("Cena musí být reálné číslo");
        }
        if(!is_integer($values[self::COLUMN_QUANTITY])){
            throw new \Exception("Množství musí být celé číslo");
        }
        if(floatval($values[self::COLUMN_PRICE]) <= 0 || floatval($values[self::COLUMN_PRICE2]) <= 0){
            throw new \Exception("Cena musí být kladné reálné číslo");
        }
        if(intval($values[self::COLUMN_QUANTITY]) < 0){
            throw new \Exception("Množství nesmí být záporné číslo");
        }
        return true;
    }

    /**
     * Metoda slouží k editaci typu produktu. Tato metoda opět není využita.
     * @param $id identifikátor typu produktu, který má být editován
     * @param $values pole hodnot z formuláře
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen.
     */
    public function editProductType($id, $values){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException();
        }
        if($this->validateProductType($values) == true) {
            $order->update(
                [self::COLUMN_ID => $id,
                    self::COLUMN_NAME => $values[self::COLUMN_NAME],
                    self::COLUMN_SERIAL_NUMBER => $values[self::COLUMN_SERIAL_NUMBER],
                    self::COLUMN_VERSION => $values[self::COLUMN_VERSION],
                    self::COLUMN_PRODUCT_COMMENT => $values[self::COLUMN_PRODUCT_COMMENT],
                    self::COLUMN_PRICE => $values[self::COLUMN_PRICE],
                    self::COLUMN_IN_EVIDENCE => $values[self::COLUMN_IN_EVIDENCE],
                    self::COLUMN_QUANTITY => $values[self::COLUMN_QUANTITY],
                    self::COLUMN_UNITS => $values[self::COLUMN_UNITS],
                    self::COLUMN_UNIT => $values[self::COLUMN_UNIT],
                    self::COLUMN_AUTHORIZED => $values[self::COLUMN_AUTHORIZED],
                    self::COLUMN_STATUS => 0,
                    self::COLUMN_SERVICE_GROUP => $values[self::COLUMN_SERVICE_GROUP],
                    self::COLUMN_SERVICE => $values[self::COLUMN_SERVICE],
                    self::COLUMN_QUANTITY2 => $values[self::COLUMN_QUANTITY2],
                    self::COLUMN_PRICE2 => $values[self::COLUMN_PRICE2],
                    self::COLUMN_PRODUCT_CATEGORY => $values[self::COLUMN_PRODUCT_CATEGORY],
                    self::COLUMN_PRICELIST => $values[self::COLUMN_PRICELIST],
                    self::COLUMN_AUTHOR => $values[self::COLUMN_AUTHOR]
                ]);
        }
    }


}