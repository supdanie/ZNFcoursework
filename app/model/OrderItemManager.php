<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.5.18
 * Time: 17:05
 */

namespace App\Model;


use Nette\Database\Context;
use Nette\Neon\Exception;

/**
 * Class OrderItemManager Třída pro správu položek v objednávkách. Umožňuje přidávání a odebírání položek z dodacího listu.
 * @package App\Model
 */
class OrderItemManager extends BaseManager
{
    const TABLE_NAME = "productorderitem",
        COLUMN_ID = "id",
        COLUMN_NAME = "name",
        COLUMN_QUANTITY = "quantity",
        COLUMN_PRODUCT_ORDER = "productorderid",
        COLUMN_PRICE = "price",
        COLUMN_CURRENCY = "currency",
        COLUMN_PRODUCT = "productid";
    /** @var ProductTypeManager  instance třídy ProductTypeManager, která slouží k získání informací o typech produktů*/
    private $productTypeManager;
    /**
     * @var OrderManager instance třídy OrderManager, která slouží k získání informací o objednávkách
     */
    private $orderManager;

    /**
     * OrderItemManager constructor.
     * @param Context $database objekt představující databázi
     * @param ProductTypeManager $productTypeManager instance třídy ProductTypeManager, která slouží k získání informací o typech produktů
     * @param OrderManager $orderManager instance třídy OrderManager, která slouží k získání informací o objednávkách
     */
    public function __construct(Context $database, ProductTypeManager $productTypeManager, OrderManager $orderManager){
        parent::__construct($database);
        $this->productTypeManager = $productTypeManager;
        $this->orderManager = $orderManager;
    }

    /**
     * Metoda přidá položku do dané objednávky
     * @param $values pole hodnot z formuláře
     * @param $productOrderID identifikátor objednávky, do které má být položka přidána
     */
    public function addItemOnOrder($values, $productOrderID){
        $order = $this->orderManager->get($productOrderID);
        if(empty($order)){
            throw new Exception("Objednávka s tímto ID neexistuje.");
        }
        $this->throwExceptionIfErrorOccured($values);
        $count = $this->database->table(self::TABLE_NAME)->count();
        $this->database->table(self::TABLE_NAME)->insert([
            self::COLUMN_ID => $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1,
            self::COLUMN_NAME => $values["name"],
            self::COLUMN_QUANTITY => $values["quantity"],
            self::COLUMN_PRODUCT_ORDER => $productOrderID,
            self::COLUMN_PRICE => $values["price"],
            self::COLUMN_CURRENCY => $values["currency"],
            self::COLUMN_PRODUCT => $values["product"]
        ]);
    }

    /**
     * Metoda upraví danou položku v objednávce.
     * @param $id identifikátor položky objednávky, která má být editována
     * @param $values pole hodnot z formuláře
     * @param $productOrderID identifikátor objednávky
     * @throws Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní.
     * @throws NoDataFoundException Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní, nebo položka s daným ID nebyla nalezena.
     */
    public function editItemOnOrder($id, $values, $productOrderID){
        $order = $this->orderManager->get($productOrderID);
        if(empty($order)){
            throw new Exception("Objednávka s tímto ID neexistuje.");
        }
        $this->throwExceptionIfErrorOccured($values);
        $items = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id);
        if($items->count() === 0){
            throw new NoDataFoundException("Položka s tímto id nebyla nalezena.");
        }
        $items->update([
                self::COLUMN_ID => $id,
                self::COLUMN_NAME => $values["name"],
                self::COLUMN_QUANTITY => $values["quantity"],
                self::COLUMN_PRODUCT_ORDER => $productOrderID,
                self::COLUMN_PRICE => $values["price"],
                self::COLUMN_CURRENCY => $values["currency"],
                self::COLUMN_PRODUCT => $values["product"]
        ]);
    }

    /**
     * Metoda validuje hodnoty z formuláře pro přidání položky do objednávky.
     * @param $values pole hodnot z formuláře
     * @throws Exception Výjimka je vyhozena, pokud nějaká hodnota z formuláře není validní.
     */
    public function throwExceptionIfErrorOccured($values){
        $productType = $this->productTypeManager->get($values["product"]);
        if(empty($productType)){
            throw new Exception("Produkt s tímto ID neexistuje");
        }
        $quantity = $values["quantity"];
        if(!is_integer($quantity)){
            throw new Exception("Množství musí být celé číslo.");
        }
        if($quantity <= 0){
            throw new Exception("Musí být objednáno kladné množství.");
        }
        $price = $values["price"];
        if(!is_numeric($price)){
            throw new Exception("Cena musí být číslo.");
        }
        if($price <= 0){
            throw new Exception("Cena musí být kladné číslo.");
        }
        $currency = $values["currency"];
        if(!is_integer($currency)){
            throw new Exception("Měna musí být celé číslo.");
        }
        if($currency <= 0 || $currency > 4){
            throw new Exception("Měna musí být číslem od 1 do 4.");
        }
    }

    /**
     * @param $order identifikátor objednávky
     * @return array|\Nette\Database\Table\IRow[]|\Nette\Database\Table\Selection seznam položek dané objednávky
     */
    public function getItemsOfOrder($order)
    {
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_PRODUCT_ORDER, $order)->fetchAll();
    }

    /**
     * @param $order identifikátor objednávky
     * @param $product identifikátor produktu
     * @return bool je objednán daný produkt v rámci dané objednávky?
     */
    public function existsItemOnOrder($order, $product){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_PRODUCT_ORDER, $order)
            ->where(self::COLUMN_PRODUCT, $product)->count() > 0;
    }


}