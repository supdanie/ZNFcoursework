<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:50
 */

namespace App\Model;

use Nette;

/**
 * Class BaseManager Základní třída, která slouží k provádění základních operací nad tabulkami.
 * @package App\Model
 */
class BaseManager
{
    use Nette\SmartObject;
    /**
     * Name of table and name of ID column which is the primary key of the table.
     */
    const TABLE_NAME="Table",
            COLUMN_ID="ID",
            COLUMN_DELETED="deleted";
    protected $database;

    /**
     * BaseManager constructor.
     * @param Nette\Database\Context $database Database context, který používáme pro základní CRUD operace
     */
    public function __construct(Nette\Database\Context $database){
        $this->database = $database;
    }

    /**
     * @param $id identifikátor dodacího listu
     * @return false|Nette\Database\Table\ActiveRow
     * @throws NoDataFoundException Výjimka je vyhozena, pokud záznam s daným ID není nalezen.
     */
    public function get($id){
        $record = $this->database->table(static::TABLE_NAME)->get($id);
        if(empty($record)){
            throw new NoDataFoundException("Nebyl nalezen odpovídající záznam s daným ID.");
        }
        return $record;
    }

    /**
     * Tato funkce vrací seznam všech řádků z tabulky.
     * @return Nette\Database\Table\Selection Objekt představující obsah tabulky.
     */
    public function getAll(){
        return $this->database->table(static::TABLE_NAME);
    }

    /**
     * @param $id identifikátor řádku, který má být smazán z databáze
     */
    public function remove($id){
        $this->database->table(static::TABLE_NAME)->where(static::COLUMN_ID, $id)->delete();
    }

    /**
     * @param $id identifikátor záznamů, který je určen ke smazání, bude na něj nahlíženo jako na smazaný, ale nebude definitivně smazán z databáze
     * @throws \App\Model\NoDataFoundException  Výjimka je vyhozena, pokud záznam s daným ID není nalezen.
     */
    public function setRemoved($id){
        $record = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($record)){
            throw new NoDataFoundException();
        }
        $record->update([self::COLUMN_DELETED => true]);
    }
}