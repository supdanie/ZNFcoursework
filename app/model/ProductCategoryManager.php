<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:50
 */

namespace App\Model;

/**
 * Class OrderManager Třída pro získání informací o kategoriích produktů.
 * @package App\Model
 */
class ProductCategoryManager extends BaseManager
{
    const TABLE_NAME = "productcategory",
        COLUMN_ID = "id",
        COLUMN_NAME = "name";

}