<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:50
 */

namespace App\Model;


use Nette\Utils\DateTime;
use Nette;

/**
 * Class DeliveryNoteManager třída pro správu dodacích listů. Umožňuje základní operace s dodacími listy a získávání informací o nich.
 * @package App\Model
 */
class DeliveryNoteManager extends BaseManager
{
    const TABLE_NAME="deliverynotes",
        COLUMN_ID="id",
        COLUMN_PRODUCT_ORDER = "productorderid",
        COLUMN_COMPANY = "companyid",
        COLUMN_STATUS = "status",
        COLUMN_TRANSACTION_DATE = "transactiondate",
        COLUMN_USER = "userid",
        COLUMN_TRANSPORT_OUTCOME = "transportoutcomeid",
        COLUMN_TRANSPORTCODE = "transportcode",
        COLUMN_RESOLUTION_DATE = "resolutiondate",
        COLUMN_USER2 = "userid2",
        COLUMN_TRANSPORTNOTE = "transportnote",
        COLUMN_INVOICE = "invoiceid",
        COLUMN_INVOICE_TYPE = "invoicetype",
        COLUMN_INVOICE_DATE = "invoicedate",
        COLUMN_INVOICE_USER = "invoiceuserid",
        COLUMN_PAYMENT_DATE = "paymentdate",
        COLUMN_PAID = "paid",
        COLUMN_PAID_DATE = "paiddate",
        COLUMN_PAID_USER = "paiduserid",
        COLUMN_PAIDNOTE = "paidnote",
        DELIVERY_NOTE_NEW = 0,
        DELIVERY_NOTE_ACCEPTED = 1,
        DELIVERY_NOTE_INVOICED = 2,
        DELIVERY_NOTE_PRODUCED = 3,
        DELIVERY_NOTE_PARTIALLY_PAID = 4,
        DELIVERY_NOTE_PAID = 5,
        DELIVERY_NOTE_IN_PRODUCTION = 6,
        DELIVERY_NOTE_PREPARED_TO_SEND = 7,
        DELIVERY_NOTE_SENT = 8,
        DELIVERY_NOTE_DELIVERED = 9,
        DELIVERY_NOTE_CLOSED = 10,
        DELIVERY_NOTE_CANCELED = 11,
        DELIVERY_NOTE_PARTIALLY_PRODUCED = 12,
        DELIVERY_NOTE_PARTIALLY_INVOICED = 13,
        DELIVERY_NOTE_PARTIALLY_EXPEDITED = 14,
        DELIVERY_NOTE_UNKNOWN = 15;

    /** @var OrderManager instance třídy pro správu objednávek */
    private $orderManager;

    /** @var OrderItemManager instance třídy pro správu položek na objednávce */
    private $orderItemManager;

    /** @var  UserManager instance třídy pro správu uživatelů */
    private $userManager;

    /**
     * DeliveryNoteManager constructor.
     * @param Nette\Database\Context $database objekt představující databázi
     * @param OrderManager $orderManager instance třídy pro správu objednávek
     * @param OrderItemManager $orderItemManager instance třídy pro správu položek na objednávce
     * @param UserManager $userManager instance třídy pro správu uživatelů
     */
    public function __construct(Nette\Database\Context $database, OrderManager $orderManager, OrderItemManager $orderItemManager,
        UserManager $userManager){
        parent::__construct($database);
        $this->orderManager = $orderManager;
        $this->orderItemManager = $orderItemManager;
        $this->userManager = $userManager;
    }

    /**
     * @param $value číselná hodnota stavu objednávky
     * @return mixed|string název stavu objednávky, pokud je číslo mimo rozsah 0 - 15, vrací prázdný řetězec
     */
    public static function getStatusTitle($value){
        if($value < 0 || $value > 15){
            return "";
        }
        $array = [0 => "Vytvořeno", 1 => "Přijato", 2 => "Fakturováno", 3 => "Vyrobeno", 4 => "Částečně zaplaceno",
            5 => "Zaplaceno", 6 => "Ve výrobě", 7 => "Připraveno k odeslání", 8 => "Odesláno", 9 => "Doručeno", 10 => "Uzavřeno",
            11 => "Zrušeno", 12 => "Částečně vyrobeno", 13 => "Částečně fakturováno", 14 => "Částečně fakturováno",
            15 => "Neznámý stav"];
        return $array[$value];
    }

    /**
     * Metoda přidá nový dodací list do databáze
     * @param $orderID identifikátor objednávky
     * @param $userID identifikátor uživatele, který přidává dodací list
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function addDeliveryNote($orderID, $userID){
        $order = $this->orderManager->get($orderID);
        $user = $this->userManager->get($userID);
        $count = $this->database->table(self::TABLE_NAME)->count();
        $id = $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1;
        $this->database->table(self::TABLE_NAME)->insert([
            self::COLUMN_ID => $id,
            self::COLUMN_PRODUCT_ORDER => $orderID,
            self::COLUMN_COMPANY => $order[OrderManager::COLUMN_COMPANY],
            self::COLUMN_STATUS => self::DELIVERY_NOTE_NEW,
            self::COLUMN_TRANSACTION_DATE => new DateTime(),
            self::COLUMN_USER => $userID
        ]);
    }


    /**
     * Metoda eviduje informace o expedici do databáze.
     * @param $id identifikátor dodacího listu
     * @param $values pole hodnot z formuláře
     * @param $userID identifikátor uživatele, který eviduje expedici zboží
     * @param bool $partially má být expedována jen část zboží?
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function expediteDeliveryNote($id, $values, $userID, $partially = false){
        $deliveryNote = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($deliveryNote)){
            throw new NoDataFoundException("Nelze nalézt dodací list s daným ID.");
        }
        if(!is_integer($values["transportoutcome"])){
            throw new Nette\Neon\Exception("Způsob dopravy musí být celé číslo.");
        }
        if($values["transportoutcome"] < 1 || $values["transportoutcome"] > 6){
            throw new Nette\Neon\Exception("Způsob dopravy musí být celé číslo od 1 do 6.");
        }
        $user = $this->userManager->get($userID);
        $status = self::DELIVERY_NOTE_PREPARED_TO_SEND;
        if($partially === true){
            $status = self::DELIVERY_NOTE_PARTIALLY_EXPEDITED;
        }
        $deliveryNote->update([
            self::COLUMN_STATUS => $status,
            self::COLUMN_TRANSPORT_OUTCOME => $values["transportoutcome"],
            self::COLUMN_TRANSPORTCODE => $values[self::COLUMN_TRANSPORTCODE],
            self::COLUMN_RESOLUTION_DATE => new DateTime(),
            self::COLUMN_USER2 => $userID,
            self::COLUMN_TRANSPORTNOTE => $values[self::COLUMN_TRANSPORTNOTE]
        ]);
    }

    /**
     * Metoda zajistí evidenci fakturace dodacího listu
     * @param $id identifikátor dodacího listu
     * @param $values pole hodnot z formuláře
     * @param $userID identifikátor uživatele, který fakturuje dodací list
     * @param bool $partially má být objednávka
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function invoiceDeliveryNote($id, $values, $userID, $partially = false){
        $deliveryNote = $this->database->table(self::TABLE_NAME)->get($id);
        $user = $this->userManager->get($userID);
        if(empty($deliveryNote)){
            throw new NoDataFoundException("Nelze nalézt dodací list s daným ID.");
        }
        if(!is_integer($values[self::COLUMN_INVOICE_TYPE])){
            throw new Nette\Neon\Exception("Způsob úhrady musí být celé číslo.");
        }
        if($values[self::COLUMN_INVOICE_TYPE] < 1 || $values[self::COLUMN_INVOICE_TYPE] > 8){
            throw new Nette\Neon\Exception("Způsob úhrady musí být celé číslo od 1 do 8.");
        }
        $status = self::DELIVERY_NOTE_INVOICED;
        if($partially === true){
            $status = self::DELIVERY_NOTE_PARTIALLY_INVOICED;
        }
        $deliveryNote->update([
            self::COLUMN_STATUS => $status,
            self::COLUMN_INVOICE => $values["invoice"],
            self::COLUMN_INVOICE_TYPE => $values[self::COLUMN_INVOICE_TYPE],
            self::COLUMN_INVOICE_DATE => $values[self::COLUMN_INVOICE_DATE],
            self::COLUMN_PAYMENT_DATE => $values["paymentdate"],
            self::COLUMN_INVOICE_USER => $userID
        ]);
    }

    /**
     * Metoda zajistí evidenci platby dodacího listu.
     * @param $id identifikátor dodacího listu
     * @param $values pole hodnot z formuláře
     * @param bool $partially má být dodací list zaplacen jen částečně?
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud záznam s daným ID není nalezen
     */
    public function payDeliveryNote($id, $values, $partially = false){
        $deliveryNote = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($deliveryNote)){
            throw new NoDataFoundException("Nelze nalézt dodací list s daným ID.");
        }
        $user = $this->userManager->get($values["paiduser"]);
        if($deliveryNote[self::COLUMN_INVOICE_DATE] > $values["paymentdate"]){
            throw new Nette\Neon\Exception("Datum fakturace nemůže být pozdější než datum platby.");
        }
        $status = self::DELIVERY_NOTE_PAID;
        if($partially === true){
            $status = self::DELIVERY_NOTE_PARTIALLY_PAID;
        }
        $deliveryNote->update([
            self::COLUMN_STATUS => $status,
            self::COLUMN_PAID => true,
            self::COLUMN_PAID_DATE => $values["paymentdate"],
            self::COLUMN_PAID_USER => $values["paiduser"],
            self::COLUMN_PAIDNOTE => $values["paidnote"],
        ]);
    }

    /**
     * @param $orderID identifikátor objednávky
     * @return static všechny dodací listy dané objednávky
     */
    public function getDeliveryNotesByOrder($orderID){
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_PRODUCT_ORDER, $orderID);
    }
}