<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:50
 */

namespace App\Model;
use Nette\Neon\Exception;
use Nette\Utils\DateTime;

/**
 * Class OrderManager Třída, která slouží k provádění operací s tabulkou companies. Slouží také k získávání informací o firmách.
 * @package App\Model
 */
class CompanyManager extends BaseManager
{
    const TABLE_NAME = "companies",
        COLUMN_ID = "id",
        COLUMN_COMPANY_NAME="companyname",
        COLUMN_ICO = "ico",
        COLUMN_ADDRESS1 = "adress1",
        COLUMN_ADDRESS2 = "adress2",
        COLUMN_ZIP = "zip",
        COLUMN_URL = "url",
        COLUMN_MAIL = "mail",
        COLUMN_PHONE = "phone",
        COLUMN_PHONE_GSM ="phonegsm",
        COLUMN_PHONE_FAX="phonefax",
        COLUMN_DIC = "dic",
        COLUMN_BANK_ACCOUNT = "bankaccount",
        COLUMN_BANK_CODE = "bankcode",
        COLUMN_PROVIDER = "provider",
        COLUMN_DELETED = "deleted",
        COLUMN_CUSTOMER = "customer",
        COLUMN_ADDRESS12 = "adress12",
        COLUMN_ADDRESS22 = "adress22",
        COLUMN_ZIP2 = "zip2",
        COLUMN_CONTACT = "contact";

    private $userManager;
    private $companyManager;

    /**
     * Metoda přidá novou firmu do databáze.
     * @param $values pole hodnot z formuláře
     */
    public function addOrder($values){
        $count = $this->database->table(self::TABLE_NAME)->count();
        if($this->validateCompany($values) == true) {
            $this->database->table(self::TABLE_NAME)->insert(
                [self::COLUMN_ID => $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1,
                    self::COLUMN_COMPANY_NAME => $values[self::COLUMN_COMPANY_NAME],
                    self::COLUMN_ICO => $values[self::COLUMN_ICO],
                    self::COLUMN_ADDRESS1 => $values[self::COLUMN_ADDRESS1],
                    self::COLUMN_ADDRESS2 => $values[self::COLUMN_ADDRESS2],
                    self::COLUMN_ZIP => $values[self::COLUMN_ZIP],
                    self::COLUMN_URL => $values[self::COLUMN_URL],
                    self::COLUMN_MAIL => $values[self::COLUMN_MAIL],
                    self::COLUMN_PHONE => $values[self::COLUMN_PHONE],
                    self::COLUMN_PHONE_GSM => $values[self::COLUMN_PHONE_GSM],
                    self::COLUMN_PHONE_FAX => $values[self::COLUMN_PHONE_FAX],
                    self::COLUMN_DIC => $values[self::COLUMN_DIC],
                    self::COLUMN_BANK_ACCOUNT => $values[self::COLUMN_BANK_ACCOUNT],
                    self::COLUMN_BANK_CODE => $values[self::COLUMN_BANK_CODE],
                    self::COLUMN_PROVIDER => $values[self::COLUMN_PROVIDER],
                    self::COLUMN_DELETED => false,
                    self::COLUMN_CUSTOMER => $values[self::COLUMN_CUSTOMER],
                    self::COLUMN_ADDRESS12 => $values[self::COLUMN_ADDRESS12],
                    self::COLUMN_ADDRESS22 => $values[self::COLUMN_ADDRESS22],
                    self::COLUMN_ZIP2 => $values[self::COLUMN_ZIP2],
                    self::COLUMN_CONTACT => $values[self::COLUMN_CONTACT]]);
        }
    }

    /**
     * @param $values pole hodnot z formuláře
     * @return bool zda hodnoty z formuláře jsou validní
     * @throws \Exception Pokud nějaká z hodnot z formuláře není validní
     */
    public function validateCompany($values){
        if(strlen($values[self::COLUMN_COMPANY_NAME]) < 1){
            throw new \Exception("Název firmy nemůže být prázdný řetězec");
        }
        return true;
    }

    /**
     * @param $id identifikátor objednávky k editaci
     * @param $values pole hodnot z formuláře
     * @throws NoDataFoundException Pokud objednávka s daným ID nebude nalezena.
     */
    public function editOrder($id, $values){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException();
        }
        if($order[self::COLUMN_DELETED] === true){
            throw new NoDataFoundException();
        }
        $order->update(
            [self::COLUMN_ID => $id,
                self::COLUMN_COMPANY_NAME => $values[self::COLUMN_COMPANY_NAME],
                self::COLUMN_ICO => $values[self::COLUMN_ICO],
                self::COLUMN_ADDRESS1 => $values[self::COLUMN_ADDRESS1],
                self::COLUMN_ADDRESS2  => $values[self::COLUMN_ADDRESS2],
                self::COLUMN_ZIP  => $values[self::COLUMN_ZIP],
                self::COLUMN_URL => $values[self::COLUMN_URL],
                self::COLUMN_MAIL => $values[self::COLUMN_MAIL],
                self::COLUMN_PHONE => $values[self::COLUMN_PHONE],
                self::COLUMN_PHONE_GSM  => $values[self::COLUMN_PHONE_GSM],
                self::COLUMN_PHONE_FAX => $values[self::COLUMN_PHONE_FAX],
                self::COLUMN_DIC  => $values[self::COLUMN_DIC],
                self::COLUMN_BANK_ACCOUNT  => $values[self::COLUMN_BANK_ACCOUNT],
                self::COLUMN_BANK_CODE  => $values[self::COLUMN_BANK_CODE],
                self::COLUMN_PROVIDER  => $values[self::COLUMN_PROVIDER],
                self::COLUMN_DELETED => false,
                self::COLUMN_CUSTOMER => $values[self::COLUMN_CUSTOMER],
                self::COLUMN_ADDRESS12  => $values[self::COLUMN_ADDRESS12],
                self::COLUMN_ADDRESS22  => $values[self::COLUMN_ADDRESS22],
                self::COLUMN_ZIP2  => $values[self::COLUMN_ZIP2],
                self::COLUMN_CONTACT  => $values[self::COLUMN_CONTACT]]);
    }

    /**
     * @param $id identifikátor firmy, která má být smazána
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud firma s daným ID neexistuje.
     */
    public function deleteCompany($id){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException();
        }
        $order->update([self::COLUMN_DELETED => true]);
    }

    /**
     * @param $id Identifikátor firmy, která má být obnovena.
     * @throws NoDataFoundException Výjimka, která je vyhozena, pokud firma s daným ID neexistuje.
     */
    public function addAgainCompany($id){
        $order = $this->database->table(self::TABLE_NAME)->get($id);
        if(empty($order)){
            throw new NoDataFoundException();
        }
        $order->update([self::COLUMN_DELETED => false]);
    }

}