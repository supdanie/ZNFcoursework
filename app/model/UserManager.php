<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'users',
		COLUMN_ID = 'id',
        COLUMN_REAL_NAME = 'name',
        COLUMN_SURNAME = "surname",
		COLUMN_NAME = 'username',
		COLUMN_PASSWORD_HASH = 'userpassword',
        COLUMN_READRIGHTS = "rrights",
        COLUMN_WRITERIGHT = "wrights",
        COLUMN_RIGHTS = "rights",
        COLUMN_EXECUTE_RIGHTS = "srights",
		COLUMN_EMAIL = 'email',
        COLUMN_SETUP = "setup",
		COLUMN_DELETEE = 'deleted',
        COLUMN_ROLE = "role";


	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$row = $this->database->table(self::TABLE_NAME)
			->where(self::COLUMN_NAME, $username)
			->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

		} elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
			$row->update([
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
			]);
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}


	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @param  string
	 * @return void
	 * @throws DuplicateNameException
	 */
	public function add($username, $email, $password)
	{
	    $count = $this->database->table(self::TABLE_NAME)->count();
        $id = $count == 0 ? 0 : $this->database->table(self::TABLE_NAME)->max(self::COLUMN_ID) + 1;
		try {
			$this->database->table(self::TABLE_NAME)->insert([
			    self::COLUMN_ID => $id,
				self::COLUMN_NAME => $username,
                self::COLUMN_REAL_NAME => $username,
                self::COLUMN_SURNAME => $username,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
				self::COLUMN_EMAIL => $email,
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

    /**
     * @param $id Identificator of the object which we find.
     * @return false|Nette\Database\Table\ActiveRow
     * @throws NoDataFoundException The exception is thrown when the record in table with the given ID is not found.
     */
    public function get($id){
        $record = $this->database->table(static::TABLE_NAME)->get($id);
        if(empty($record)){
            throw new NoDataFoundException("Nebyl nalezen odpovídající záznam s daným ID. ". $id);
        }
        return $record;
    }

    /**
     * This function returns all rows from the database table.
     * @return Nette\Database\Table\Selection Content of the table.
     */
    public function getAll(){
        return $this->database->table(static::TABLE_NAME);
    }
}



class DuplicateNameException extends \Exception
{
}
