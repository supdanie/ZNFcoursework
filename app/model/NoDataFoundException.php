<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21.3.18
 * Time: 21:52
 */

namespace App\Model;

/**
 * Class NoDataFoundException Výjimka, která je vyhozena, pokud nějaký záznam v databázi není nalezen.
 * @package App\Model
 */
class NoDataFoundException extends \Exception {

};
