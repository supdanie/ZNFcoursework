<?php

namespace App\Forms;

use App\Model;
use Nette;
use Nette\Application\UI\Form;


/**
 * Class SignUpFormFactory továrnička na formulář pro registraci uživatele
 * @package App\Forms
 */
class SignUpFormFactory
{
	use Nette\SmartObject;

    /**
     * Minimální délka hesla.
     */
	const PASSWORD_MIN_LENGTH = 7;

	/** @var FormFactory základní továrna na formuláře */
	private $factory;

	/** @var Model\UserManager instance třídy pro správu uživatelů */
	private $userManager;


    /**
     * SignUpFormFactory constructor.
     * @param FormFactory $factory základní továrna na formuláře
     * @param Model\UserManager $userManager instance třídy pro správu uživatelů
     */
	public function __construct(FormFactory $factory, Model\UserManager $userManager)
	{
		$this->factory = $factory;
		$this->userManager = $userManager;
	}


	/**
	 * @return Form formulář pro registraci nového uživatele
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->addText('username', 'Pick a username:')
			->setRequired('Please pick a username.')
            ->addRule(Form::MAX_LENGTH, "Uživatelské jméno nesmí být delší než 10 znaků.", 10);

		$form->addEmail('email', 'Your e-mail:')
			->setRequired('Please enter your e-mail.');

		$form->addPassword('password', 'Create a password:')
			->setOption('description', sprintf('at least %d characters', self::PASSWORD_MIN_LENGTH))
			->setRequired('Please create a password.')
			->addRule($form::MIN_LENGTH, null, self::PASSWORD_MIN_LENGTH);

		$form->addSubmit('send', 'Sign up');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->userManager->add($values->username, $values->email, $values->password);
			} catch (Model\DuplicateNameException $e) {
				$form['username']->addError('Username is already taken.');
				return;
			}
			$onSuccess();
		};

		return $form;
	}
}
