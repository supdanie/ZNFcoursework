<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;

/**
 * Class FormFactory Třída, která zajišťuje vytváření formulářů.
 * @package App\Forms
 */
class FormFactory
{
	use Nette\SmartObject;

	/**
	 * @return Form Základní formulář bez políček.
	 */
	public function create()
	{
		$form = new Form;
        $form->getElementPrototype()->novalidate('novalidate');
		return $form;
	}
}
