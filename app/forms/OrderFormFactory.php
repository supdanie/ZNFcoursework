<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.5.18
 * Time: 12:35
 */

namespace App\Forms;

use App\Model\CompanyManager;
use App\Model\OrderManager;
use App\Model\UserManager;
use Nette;

/**
 * Class OrderFormFactory továrnička na formuláře pro správu objednávek
 * @package App\Forms
 */
class OrderFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory základní továrnička na formuláře */
    private $factory;
    /** @var  OrderManager instance třídy pro správu objednávek */
    private $orderManager;
    /** @var  CompanyManager instance třídy pro získání informací o firmách */
    private $companyManager;
    /** @var  UserManager instance třídy pro správu uživatelů*/
    private $userManager;

    /**
     * OrderFormFactory constructor.
     * @param FormFactory $factory základní továrnička na formuláře
     * @param OrderManager $orderManager instance třídy pro správu objednávek
     * @param CompanyManager $companyManager instance třídy pro získání informací o firmách
     * @param UserManager $userManager instance třídy pro správu uživatelů
     */
    public function __construct(FormFactory $factory, OrderManager $orderManager, CompanyManager $companyManager,
    UserManager $userManager)
    {
        $this->factory = $factory;
        $this->orderManager = $orderManager;
        $this->companyManager = $companyManager;
        $this->userManager = $userManager;
    }

    /**
     * @return Nette\Application\UI\Form Formulář pro přidání nové objednávky
     */
    public function createNewForm(){
        $form = $this->factory->create();
        $form->addSelect(OrderManager::COLUMN_COMPANY, "Firma")
            ->setRequired("Prosím zvolte firmu, která danou objednávku provedla.");
        $form->addText(OrderManager::COLUMN_COMPANY_ORDER_NUMBER, "Číslo objednávky firmy")
            ->setRequired(false);
        $form->addSelect(OrderManager::COLUMN_ORDERTYPE, "Způsob objednání")
            ->setRequired("Prosím zvolte způsob objednání.");
        $form->addUpload(OrderManager::COLUMN_ORDER_FILE, "Soubor s objednávkou")
            ->setRequired(false)
            ->addRule(Nette\Application\UI\Form::MAX_FILE_SIZE,  "Soubor může mít maximálně 64 MiB.", 64*1024*1024);
        $form->addSubmit("addOrder", "Vložit objednávku");
        $allCompanies = $this->companyManager->getAll();
        $companies = array();
        foreach($allCompanies as $company){
            $id = $company[CompanyManager::COLUMN_ID];
            $name = $company[CompanyManager::COLUMN_COMPANY_NAME];
            $address = $company[CompanyManager::COLUMN_ADDRESS1]." ".
                $company[CompanyManager::COLUMN_ADDRESS12]." ".
                $company[CompanyManager::COLUMN_ADDRESS2]." ".
                $company[CompanyManager::COLUMN_ADDRESS22];
            $companies[$id] = $name." (".$address.")"." (".$id.")";
        }
        $form[OrderManager::COLUMN_COMPANY]->setItems($companies);
        $form[OrderManager::COLUMN_ORDERTYPE]->setItems([1 => "Ústní", 2 => "Telefonická", 3 => "Osobní", 4 => "E-mailem"]);
        $form->onSuccess[] = [$this, "newFormSucceded"];
        return $form;
    }


    /**
     * Metoda zajistí přidání nové objednávky do databáze.
     * @param Nette\Application\UI\Form $form formulář pro přidání nové objednávky
     * @param $values pole hodnot z formuláře
     */
    public function newFormSucceded(Nette\Application\UI\Form $form, $values){
        try {
            $this->orderManager->addNewOrder($values);
        } catch(Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

    /**
     * @param $id identifikátor objednávky, která má být expedována
     * @param $userID identifikátor uživatele, který zajistí expedici
     * @return Nette\Application\UI\Form formulář pro evidenci expedice objednávky
     */
    public function expediteOrderForm($id, $userID){
        $form = $this->factory->create();
        $form->addSelect(OrderManager::COLUMN_EXPEDITION_USER, "Expedoval");
        $form->addDatePicker(OrderManager::COLUMN_EXPEDITION_DATE, "Datum expedice");
        $form->addHidden("id", $id);
        $form->addHidden("user", $userID);
        $form->addSubmit("expediteOrder", "Expedovat objednávku");
        $form->onSuccess[] = [$this, "expediteFormSucceded"];
        $allUsers = $this->userManager->getAll();
        $users = array();
        foreach($allUsers as $user){
            $id = $user[UserManager::COLUMN_ID];
            $name = $user[UserManager::COLUMN_REAL_NAME];
            $users[$id] = $name;
        }
        $form[OrderManager::COLUMN_EXPEDITION_USER]->setItems($users);
        return $form;
    }

    /**
     * Metoda zajistí evidenci informací o expedici do databáze.
     * @param Nette\Application\UI\Form $form formulář pro evidenci expedice objednávky
     * @param $values pole hodnot z formuláře
     */
    public function expediteFormSucceded(Nette\Application\UI\Form $form, $values){
        try{
            $this->orderManager->expediteOrder(intval($values["id"]), $values, intval($values["user"]));
        } catch (Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

    /**
     * @param $id identifikátor objednávky, která má být přijata
     * @param $userID identifikátor uživatele, který přijímá objednávku
     * @return Nette\Application\UI\Form formulář pro evidenci přijetí objednávky
     */
    public function acceptOrderForm($id, $userID){
        $form = $this->factory->create();
        $form->addText(OrderManager::COLUMN_APPROVED_NOTE, "Poznámka");
        $form->addHidden("id", $id);
        $form->addHidden("user", $userID);
        $form->addSubmit("acceptOrder", "Přijmout objednávku");
        $form->onSuccess[] = [$this, "approveOrderFormSucceded"];
        return $form;
    }

    /**
     * Metoda zajistí evidenci přijetí objednávky
     * @param Nette\Application\UI\Form $form formulář pro evidenci přijetí objednávky
     * @param $values pole hodnot z formuláře
     */
    public function approveOrderFormSucceded(Nette\Application\UI\Form $form, $values){
        try{
            $this->orderManager->acceptOrder(intval($values["id"]), $values, intval($values["user"]));
        } catch (Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

    /**
     * @param $id identifikátor objednávky, která má být dána do výroby
     * @return Nette\Application\UI\Form formulář pro evidenci informací o tom, že objednávka má být dána do výroby
     */
    public function giveOrderToProductionForm($id){
        $form = $this->factory->create();
        $form->addDatePicker("deadline", "Vyrobit objednávku do");
        $form->addHidden("id", $id);
        $form->addSubmit("giveToProduction", "Dát objednávku do výroby");
        $form->onSuccess[] = [$this, "giveOrderToProductionFormSucceded"];
        return $form;
    }

    /**
     * Metoda zajistí evidenci informací o tom, že objednávka má být dána do výroby
     * @param Nette\Application\UI\Form $form formulář pro evidenci informací o tom, že objednávka má být dána do výroby
     * @param $values pole hodnot z formuláře
     */
    public function giveOrderToProductionFormSucceded(Nette\Application\UI\Form $form, $values){
        try{
            $this->orderManager->giveOrderToProduction(intval($values["id"]), $values);
        } catch (Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

    /**
     * @param $id identifikátor objednávky, která má být zaplacena
     * @return Nette\Application\UI\Form formulář pro evidenci platby objednávky
     */
    public function payOrderForm($id){
        $form = $this->factory->create();
        $form->addSelect("user", "Zaplatil");
        $form->addDatePicker("paymentdate", "Datum platby");
        $form->addText("paidnote", "Poznámka");
        $form->addHidden("id", $id);
        $form->addSubmit("payOrder", "Evidovat platbu");
        $form->onSuccess[] = [$this, "payOrderFormSucceded"];
        $allUsers = $this->userManager->getAll();
        $users = array();
        foreach($allUsers as $user){
            $id = $user[UserManager::COLUMN_ID];
            $name = $user[UserManager::COLUMN_REAL_NAME];
            $users[$id] = $name;
        }
        $form["user"]->setItems($users);
        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form formulář pro evidenci platby objednávky
     * @param $values pole hodnot z formuláře
     */
    public function payOrderFormSucceded(Nette\Application\UI\Form $form, $values){
        try{
            $this->orderManager->payOrder(intval($values["id"]), $values);
        } catch (Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }
}