<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.5.18
 * Time: 12:36
 */

namespace App\Forms;

use App\Model\DeliveryNoteManager;
use App\Model\UserManager;
use Nette;

/**
 * Class DeliveryNoteFormFactory Továrnička na formuláře pro správu dodacích listů
 * @package App\Forms
 */
class DeliveryNoteFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory základní továrnička na formuláře */
    private $factory;
    /** @var  DeliveryNoteManager instance třídy pro správu dodacích listů */
    private $deliveryNoteManager;
    /** @var  UserManager instance třídy pro správu uživatelů */
    private $userManager;

    /**
     * DeliveryNoteFormFactory constructor.
     * @param FormFactory $formFactory základní továrnička na formuláře
     * @param DeliveryNoteManager $deliveryNoteManager instance třídy pro správu dodacích listů
     * @param UserManager $userManager instance třídy pro správu uživatelů
     */
    public function __construct(FormFactory $formFactory, DeliveryNoteManager $deliveryNoteManager,
        UserManager $userManager){
        $this->factory = $formFactory;
        $this->deliveryNoteManager = $deliveryNoteManager;
        $this->userManager = $userManager;
    }

    /**
     * @param $id identifikátor dodacího listu, který má být fakturován
     * @param $userID identifikátor uživatele, který vydal fakturu
     * @return Nette\Application\UI\Form formulář pro fakturaci dodacího listu
     */
    public function invoiceForm($id, $userID){
        $form = $this->factory->create();
        $form->addUpload("invoice", "Soubor s fakturou")
            ->setRequired(false)
            ->addRule(Nette\Application\UI\Form::MAX_FILE_SIZE, "Soubor s fakturou nesmí být větší než 64 MiB." , 64*1024*1024);
        $form->addSelect(DeliveryNoteManager::COLUMN_INVOICE_TYPE, "Způsob úhrady")
            ->setRequired("Prosím zvolte způsob úhrady");
        $form->addDatePicker(DeliveryNoteManager::COLUMN_INVOICE_DATE, "Datum vystavení faktury")
            ->setRequired("Prosím vyplňte datum splatnosti.");
        $form->addDatePicker("paymentdate", "Datum splatnosti")->setRequired("Prosím vyplňte datum splatnosti.");
        $form->addHidden("id", $id);
        $form->addHidden("user", $userID);
        $form->addSubmit("invoiceDeliveryNote", "Fakturovat");
        $form->onSuccess[] = [$this, "invoiceFormSucceded"];
        $form[DeliveryNoteManager::COLUMN_INVOICE_TYPE]->setItems([1 => "Nájem", 2 => "prodej - faktura",
            3 => "prodej - zálohová faktura",
            4 => "prodej - dobírka", 5 => "prodej - hotovost", 6 => "zápůjčka - 30 dní",
            7 => "zápůjčka - dlouhodobě", 8 => "zápůjčka - paralelní spoj"]);
        return $form;
    }

    /**
     * Metoda zajistí fakturaci daného dodacího listu po úspěšném odeslání formuláře.
     * @param Nette\Application\UI\Form $form formulář pro fakturaci dodacího listu
     * @param $values pole hodnot z formuláře
     */
    public function invoiceFormSucceded(Nette\Application\UI\Form $form, $values){
        try {
            $this->deliveryNoteManager->expediteDeliveryNote(intval($values["id"]), $values, intval($values["user"]));
        } catch(Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

    /**
     * @param $id identifikátor dodacího listu, který má být zaplacen
     * @return Nette\Application\UI\Form formulář pro evidenci platby dodacího listu
     */
    public function payForm($id){
        $form = $this->factory->create();
        $form->addDatePicker("paymentdate", "Datum zaplacení");
        $form->addSelect("paiduser", "Zaplatil");
        $form->addText("paidnote", "Poznámka");
        $form->addSubmit("payDeliveryNote", "Evidovat platbu");
        $form->addHidden("id", $id);
        $form->onSuccess[] = [$this, "payFormSucceded"];
        $users = array();
        $allUsers = $this->userManager->getAll();
        foreach($allUsers as $user){
            $id = $user[UserManager::COLUMN_ID];
            $name = $user[UserManager::COLUMN_REAL_NAME];
            $users[$id] = $name;
        }
        $form["paiduser"]->setItems($users);
        return $form;
    }

    /**
     * Metoda zajistí evidenci platby daného dodacího listu.
     * @param Nette\Application\UI\Form $form formulář pro evidenci platby dodacího listu
     * @param $values pole hodnot z formuláře
     */
    public function payFormSucceded(Nette\Application\UI\Form $form, $values){
        try {
            $this->deliveryNoteManager->payDeliveryNote(intval($values["id"]), $values);
        } catch(Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

    /**
     * @param $id identifikátor dodacího listu, u kterého chceme evidovat informace o expedici
     * @param $userID identifikátor uživatele, který zajistí expedici
     * @return Nette\Application\UI\Form formulář pro vyplnění údajů o expedici zboží
     */
    public function expediteForm($id, $userID){
        $form = $this->factory->create();
        $form->addSelect("transportoutcome", "Způsob doručení");
        $form->addText(DeliveryNoteManager::COLUMN_TRANSPORTCODE, "Vyzvednul / Číslo zásilky");
        $form->addText(DeliveryNoteManager::COLUMN_TRANSPORTNOTE, "Poznámka");
        $form->addSubmit("expediteDeliveryNote", "Expedice");
        $form->addHidden("id", $id);
        $form->addHidden("user", $userID);
        $form->onSuccess[] = [$this, "expediteFormSucceded"];
        $form["transportoutcome"]->setItems([0 => "N", 1 => "Osobní", 2 => "Poštou", 3 => "Vlakem",
            4 => "PPL", 5 => "Doprava", 6 => "Neznámý"]);
        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form formulář pro vyplnění údajů o expedici zboží
     * @param $values pole hodnot z formuláře
     */
    public function expediteFormSucceded(Nette\Application\UI\Form $form, $values){
        try {
            $this->deliveryNoteManager->expediteDeliveryNote(intval($values["id"]), $values, intval($values["user"]));
        } catch(Nette\Neon\Exception $ex){
            $form->addError($ex->getMessage());
        }
    }

}