<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2.6.18
 * Time: 9:19
 */

namespace App\Forms;


use App\Model\ItemsManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\OrderItemManager;
use App\Model\ProductTypeManager;
use Nette\Application\UI\Form;
use Nette\Neon\Exception;
use Nette;

/**
 * Class ItemsOnDeliveryNoteFormFactory Továrnička na formuláře pro správu položek na dodacím listu
 * @package App\Forms
 */
class ItemsOnDeliveryNoteFormFactory
{
    use Nette\SmartObject;

    /** @var FormFactory základní továrna na formuláře */
    private $factory;

    /** @var  ItemsOnDeliveryNoteManager instance třídy pro správu položek na dodacím listu */
    private $itemsOnDeliveryNoteManager;
    /** @var  ProductTypeManager instance třídy pro získání informací o jednotlivých položkách */
    private $productTypeManager;

    /**
     * ItemsOnDeliveryNoteFormFactory constructor.
     * @param FormFactory $factory základní továrna na formuláře
     * @param ProductTypeManager $productTypeManager instance třídy pro správu položek na dodacím listu
     * @param ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager instance třídy pro získání informací o jednotlivých položkách
     */
    public function __construct(FormFactory $factory,
    ProductTypeManager $productTypeManager, ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager)
    {
        $this->factory = $factory;
        $this->productTypeManager = $productTypeManager;
        $this->itemsOnDeliveryNoteManager = $itemsOnDeliveryNoteManager;
    }

    /**
     * @param $deliveryNoteID identifikátor dodacího listu
     * @return \Nette\Application\UI\Form formulář pro přidání položky na dodací list
     */
    public function addItemForm($deliveryNoteID){
        $form = $this->factory->create();
        $form->addText("name", "Název produktu");
        $form->addInteger("quantity", "Množství")->setRequired("Prosím vyplňte množství")
            ->addRule(Form::INTEGER, "Množství musí být celé číslo")
            ->addRule(Form::MIN, "Je nutné objednat minimálně 1 kus", 1)
            ->setAttribute("onchange", "changePriceForProduct()")
            ->setAttribute("onKeyUp", "changePriceForProduct()");
        $form->addInteger("price", "Cena")->setRequired("Prosím vyplňte cenu")
            ->addRule(Form::NUMERIC, "Cena musí být číslo")
            ->addRule(Form::MIN, "Cena musí být kladné číslo.", 1)
            ->setAttribute("onchange", "changePriceForProduct()")
            ->setAttribute("onKeyUp", "changePriceForProduct()");
        $form->addSelect("product", "Produkt");
        $form->addHidden("deliveryNote", $deliveryNoteID);
        $form->addSubmit("addItem", "Přidat položku");
        $form->onSuccess[] = [$this, "addItemFormSucceded"];
        $form["product"]->setItems($this->products($deliveryNoteID));
        return $form;
    }

    /**
     * @param $deliveryNoteID identifikátor dodacího listu
     * @return array seznam produktů, které lze přidat na daný dodací list
     */
    public function products($deliveryNoteID){
        $productTypes = $this->productTypeManager->getAll();
        $array = array();
        foreach($productTypes as $productType){
            $productTypeID = $productType[ProductTypeManager::COLUMN_ID];
            $productTypeTitle = $productType[ProductTypeManager::COLUMN_NAME];
            $array[$productTypeID] = $productTypeTitle;
        }
        $items = $this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote($deliveryNoteID);
        foreach($items as $item){
            unset($array[$item[OrderItemManager::COLUMN_PRODUCT]]);
        }
        return $array;
    }

    /**
     * @param Form $form formulář pro přidání položky na dodací list
     * @param $values pole hodnot z formuláře
     */
    public function addItemFormSucceded(Form $form, $values){
        try {
            $this->itemsOnDeliveryNoteManager->addItemOnDeliveryNote($values, intval($values["deliveryNote"]));
        } catch(Exception $ex){
            $form->addError($ex->getMessage());
        }
    }
}