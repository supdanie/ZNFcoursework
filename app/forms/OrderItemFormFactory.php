<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.5.18
 * Time: 12:55
 */

namespace App\Forms;


use App\Model\DeliveryNoteManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\OrderItemManager;
use App\Model\ProductTypeManager;
use Nette\Application\UI\Form;
use Nette\Neon\Exception;

class OrderItemFormFactory
{
    /** @var FormFactory základní továrna na formuláře */
    private $factory;

    /** @var  OrderItemManager instance třídy pro správu položek objednávky */
    private $orderItemManager;

    /** @var  ProductTypeManager instance třídy pro získání informací o jednotlivých produktech */
    private $productTypeManager;

    /**
     * OrderItemFormFactory constructor.
     * @param FormFactory $factory základní továrna na formuláře
     * @param OrderItemManager $orderItemManager instance třídy pro správu položek objednávky
     * @param ProductTypeManager $productTypeManager instance třídy pro získání informací o jednotlivých produktech
     */
    public function __construct(FormFactory $factory, OrderItemManager $orderItemManager, ProductTypeManager $productTypeManager)
    {
        $this->factory = $factory;
        $this->orderItemManager = $orderItemManager;
        $this->productTypeManager = $productTypeManager;
    }

    /**
     * @param $orderID identifikátor objednávky, do které má být položka přidána
     * @return Form formulář pro přidání položky do objednávky
     */
    public function addItemForm($orderID){
        $form = $this->factory->create();
        $form->addText("name", "Název produktu");
        $form->addInteger("quantity", "Množství")->setRequired("Prosím vyplňte množství")
            ->addRule(Form::INTEGER, "Množství musí být celé číslo")
            ->addRule(Form::MIN, "Je nutné objednat minimálně 1 kus", 1)
            ->setAttribute("onchange", "changePriceForProduct()")
            ->setAttribute("onKeyUp", "changePriceForProduct()");
        $form->addInteger("price", "Cena")->setRequired("Prosím vyplňte cenu")
            ->addRule(Form::NUMERIC, "Cena musí být číslo")
            ->addRule(Form::MIN, "Cena musí být kladné číslo.", 1)
            ->setAttribute("onchange", "changePriceForProduct()")
            ->setAttribute("onKeyUp", "changePriceForProduct()");
        $form->addSelect("product", "Produkt");
        $form->addSelect("currency", "Měna");
        $form->addHidden("order", $orderID);
        $form->addSubmit("addItem", "Přidat položku");
        $form->onSuccess[] = [$this, "addItemFormSucceded"];
        $form["currency"]->setItems($this->currencies($orderID));
        $form["product"]->setItems($this->products($orderID));
        return $form;
    }

    /**
     * @param $orderID identifikátor objednávky
     * @return array produkty, které mohou být přidány do objednávky
     */
    public function products($orderID){
        $productTypes = $this->productTypeManager->getAll();
        $array = array();
        foreach($productTypes as $productType){
            $productTypeID = $productType[ProductTypeManager::COLUMN_ID];
            $productTypeTitle = $productType[ProductTypeManager::COLUMN_NAME];
            $array[$productTypeID] = $productTypeTitle;
        }
        $items = $this->orderItemManager->getItemsOfOrder($orderID);
        foreach($items as $item){
            unset($array[$item[OrderItemManager::COLUMN_PRODUCT]]);
        }
        return $array;
    }

    /**
     * @param $orderID identifikátor objednávka
     * @return array pole měn, pokud objednávka nemá žádnou položku, jinak měnu, ve které jsou ostatní položky objednávky účtovány
     */
    public function currencies($orderID){
        $array = [1 => "CZK", 2 => "EUR", 3 => "USD", 4 => "GPB"];
        $items = $this->orderItemManager->getItemsOfOrder($orderID);
        if(count($items) == 0){
            return $array;
        } else {
            $arrayWithCurrency = array();
            foreach($items as $item){
                $currency = $item[OrderItemManager::COLUMN_CURRENCY];
                $arrayWithCurrency  = [$currency => $array[$currency]];
            }
            return $arrayWithCurrency;
        }
    }

    /**
     * Metoda zajistí přidání položky do dané objednávky
     * @param Form $form formulář pro přidání položky do objednávky
     * @param $values pole hodnot z formuláře
     */
    public function addItemFormSucceded(Form $form, $values){
        try{
            $this->orderItemManager->addItemOnOrder($values, intval($values["order"]));
        } catch(Exception $ex){
            $form->addError($ex->getMessage());
        }
    }


}