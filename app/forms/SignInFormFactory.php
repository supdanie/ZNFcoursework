<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;

/**
 * Class SignInFormFactory továrnička na formulář pro přihlášení uživatele
 * @package App\Forms
 */
class SignInFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory základní továrna na formuláře */
	private $factory;

	/** @var User objekt reprezentující uživatele, který má být přihlášen*/
	private $user;

    /**
     * SignInFormFactory constructor.
     * @param FormFactory $factory základní továrna na formuláře
     * @param User $user objekt reprezentující uživatele, který má být přihlášen
     */
	public function __construct(FormFactory $factory, User $user)
	{
		$this->factory = $factory;
		$this->user = $user;
	}


	/**
	 * @return Form formulář pro přihlášení uživatele
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->addText('username', 'Username:')
			->setRequired('Please enter your username.');

		$form->addPassword('password', 'Password:')
			->setRequired('Please enter your password.');

		$form->addCheckbox('remember', 'Keep me signed in');

		$form->addSubmit('send', 'Sign in');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
				$this->user->login($values->username, $values->password);
			} catch (Nette\Security\AuthenticationException $e) {
				$form->addError('The username or password you entered is incorrect.');
				return;
			}
			$onSuccess();
		};

		return $form;
	}
}
