<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
        $router[] = new Route("", "Homepage:default");
        $router[] = new Route("sign/in", "Sign:in");
        $router[] = new Route("sign/out", "Sign:out");
        $router[] = new Route("sign/up", "Sign:up");
		$router[] = new Route('order/<action>[/<id>]', 'Order:default');
        $router[] = new Route('deliverynote/<order>/<action>[/<id>]', 'Deliverynote:default');
		return $router;
	}
}
