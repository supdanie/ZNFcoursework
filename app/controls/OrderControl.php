<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2.6.18
 * Time: 13:42
 */

namespace App\Controls;


use App\Forms\DeliveryNoteFormFactory;
use App\Forms\ItemsOnDeliveryNoteFormFactory;
use App\Forms\OrderItemFormFactory;
use App\Model\DeliveryNoteManager;
use App\Model\ItemsManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\NoDataFoundException;
use App\Model\OrderItemManager;
use App\Model\UserManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Neon\Exception;
use Ublaboo\DataGrid\DataGrid;

/**
 * Class OrderControl Komponenta pro správu objednávky.
 * @package App\Controls
 */
class OrderControl extends Control
{
    /** @var  DeliveryNoteManager instance třídy DeliveryNoteManager */
    private $deliveryNoteManager;
    /** @var  OrderItemManager instance třídy OrderItemManager */
    private $orderItemManager;
    /** @var  DeliveryNoteFormFactory Továrnička na formuláře pro správu dodacích listů*/
    private $deliveryNoteFormFactory;
    /** @var  OrderItemFormFactory Továrnička na formuláře pro správu položek na objednávce */
    private $orderItemFormFactory;
    /** @var  ItemsOnDeliveryNoteManager Instance třídy ItemsOnDeliveryNoteManager */
    private $itemsOnDeliveryNoteManager;
    /** @var  UserManager instance třídy pro správu uživatelů*/
    private $userManager;
    private $orderID;
    private $userID;

    /**
     * OrderControl constructor.
     * @param DeliveryNoteManager $deliveryNoteManager instance třídy DeliveryNoteManager
     * @param OrderItemManager $orderItemManager instance třídy OrderItemManager
     * @param DeliveryNoteFormFactory $deliveryNoteFormFactory Továrnička na formuláře pro správu dodacích listů
     * @param OrderItemFormFactory $orderItemFormFactory  Továrnička na formuláře pro správu položek na objednávce
     * @param ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager Instance třídy ItemsOnDeliveryNoteManager
     * @param UserManager $userManager instance třídy pro správu uživatelů
     * @param null $orderID identifikátor objednváky
     * @param null $userID identifikátor uživatele
     */
    public function __construct(DeliveryNoteManager $deliveryNoteManager,
                                OrderItemManager $orderItemManager,
                                DeliveryNoteFormFactory $deliveryNoteFormFactory,
                                OrderItemFormFactory $orderItemFormFactory,
                                ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager,
                                UserManager $userManager,
                                $orderID = null, $userID = null){
        parent::__construct();
        $this->deliveryNoteManager = $deliveryNoteManager;
        $this->orderItemManager = $orderItemManager;
        $this->deliveryNoteFormFactory = $deliveryNoteFormFactory;
        $this->orderItemFormFactory = $orderItemFormFactory;
        $this->itemsOnDeliveryNoteManager = $itemsOnDeliveryNoteManager;
        $this->userManager = $userManager;
        $this->orderID = $orderID;
        $this->userID = $userID;
    }


    /**
     * Metoda přidá nový dodací list do databáze. Dodací list bude vydán k této objednávce.
     */
    public function handleAdddeliverynote(){
        $this->deliveryNoteManager->addDeliveryNote($this->orderID, $this->userID);
    }

    /**
     * @return \Nette\Application\UI\Form formulář pro přidání nové položky do objednávky
     */
    public function createComponentAddItemForm(){
        return $this->orderItemFormFactory->addItemForm($this->orderID);
    }

    /**
     * @return Multiplier formulář, který má přidat položku na dodací list s daným identifikátorem
     */
    public function createComponentAddItemOnDeliveryNoteForm(){
        return new Multiplier(function ($id){
            return $this->itemsOnDeliveryNoteFormFactory->addItemForm($id);
        });
    }

    /**
     * Metoda smaže položku z objednávky.
     * @param $itemID identifikátor položky, která má být smazána
     */
    public function handleRemoveItem($itemID){
        $this->orderItemManager->remove($itemID);
    }

    /**
     * @return DataGrid Mřížka s položkami v dané objednávce.
     */
    public function createComponentGridOfItems(){
        $grid = new DataGrid();
        $grid->setDataSource($this->orderItemManager->getItemsOfOrder($this->orderID));
        $grid->addColumnText(OrderItemManager::COLUMN_NAME, "Název")->setSortable();
        $grid->addColumnNumber(OrderItemManager::COLUMN_QUANTITY, "Množství")->setSortable();
        $grid->addColumnNumber(OrderItemManager::COLUMN_PRICE, "Jednotková cena")->setSortable();
        $grid->addColumnText(OrderItemManager::COLUMN_CURRENCY, "Měna")->setRenderer(
            function($item){
                $currencyTitle = OrderItemManager::COLUMN_CURRENCY;
                $currency = $item->$currencyTitle;
                if($currency === null){
                    return "";
                }
                if($currency == 1){
                    return "CZK";
                }
                if($currency == 2){
                    return "EUR";
                }
                if($currency == 3){
                    return "USD";
                }
                if($currency == 4){
                    return "GPB";
                }
                return "";
            }
        );
        $grid->addColumnNumber("priceForProduct", "Cena")->setRenderer(
            function($item){
                $quantityTitle = OrderItemManager::COLUMN_QUANTITY;
                $priceTitle = OrderItemManager::COLUMN_PRICE;
                $quantity = $item->$quantityTitle;
                $price = $item->$priceTitle;
                return $quantity * $price;
            }
        )->setSortable();
        $grid->addColumnText("currency2", "Měna")->setRenderer(
            function($item){
                $currencyTitle = OrderItemManager::COLUMN_CURRENCY;
                $currency = $item->$currencyTitle;
                if($currency === null){
                    return "";
                }
                if($currency == 1){
                    return "CZK";
                }
                if($currency == 2){
                    return "EUR";
                }
                if($currency == 3){
                    return "USD";
                }
                if($currency == 4){
                    return "GPB";
                }
                return "";
            }
        );
        $grid->addAction("delete", "Smazat", "deleteitem!");
        $grid->addInlineEdit()
            ->onControlAdd[] = function($container) {
            $container->addText(OrderItemManager::COLUMN_NAME, '');
            $container->addText(OrderItemManager::COLUMN_QUANTITY, '');
            $container->addText(OrderItemManager::COLUMN_PRICE, '');
        };

        $grid->getInlineEdit()->onSetDefaults[] = function($container, $item) {
            $name = OrderItemManager::COLUMN_NAME;
            $quantity = OrderItemManager::COLUMN_QUANTITY;
            $price = OrderItemManager::COLUMN_PRICE;
            $container->setDefaults([
                $name => $item->$name,
                $quantity => $item->$quantity,
                $price => $item->$price,
            ]);
        };

        $grid->getInlineEdit()->onSubmit[] = function($id, $values) {
            $nameTitle = OrderItemManager::COLUMN_NAME;
            $quantityTitle = OrderItemManager::COLUMN_QUANTITY;
            $priceTitle = OrderItemManager::COLUMN_PRICE;
            $name = $values[$nameTitle];
            $quantity = $values[$quantityTitle];
            $price = $values[$priceTitle];
            $items = $this->orderItemManager->getAll()->where(OrderItemManager::COLUMN_ID, $id);
            foreach($items as $item) {
                $currency = $item[OrderItemManager::COLUMN_CURRENCY];
                $product = $item[OrderItemManager::COLUMN_PRODUCT];
                $order = $item[OrderItemManager::COLUMN_PRODUCT_ORDER];
                try {
                    $this->orderItemManager->editItemOnOrder($id, ["name" => $name,
                        "quantity" => $quantity, "price" => $price, "currency" => $currency, "product" => $product], $order);
                } catch (NoDataFoundException $ex) {
                } catch(Exception $e1){
                }
            }
            $this->redrawControl("gridOfItems");
        };
        return $grid;
    }

    /**
     * @param $id identifikátor položky, která bude smazána
     */
    public function handleDeleteitem($id){
        $this->itemsOnDeliveryNoteManager->remove($id);
    }


    /**
     * Metoda vykreslí komponentu pro správu objednávky.
     */
    public function render(){
        $this->template->setFile(__DIR__. "/../presenters/templates/Order/detail.latte");
        $this->template->number = $this->orderID;
        if(!isset($this->template->priceForAll)) {
            $this->template->priceForAll = 0;
        }
        $this->template->render();
    }


}