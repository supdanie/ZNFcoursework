<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2.6.18
 * Time: 9:05
 */

namespace App\Controls;


use App\Forms\DeliveryNoteFormFactory;
use App\Forms\ItemsOnDeliveryNoteFormFactory;
use App\Forms\OrderItemFormFactory;
use App\Model\DeliveryNoteManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\OrderItemManager;
use App\Model\UserManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Exception\DataGridActionCallbackException;

/**
 * Class DeliveryNoteControl Komponenta, která slouží pro správu dodacího listu. Obsahuje DataGrid se seznamem položek.
 * @package App\Controls
 */
class DeliveryNoteControl extends Control
{
    /** @var  DeliveryNoteManager instance třídy DeliveryNoteManager*/
    private $deliveryNoteManager;
    /** @var  ItemsOnDeliveryNoteManager instance třídy pro správu položek na dodacím listu */
    private $itemsOnDeliveryNoteManager;
    /** @var  ItemsOnDeliveryNoteFormFactory továrnička na vytvoření formuláře pro správu položek na dodacím listu*/
    private $itemsOnDeliveryNoteFormFactory;
    /** @var  OrderItemManager instance třídy pro správu položek na objednávce */
    private $orderItemManager;
    /** @var  UserManager instance třídy pro správu uživatelů*/
    private $userManager;
    private $deliveryNoteID;
    private $userID;

    /**
     * DeliveryNoteControl constructor.
     * @param DeliveryNoteManager $deliveryNoteManager instance třídy DeliveryNoteManager
     * @param ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager instance třídy ItemsOnDeliveryNoteManager
     * @param ItemsOnDeliveryNoteFormFactory $itemsOnDeliveryNoteFormFactory instance třídy ItemsOnDeliveryNoteFormFactory
     * @param $deliveryNoteID identifikátor dodacího listu
     * @param $userID identifikátor uživatele
     */
    public function __construct(DeliveryNoteManager $deliveryNoteManager,
                                ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager,
                                ItemsOnDeliveryNoteFormFactory $itemsOnDeliveryNoteFormFactory,
                                OrderItemManager $orderItemManager,
                                UserManager $userManager,
                                $deliveryNoteID = null, $userID = null){
        parent::__construct();
        $this->deliveryNoteManager = $deliveryNoteManager;
        $this->itemsOnDeliveryNoteManager = $itemsOnDeliveryNoteManager;
        $this->itemsOnDeliveryNoteFormFactory = $itemsOnDeliveryNoteFormFactory;
        $this->orderItemManager = $orderItemManager;
        $this->userManager = $userManager;
        $this->deliveryNoteID = $deliveryNoteID;
        $this->userID = $userID;
    }

    /**
     * @return DataGrid Mřížka, ve které jsou všechny položky na daném dodacím listu.
     */
    public function createComponentItemGrid(){
        $grid = new DataGrid();
        $grid->setDataSource($this->itemsOnDeliveryNoteManager->getItemsOnDeliveryNote($this->deliveryNoteID)->fetchAll());
        $grid->addColumnText(ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_NAME, "Název")->setSortable();
        $grid->addColumnNumber(ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY, "Množství")->setSortable();
        $grid->addColumnNumber(ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE, "Jednotková cena")->setSortable();
        $grid->addColumnText("currency", "Měna")->setRenderer(
            function($item){
                $orderID = $this->deliveryNoteManager->get($this->deliveryNoteID)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
                $currencies = $this->orderItemManager->getItemsOfOrder($orderID);
                $currencyOfOrder = 0;
                foreach($currencies as $currency){
                    $currencyOfOrder = $currency[OrderItemManager::COLUMN_CURRENCY];
                    break;
                }
                $currencyArray = [1 => "CZK", 2 => "EUR", 3 => "USD", 4 => "GBP"];
                return $currencyArray[$currencyOfOrder];
            }
        );
        $grid->addColumnNumber("priceForProduct", "Cena")->setRenderer(
            function($item){
                $quantityTitle = ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY;
                $priceTitle = ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE;
                $quantity = $item->$quantityTitle;
                $price = $item->$priceTitle;
                return $quantity * $price;
            }
        )->setSortable();
        $grid->addColumnText("currency2", "Měna")->setRenderer(
            function($item){
                $orderID = $this->deliveryNoteManager->get($this->deliveryNoteID)[DeliveryNoteManager::COLUMN_PRODUCT_ORDER];
                $currencies = $this->orderItemManager->getItemsOfOrder($orderID);
                $currencyOfOrder = 0;
                foreach($currencies as $currency){
                    $currencyOfOrder = $currency[OrderItemManager::COLUMN_CURRENCY];
                    break;
                }
                $currencyArray = [1 => "CZK", 2 => "EUR", 3 => "USD", 4 => "GBP"];
                return $currencyArray[$currencyOfOrder];
            }
        );
        $grid->addAction("remove", "Smazat položku", "removeitem!");
        $grid->addInlineEdit()
            ->onControlAdd[] = function($container) {
            $container->addText(ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_NAME, '');
            $container->addText(ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY, '');
            $container->addText(ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE, '');
        };

        $grid->getInlineEdit()->onSetDefaults[] = function($container, $item) {
            $name = ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_NAME;
            $quantity = ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY;
            $price = ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE;
            $container->setDefaults([
                $name => $item->$name,
                $quantity => $item->$quantity,
                $price => $item->$price,
            ]);
        };

        $grid->getInlineEdit()->onSubmit[] = function($id, $values) {
            $nameTitle = ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_NAME;
            $quantityTitle = ItemsOnDeliveryNoteManager::COLUMN_PRODUCT_QUANTITY;
            $priceTitle = ItemsOnDeliveryNoteManager::COLUMN_UNIT_PRICE;
            $name = $values[$nameTitle];
            $quantity = $values[$quantityTitle];
            $price = $values[$priceTitle];
            $items = $this->itemsOnDeliveryNoteManager->getAll()->where(OrderItemManager::COLUMN_ID, $id);
            foreach($items as $item) {
                $product = $item[ItemsOnDeliveryNoteManager::COLUMN_PRODUCT];
                $deliveryNote = $item[ItemsOnDeliveryNoteManager::COLUMN_DELIVERY_NOTE];
                try {
                    $this->itemsOnDeliveryNoteManager->editItemOnDeliveryNote($id, ["name" => $name,
                        "quantity" => $quantity, "price" => $price,  "product" => $product], $deliveryNote);
                } catch (NoDataFoundException $ex) {
                } catch(Exception $e1){
                }
            }
            $this->redrawControl("itemGrid");
        };
        return $grid;
    }

    /**
     * Metoda smaže položku z databáze
     * @param $id identifikátor položky, která má být smazána
     */
    public function handleRemoveitem($id){
        $this->itemsOnDeliveryNoteManager->remove($id);
    }


    /**
     * Metoda zajistí vykreslení komponenty
     */
    public function render(){
        $this->template->setFile(__DIR__."/../presenters/templates/Deliverynote/detail.latte");
        $this->template->render();
    }
}