<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2.6.18
 * Time: 9:05
 */

namespace App\Controls;


use App\Forms\DeliveryNoteFormFactory;
use App\Forms\ItemsOnDeliveryNoteFormFactory;
use App\Forms\OrderItemFormFactory;
use App\Model\DeliveryNoteManager;
use App\Model\ItemsManager;
use App\Model\ItemsOnDeliveryNoteManager;
use App\Model\OrderItemManager;
use App\Model\UserManager;

/**
 * Class OrderControlFactory Továrnička na vytváření komponent pro správu objednávky, či dodacího listu
 * @package App\Controls
 */
class OrderControlFactory
{
    /** @var  DeliveryNoteManager instance třídy pro správu dodacích listů */
    private $deliveryNoteManager;
    /** @var  OrderItemManager instance třídy pro správu objednávek */
    private $orderItemManager;
    /** @var  DeliveryNoteFormFactory továrnička na formuláře pro správu dodacích listů */
    private $deliveryNoteFormFactory;
    /** @var  OrderItemFormFactory továrnička na formuláře pro správu položek na objednávce */
    private $orderItemFormFactory;
    /** @var  ItemsOnDeliveryNoteManager instance třídy pro správu položek na dodacím listu*/
    private $itemsOnDeliveryNoteManager;
    /** @var  ItemsOnDeliveryNoteFormFactory továrnička na formuláře pro správu položek na dodacím listu */
    private $itemsOnDeliveryNoteFormFactory;
    /** @var  UserManager instance třídy pro správu uživatelů */
    private $userManager;

    /**
     * OrderControlFactory constructor.
     * @param DeliveryNoteManager $deliveryNoteManager  instance třídy pro správu dodacích listů
     * @param OrderItemManager $orderItemManager instance třídy pro správu objednávek
     * @param DeliveryNoteFormFactory $deliveryNoteFormFactory  továrnička na formuláře pro správu dodacích listů
     * @param OrderItemFormFactory $orderItemFormFactory továrnička na formuláře pro správu položek na objednávce
     * @param ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager instance třídy pro správu položek na dodacím listu
     * @param ItemsOnDeliveryNoteFormFactory $itemsOnDeliveryNoteFormFactory v
     * @param UserManager $userManager instance třídy pro správu uživatelů
     */
    public function __construct(DeliveryNoteManager $deliveryNoteManager,
                                OrderItemManager $orderItemManager,
                                DeliveryNoteFormFactory $deliveryNoteFormFactory,
                                OrderItemFormFactory $orderItemFormFactory,
                                ItemsOnDeliveryNoteManager $itemsOnDeliveryNoteManager,
                                ItemsOnDeliveryNoteFormFactory $itemsOnDeliveryNoteFormFactory,
                                UserManager $userManager){
        $this->deliveryNoteManager = $deliveryNoteManager;
        $this->orderItemManager = $orderItemManager;
        $this->deliveryNoteFormFactory = $deliveryNoteFormFactory;
        $this->orderItemFormFactory = $orderItemFormFactory;
        $this->itemsOnDeliveryNoteManager = $itemsOnDeliveryNoteManager;
        $this->itemsOnDeliveryNoteFormFactory = $itemsOnDeliveryNoteFormFactory;
        $this->userManager = $userManager;
    }

    /**
     * @param $id identifikátor objednávky, pro níž má být komponenta vytvořena
     * @param $userID identifikátor aktuálně přihlášeného uživatele
     * @return OrderControl komponenta pro správu dané objednávky
     */
    public function createOrderControl($id, $userID = null){
        return new OrderControl($this->deliveryNoteManager, $this->orderItemManager,
            $this->deliveryNoteFormFactory, $this->orderItemFormFactory, $this->itemsOnDeliveryNoteManager,
            $this->userManager, $id, $userID);
    }

    /**
     * @param $id identifikátor dodacího listu, pro nějž má být vytvořena komponenta
     * @param $userID identifikátor aktuálně přihlášeného uživatele
     * @return DeliveryNoteControl komponenta pro správu daného dodacího listu
     */
    public function createDeliveryNoteControl($id, $userID = null){
        return new DeliveryNoteControl($this->deliveryNoteManager, $this->itemsOnDeliveryNoteManager,
            $this->itemsOnDeliveryNoteFormFactory, $this->orderItemManager, $this->userManager, $id, $userID);
    }
}