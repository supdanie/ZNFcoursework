Modul pro správu objednávek a dodacích listů
=============

součástí zadání je i původní struktura DP (velmi špatně udělaná) - bude třeba upravit a připravit export původních dat do nové DB

Tabulkové zobrazení objednávek a dodacích listů (stránkování, filtrování, řazení)
    
Formuláře pro vytváření vkládání, editaci a mazání objednávek i dodacích listů (včetně párování (1:N) mezi objednávkami a dodacími listy)

Export objednávek a dodacích listů do PDF