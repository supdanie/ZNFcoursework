--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: companies; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE companies (
    id integer DEFAULT nextval(('public."companies_id_seq"'::text)::regclass) NOT NULL,
    companyname character varying(50) NOT NULL,
    ico character varying(15) NOT NULL,
    adress1 character varying(50),
    adress2 character varying(50),
    zip text,
    url character varying(50),
    mail character varying(50),
    phone character varying(15),
    phonegsm character varying(15),
    phonefax character varying(15),
    dic character varying(25),
    bankacount character varying(25),
    bankcode character varying(4),
    provider boolean,
    deleted boolean DEFAULT false NOT NULL,
    customer boolean,
    adress12 text,
    adress22 text,
    zip2 text,
    contact text
);


ALTER TABLE public.companies OWNER TO svm;

--
-- Name: deliverynotes; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE deliverynotes (
    id integer NOT NULL,
    productorderid integer,
    companyid integer NOT NULL,
    status integer,
    transactiondate timestamp without time zone DEFAULT now() NOT NULL,
    userid integer NOT NULL,
    transportoutcomeid integer,
    transportcode text,
    resolutiondate date,
    userid2 integer,
    transportnote text,
    invoiceid text,
    invoicetype integer,
    invoicedate date,
    invoiceuserid integer,
    paymentdate date,
    paid boolean DEFAULT false NOT NULL,
    paiddate date,
    paiduserid integer,
    paidnote text,
    deleted boolean DEFAULT false
);


ALTER TABLE public.deliverynotes OWNER TO svm;

--
-- Name: items; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE items (
    id integer,
    groupid integer,
    name text
);


ALTER TABLE public.items OWNER TO svm;

--
-- Name: locations; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE locations (
    id integer NOT NULL,
    locationname character varying(100)
);


ALTER TABLE public.locations OWNER TO svm;

SET default_with_oids = false;

--
-- Name: productcategory; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE productcategory (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.productcategory OWNER TO svm;

SET default_with_oids = true;

--
-- Name: productorderitem; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE productorderitem (
    id integer,
    name text,
    quantity double precision,
    productorderid integer,
    price double precision,
    currency character varying(20) DEFAULT 'CZK'::character varying NOT NULL,
    productid integer
);


ALTER TABLE public.productorderitem OWNER TO svm;

SET default_with_oids = false;

--
-- Name: productorders; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE productorders (
    id integer NOT NULL,
    orderstatus integer,
    companyid integer,
    companyordernumber text,
    ordertype integer,
    recievedate date,
    recieverid integer,
    orderfile text,
    approved boolean,
    approveddate date,
    approveduserid integer,
    approvednote text,
    paidnote text,
    productiondeadline date,
    productionstatus integer,
    closed boolean DEFAULT false NOT NULL,
    expeditionuserid integer,
    expeditiondate date,
    expeditionreciever text,
    expeditiondate2 date,
    expeditionuserid2 integer,
    invoiceuserid integer,
    invoiceid text,
    invoiceprice double precision,
    invoicetype integer,
    invoicedate date,
    paid integer,
    paiddate date,
    paiduserid integer,
    paymentdate date,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.productorders OWNER TO svm;

--
-- Name: products; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    producttypeid integer NOT NULL,
    procuctcomment text,
    statusid integer,
    barcoderecipientid integer,
    created date,
    locationid integer,
    ownerid integer,
    counter integer
);


ALTER TABLE public.products OWNER TO svm;

--
-- Name: productsondeliverynote; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE productsondeliverynote (
    id integer NOT NULL,
    productid integer,
    deliverynoteid integer NOT NULL,
    productname text,
    productquantity integer,
    unitprice double precision,
    inevidence integer
);


ALTER TABLE public.productsondeliverynote OWNER TO svm;

--
-- Name: productstatus; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE productstatus (
    id integer NOT NULL,
    statusname character varying(100)
);


ALTER TABLE public.productstatus OWNER TO svm;

--
-- Name: producttypes; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE producttypes (
    id integer DEFAULT nextval(('public."material_products_seq"'::text)::regclass) NOT NULL,
    name character varying(100),
    serialnumber character varying(50),
    version character varying(15),
    productcomment character varying(100),
    barcodetitle character varying(20),
    price numeric,
    inevidence integer DEFAULT 1 NOT NULL,
    quantity integer DEFAULT 1 NOT NULL,
    units character varying(10),
    unit integer,
    authorized integer,
    status integer,
    deleted boolean DEFAULT false NOT NULL,
    servicegroupid integer,
    service boolean DEFAULT false NOT NULL,
    quantity2 numeric,
    price2 numeric,
    productcategoryid integer,
    pricelist boolean DEFAULT true NOT NULL,
    author integer
);


ALTER TABLE public.producttypes OWNER TO svm;

--
-- Name: users; Type: TABLE; Schema: public; Owner: svm; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    surname character varying(50) NOT NULL,
    username character varying(20) NOT NULL,
    userpassword character varying(150),
    email text,
    deleted boolean DEFAULT false NOT NULL,
    role text DEFAULT 'customer'::text NOT NULL,
    companyid integer
);


ALTER TABLE public.users OWNER TO svm;

--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY companies (id, companyname, ico, adress1, adress2, zip, url, mail, phone, phonegsm, phonefax, dic, bankacount, bankcode, provider, deleted, customer, adress12, adress22, zip2, contact) FROM stdin;
1	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	\N	\N	\N	\N	\N
136	firma	1234578	ulice	Praha 4	14311	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
138	firma	1234578	ulice	Praha 8	18200	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
139	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
152	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
153	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
154	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
155	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
156	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
157	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
162	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
163	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
164	firma	1234578	ulice	Plzeň	32600	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	\N	\N	\N	\N	\N
170	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
171	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
172	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
173	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
175	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
176	firma	1234578	ulice	Přerov	53501	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	\N	\N	\N	\N	\N
178	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
179	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
180	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
182	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
183	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
184	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
185	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
186	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
187	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
188	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
189	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
190	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
191	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
193	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
166	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	\N	\N	\N	\N	\N
195	firma	1234578	ulice	Lanškroun	56301	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
194	firma	1234578	ulice	Praha 8	18000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
169	firma	1234578	ulice	sgsdagdagsd	11111	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	\N	\N	\N	\N	\N
168	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	\N	\N	\N	\N	\N
200	firma	1234578	ulice	Strakonice 2	38601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
199	firma	1234578	ulice	Český Těšín	73701	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
201	firma	1234578	ulice	Pražmo	73904	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
203	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
204	firma	1234578	ulice	Dolní Bojanovice	69617	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t	\N	\N	\N	\N
205	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
207	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
208	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
210	firma	1234578	ulice	Praha 9	19000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
209	firma	1234578	ulice	Praha 7 	17000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
211	firma	1234578	ulice	Praha 5	15800	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
158	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
61	firma	1234578	ulice	Rokycany	33701	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
213	firma	1234578	ulice	Praha 5 Radotín	15000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
192	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
216	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
218	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
219	firma	1234578	ulice	Praha-Chodov		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	f	\N	\N	\N	\N
137	firma	1234578	ulice	Chrudim	53701	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	\N	\N	\N	\N	\N
129	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
220	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
221	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
222	firma	1234578	ulice	Olomouc, Řepčín	77900	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
223	firma	1234578	ulice	Waldenburg Germany	74638	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
217	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
202	firma	1234578	ulice	Praha 9 - Kyje	19800	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
42	firma	1234578	ulice	Liberec 1	46001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
4	firma	1234578	ulice	Praha 8 - Ďáblice	18200	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
214	firma	1234578	ulice	Brno	61700	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
128	firma	1234578	ulice	Praha 4		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
131	firma	1234578	ulice	Praha 9 - Letňany	19900	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
196	firma	1234578	ulice	Praha 4-Modřany	14300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
141	firma	1234578	ulice	Praha 2	12000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
212	firma	1234578	ulice	Praha 4	14800	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
197	firma	1234578	ulice	Praha 4, (budova DVAN)	14300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
125	firma	1234578	ulice	Jesenice	25242	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
127	firma	1234578	ulice	Praha 6 - Řepy	16300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
122	firma	1234578	ulice		27201	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
124	firma	1234578	ulice	Podnikatelská 556	19000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
148	firma	1234578	ulice	Děčín 1	40502	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
198	firma	1234578	ulice	Praha 1		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
224	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	aaa	t	t	t				\N
23	firma	1234578	ulice	Praha 10 - Strašnice	10000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	3500	t	f	\N	\N	\N	\N	\N
3	firma	1234578	ulice	Liberec 10	46010	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t	\N	\N	\N	\N
119	firma	1234578	ulice	Brno - Žabovřesky	61600	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
227	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	t	t				\N
226	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	t	f				\N
41	firma	1234578	ulice	Praha 3	13000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				\N
228	firma	1234578	ulice	Litomyšl	57001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	t	f				\N
229	firma	1234578	ulice	Litomyšl	57001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				\N
230	firma	1234578	ulice	ahoj	23333	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	t	f				\N
231	firma	1234578	ulice	25961152	15000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	t	f				\N
232	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
233	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	t	f				\N
7	firma	1234578	ulice	Hradec Králové 2	50002	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
5	firma	1234578	ulice	Mimoň V	47124	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0600	\N	f	t	\N	\N	\N	\N
108	firma	1234578	ulice	Praha 8	18000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
8	firma	1234578	ulice	Luhačovice	76326	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0300	\N	f	t	\N	\N	\N	\N
76	firma	1234578	ulice	Olomouc	77901	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
116	firma	1234578	ulice		73901	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
142	firma	1234578	ulice		10000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
215	firma	1234578	ulice	Praha 4	14000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
133	firma	1234578	ulice	Pardubice	53002	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
9	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
10	firma	1234578	ulice	Praha 3	13052	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
97	firma	1234578	ulice	Hodonín	69501	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
140	firma	1234578	ulice	Praha 3	13000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
96	firma	1234578	ulice	Domažlice	34401	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
11	firma	1234578	ulice	Dolní Libchava, Česká Lípa	47001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0300	\N	f	t	\N	\N	\N	\N
2	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
65	firma	1234578	ulice	Ostrava Vítkovice	70300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
109	firma	1234578	ulice	190 00  Praha-Hrdlořezy		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
12	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
161	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	t	\N	\N	\N	\N	\N
59	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	t	\N	\N	\N	\N	\N
14	firma	1234578	ulice	Brno - město	60200	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
92	firma	1234578	ulice	Škvorec 	25083	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
110	firma	1234578	ulice	Tábor	39001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
98	firma	1234578	ulice	Hodonín	69500	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t	Hornická 1107	Dubňany	69603	\N
106	firma	1234578	ulice	Dubňany	69603	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	t	f				\N
94	firma	1234578	ulice	Kladno	27201	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	2400	\N	f	t	\N	\N	\N	\N
15	firma	1234578	ulice	Uh. Hradiště	68601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
16	firma	1234578	ulice	Plzeň	30111	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0600	\N	f	t	\N	\N	\N	\N
17	firma	1234578	ulice	Kosmonosy	29306	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
18	firma	1234578	ulice	Praha 6 - Dejvice	16000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
101	firma	1234578	ulice	Benešov	25601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
6	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	t	\N	\N	\N	\N
20	firma	1234578	ulice	Praha 10	10221	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
48	firma	1234578	ulice	Olomouc	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
149	firma	1234578	ulice	Praha 3	13000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
62	firma	1234578	ulice	Praha 9 - Běchovice	190 1	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
24	firma	1234578	ulice	Praha 1	11182	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
56	firma	1234578	ulice	Břeclav	69002	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
81	firma	1234578	ulice	Brno	61400	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
25	firma	1234578	ulice	Olomouc	77900	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
73	firma	1234578	ulice	Dolní Němčí	68762	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
26	firma	1234578	ulice	Praha-Vinohrady	12000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
28	firma	1234578	ulice	Praha 8	18600	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
99	firma	1234578	ulice	Praha 1	11000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
29	firma	1234578	ulice	Jablonec n.N.	46601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0800	\N	f	t	\N	\N	\N	\N
32	firma	1234578	ulice	Praha 11- Roztyly	14800	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	2400	\N	f	t	\N	\N	\N	\N
31	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
33	firma	1234578	ulice	Brno	60300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
80	firma	1234578	ulice	Uh. Hradiště	68601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
34	firma	1234578	ulice	Hradec Králové 2	50003	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
71	firma	1234578	ulice	Frýdek-Místek	73953	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
35	firma	1234578	ulice	Chomutov	43001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	2600	\N	f	t	\N	\N	\N	\N
36	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				\N
151	firma	1234578	ulice	Praha 5		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	t	\N	\N	\N	\N
104	firma	1234578	ulice	Praha 10	10200	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
93	firma	1234578	ulice	 Kralupy nad Vltavou	27801	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
38	firma	1234578	ulice	Vsetín	75501	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
74	firma	1234578	ulice	Domažlice - Město	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
123	firma	1234578	ulice	Praha 9 Horní Počernice	19300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
40	firma	1234578	ulice	Česká Skalice		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
177	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	\N	\N	\N	\N	\N
53	firma	1234578	ulice	Chrudim	53701	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
47	firma	1234578	ulice	Praha západ		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
66	firma	1234578	ulice	Přerov	75000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
68	firma	1234578	ulice	Přerov	75002	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
117	firma	1234578	ulice	Praha 10	10100	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
132	firma	1234578	ulice	Boskovice	68001	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
69	firma	1234578	ulice	Frýdek-Místek	73801	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
67	firma	1234578	ulice	Šumperk	78701	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
111	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
90	firma	1234578	ulice	Lipník nad Bečvou	75131	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
118	firma	1234578	ulice	Palackého 104	67201	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
55	firma	1234578	ulice	Nový Jičín	74101	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
19	firma	1234578	ulice	Praha 4	14000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
21	firma	1234578	ulice	Přerov 2	75002	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
39	firma	1234578	ulice	Brno	60200	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0600	\N	f	\N	\N	\N	\N	\N
27	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
22	firma	1234578	ulice	Praha 1	11000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
37	firma	1234578	ulice	Praha 6	16000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
58	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
70	firma	1234578	ulice	Nymburk	28802	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
13	firma	1234578	ulice	Praha 3 - Žižkov	13000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
85	firma	1234578	ulice		66441	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
88	firma	1234578	ulice		76364	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0400	\N	f	\N	\N	\N	\N	\N
89	firma	1234578	ulice	674 01 Trebíč	67401	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
91	firma	1234578	ulice	Praha 10	10000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
100	firma	1234578	ulice	Třebihošť 83	54401	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
107	firma	1234578	ulice	Praha 1	110 0	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
126	firma	1234578	ulice	Praha 8	18600	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
112	firma	1234578	ulice		87444	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
114	firma	1234578	ulice	Hranice I - město	75301	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
115	firma	1234578	ulice	Lanškroun	56301	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
75	firma	1234578	ulice	Hodonín	69501	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
95	firma	1234578	ulice	Troubsko	66441	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
54	firma	1234578	ulice	Mladá Boleslav	29301	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
160	firma	1234578	ulice	Praha 10	10000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	t	\N	\N	\N	\N
45	firma	1234578	ulice	Praha 4	14201	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t			\N	
206	firma	1234578	ulice	Praha 10	10900	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
150	firma	1234578	ulice	Praha 10	10000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				\N
143	firma	1234578	ulice	Opava	74641	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
44	firma	1234578	ulice	Velké Bílovice	69101	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
86	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	f	\N	\N	\N	\N
77	firma	1234578	ulice	Praha 6	16200	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
78	firma	1234578	ulice	Plzeň	30100	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
87	firma	1234578	ulice		15000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
134	firma	1234578	ulice	Třešť	58901	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
102	firma	1234578	ulice	Olšany	78902	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
79	firma	1234578	ulice	Břeclav	69002	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
72	firma	1234578	ulice	Praha 6	16000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
43	firma	1234578	ulice	Šumperk	78701	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
46	firma	1234578	ulice	Zlechov	68710	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t	Dlouhá 1303	Uherské Hradiště 	68601	\N
105	firma	1234578	ulice	Uherské Hradiště	68601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	t	t	\N	\N	\N	\N
130	firma	1234578	ulice	Praha 5 	15000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	\N	\N	\N	\N	\N
60	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
57	firma	1234578	ulice	Brno	63500	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
63	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
113	firma	1234578	ulice		38473	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
135	firma	1234578	ulice	Vestec	25242	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
52	firma	1234578	ulice	Pozořice	66407	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	t	\N	\N	\N	\N
49	firma	1234578	ulice	Hodonín	69501	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
64	firma	1234578	ulice	Železný Brod	46822	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	\N	f	t	\N	\N	\N	\N
51	firma	1234578	ulice	Praha 1	11000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
103	firma	1234578	ulice	Veřovice	74273	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	0800	\N	f	t	\N	\N	\N	\N
120	firma	1234578	ulice	Šlapanice	66451	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
225	firma	1234578	ulice	Lipník nad Bečvou	75131	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	t	\N	\N	\N	\N
234	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
235	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
236	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
174	firma	1234578	ulice	Praha 4 Modřany	14300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f	\N	\N	\N	\N
167	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	t	t				
165	firma	1234578	ulice			www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	t	t				
181	firma	1234578	ulice	ydydc	+++	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	t	t	aaa	aaa	111	CYXCcy
237	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
238	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
239	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
240	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
241	firma	1234578	ulice	Brno	62100	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	2700	t	f	f				
242	firma	1234578	ulice	Praha 1	11000	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				Horák
243	firma	1234578	ulice	 Blansko	67801	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				Souček
244	firma	1234578	ulice	Senec u Plzně	33008	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				Vlastimil Dolanský
245	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
246	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
247	firma	1234578	ulice	Hostivice	253 01	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				
248	firma	1234578	ulice	Most	43401	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				Miroslav Bára
249	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
250	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
251	firma	1234578	ulice	Týnec nad Sázavou	25741	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				pi. Cyraniová
253	firma	1234578	ulice	Benešov	25601	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		\N	f	\N	\N	\N	\N	\N
50	firma	1234578	ulice	Hulín	76824	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	t				
254	firma	1234578	ulice	Arnoltice, Děčín		www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				Ladislav Hruška
255	firma	1234578	ulice	Praha 1	11800	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t	Pod Koupalištěm 901 	Kosmonosy	29306	Jan Rechtberger
256	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
257	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
258	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
259	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
260	firma	1234578	ulice	Děčín 5	40502	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				Pavel Bureš
261	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
121	firma	1234578	ulice	Praha 1	100 00	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				
262	firma	1234578	ulice	Třebíč 1	674 01	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		t	f	f				
252	firma	1234578	ulice	Praha 10, Benice	10300	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				Vojtěch Hami
263	firma	1234578	ulice	Praha 12	143 00	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				ing. Jan Šilhavý
264	firma	1234578	ulice	Slaný	274 01	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789		f	f	t				Helena Majrichová
265	firma	1234578	ulice	\N	\N	www.cz	mail.cz	123456789	222333444	444555666	cz1234578	123456789	\N	t	f	\N	\N	\N	\N	\N
\.


--
-- Data for Name: deliverynotes; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY deliverynotes (id, productorderid, companyid, status, transactiondate, userid, transportoutcomeid, transportcode, resolutiondate, userid2, transportnote, invoiceid, invoicetype, invoicedate, invoiceuserid, paymentdate, paid, paiddate, paiduserid, paidnote, deleted) FROM stdin;
49	\N	41	5	2006-11-28 17:19:10.228185	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
74	\N	60	5	2007-01-24 09:09:54.911862	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
75	\N	47	5	2007-01-24 14:44:22.664626	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
76	\N	32	5	2007-01-24 18:19:28.636405	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
77	\N	75	5	2007-01-29 10:48:20.799573	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
78	\N	11	5	2007-01-30 09:41:02.815054	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
79	\N	11	5	2007-01-30 13:40:20.826598	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
80	\N	54	5	2007-02-01 15:20:04.69252	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
81	\N	45	5	2007-02-05 11:19:32.158874	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
82	\N	32	5	2007-02-07 14:37:15.265458	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
83	\N	54	5	2007-02-12 09:37:08.714616	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
84	\N	45	5	2007-02-12 11:10:32.684941	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
85	\N	56	5	2007-02-13 10:45:46.408369	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
86	\N	56	5	2007-02-13 11:15:47.719917	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
87	\N	15	5	2007-02-16 10:40:20.707219	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
88	\N	25	5	2007-02-21 12:56:47.5105	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
89	\N	43	5	2007-02-21 16:22:52.568018	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90	\N	43	5	2007-02-21 16:36:57.448552	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
91	\N	54	5	2007-02-23 12:58:30.10065	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
92	\N	54	5	2007-02-26 10:18:08.345234	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
93	\N	50	5	2007-02-26 10:56:20.962959	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
94	\N	76	5	2007-03-05 12:53:01.318169	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
95	\N	10	5	2007-03-05 13:47:34.259857	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
96	\N	77	5	2007-03-06 14:14:28.103946	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
97	\N	77	5	2007-03-07 12:06:19.17746	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
98	\N	78	5	2007-03-08 13:20:39.791353	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
99	\N	78	5	2007-03-08 13:24:13.628823	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
100	\N	10	5	2007-03-08 14:44:15.901889	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
101	\N	15	5	2007-03-08 15:21:38.994667	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
102	\N	16	5	2007-03-12 12:18:01.175792	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
103	\N	17	5	2007-03-14 12:24:01.774263	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
104	\N	50	5	2007-03-14 14:37:44.616725	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
105	\N	41	5	2007-03-14 15:00:17.343953	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
106	\N	54	5	2007-03-15 11:45:54.895388	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
107	\N	47	5	2007-03-16 09:01:38.224852	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
108	\N	43	5	2007-03-16 10:27:39.190621	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
109	\N	41	5	2007-03-16 11:32:20.780365	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
110	\N	47	5	2007-03-16 13:51:26.778617	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
111	\N	15	5	2007-03-19 14:18:35.649224	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
112	\N	47	5	2007-03-19 15:27:54.691308	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
113	\N	76	5	2007-03-19 16:36:25.802665	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
114	\N	41	5	2007-03-23 10:20:10.362385	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
115	\N	41	5	2007-03-23 11:49:35.261128	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
116	\N	41	5	2007-03-23 15:35:39.409204	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
117	\N	41	5	2007-03-26 11:11:54.661551	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
118	\N	66	5	2007-03-30 09:36:06.490292	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
119	\N	52	5	2007-03-30 09:43:20.138006	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
120	\N	47	5	2007-03-30 13:33:31.96483	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
315	\N	61	5	2008-02-12 15:41:37.389481	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
316	\N	45	5	2008-02-14 16:37:47.331353	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
189	\N	98	5	2007-06-13 15:10:37.357231	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
190	\N	43	5	2007-06-13 15:13:49.9168	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
317	\N	34	5	2008-02-19 14:20:25.127119	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
155	\N	90	5	2007-05-09 14:53:42.465802	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
156	\N	29	5	2007-05-11 09:03:57.21802	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
157	\N	41	5	2007-05-11 13:31:24.788161	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
158	\N	51	5	2007-05-14 07:27:02.886702	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
159	\N	45	5	2007-05-14 07:29:16.694771	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
160	\N	66	5	2007-05-14 15:46:35.524385	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
161	\N	45	5	2007-05-15 11:08:18.098473	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
162	\N	45	5	2007-05-18 07:21:29.644112	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
163	\N	45	5	2007-05-18 09:49:16.050735	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
164	\N	10	5	2007-05-22 13:52:46.052026	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
165	\N	10	5	2007-05-22 13:55:56.347743	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
166	\N	96	5	2007-05-23 11:00:41.519043	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
416	\N	136	5	2008-09-11 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
413	\N	128	5	2008-09-04 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
419	\N	138	5	2008-09-23 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
420	\N	122	5	2008-09-25 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
415	\N	131	5	2008-09-09 00:00:00	1	1		2008-09-09	1		\N	\N	\N	\N	\N	f	\N	\N	\N	f
425	\N	139	5	2008-10-15 00:00:00	10	1		\N	\N	Platba hotove.	\N	\N	\N	\N	\N	f	\N	\N	\N	f
410	\N	127	5	2008-08-27 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
424	\N	16	5	2008-10-06 12:09:20.561002	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
422	\N	38	5	2008-10-06 10:46:06.410991	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
418	\N	137	5	2008-09-22 14:19:33.144087	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
417	\N	15	5	2008-09-22 10:55:03.855523	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
411	\N	10	5	2008-09-01 11:51:43.133339	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
409	\N	124	5	2008-08-27 12:57:13.10483	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
50	\N	41	5	2006-11-28 17:22:59.03007	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
51	\N	57	5	2006-11-29 15:26:53.936037	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
52	\N	10	5	2006-12-04 12:09:47.773044	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
54	\N	41	5	2006-12-06 10:05:51.179172	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
55	\N	41	5	2006-12-13 09:53:51.044004	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
57	\N	17	5	2006-12-13 11:56:02.092683	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
58	\N	32	5	2006-12-15 13:30:15.280767	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
59	\N	53	5	2006-12-19 12:41:49.332651	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
60	\N	53	5	2006-12-19 12:44:29.828159	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
61	\N	53	5	2006-12-19 14:08:00.714868	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
62	\N	57	5	2006-12-20 09:43:11.582008	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
63	\N	56	5	2006-12-20 15:21:50.218631	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
64	\N	56	5	2006-12-22 13:09:03.264288	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
65	\N	11	5	2007-01-05 09:03:04.909142	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
66	\N	6	5	2007-01-10 15:13:48.330271	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
68	\N	25	5	2007-01-11 13:02:39.117721	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
69	\N	8	5	2007-01-12 09:17:25.366022	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
70	\N	53	5	2007-01-18 13:51:59.146233	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
71	\N	47	5	2007-01-19 12:09:26.164999	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
72	\N	74	5	2007-01-19 15:06:31.010806	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
73	\N	15	5	2007-01-22 10:57:28.119078	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
167	\N	29	5	2007-05-24 08:27:18.755642	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
168	\N	47	5	2007-05-24 12:07:37.754499	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
201	\N	77	5	2007-07-02 14:16:28.035156	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
209	\N	57	5	2007-07-12 13:36:36.840727	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
213	\N	57	5	2007-07-26 13:16:10.493246	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
227	\N	54	5	2007-08-08 13:50:54.426626	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
228	\N	54	5	2007-08-08 13:53:17.295296	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
226	\N	32	5	2007-08-06 13:40:05.642284	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
224	\N	80	5	2007-08-06 10:48:53.96456	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
223	\N	29	5	2007-08-06 09:06:24.773467	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
222	\N	102	5	2007-08-03 17:21:57.584942	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
221	\N	102	5	2007-08-03 14:42:02.327379	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
220	\N	45	5	2007-08-02 10:43:30.931931	14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
217	\N	43	5	2007-07-30 15:02:53.079141	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
216	\N	32	5	2007-07-27 08:01:44.89051	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
215	\N	32	5	2007-07-27 07:59:41.375779	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
214	\N	57	5	2007-07-26 13:21:26.816937	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
212	\N	15	5	2007-07-26 08:15:53.24774	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
211	\N	103	5	2007-07-26 08:12:59.202354	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
210	\N	45	5	2007-07-16 13:17:28.897931	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
208	\N	57	5	2007-07-12 13:33:45.721084	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
206	\N	43	5	2007-07-10 08:10:30.87408	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
205	\N	70	5	2007-07-04 16:28:53.889143	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
204	\N	47	5	2007-07-03 11:21:50.215637	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
203	\N	47	5	2007-07-03 10:14:22.351378	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
202	\N	77	5	2007-07-02 14:26:47.829468	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
198	\N	52	5	2007-07-02 08:05:49.058175	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
197	\N	32	5	2007-06-27 09:45:34.043388	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
196	\N	32	5	2007-06-26 11:51:38.325841	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
195	\N	47	5	2007-06-26 11:01:53.78193	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
318	\N	118	5	2008-02-25 12:22:17.262724	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
123	\N	29	5	2007-04-02 14:34:17.957763	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
124	\N	85	5	2007-04-03 14:31:47.016687	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
126	\N	73	5	2007-04-11 09:51:23.621957	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
129	\N	89	5	2007-04-12 16:27:37.241393	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
134	\N	47	5	2007-04-16 14:52:14.901312	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
135	\N	47	5	2007-04-16 15:21:58.518031	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
137	\N	45	5	2007-04-18 14:35:56.02197	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
138	\N	32	5	2007-04-18 14:59:53.857442	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
140	\N	32	5	2007-04-19 14:15:40.523324	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
142	\N	32	5	2007-04-20 10:25:24.54043	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
149	\N	32	5	2007-04-25 11:22:31.057884	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
151	\N	61	5	2007-04-25 14:31:34.202522	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
169	\N	29	5	2007-05-28 11:11:52.502505	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
170	\N	15	5	2007-05-28 15:10:23.355971	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
171	\N	11	5	2007-05-29 12:08:03.930331	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
154	\N	90	5	2007-05-07 13:51:37.758398	14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
153	\N	90	5	2007-05-07 10:06:03.048344	14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
150	\N	77	5	2007-04-25 14:24:11.895608	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
148	\N	77	5	2007-04-24 15:57:17.982347	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
182	\N	51	5	2007-06-11 14:20:45.138279	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
147	\N	41	5	2007-04-20 13:23:24.454161	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
146	\N	91	5	2007-04-20 13:00:28.711945	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
141	\N	45	5	2007-04-19 14:50:09.832391	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
136	\N	92	5	2007-04-17 08:35:51.357995	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
133	\N	73	5	2007-04-13 15:12:49.482097	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
132	\N	73	5	2007-04-13 15:10:56.441194	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
131	\N	47	5	2007-04-13 13:45:09.805869	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
130	\N	73	5	2007-04-13 12:33:24.72925	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
128	\N	73	5	2007-04-12 11:02:53.969856	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
127	\N	73	5	2007-04-11 14:44:20.531091	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
173	\N	15	5	2007-06-03 18:55:50.073541	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
232	\N	57	5	2007-08-14 14:03:34.967478	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
234	\N	57	5	2007-08-14 14:16:52.357687	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
243	\N	110	5	2007-08-30 13:24:32.436155	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
172	\N	61	5	2007-05-30 12:05:37.127592	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
247	\N	76	5	2007-09-06 09:41:02.610074	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
246	\N	41	5	2007-09-05 15:09:02.950553	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
245	\N	96	5	2007-09-03 12:25:05.890279	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
244	\N	16	5	2007-08-30 13:59:09.80195	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
242	\N	43	5	2007-08-29 14:04:38.184561	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
241	\N	96	5	2007-08-28 11:55:29.572542	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
319	\N	34	5	2008-02-26 13:40:41.546208	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
289	\N	52	5	2007-11-28 11:33:20.306306	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
285	\N	51	5	2007-11-16 11:30:56.077579	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
284	\N	110	5	2007-11-13 13:01:56.409302	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
283	\N	96	5	2007-11-08 11:17:01.14335	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
282	\N	96	5	2007-11-08 11:11:42.144264	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
281	\N	47	5	2007-11-06 08:20:50.771255	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
280	\N	17	5	2007-11-02 11:21:19.502532	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
279	\N	10	5	2007-10-24 13:02:21.996547	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
278	\N	10	5	2007-10-24 12:59:17.104728	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
277	\N	6	5	2007-10-24 11:58:15.063418	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
276	\N	112	5	2007-10-18 11:23:35.422399	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
275	\N	32	5	2007-10-17 11:50:47.648529	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
274	\N	32	5	2007-10-17 11:49:30.669685	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
273	\N	77	5	2007-10-17 11:09:40.341878	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
271	\N	47	5	2007-10-05 14:22:31.353184	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
270	\N	47	5	2007-10-03 13:24:41.030071	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
269	\N	47	5	2007-10-03 12:56:25.55573	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
268	\N	77	5	2007-10-03 11:34:15.979656	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
267	\N	96	5	2007-10-03 10:27:24.448557	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
266	\N	96	5	2007-10-03 10:24:27.134181	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
265	\N	11	5	2007-10-03 10:07:58.205415	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
264	\N	11	5	2007-10-03 10:05:57.92319	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
263	\N	77	5	2007-10-02 09:52:35.937009	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
262	\N	77	5	2007-10-02 09:50:09.717709	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
261	\N	45	5	2007-09-26 11:55:25.559962	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
260	\N	32	5	2007-09-21 12:45:47.118661	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
259	\N	38	5	2007-09-21 12:10:24.207071	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
257	\N	79	5	2007-09-21 10:11:38.461712	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
256	\N	43	5	2007-09-21 09:02:18.994986	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
255	\N	43	5	2007-09-21 08:57:43.250462	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
253	\N	43	5	2007-09-19 08:31:43.454861	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
122	\N	78	5	2007-03-30 15:02:49.019011	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
121	\N	45	5	2007-03-30 14:04:50.544508	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
330	\N	57	5	2008-03-27 13:25:55.689629	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
327	\N	120	5	2008-03-14 12:37:00.904914	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
326	\N	57	5	2008-03-12 09:32:56.772246	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
325	\N	57	5	2008-03-12 09:30:04.367064	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
321	\N	117	5	2008-03-06 11:09:39.482882	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
320	\N	47	5	2008-03-03 11:02:13.427234	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
310	\N	34	5	2008-01-11 10:49:14.67954	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
309	\N	34	5	2008-01-11 10:44:38.98789	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
308	\N	117	5	2008-01-10 14:35:47.457822	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
307	\N	32	5	2008-01-09 12:36:39.909044	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
305	\N	117	5	2008-01-04 15:21:35.530692	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
304	\N	41	5	2007-12-21 12:41:52.449268	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
302	\N	57	5	2007-12-21 09:09:07.368466	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
301	\N	50	5	2007-12-19 16:11:32.063673	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
299	\N	117	5	2007-12-19 15:54:22.045788	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
298	\N	117	5	2007-12-19 14:56:55.97546	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
296	\N	118	5	2007-12-18 12:09:36.826055	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
125	\N	41	5	2007-04-05 13:32:43.106094	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
152	\N	38	5	2007-05-04 11:36:08.148051	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
251	\N	29	5	2007-09-14 10:29:24.948269	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
341	\N	10	5	2008-04-16 11:42:57.778135	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
339	\N	32	5	2008-04-09 12:41:31.556451	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
337	\N	57	5	2008-04-07 14:10:33.898275	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
249	\N	96	5	2007-09-07 12:29:40.174846	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
334	\N	57	5	2008-03-28 15:28:49.262792	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
344	\N	47	5	2008-04-21 13:38:57.343988	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
346	\N	47	5	2008-04-25 14:53:14.958328	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
347	\N	47	5	2008-05-02 09:11:12.001036	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
343	\N	45	5	2008-04-17 14:47:26.788019	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
368	\N	130	5	2008-05-20 12:19:13.649759	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
367	\N	130	5	2008-05-20 12:17:57.118014	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
366	\N	130	5	2008-05-20 12:15:43.3761	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
365	\N	130	5	2008-05-20 12:14:17.331304	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
364	\N	130	5	2008-05-20 12:12:09.744256	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
363	\N	130	5	2008-05-20 12:08:49.44128	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
362	\N	130	5	2008-05-20 12:04:13.629791	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
361	\N	130	5	2008-05-20 12:03:18.907815	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
360	\N	130	5	2008-05-20 12:02:26.894234	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
359	\N	130	5	2008-05-20 12:00:59.300825	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
358	\N	130	5	2008-05-20 11:59:53.926243	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
357	\N	130	5	2008-05-20 11:58:39.066238	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
356	\N	130	5	2008-05-20 11:32:04.547578	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
355	\N	129	5	2008-05-20 09:55:31.004185	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
340	\N	10	5	2008-04-16 11:32:38.974304	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
331	\N	57	5	2008-03-27 13:28:07.220393	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
335	\N	47	5	2008-03-31 15:30:19.496984	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
312	\N	113	5	2008-01-15 12:32:49.634806	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
311	\N	38	5	2008-01-11 10:51:08.726469	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
306	\N	32	5	2008-01-09 12:31:37.510455	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
293	\N	42	5	2007-12-04 11:03:14.928482	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
290	\N	42	5	2007-11-30 10:29:28.206459	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
323	\N	10	5	2008-03-07 12:20:25.494602	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
322	\N	10	5	2008-03-07 12:19:29.770307	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
324	\N	10	5	2008-03-07 12:28:03.381067	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
297	\N	32	5	2007-12-18 13:15:00.967147	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
295	\N	61	5	2007-12-12 13:02:18.024635	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
300	\N	117	5	2007-12-19 16:01:27.605992	14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
303	\N	57	5	2007-12-21 09:31:15.231475	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
350	\N	128	5	2008-05-05 14:46:26.088395	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
349	\N	127	5	2008-05-05 14:31:41.095492	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
333	\N	122	5	2008-03-27 15:54:42.760772	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
332	\N	122	5	2008-03-27 15:48:51.984025	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
288	\N	45	5	2007-11-28 11:03:42.310968	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
353	\N	51	5	2008-05-13 08:57:51.860787	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
352	\N	77	5	2008-05-06 14:03:41.426161	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
345	\N	47	5	2008-04-24 11:13:27.839686	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
292	\N	76	5	2007-11-30 15:13:24.504305	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
291	\N	105	5	2007-11-30 15:08:10.165548	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
287	\N	32	5	2007-11-23 12:24:23.736473	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
286	\N	32	5	2007-11-23 12:22:37.543489	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
272	\N	11	5	2007-10-17 08:19:20.111895	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
258	\N	79	5	2007-09-21 10:14:08.610215	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
252	\N	43	5	2007-09-19 08:14:54.085675	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
250	\N	96	5	2007-09-07 12:30:22.602042	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
248	\N	72	5	2007-09-06 11:10:55.637236	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
240	\N	96	5	2007-08-28 11:23:02.455096	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
239	\N	65	5	2007-08-28 09:11:28.776237	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
238	\N	68	5	2007-08-21 11:24:29.498503	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
237	\N	50	5	2007-08-21 09:20:26.511544	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
236	\N	65	5	2007-08-20 14:15:41.03193	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
235	\N	109	5	2007-08-20 12:33:15.319796	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
233	\N	57	5	2007-08-14 14:10:58.681218	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
231	\N	65	5	2007-08-14 10:16:47.209639	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
230	\N	47	5	2007-08-13 12:12:30.605826	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
229	\N	45	5	2007-08-13 09:52:37.488342	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
175	\N	43	5	2007-06-04 12:59:15.65761	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
176	\N	15	5	2007-06-05 13:13:56.823506	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
179	\N	15	5	2007-06-08 08:03:27.397455	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
180	\N	44	5	2007-06-08 08:10:12.226849	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
181	\N	51	5	2007-06-11 13:47:07.068815	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
185	\N	47	5	2007-06-13 11:10:41.531391	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
186	\N	70	5	2007-06-13 13:01:03.611516	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
187	\N	70	5	2007-06-13 14:26:52.628353	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
188	\N	99	5	2007-06-13 14:40:11.499967	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
191	\N	47	5	2007-06-13 16:09:17.270779	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
192	\N	99	5	2007-06-14 11:00:47.732684	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
193	\N	41	5	2007-06-20 08:08:42.177663	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
194	\N	32	5	2007-06-21 12:04:48.985293	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
338	\N	124	5	2008-04-07 15:45:48.839863	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
336	\N	123	5	2008-04-04 11:12:18.805588	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
374	\N	109	5	2008-06-02 15:04:55.769854	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
351	\N	77	5	2008-05-06 13:51:33.7425	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
348	\N	57	5	2008-05-05 13:38:43.135901	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
200	\N	77	5	2007-07-02 12:21:03.225157	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
199	\N	15	5	2007-07-02 09:07:11.012533	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
376	\N	45	5	2008-06-04 14:40:25.549138	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
373	\N	50	5	2008-06-02 14:08:06.536202	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
372	\N	10	5	2008-05-28 13:22:11.721634	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
254	\N	78	5	2007-09-20 11:22:11.834586	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
354	\N	122	5	2008-05-16 10:20:02.776044	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
379	\N	32	5	2008-06-12 14:19:25.185666	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
380	\N	11	5	2008-06-12 14:20:51.388475	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
369	\N	131	5	2008-05-21 13:45:41.898655	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
342	\N	125	5	2008-04-16 12:24:59.648597	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
370	\N	122	5	2008-05-23 12:26:20.274621	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
383	\N	47	5	2008-06-26 10:46:42.72286	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
375	\N	33	5	2008-06-04 11:48:24.863665	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
377	\N	128	5	2008-06-09 12:25:37.603401	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
384	\N	38	5	2008-06-26 17:18:12.871937	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
378	\N	124	5	2008-06-09 12:46:03.198592	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
294	\N	116	5	2007-12-07 12:05:29.484457	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
389	\N	134	5	2008-07-09 15:59:23.067426	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
393	\N	54	5	2008-07-21 11:14:38.323729	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
397	\N	47	5	2008-07-22 10:38:06.349142	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
387	\N	132	5	2008-06-30 13:54:55.328341	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
400	\N	29	5	2008-07-25 12:23:15.041491	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
406	\N	17	5	2008-08-14 12:41:48.925753	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
388	\N	125	5	2008-07-08 13:50:26.864528	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
396	\N	121	5	2008-07-21 18:54:28.168454	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
395	\N	121	5	2008-07-21 11:54:06.538764	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
390	\N	135	5	2008-07-10 10:40:38.943567	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
407	\N	54	5	2008-08-15 12:39:05.030643	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
392	\N	61	5	2008-07-18 16:26:54.577935	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
398	\N	47	5	2008-07-22 12:15:17.250538	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
371	\N	16	5	2008-05-28 11:45:04.475928	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
386	\N	51	5	2008-06-27 12:53:04.496896	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
401	\N	10	5	2008-07-30 12:51:18.014499	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
404	\N	99	5	2008-08-11 15:30:40.794368	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
405	\N	17	5	2008-08-14 12:28:58.217451	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
394	\N	54	5	2008-07-21 11:28:00.399241	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
385	\N	38	5	2008-06-26 17:26:25.590944	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
403	\N	8	5	2008-08-07 14:36:49.091566	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
382	\N	108	5	2008-06-19 15:27:09.211705	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
399	\N	81	5	2008-07-24 15:10:20.899945	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
381	\N	125	5	2008-06-19 14:30:46.657892	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
408	\N	48	5	2008-08-26 09:26:32.199866	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
427	\N	140	5	2008-10-21 00:00:00	1	1		\N	\N	placeno hotově	\N	\N	\N	\N	\N	f	\N	\N	\N	f
423	\N	38	5	2008-10-06 11:07:39.571314	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
414	\N	38	5	2008-09-09 11:52:48.497588	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
412	\N	10	5	2008-09-01 13:29:54.585002	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
435	\N	15	5	2008-11-07 00:00:00	10	2	PB0435303056M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
436	\N	70	5	2008-11-07 14:44:24.342253	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
441	\N	130	5	2008-11-12 12:33:14.387504	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
439	\N	143	5	2008-11-11 00:00:00	10	2	PB0435302285M	\N	\N	Na dobírku.	\N	\N	\N	\N	\N	f	\N	\N	\N	f
446	\N	57	5	2008-11-20 14:02:06.164585	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
447	\N	10	5	2008-11-28 09:05:03.277571	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
431	\N	121	5	2008-10-31 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
426	\N	11	5	2008-10-17 11:19:59.893585	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
430	\N	70	5	2008-10-31 12:59:06.57125	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
428	\N	141	5	2008-10-23 00:00:00	1	1		\N	\N	odesláno emailem	\N	\N	\N	\N	\N	f	\N	\N	\N	f
421	\N	121	5	2008-10-01 00:00:00	1	2	PB0435302254M	\N	\N	Dušan Svoboda, Zahradnická 17, 603 00 Brno	\N	\N	\N	\N	\N	f	\N	\N	\N	f
438	\N	128	5	2008-11-11 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
429	\N	50	5	2008-10-29 15:09:16.283246	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
432	\N	10	5	2008-11-04 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
437	\N	70	5	2008-11-07 00:00:00	10	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
440	\N	41	5	2008-11-12 11:36:21.060692	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
442	\N	47	5	2008-11-13 11:05:07.51164	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
445	\N	57	5	2008-11-20 00:00:00	10	2	PV0435301934M, PV0435301948M	\N	\N	Pouzita antena 65cm.	\N	\N	\N	\N	\N	f	\N	\N	\N	f
448	\N	10	5	2008-12-03 00:00:00	1	1		\N	\N	předávací protokol 447	\N	\N	\N	\N	\N	f	\N	\N	\N	f
443	\N	141	5	2008-11-18 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90002	\N	57	5	2009-01-07 13:09:24.840468	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90004	\N	149	5	2009-01-08 10:43:27.61939	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90005	\N	149	5	2009-01-08 10:48:16.492547	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
449	\N	127	5	2008-12-03 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
451	\N	148	5	2008-12-04 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90003	\N	57	5	2009-01-07 13:17:44.066182	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90006	\N	149	5	2009-01-08 11:49:00.665567	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90007	\N	149	5	2009-01-08 13:58:47.02228	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90009	\N	95	5	2009-01-12 14:15:06.570436	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
450	\N	121	5	2008-12-04 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
456	\N	73	5	2008-12-19 09:42:33.302466	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
452	\N	47	5	2008-12-10 13:20:27.211087	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
453	\N	47	5	2008-12-10 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
455	\N	61	5	2008-12-17 12:15:59.487548	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
444	\N	34	5	2008-11-20 12:11:16.711806	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90008	\N	138	5	2009-01-08 00:00:00	1	1		\N	\N	předáno v prosinci	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90013	\N	87	5	2009-01-27 10:16:23.468959	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90014	\N	87	5	2009-01-27 10:17:32.534989	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90017	\N	87	5	2009-01-27 12:24:53.459072	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90011	\N	128	5	2009-01-13 00:00:00	1	1		2009-01-13	1		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90001	\N	121	5	2009-01-06 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90010	\N	125	5	2009-01-12 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90018	\N	141	5	2009-01-28 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90012	\N	50	5	2009-01-15 11:50:47.447345	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90019	\N	50	5	2009-02-05 09:15:48.528346	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
454	\N	131	5	2008-12-11 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90032	\N	77	5	2009-03-19 00:00:00	10	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90020	\N	164	5	2009-02-10 00:00:00	1	2	PB0435303100M	\N	\N	dobírka	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90039	\N	148	5	2009-03-30 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90040	\N	148	5	2009-03-30 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90041	\N	148	5	2009-03-30 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90042	\N	148	5	2009-03-30 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90043	\N	148	5	2009-03-30 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90044	\N	148	5	2009-03-30 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90046	\N	165	5	2009-04-01 00:00:00	10	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90028	\N	141	5	2009-03-18 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90038	\N	148	5	2009-03-25 00:00:00	10000	0		\N	\N	s	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90037	\N	148	5	2009-03-25 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90036	\N	148	5	2009-03-25 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90035	\N	148	5	2009-03-25 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90034	\N	148	5	2009-03-24 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90052	\N	47	5	2009-05-13 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90059	\N	150	5	2009-06-03 00:00:00	10	1		\N	\N	Oprava spoje.	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90057	\N	102	5	2009-05-29 00:00:00	1	2	PB0435300721M	\N	\N	prodej za vyhořelý	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90049	\N	141	5	2009-04-24 00:00:00	1	2	PV0435301713M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90047	\N	127	5	2009-04-10 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90055	\N	132	5	2009-05-19 00:00:00	1	2	PB0435302311M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90056	\N	57	5	2009-05-29 00:00:00	1	2	PV0435302271M, PV0435302285M	\N	\N	dobírka	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90053	\N	78	5	2009-05-13 00:00:00	1	1		\N	\N	placeno hotově	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90029	\N	50	5	2009-03-18 00:00:00	1	2	PB0435302308M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90024	\N	176	5	2009-03-16 12:25:03.04835	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90050	\N	50	5	2009-04-24 00:00:00	8	2	pb0435303232M,PB0435303246M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90048	\N	45	5	2009-04-14 11:44:57.975162	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90054	\N	121	5	2009-05-14 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90065	\N	45	5	2009-07-03 00:00:00	10	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90068	\N	45	5	2009-07-13 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90072	\N	165	5	2009-07-27 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	t
90061	\N	121	5	2009-06-22 00:00:00	1	2	PV0435301885M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90064	\N	127	5	2009-07-01 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90063	\N	121	5	2009-07-01 00:00:00	1	2	PB0435300695M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90069	\N	17	5	2009-07-15 00:00:00	8	2	PB0435303334M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90067	\N	56	5	2009-07-09 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90073	\N	17	5	2009-08-05 00:00:00	8	2		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90081	\N	198	5	2009-08-19 00:00:00	1	1		2009-08-19	1		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90071	\N	196	5	2009-07-27 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90084	\N	48	5	2009-08-27 00:00:00	8	2	PB0435303507M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90085	\N	201	5	2009-08-28 00:00:00	10	2	PB0435303475M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90087	\N	202	5	2009-09-14 00:00:00	1	1		2009-09-14	1		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90045	\N	181	5	2009-04-01 00:00:00	99999	2	PB0435300695M	2009-04-01	10	peogjpowrgj	\N	\N	\N	\N	\N	f	\N	\N	\N	t
90095	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	09-0094-S\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90096	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	09-0093-S\n\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90097	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	09-0092-S\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90098	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	09-0090-S\n\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90099	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	09-0091-S\n\n\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90077	\N	121	5	2009-08-13 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90080	\N	197	5	2009-08-19 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90083	\N	141	5	2009-08-21 00:00:00	1	2	PV0435301829M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90086	\N	197	5	2009-09-11 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90051	\N	78	5	2009-04-28 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90015	\N	73	5	2009-01-27 10:24:38.553425	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90070	\N	32	5	2009-07-17 00:00:00	10	1		2009-07-17	5	Oprava dílů SDM10DE-25\nvýrobní čísla dílů:\n3VC5OR\n3VC51I\n3VC5OS\n3UZ5U8\n3UY5W9\n3UZ5U7\n3UZ5U6\n3VC51G\n3VC5OT\n3VC5OV\n3VC51H\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90104	\N	95	5	2009-10-07 00:00:00	8	2	PV0435301877M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90079	\N	54	5	2009-08-14 00:00:00	8	1		\N	\N	Použitý radom	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90078	\N	165	5	2009-08-14 00:00:00	10	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	t
90021	\N	73	5	2009-02-13 15:35:10.782823	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90106	\N	165	5	2009-10-20 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	t
90088	\N	133	5	2009-09-16 00:00:00	10	5		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90089	\N	204	5	2009-09-16 00:00:00	10	2		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90076	\N	33	5	2009-08-11 00:00:00	10	2		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90016	\N	120	5	2009-01-27 10:25:33.355083	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90105	\N	46	5	2009-10-14 00:00:00	10	2	PB0435303484M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90103	\N	197	5	2009-10-05 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90101	\N	197	5	2009-09-24 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90060	\N	35	5	2009-06-04 00:00:00	10	2	PB0435303379M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90062	\N	194	5	2009-06-26 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90066	\N	45	5	2009-07-09 00:00:00	10	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90075	\N	47	5	2009-08-10 00:00:00	10	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90100	\N	70	5	2009-09-24 00:00:00	10	1		2009-10-20	10		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90108	\N	6	5	2009-10-30 00:00:00	10	1		\N	\N	Oprava za 1000 Kč.	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90090	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	090911-1	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90112	\N	56	5	2009-11-05 00:00:00	10	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	t
90074	\N	53	5	2009-08-05 00:00:00	10	2	PB0435303422M	\N	\N	Pozáruční servis.	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90082	\N	77	5	2009-08-20 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90102	\N	77	5	2009-10-02 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90031	\N	77	5	2009-03-19 00:00:00	10	1		\N	\N	Mikrovlnny spoj 3VE3VF	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90033	\N	77	5	2009-03-20 13:16:06.836282	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90093	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	090910-1	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90091	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	090914-1	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90092	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	090916-\n\n	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90094	\N	57	5	2009-09-18 00:00:00	8	1		\N	\N	090915-1	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90111	\N	17	5	2009-11-03 00:00:00	1	2	PB0435303569M	\N	\N	NA DOBÍRKU	\N	\N	\N	\N	\N	f	\N	\N	\N	f
328	\N	121	5	2008-03-21 13:44:42.042883	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90110	\N	148	5	2009-11-02 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90109	\N	206	5	2009-11-02 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90113	\N	121	5	2009-11-05 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90119	\N	99	5	2009-12-02 00:00:00	8	2	PV0435302311M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90118	\N	105	5	2009-12-01 00:00:00	8	2	PB0435303630M	\N	\N	Dobírka	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90123	\N	57	5	2009-12-15 00:00:00	1	2	PB0435303538M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90117	\N	212	5	2009-12-01 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90120	\N	124	5	2009-12-04 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90122	\N	212	5	2009-12-15 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90121	\N	213	5	2009-12-15 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100000	\N	192	5	2010-01-04 09:28:52.074815	99999	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90116	\N	197	5	2009-11-30 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100005	\N	215	5	2010-01-13 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100008	\N	29	5	2010-01-25 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100009	\N	29	5	2010-01-25 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100012	\N	150	5	2010-01-28 00:00:00	8	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100018	\N	222	5	2010-02-16 00:00:00	8	2	PV0435302325M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90107	\N	125	5	2009-10-23 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
90124	\N	121	5	2009-12-17 00:00:00	1	1		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100001	\N	214	5	2010-01-13 00:00:00	1	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100019	100006	222	5	2010-02-16 00:00:00	8	2	PV0435302325M	2010-03-19	1		/15_100004_100019_FVZ-5_2010 ELPREMO.PDF	0	2010-02-22	1	2010-03-08	t	2010-03-11	1		f
100013	100003	219	5	2010-02-01 00:00:00	8	6		2010-02-01	1		/15_100006_100013_FVZ-3_2010 Brich Jan.PDF	5	2010-02-01	1	2010-02-01	t	2010-02-01	1		f
100004	100001	215	5	2010-01-13 00:00:00	1	1	Baránek Jan	2010-01-13	1		/15_100007_100004_FVZ-2_2010 BKS.PDF	2	2010-01-13	1	\N	t	2010-02-02	1		f
100002	100002	16	5	2010-01-13 00:00:00	1	1		2010-01-13	1		/15_100008_100002_FVZ-1_2010 Disk.PDF	0	2010-01-13	1	2010-01-27	t	2010-03-04	1		f
100016	100009	128	5	2010-02-11 00:00:00	1	1	Tesař 	2010-02-11	1	Faktura 4,3	/15_100013_100016_FVB-3_2010 Beulentechnik.PDF	2	2010-04-01	1	2010-03-13	t	2010-02-25	1		f
100007	100009	128	5	2010-01-20 00:00:00	1	1	Tesař 	2010-02-11	1	Faktura 4,3 	/15_100013_100007_FVB-3_2010 Beulentechnik.PDF	2	2010-04-01	1	2010-03-13	t	2010-02-25	1		f
100006	100009	128	5	2010-01-19 00:00:00	1	1	Tesař 	2010-02-11	1	Faktura 4,3 	/15_100013_100006_FVB-3_2010 Beulentechnik.PDF	2	2010-02-11	1	2010-03-13	t	2010-02-25	1		f
90115	0	56	5	2009-11-09 00:00:00	8	2	PV0435301850M	\N	\N	RMA90178, RMA90173, RMA90174, RMA90175, RMA90176, RMA90177		0	\N	0	\N	f	\N	\N		f
90114	0	56	5	2009-11-09 00:00:00	8	2	PV0435301850M	\N	\N	rma90181, 90180, 90179, 90177, 90182, 90183		0	\N	0	\N	f	\N	\N		f
100014	100012	197	5	2010-02-02 00:00:00	1	1		2010-02-02	1		/15_100014_100014_FVB-2_2010 KPZ electronics.PDF	2	2010-02-02	1	2010-02-16	t	2010-03-04	1		f
100017	100011	125	5	2010-02-16 00:00:00	1	1	Krieger 	2010-02-16	10		/15_100015_100017_FVB-6_2010 LG SYSTEM.PDF	2	2010-03-03	10	2010-03-17	t	2010-04-02	10	slíbil, že zaplatí do 29.3.	f
100021	100010	214	5	2010-03-02 00:00:00	1	2	PB0435303609M	2010-03-02	1	ZLV1-2010	/15_100011_100021_FVB-5_2010 Autogard.PDF	2	2010-04-01	10	2010-03-17	t	2010-04-08	10		f
100025	0	225	5	2010-03-29 00:00:00	1	2	BO0435300179M	\N	\N		/19_100025_FVZ-9_2010 Zenit.PDF	2	2010-03-29	10	2010-04-12	t	2010-04-01	10		f
100015	100004	32	5	2010-02-10 00:00:00	1	6		2010-02-10	1		/15_100002_100015_FVZ-4_2010 ha-vel.PDF	2	2010-02-22	1	2010-03-08	t	2010-04-14	10		f
100020	100013	197	5	2010-02-22 00:00:00	1	1		2010-02-22	1		/15_100016_100020_FVB-7_2010 KPZ electronics.PDF	2	2010-04-01	1	2010-03-17	t	2010-04-30	10		f
100027	100017	127	5	2010-04-12 00:00:00	1	1	Brousil	2010-04-13	\N		/19_100027_FVB-11_2010 MAT.PDF	2	2010-04-22	10	2010-05-06	t	2010-05-05	10		f
100031	100020	42	5	2010-04-29 00:00:00	1	2	BO0435300284M	\N	\N		/19_100031_FVZ-22_2010 ADV.PDF	2	2010-04-30	10	2010-05-14	t	2010-05-05	10		f
100026	100014	121	5	2010-04-06 00:00:00	1	1	p. Procházka	2010-04-02	10		/19_100026_FVB-10_2010 Dynex.PDF	2	2010-04-22	10	2010-05-06	t	2010-05-07	10		f
100030	0	52	5	2010-04-29 00:00:00	1	2	OV0435300196M	\N	\N		/19_100030_FVZ-21_2010 Sochor.PDF	2	2010-04-30	10	2010-05-14	t	2010-05-10	10		f
100037	100026	134	5	2010-05-12 00:00:00	1	1	Šťastný	2010-05-12	1	hotově	/19_100037_FVZ-27_2010 Radioklub.PDF	2	2010-05-12	10	2010-05-12	t	2010-05-12	10	v hotovosti	f
100032	0	97	5	2010-04-30 00:00:00	1	2	PB0435303590M	2010-05-10	1		/19_100032_FVZ-23_2010 CC System.PDF	2	2010-04-30	10	2010-05-14	t	2010-05-19	10		f
100036	100025	114	5	2010-05-10 00:00:00	1	2	BO0435300267M	2010-05-10	1	dobírka	/19_100036_FVZ-26_2010 Kujal.PDF	2	2010-05-10	10	2010-05-24	t	2010-05-14	10		f
100028	100019	124	5	2010-04-28 00:00:00	1	1		\N	\N		/19_100028_FVB-13_2010 Secheron.PDF	2	2010-04-28	10	2010-05-12	t	2010-05-24	10		f
100040	100027	97	5	2010-05-18 00:00:00	1	2	BO0435300222M	\N	\N	dobírka	/19_100040_FVZ-28_2010 CC Systems.PDF	4	2010-05-18	10	2010-06-02	t	2010-05-21	10		f
100024	100016	50	5	2010-03-29 00:00:00	1	2	BO0435300182M	\N	\N		/19_100024_FVZ-8_2010 NWT.PDF	2	2010-03-29	10	2010-04-12	t	2010-05-26	10		f
100035	100024	50	5	2010-05-10 00:00:00	1	2	BO0435300240M	2010-05-10	1		/19_100035_FVZ-25_2010 NWT.PDF	2	2010-05-10	10	2010-05-24	t	2010-05-26	10		f
100045	100031	242	5	2010-06-09 00:00:00	1	1	Horák	\N	\N		/19_100045_FVB-18_2010 Sattva.PDF	5	2010-06-10	10	\N	t	2010-06-10	1		f
100042	100030	73	5	2010-05-21 00:00:00	1	2	BO0435300236M	\N	\N	dobírka	/19_100042_FVZ-29_2010 Gemnet.PDF	4	2010-05-24	10	2010-06-07	t	2010-05-27	10	sleva na anténu 20%, zasláno na dobírku za 8098	f
100041	100030	73	5	2010-05-21 00:00:00	1	2	BO0435300275M	\N	\N	dobírka	/19_100041_FVZ-29_2010 Gemnet.PDF	4	2010-05-24	10	2010-06-07	t	2010-05-27	10	sleva na anténu 20%, zasláno na dobírku za 8098	f
100034	0	47	5	2010-05-04 00:00:00	1	1		\N	\N	RMA  10-0016	/19_100034_FVZ-24_2010 LamPlus.PDF	2	2010-05-04	10	2010-05-18	t	2010-06-17	10		f
100044	100028	212	5	2010-05-31 00:00:00	1	1	p. Zámostný	2010-05-31	\N		/19_100044_FVB-16_2010 KNZ.PDF	2	2010-05-31	10	2010-06-14	t	2010-06-07	10		f
100039	100028	212	5	2010-05-17 00:00:00	1	1	Zámostný	2010-05-17	\N		/19_100039_FVB-16_2010 KNZ.PDF	2	2010-05-31	10	2010-06-14	t	2010-06-07	10		f
100033	100021	197	5	2010-05-03 00:00:00	1	1		\N	\N		/19_100033_FVB-14_2010 KPZ electronics.PDF	2	2010-05-03	10	2010-05-17	t	2010-06-09	10	zaplatili o 12 haléřů víc	f
100038	100023	121	5	2010-05-12 00:00:00	1	1		\N	\N		/19_100038_FVB-15_2010 Dynex.PDF	2	2010-05-21	10	2010-06-04	t	2010-06-09	10		f
100043	100029	121	5	2010-05-31 00:00:00	1	1	p. Procházka	2010-06-02	\N		/19_100043_FVB-17_2010 Dynex.PDF	2	2010-06-10	10	2010-06-24	t	2010-06-28	10		f
100046	100033	124	5	2010-06-11 00:00:00	1	1		\N	\N		/19_100046_FVB-20_2010 Secheron.PDF	2	2010-06-14	10	2010-06-28	t	2010-06-30	10		f
100048	100034	1	5	2010-06-21 00:00:00	99999	1		2010-06-22	99999		/17_100034_100048_Cenik_EXTRANET.pdf	1	2010-06-22	99999	\N	t	\N	99999		f
100047	100032	127	5	2010-06-11 00:00:00	1	1		\N	\N		/19_100047_FVB-19_2010 MAT.PDF	2	2010-06-14	10	2010-07-14	t	2010-07-20	10		f
100053	0	73	5	2010-07-27 00:00:00	8	2	BO0435300094M	\N	\N	Dobírka 1560Kč s daní	/19_100053_FVZ-36_2010 Gemnet.PDF	4	2010-07-27	10	2010-08-10	t	2010-08-02	10	Česká pošta 9211000001	f
100054	0	70	5	2010-07-30 00:00:00	8	1		\N	\N		/19_100054_FVZ-38_2010 Lica.PDF	2	2010-07-30	10	2010-08-13	t	2010-08-12	10		f
100010	100007	29	5	2010-01-25 00:00:00	8	1	Kroupa	2010-03-19	1		/15_100005_100010_FVZ-6_2010 GREPA Networks.PDF	2	2010-03-29	1	2010-03-17	t	2010-08-25	10		f
100011	100007	29	5	2010-01-25 00:00:00	8	1	Kroupa	2010-03-19	1		/15_100005_100011_FVZ-6_2010 GREPA Networks.PDF	2	2010-03-29	1	2010-03-17	t	2010-08-25	10		f
100051	100039	197	5	2010-07-23 00:00:00	1	1		\N	\N		/19_100051_FVB-24_2010 KPZ.PDF	2	2010-08-02	10	2010-08-16	t	2010-09-06	10		f
100056	100042	127	5	2010-08-19 00:00:00	1	1	Brousil	2010-08-23	\N		/19_100056_FVB-25_2010 MAT.PDF	2	2010-08-25	10	2010-09-08	t	2010-09-07	10		f
100057	100044	252	5	2010-08-25 00:00:00	1	1		2010-08-25	\N		/19_100057_FVB-27_2010 Atico Moneo.PDF	2	2010-08-25	10	2010-09-08	t	2010-09-09	10		f
100058	100045	253	5	2010-08-27 00:00:00	1	1	Zámostný	2010-08-27	\N		/19_100058_FVB-26_2010 Terratel.PDF	2	2010-08-27	10	2010-09-10	t	2010-09-09	10		f
100063	100046	196	5	2010-09-08 00:00:00	1	1		\N	\N		/19_100063_FVB-30_2010 EVOTEC.PDF	2	2010-09-10	10	2010-09-24	t	2010-09-23	10		f
100003	100008	125	5	2010-01-13 00:00:00	1	1	Krieger	2010-01-13	10		/15_100012_100003_FVB-1_2010 LG system.PDF	2	2010-01-14	10	2010-01-27	t	2010-03-25	10	doplatili 640,- 13.09.2010	f
100066	100050	124	5	2010-09-21 00:00:00	1	1		\N	\N		/19_100066_FVB-33_2010 Secheron.PDF	2	2010-09-24	10	2010-10-08	t	2010-10-11	10		f
100061	100047	197	5	2010-09-02 00:00:00	1	1		\N	\N		/19_100061_FVB-29_2010 KPZ.PDF	2	2010-09-03	10	2010-09-29	t	2010-10-06	10		f
100059	100044	252	5	2010-08-31 00:00:00	1	1	Hami	2010-08-20	\N	dali zálohu 2000 hotově, je třeba připravit na to papíry	/19_100059_FVB-28_2010 Atico Moneo.PDF	2	2010-09-01	10	2010-09-22	t	2010-10-01	10		f
100064	100048	124	5	2010-09-10 00:00:00	1	1		\N	\N		/19_100064_FVB-31_2010 Secheron.PDF	2	2010-09-13	10	2010-09-27	t	2010-10-12	10		f
100055	100043	16	5	2010-08-19 00:00:00	1	1	Zítko	2010-08-20	\N		/19_100055_FVZ-39_2010 Disk.PDF	2	2010-08-20	10	2010-09-03	t	2010-10-25	10		f
100069	100051	136	5	2010-10-11 00:00:00	1	1		2010-10-13	\N		/19_100069_FVB-35_2010 ELDIS.PDF	2	2010-10-18	10	2010-11-01	t	2010-11-01	10		f
100070	100049	125	5	2010-10-20 00:00:00	1	1		\N	\N		/19_100070_FVB-36_2010 LG System.PDF	2	2010-10-22	10	2010-11-05	t	2010-11-19	10		f
100071	100054	197	5	2010-10-22 00:00:00	1	1		\N	\N		/19_100071_FVB-37_2010 KPZ.PDF	2	2010-10-22	10	2010-11-05	t	2010-11-18	10		f
100062	100040	97	5	2010-09-06 00:00:00	1	2		\N	\N		/19_100062_FVZ-40_2010 CC Systems.PDF	2	2010-09-06	10	2010-09-20	f	\N	\N		f
100074	100057	197	5	2010-11-10 00:00:00	1	1		\N	\N		/19_100074_FVB-39_2010 KPZ.PDF	2	2010-11-12	10	2010-11-26	t	2010-11-26	10		f
100079	100063	10	5	2010-11-30 00:00:00	1	1		\N	\N		/19_100079_FVZ-48_2010 Casablanca.PDF	2	2010-12-01	10	2010-12-15	t	2010-12-17	10		f
100075	100059	197	5	2010-11-19 00:00:00	1	1		\N	\N		/19_100075_FVB-41_2010 KPZ.PDF	2	2010-11-22	10	2010-12-06	t	2010-12-27	10		f
100076	100058	125	5	2010-11-22 00:00:00	1	1		\N	\N		/19_100076_FVB-42_2010 LG SYSTEM.PDF	2	2010-11-22	10	2010-12-10	t	2010-12-27	10		f
100072	100055	128	5	2010-11-03 00:00:00	1	1		\N	\N		/19_100072_FVB-40_2010 Beulentechnik.PDF	2	2011-01-12	10	2010-11-26	t	2011-01-07	10		f
100082	100068	124	5	2010-12-10 00:00:00	1	1		\N	\N		/19_100082_FVB-45_2010 Secheron.PDF	2	2010-12-16	10	2010-12-30	t	2011-01-13	10		f
100084	100069	127	5	2010-12-20 00:00:00	1	1		\N	\N		/19_100084_FVB-46_2010 MAT.PDF	2	2010-12-21	10	2011-01-04	t	2011-01-04	10		f
100077	100062	16	5	2010-11-24 00:00:00	1	1		\N	\N		/19_100077_FVZ-47_2010 Disk.PDF	2	2010-11-26	10	2010-12-10	t	2011-01-13	10		f
100081	100066	16	5	2010-12-02 00:00:00	8	1		\N	\N		/19_100081_FVZ-49_2010 Disk.PDF	2	2010-12-07	10	2010-12-21	t	2011-01-13	10		f
100068	100053	255	5	2010-10-07 00:00:00	1	2	BO0435300474M	\N	\N		/19_100068_FVZ-44_2010 Metronet.PDF	4	2010-10-15	10	2010-10-29	t	2010-10-20	10		f
100085	100072	197	5	2010-12-31 00:00:00	1	1		2011-01-11	\N		/19_100086_FVB-50_2010 KPZ.PDF	0	2010-12-31	10	2010-01-20	t	2011-01-14	10		f
100080	100060	197	5	2010-11-30 00:00:00	1	1		\N	\N		/19_100080_FVB-44_2010 KPZ.PDF	5	2010-12-01	10	2010-12-15	t	2011-01-18	1		f
100023	0	47	5	2010-03-24 00:00:00	8	1		\N	\N	Zaplaceno hotově		5	\N	0	\N	f	\N	\N	uzavřeno	f
100052	100041	50	5	2010-07-27 00:00:00	8	2	BO0435300514M	\N	\N		/19_100052_FVZ-37_2010 NWT.PDF	2	2010-07-27	10	2010-08-10	t	2010-08-12	10	chybný VS 102831 - má být 102837	f
100067	\N	255	2	2010-10-07 00:00:00	8	2	BO0435300474M	\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100060	100044	252	7	2010-08-31 00:00:00	1	1		2010-08-25	\N			0	\N	0	\N	f	\N	\N		t
100065	100038	121	4	2010-09-14 00:00:00	1	1	Procházka	2010-09-15	\N		/19_100065_FVB-32_2010 Dynex.PDF	2	2010-09-14	10	2010-12-13	t	\N	10	16.9.2010 Factoring 338143,60	f
100022	100005	16	5	2010-03-03 00:00:00	1	0		\N	\N		/15_100003_100022_FVZ-7_2010 Disk.PDF	2	2010-03-03	1	2010-03-17	t	2010-05-17	10		f
110004	110003	44	5	2011-01-17 00:00:00	8	2	BO0435300324M	\N	\N		/19_110004_FVZ-4_2011 Oxid.PDF	2	2011-01-17	10	2010-01-31	t	2011-01-20	10	dobírka	f
110008	110004	263	5	2011-01-24 00:00:00	1	1		\N	\N	platit hotově	/19_110008_FVB-5_2011 Datacon.PDF	5	2011-01-24	10	2011-02-07	t	2011-01-25	10	podepsal Sedláček, hotovost převzal 28.1. Dufek 	f
100083	100063	10	5	2010-12-15 00:00:00	1	1		\N	\N		/19_100083_FVZ-2_2011 Casablanca.PDF	2	2011-01-14	10	2010-01-28	t	2011-01-31	10		f
110006	0	16	5	2011-01-20 00:00:00	8	1		\N	\N	Viz neopráněná rklamace  RMA100102	/19_110006_FVZ-5_2011 Disk.PDF	2	2011-01-24	10	2011-02-07	t	2011-02-04	10		f
110010	110001	125	5	2011-02-10 00:00:00	1	1		\N	\N		/19_110010_FVB-8_2011 LG System.PDF	2	2011-02-18	10	2011-03-04	f	\N	\N		f
100050	100035	1	3	2010-07-20 00:00:00	10000	0		\N	\N		\N	\N	\N	\N	\N	f	\N	\N	\N	f
100049	100036	131	2	2010-06-29 00:00:00	1	1		\N	\N		/19_100049_FVB-22_2010 EPV.PDF	2	2010-07-07	10	2010-07-21	f	\N	\N		f
100073	100056	151	2	2010-11-05 00:00:00	1	1	Zicha	\N	\N		/19_100073_FVB-38_2010 IKT Adv..PDF	2	2010-11-10	10	2010-11-24	f	\N	\N		f
100078	100061	121	2	2010-11-29 00:00:00	1	1		\N	\N		/19_100078_FVB-43_2010 Dynex.PDF	2	2010-11-30	10	2011-02-28	f	\N	\N		f
110001	100067	51	2	2011-01-06 00:00:00	8	1		\N	\N		/19_110001_FVZ-1_2011 VanCo.PDF	2	2011-01-07	10	2011-01-21	f	\N	\N		f
110002	100064	121	2	2011-01-01 00:00:00	1	1		\N	\N		/19_110002_FVB-1_2011 Dynex.PDF	2	2011-01-18	10	2011-04-18	f	\N	\N		f
110007	110002	197	2	2011-01-21 00:00:00	1	1		\N	\N		/19_110007_FVB-4_2011 KPZ.PDF	2	2011-01-24	10	2011-02-07	f	\N	\N		f
110005	100071	121	4	2011-01-18 00:00:00	1	1		\N	\N	Faktoring	/19_110005_FVB-3_2011 Dynex.PDF	2	2011-01-21	10	2011-04-21	t	2011-01-27	10	27.1.Factoring 12216,60	f
313	\N	110	5	2008-02-04 12:21:55.269618	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
314	\N	52	5	2008-02-08 11:46:45.85611	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
10	\N	15	5	2006-08-31 14:39:34.957084	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
11	\N	43	5	2006-08-31 14:40:23.463988	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
9	\N	16	5	2006-08-31 14:39:19.91731	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
12	\N	11	5	2006-08-30 14:41:10.263473	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
2	\N	47	5	2006-08-22 14:37:41.874231	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
3	\N	47	5	2006-08-22 14:37:46.26459	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
7	\N	16	5	2006-08-28 14:38:53.895437	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
8	\N	16	5	2006-08-28 14:39:16.663465	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
4	\N	10	5	2006-08-29 14:38:04.017707	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
5	\N	10	5	2006-08-29 14:38:21.531984	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
6	\N	10	5	2006-08-29 14:38:24.606656	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
13	\N	10	5	2006-08-25 14:50:58.690073	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
14	\N	53	5	2006-09-11 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
15	\N	41	5	2006-09-29 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
16	\N	41	5	2006-09-22 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
17	\N	15	5	2006-10-05 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
18	\N	16	5	2006-09-21 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
19	\N	54	5	2006-09-27 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
20	\N	53	5	2006-10-05 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
21	\N	56	5	2006-10-05 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
22	\N	16	5	2006-09-05 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
23	\N	16	5	2006-09-01 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
24	\N	56	5	2006-09-01 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
25	\N	55	5	2006-10-02 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
26	\N	51	5	2006-09-07 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
27	\N	50	5	2006-09-07 00:00:00	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
28	\N	25	5	2006-11-10 16:01:02.586241	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
29	\N	49	5	2006-11-10 16:03:16.210402	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
30	\N	15	5	2006-11-10 16:06:48.063822	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
31	\N	41	5	2006-11-10 16:13:08.294833	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
32	\N	47	5	2006-11-15 12:57:00.878869	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
33	\N	65	5	2006-11-16 11:25:19.343051	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
34	\N	38	5	2006-11-20 15:24:52.233989	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
35	\N	38	5	2006-11-21 09:09:46.177567	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
36	\N	42	5	2006-11-22 11:11:03.369453	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
37	\N	42	5	2006-11-22 11:13:32.10471	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
38	\N	16	5	2006-11-22 11:20:56.728694	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
39	\N	66	5	2006-11-22 11:54:26.554638	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
40	\N	68	5	2006-11-23 13:41:04.783337	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
41	\N	45	5	2006-11-27 12:56:40.861303	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
42	\N	45	5	2006-11-27 12:57:57.845745	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
43	\N	52	5	2006-11-28 09:23:12.346816	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
44	\N	17	5	2006-11-28 09:33:29.489625	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
45	\N	11	5	2006-11-28 10:03:19.302641	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
46	\N	10	5	2006-11-28 11:25:12.975707	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
48	\N	42	5	2006-11-28 15:04:47.585354	10000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
90058	\N	150	5	2009-06-03 10:31:11.858129	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f
100029	100015	128	5	2010-04-28 00:00:00	1	1		\N	\N		/19_100029_FVB-12_2010 Beulentechnik.PDF	2	2010-04-28	10	2010-06-28	t	2010-06-28	10		f
110003	100044	252	5	2011-01-14 00:00:00	1	1		\N	\N	poslat emilem + originál poštou	/19_110003_FVB-2_2011 Atico moneo.PDF	2	2011-01-14	10	2011-01-28	t	2011-02-11	10		f
110009	110005	264	5	2011-02-04 00:00:00	1	1		\N	\N		/19_110009_FVB-6_2011 Klarus.PDF	2	2011-02-04	10	2011-02-18	t	2011-02-18	10		f
110011	110005	264	5	2011-02-11 00:00:00	1	1		\N	\N		/19_110011_FVB-7_2011 Klarus.PDF	2	2011-02-11	10	2011-02-25	t	2011-02-21	10		f
110013	110006	263	5	2011-02-17 00:00:00	1	1		\N	\N		/19_110013_FVB-9_2011 Datacon.PDF	2	2011-02-22	10	2011-03-08	t	2011-02-22	10		f
110012	100070	128	2	2011-02-11 00:00:00	1	1		\N	\N		/19_110012_FVB-10_2011 Beulentechnik.PDF	2	2011-02-22	10	2011-04-22	f	\N	\N		f
110014	110005	264	2	2011-02-22 00:00:00	1	1		\N	\N			1	2018-06-04	99999	2018-06-04	f	\N	\N		f
\.


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY items (id, groupid, name) FROM stdin;
1	1	Výroba spoje
2	1	Náhradní díly
1	2	Nájem
3	2	prodej - zálohová faktura
4	2	prodej - dobírka
5	2	prodej - hotovost
6	2	zápůjčka - 30 dní
7	2	zápůjčka - dlouhodobě
8	2	zápůjčka - paralelní spoj
2	2	prodej - faktura
1	3	Kč
2	3	EU
1	4	žádost o rezervaci
2	4	rezervováno
3	4	půjčeno
4	4	vráceno
5	4	zkontrolováno KO
6	4	zkontrolováno OK
7	4	zamítnuto
1	5	\N
2	5	prázdná
3	5	1/4
4	5	1/2
1	6	\N
5	5	3/4
6	5	plná
2	6	silně znečištěná
3	6	znečištěná
4	6	trochu znečištěná
5	6	čistá
2	7	služební
1	7	soukromý
3	3	USD
1	8	Ks
2	9	Vyskladnění
3	9	Zasláno do výroby
4	9	Vráceno z výroby
6	9	Korekce při inventuře
7	9	Vyřazeno z evidence
1	10	Na skladě
2	10	Ve výrobě
3	10	Vyřazen
1	9	Naskladnění\n
1	11	Ok
2	11	Problém
1	12	Přijata
3	12	Odeslána
2	12	Vyřešena
1	13	Ústní
2	13	Telefonická
3	13	Osobní
4	13	emailem
1	14	Ne
2	14	Částečně
3	14	Ano
1	15	Vytvořeno
2	15	Schváleno
3	15	Ve výrobě
4	15	Vyrobeno
5	15	Expedováno
6	15	Fakturováno
7	15	Zaplaceno
8	15	Zrušeno
9	15	Uzavřeno
10	15	Neznámý stav
14	15	Částečne vyrobeno
15	15	Částečne expedováno
16	15	Částečně fakturováno
17	15	Částečně zaplaceno
1	17	Kus
2	17	Kilometr
3	17	Minuta
4	17	Úkon
2	8	km
3	8	min
4	8	x
4	3	
1	18	Kamenné CRM
2	18	WEB
3	21	3 - střední
4	21	4 - vyšší
2	21	2 - nizká
1	21	1 - až bude čas
1	20	Kritická chyba
2	20	Chyba
3	20	Úprava
4	20	Nápad
5	21	5 - nejvyšší
1	22	Zadáno
2	22	Řešeno
3	22	Opraveno
100	19	WEB 1.0.0
102	19	WEB 1.0.2
103	19	WEB 1.0.3
101	19	WEB 1.0.1
2	19	CRM 15
3	19	CRM 16
4	19	CRM 17
1	19	CRM 14
1	23	Všechny počítače
2	23	Hanzovo PC
3	23	Ládíkovo PC
4	23	Sklad
5	23	Ital
6	23	VHDL
7	23	Stroj šéfa
8	23	Tomášovo PC
1	24	Spokojen
2	24	Nespokojen
3	24	Zrušeno
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY locations (id, locationname) FROM stdin;
1	Zličín - výroba
2	Zličín - vývoj
3	Mrázovka
4	Na cestě - směr Mrázovka
5	Na cestě - směr Zličín výroba
6	Na cestě - směr Zličín vývoj
7	Běchovice
8	Na cestě - směr Běchovice
0	Neznámá
\.


--
-- Data for Name: productcategory; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY productcategory (id, name) FROM stdin;
1	Anténa 35
2	Anténa 65
3	Anténa 120
4	Ostatní materiál
5	Služby
\.


--
-- Data for Name: productorderitem; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY productorderitem (id, name, quantity, productorderid, price, currency, productid) FROM stdin;
1	Oprava a naladění spoje (parohy)	1	100001	4800	1	\N
1	oprava, naladění spoje, výměna antén (dodal vlastní)	1	100007	2890	1	\N
2	oprava, naladění spoje, výměna antén (dodal vlastní)	1	100007	2440	1	\N
1	Anténa 35 cm	2	100005	7440	1	\N
1	anténa 35 cm	1	100003	7440	1	\N
1	oprava ladičky mikrovlnných vysílačů	1	100004	10000	1	\N
1	RMA100001 oprava radiové desky po připojení na 230V	1	100002	2400	1	\N
1	Repasovaná mikrovlnná jednotka	1	100016	20000	1	\N
2	propojovací drát 5.8311.509.08	1000	100019	2	1	\N
2	kompletní anténa 35 cm levá	1	100020	7440	1	\N
3	poštovné a balné	1	100020	796	1	\N
4	stínící přípravek	6	100020	0	1	\N
2	PWS-73-N	300	100021	0	1	\N
1	magnetický senzor A	20	100023	0	1	\N
2	magnetický senzor B	20	100023	0	1	\N
3	magnetický senzor C	20	100023	0	1	\N
2	anténa 65 cm levá	1	100024	9659	1	\N
1	útlumový článek	3	100025	280	1	\N
2	ehternetová karta	1	100025	10000	1	\N
2	stínící krabička	2	100026	300	1	\N
2	transormátor 230/21 V	1	100027	793	1	\N
1	periferie Dynablot 7 pump	12	100029	0	1	\N
1	kompletní anténa 35	2	100030	0	1	\N
1	osazení vzorku display z dodaných součástek	1	100028	0	1	\N
2	osazení vzorku uniprog z dodaných součástek	1	100028	0	1	\N
1	LED pásek	150	100031	54	1	\N
2	jendorázové náklady šablona, filmy, frézování, program, návrh	1	100031	8200	1	\N
3	příprava výroby	1	100031	1200	1	\N
2	5.8311.509.01	2000	100033	1	1	\N
3	5.8311.509.03	1000	100033	1	1	\N
2	pol1	3	100035	222	1	\N
3	pol2	1	100035	44	1	\N
4	pol3	2	100035	0	1	\N
1	polozka1	1	100034	11	1	\N
2	pokus	2	100034	22	1	\N
3	gogo	3	100034	0	1	\N
2	Jukebox včetně desek a součástek	60	100036	325	1	\N
2	1x radiová deska	0	100040	0	1	\N
2	anténa průměr 35 cm	1	100041	0	1	\N
2	radom 35 cm	2	100043	1507	1	\N
1	slučovač 24V - 1 den	5	100044	0	1	\N
2	slučovač 24V - 2 dny	5	100044	0	1	\N
1	ATP 101 EBOR	130	100047	0	1	\N
2	ATP 101 LBR	504	100047	0	1	\N
3	GIV	250	100047	0	1	\N
1	AT12-10	400	100046	0	1	\N
1	ruční osazení deska 1	0	100045	0	1	\N
2	ruční osazení deska 2	0	100045	0	1	\N
1	Osazení 84 ks desek ZBI	0	100042	0	1	\N
1	propojovací drát 5.8311.509/02	1000	100048	1	1	\N
2	propojovací drát 5.8311.509/04	1000	100048	2	1	\N
3	propojovací drát 5.8311.509/05	1000	100048	2	1	\N
4	propojovací drát 5.8311.510/01	1000	100048	3	1	\N
1	Kompletní osazování SGB01	198	100049	0	1	\N
2	Pajení desek plošných spojů SVB01 (vlna)	80	100049	0	1	\N
3	Pajení desek plošných spojů LGREG01 (vlna)	180	100049	0	1	\N
1	propojovací drát 5.8311.509/06	1000	100050	2	1	\N
2	propojovací drát 5.8311.509/08	1000	100050	2	1	\N
2	Osazení PS ED0372A	54	100051	0	1	\N
2	Radiová deska, přeměření spoje	1	100053	5000	1	\N
1	součástky na dynablot cena včetně DPH	100	100052	1140	1	\N
2	ATP-101LBT	252	100054	37	1	\N
3	Osazovací program	1	100054	3200	1	\N
2	laser_rx_05	4	100056	0	1	\N
3	laser_tx_05	8	100056	0	1	\N
1	4100-009	60	100055	0	1	\N
1	PWS-73-N	0	100057	0	1	\N
1	ZRM01-3	125	100058	0	1	\N
1	osazení GIV2/2	150	100059	39	1	\N
2	úprava programu  GIV2/2	1	100059	1200	1	\N
3	připrava výroby  GIV2/2	1	100059	1600	1	\N
2	expresní výroba DB periferie 7 pump	36	100061	0	1	\N
1	PWRS-3	96	100060	28	1	\N
2	příprava výroby	1	100060	1200	1	\N
2	kompletní mechanika 35 levá	1	100062	7440	1	\N
1	set Dynablot 4.1	60	100064	5591	1	\N
1	transformátor 240/21V prodej	1	100066	793.200000000000045	1	\N
2	datová karta 3UY0GW - zápůjčka 30 dní http://10.4.1.250/borowlistdetail.php?id=100011	0	100066	0	1	\N
2	zápůjčka datová deska 	2	100067	0	1	\N
3	zápůjčka radiová deska 	2	100067	0	1	\N
4	zápůjčka mikrovlna	2	100067	0	1	\N
5	zápůjčka transformátor	2	100067	0	1	\N
1	propojka 5.8311.509/7	1800	100068	2	1	\N
1	GPRS COM	20	100069	0	1	\N
1	Dynawash - úprava desky, výroba kabeláže	20	100071	972	1	\N
2	Dyna vývěva - výroba kabeláže	20	100071	525	1	\N
1	RVA - 102E	200	100072	35	1	\N
2	připrava výroby	1	100072	1600	1	\N
1	BT 4100-004	200	100070	192	1	\N
2	Transformátor 230/21V	3	110003	793	1	\N
1	stínící přípravek	3	100063	600	1	\N
2	stínící přípravek	3	100063	600	1	\N
1	ATP-101LBT	192	110002	37	1	\N
2	příprava výroby	1	110002	1600	1	\N
1	osazení ADuC 847 / BCPZ na desku DCPS 305	4	110004	0	1	\N
2	osazení ADuC 847 na desku DCPS 303	6	110004	0	1	\N
3	osazení ADuC 845 na desku DCPS 303	4	110004	0	1	\N
1	ROBOFOOD	10000	110005	0.0840000000000000052	2	\N
2	PC100	1000	110005	0.770000000000000018	2	\N
1	prototyp smd 6x6 cm	30	110001	0	1	\N
2	montáž IO (10 kusů) na desky DCPS306	3	110006	700	1	\N
1	set Dynablot 4.1	50	100065	5591	1	\N
5	EVI 3	51	110007	35	1	\N
6	příprava výroby (400+600)	1	110007	1000	1	\N
\.


--
-- Data for Name: productorders; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY productorders (id, orderstatus, companyid, companyordernumber, ordertype, recievedate, recieverid, orderfile, approved, approveddate, approveduserid, approvednote, paidnote, productiondeadline, productionstatus, closed, expeditionuserid, expeditiondate, expeditionreciever, expeditiondate2, expeditionuserid2, invoiceuserid, invoiceid, invoiceprice, invoicetype, invoicedate, paid, paiddate, paiduserid, paymentdate, deleted) FROM stdin;
100040	2	97	100721	4	2010-07-21	1		t	2010-07-21	1	zápůjčka 30 dní	\N	2010-07-21	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100022	1	121		4	2010-04-29	1		t	2010-04-29	1		\N	2010-04-29	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
100010	5	214	700100008	4	2010-01-11	1	productorders/10_100011_ov7000100008.pdf	t	2010-01-11	10	micro.cz, záloha 10000Kč		\N	\N	f	1	2010-03-02	PB0435303609M	2010-03-02	1	1	productorders/26_100011_FVB-5_2010 Autogard.PDF	\N	2	2010-03-23	2	2010-01-29	\N	2010-03-17	f
100038	4	121	2010-0040	4	2010-07-21	1	/13_100038_Doklad VYR-2010-00040.pdf	t	2010-07-21	1		\N	2010-09-10	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100044	4	252		2	2010-08-25	1		t	2010-08-25	1		\N	2010-08-25	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100001	10	215	osobní	\N	2010-01-13	1		t	2010-01-13	1	osobní		\N	\N	t	1	2010-01-13	Baránek Jan	2010-01-13	1	1	productorders/25_100007_FVZ-2_2010 BKS.PDF	\N	2	2010-01-13	3	2010-02-02	\N	\N	f
100026	10	134	100511	4	2010-05-12	1		t	2010-05-12	1	vyzvedne osobne ve stredu 12.5.	\N	2010-05-12	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100017	10	127	667	4	2010-04-09	1	/11_100017_Obj667svm.doc	t	2010-04-09	99999		\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100031	10	242	100524	4	2010-05-25	1		t	2010-05-25	1		\N	2010-06-09	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100034	10	1		3	2010-06-22	99999		t	2010-06-22	99999	test	\N	2010-06-22	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100023	10	121	280410	4	2010-04-29	1		t	2010-04-29	1		\N	2010-04-29	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100033	10	124	12021751-10	4	2010-06-11	1	/13_100033_20100608090354467.pdf	t	2010-06-11	1		\N	2010-06-11	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100029	10	121	VYR-2010-00028	4	2010-05-19	1	/13_100029_VYR-2010-00028 SVM.rtf	t	2010-05-19	1		\N	2010-05-19	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100028	10	212	100513	3	2010-05-18	1		t	2010-05-18	1		\N	2010-05-18	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100021	10	197	OV10-115/2010	4	2010-04-29	1	/13_100021_Formulář objednávky vydané-20100429-121305.PDF	t	2010-04-29	1		\N	2010-04-29	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100019	10	124	12021751-9	4	2010-04-22	1	/11_100019_20100421145304319.pdf	t	2010-04-22	1		\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100015	10	128	182-10	4	2010-03-29	1	productorders/10_100018_BEULENTECHNIK - objednávka 182-10.pdf	t	2010-03-29	1	vyrobit do 16 týdne		2010-04-23	\N	t	\N	\N		\N	\N	\N		\N	0	\N	0	\N	\N	\N	f
100013	10	197	OV10-46/2010	4	2010-02-11	1	productorders/10_100016_Formulář objednávky vydané-20100211-152615.PDF	t	2010-02-11	1			\N	\N	t	1	2010-02-22	osobní	2010-02-22	1	1	productorders/26_100016_FVB-7_2010 KPZ electronics.PDF	\N	2	2010-03-03	0	\N	\N	2010-03-17	f
100012	10	197	OV10-25/2010	4	2010-01-27	1	productorders/10_100014_Formulář objednávky vydané-20100128-140931.PDF	t	2010-01-27	1			\N	\N	t	1	2010-02-02	osobní	2010-02-02	1	1	productorders/26_100014_FVB-2_2010 KPZ electronics.PDF	\N	0	2010-02-02	3	2010-03-04	\N	2010-02-16	f
100011	10	125	2009213	4	2010-01-19	1	productorders/10_100015_OBJ.HTM	t	2010-01-19	1		slíbil, že zaplatí do 29.3.	\N	\N	t	1	2010-02-16	Krieger	2010-02-16	1	1	productorders/26_100015_FVB-6_2010 LG SYSTEM.PDF	\N	2	2010-03-03	0	\N	\N	2010-03-17	f
100009	10	128	106-10	4	2010-01-05	1	productorders/10_100013_BEULENTECHHNIK - objednávka 106-10.pdf	t	2010-01-06	1	dodací listy 16,7,6!!!		\N	\N	t	1	2010-02-11	Tesař	2010-02-11	1	1	productorders/26_100013_FVB-3_2010 Beulentechnik.PDF	\N	2	2010-02-11	3	2010-02-25	\N	2010-03-13	f
100030	10	73	100521	4	2010-05-24	10		t	2010-05-25	1	DOBÍRKA	\N	2010-05-24	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100027	10	97	100518	2	2010-05-18	1		t	2010-05-18	1		\N	2010-05-18	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100024	10	50	07052010	4	2010-05-10	1		t	2010-05-10	1		\N	2010-05-10	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100032	10	127	obj674	4	2010-06-09	1	/13_100032_Obj674svm.doc	t	2010-06-09	1		\N	2010-06-09	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100042	10	127	680	4	2010-08-16	1	/13_100042_Obj680svm.pdf	t	2010-08-16	1		\N	2010-08-16	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100045	10	253	007/2010	4	2010-08-25	1	/13_100045_OBJEDNAVKA_OS_DPS_110810.pdf	t	2010-08-25	1		\N	2010-08-25	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100039	10	197	OV10-211/2010	4	2010-07-21	1	/13_100039_Formulář objednávky vydané-20100720-153621.PDF	t	2010-07-21	1		\N	2010-07-21	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100025	10	114	100510	4	2010-05-10	1		t	2010-05-10	1		\N	2010-05-10	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100020	10	42	280410	4	2010-04-29	1		t	2010-04-29	1		\N	2010-04-29	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100016	10	50	RMA 10-0014	\N	2010-03-29	1		t	2010-03-29	10	telefonická objednávka, viz RMA10-0014		\N	\N	t	10	2010-03-29	BO0435300182M	2010-03-29	10	10	productorders/26_100019_FVZ-8_2010 NWT.PDF	\N	2	2010-03-29	0	\N	\N	2010-04-12	f
100007	10	29		1	2010-03-19	1		t	2010-03-19	1			\N	\N	t	1	2010-03-19	Kroupa	2010-03-19	1	1	productorders/25_100005_FVZ-6_2010 GREPA Networks.PDF	\N	2	2010-03-03	0	\N	1	2010-03-17	f
100006	10	222	47-2010	4	2010-03-19	1	productorders/9_100004_Objednávka vydaná - OVOT-47-2010.PDF	t	2010-03-19	1			\N	\N	t	1	2010-03-19	poštou	2010-03-19	1	1	productorders/25_100004_FVZ-5_2010 ELPREMO.PDF	\N	0	2010-02-22	3	2010-03-11	\N	2010-03-08	f
100004	10	32	9.2.2010	4	2010-02-09	1		t	2010-03-19	1			\N	\N	t	1	2010-03-19		2010-02-10	\N	1	productorders/25_100002_FVZ-4_2010 ha-vel.PDF	\N	2	2010-02-22	0	\N	\N	2010-03-08	f
100003	10	219	telefonická	2	2010-02-01	1		t	2010-02-01	1			\N	\N	t	1	2010-02-01		2010-02-01	\N	1	productorders/25_100006_FVZ-3_2010 Brich Jan.PDF	\N	5	2010-02-01	3	2010-02-01	1	2010-02-01	f
100002	10	16	telefonická	\N	2010-01-13	1		t	2010-01-13	1	telefonická		\N	\N	t	1	2010-01-13		\N	\N	1	productorders/25_100008_FVZ-1_2010 Disk.PDF	\N	0	2010-01-13	3	2010-03-04	\N	2010-01-27	f
100046	10	196	AT10-12	4	2010-08-25	1	/13_100046_Evotec-AT10-12.doc	t	2010-08-25	1		\N	2010-08-25	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100047	10	197	OV10-240/2010	0	2010-08-30	1	/13_100047_Formulář objednávky vydané-20100827-142156.PDF	t	2010-08-30	1		\N	2010-08-30	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100048	10	124	12021751-11	4	2010-09-02	1	/13_100048_20100901122057594-1.pdf	t	2010-09-02	1		\N	2010-09-10	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100050	10	124	12021751-12	2	2010-09-17	1	/13_100050_totonenicisloobjednavky.pdf	t	2010-09-17	1		\N	2010-09-30	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100051	10	136	201010135	4	2010-09-22	1	/13_100051_201010135.pdf	t	2010-09-22	1		\N	2010-09-22	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100043	10	16	100819	4	2010-08-19	1		t	2010-08-19	1		\N	2010-08-19	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100049	10	125	1020142	4	2010-09-09	1	/13_100049_1020142.pdf	t	2010-09-09	1		\N	2010-10-08	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100054	10	197	OV10-309/2010	4	2010-10-15	1	/13_100054_OV10-309-2010.pdf	t	2010-10-15	1		\N	2010-10-15	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100057	10	197	OV10-325/2010	4	2010-11-03	1		t	2010-11-10	1		\N	2010-11-03	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100058	10	125	1020183	4	2010-11-04	1	/13_100058_10200183.pdf	t	2010-11-12	1	osadit v terminu 15.-22.11.	\N	2010-11-04	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100059	10	197	OV10/348/2010	4	2010-11-12	1	/13_100059_KPZ-objednavkaOV10-348-2010_0001-1.pdf	t	2010-11-12	1		\N	2010-11-12	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100008	10	125	2009162	4	2009-11-10	1	productorders/10_100012_OBJ.HTM	t	2009-11-10	1	dokončeno 2010, fakturováno 2010	slíbil, že zaplatí do 29.3.	\N	\N	t	1	2010-01-13	Krieger	2010-01-13	1	1	productorders/26_100012_FVB-1_2010 LG system.PDF	\N	2	2010-01-14	0	\N	\N	2010-01-27	f
100055	10	128	337-10	4	2010-11-03	1	/13_100055_BEULENTECHNIK - objednavka 337-10.pdf	t	2010-11-03	1		\N	2010-11-03	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100068	10	124	12021751-13	4	2010-12-10	1		t	2010-12-10	1		\N	2010-12-10	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100069	10	127	695	4	2010-12-15	1		t	2010-12-15	1		\N	2010-12-17	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100072	10	197	OV392/2010	4	2010-12-20	1	/13_100072_KPZ-OV10-392-2010.pdf	t	2011-01-11	1		\N	2011-01-10	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100060	10	197	OV-10/361/2010	4	2010-11-22	1	/13_100060_Formulář objednávky vydané-1.pdf	t	2010-11-23	1		\N	2010-11-22	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100036	10	131	100526	4	2010-07-01	1		t	2010-07-01	1		\N	2010-07-01	1	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100066	10	16	101202	1	2010-12-02	1		t	2010-12-06	1		\N	2010-12-06	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100062	10	16	716	4	2010-11-24	1		t	2010-11-24	1		\N	2010-11-24	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100053	10	255	101007	4	2010-10-07	1		t	2010-10-07	1		\N	2010-10-07	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100041	10	50	100726	4	2010-07-27	1		t	2010-07-27	1		\N	2010-07-27	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110004	10	263	AL 2332	2	2011-01-19	1		t	2011-01-19	1		\N	2011-01-19	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110006	10	263	AL2337	3	2011-02-17	1		t	2011-02-17	1		\N	2011-02-17	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100005	10	16	2.3.2010	4	2010-03-02	1		t	2010-03-02	1			\N	\N	t	1	2010-03-03		\N	\N	1	productorders/25_100003_FVZ-7_2010 Disk.PDF	\N	2	2010-03-03	0	\N	\N	2010-03-17	f
100056	2	151	02112010	4	2010-11-03	1		t	2010-11-03	1		\N	2010-11-03	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100061	2	121	22112010	4	2010-11-23	1		t	2010-11-23	1		\N	2010-11-23	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100064	2	121	2010-00143	4	2010-12-01	1	/13_100064_Objednavka VYR-2010-00143 SVM Microwaves.pdf	t	2010-12-01	1		\N	2011-01-15	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100067	2	51	101207-1	4	2010-12-07	1		t	2010-12-07	1		\N	2010-12-07	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100070	2	128	374-10	4	2010-12-15	1		t	2010-12-15	1		\N	2011-01-31	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110002	2	197	OV4/2011	4	2011-01-13	1	/13_110002_Formulář objednávky vydané.jpg	t	2011-01-19	1		\N	2011-01-13	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110001	2	125	1120017	4	2011-01-13	1	/13_110001_1120017.pdf	t	2011-02-10	1	pošlou podklady, chtějí vyrobit ve 3.týdnu 	\N	2011-01-28	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100065	1	121	2010-00134	4	2010-12-01	1	/13_100065_Objednavka VYR-2010-00143 SVM Microwaves.pdf	t	2011-02-23	1		\N	2011-03-15	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110007	1	127	0701	4	2011-02-24	1		t	2011-02-24	1		\N	2011-03-16	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110003	5	44	110117	4	2011-01-18	1		t	2011-01-17	1	zasláno na dobírku	\N	2011-01-18	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100063	5	10	101130-1	3	2010-11-30	1		t	2010-11-30	1		\N	2010-11-30	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100071	4	121	2010-00167	4	2010-12-16	1	/13_100071_VYR-2010-00167 SVM Microwaves.pdf	t	2010-12-16	1		\N	2010-12-16	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
110005	4	264	110131	4	2011-01-31	1	/13_110005_KLARUS objednávka.doc	t	2011-01-31	1		\N	2011-02-04	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100037	11	248	20101407-MB01	4	2010-07-14	1	/13_100037_10Ghz_711_Lom.doc	t	2010-07-14	1	zápůjčka na 30 dní, vráceno, nic nefakturovat	\N	2010-07-14	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100018	11	181	1	4	2010-04-13	1		t	2010-04-13	1	mmm	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t
100000	11	1	abc	\N	2010-02-01	\N	crmdata/data/productorders/_0_telefonni_seznam_SVM.ods	t	2010-01-02	99999	test	test	\N	\N	t	\N	2010-02-03	Mr. Gogo	\N	\N	\N	123	\N	2	2010-02-04	3	2010-02-05	\N	\N	t
110000	11	1	test	\N	2011-01-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100035	11	1		0	2010-06-22	99999		f	\N	\N		\N	2010-06-22	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100052	0	121	2010-00074	3	2010-09-29	1		f	\N	\N	dodat 31.10.2010	\N	2010-09-29	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
100014	10	121	VYR-2009-00253	4	2010-02-05	1	productorders/10_100017_Doklad VYR-2009-00253.pdf	t	2010-02-05	99999	změna Mainboardu - úprava schématu, nové filmy		\N	\N	t	\N	\N		\N	\N	\N		\N	0	\N	0	\N	\N	\N	f
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY products (id, producttypeid, procuctcomment, statusid, barcoderecipientid, created, locationid, ownerid, counter) FROM stdin;
6378	46	\N	5	\N	\N	1	8	\N
6379	49	\N	5	\N	\N	1	8	\N
6380	61	\N	5	\N	\N	1	1	\N
6381	71	\N	5	\N	\N	1	1	\N
6382	87	\N	5	\N	\N	1	1	\N
6383	84	\N	5	\N	\N	1	1	\N
6384	99	\N	5	\N	\N	1	1	\N
6369	2	\N	21	8	\N	1	1	\N
6329	2	\N	18	10	\N	\N	\N	\N
6330	2	\N	18	10	\N	\N	\N	\N
6361	3	\N	21	8	\N	1	8	\N
6366	2	\N	21	8	\N	1	8	\N
6357	3	\N	21	8	\N	1	8	\N
6367	2	\N	21	8	\N	1	8	\N
36	14	\N	18	\N	\N	\N	\N	\N
1	14	\N	5	\N	\N	\N	\N	\N
94	2	\N	5	\N	\N	\N	\N	\N
2	14	\N	5	\N	\N	\N	\N	\N
3	14	\N	5	\N	\N	\N	\N	\N
4	14	\N	5	\N	\N	\N	\N	\N
6	14	\N	5	\N	\N	\N	\N	\N
7	14	\N	5	\N	\N	\N	\N	\N
9	14	\N	5	\N	\N	\N	\N	\N
10	14	\N	5	\N	\N	\N	\N	\N
11	14	\N	5	\N	\N	\N	\N	\N
12	14	\N	5	\N	\N	\N	\N	\N
13	14	\N	5	\N	\N	\N	\N	\N
14	14	\N	5	\N	\N	\N	\N	\N
15	14	\N	5	\N	\N	\N	\N	\N
17	14	\N	5	\N	\N	\N	\N	\N
19	14	\N	5	\N	\N	\N	\N	\N
20	14	\N	5	\N	\N	\N	\N	\N
21	14	\N	5	\N	\N	\N	\N	\N
22	14	\N	5	\N	\N	\N	\N	\N
23	14	\N	5	\N	\N	\N	\N	\N
24	14	\N	5	\N	\N	\N	\N	\N
25	14	\N	5	\N	\N	\N	\N	\N
26	14	\N	5	\N	\N	\N	\N	\N
27	14	\N	5	\N	\N	\N	\N	\N
28	14	\N	5	\N	\N	\N	\N	\N
29	14	\N	5	\N	\N	\N	\N	\N
30	14	\N	5	\N	\N	\N	\N	\N
31	14	\N	5	\N	\N	\N	\N	\N
32	14	\N	5	\N	\N	\N	\N	\N
33	14	\N	5	\N	\N	\N	\N	\N
34	14	\N	5	\N	\N	\N	\N	\N
35	14	\N	5	\N	\N	\N	\N	\N
37	14	\N	5	\N	\N	\N	\N	\N
38	14	\N	5	\N	\N	\N	\N	\N
39	14	\N	5	\N	\N	\N	\N	\N
40	14	\N	5	\N	\N	\N	\N	\N
41	14	\N	5	\N	\N	\N	\N	\N
42	14	\N	5	\N	\N	\N	\N	\N
43	14	\N	5	\N	\N	\N	\N	\N
44	14	\N	5	\N	\N	\N	\N	\N
45	14	\N	5	\N	\N	\N	\N	\N
46	14	\N	5	\N	\N	\N	\N	\N
47	14	\N	5	\N	\N	\N	\N	\N
48	14	\N	5	\N	\N	\N	\N	\N
49	14	\N	5	\N	\N	\N	\N	\N
50	14	\N	5	\N	\N	\N	\N	\N
51	14	\N	5	\N	\N	\N	\N	\N
52	14	\N	5	\N	\N	\N	\N	\N
53	14	\N	5	\N	\N	\N	\N	\N
54	14	\N	5	\N	\N	\N	\N	\N
55	14	\N	5	\N	\N	\N	\N	\N
56	14	\N	5	\N	\N	\N	\N	\N
57	14	\N	5	\N	\N	\N	\N	\N
58	14	\N	5	\N	\N	\N	\N	\N
59	14	\N	5	\N	\N	\N	\N	\N
60	14	\N	5	\N	\N	\N	\N	\N
61	2	\N	5	\N	\N	\N	\N	\N
62	2	\N	5	\N	\N	\N	\N	\N
63	2	\N	5	\N	\N	\N	\N	\N
64	2	\N	5	\N	\N	\N	\N	\N
65	2	\N	5	\N	\N	\N	\N	\N
66	2	\N	5	\N	\N	\N	\N	\N
67	2	\N	5	\N	\N	\N	\N	\N
68	2	\N	5	\N	\N	\N	\N	\N
69	2	\N	5	\N	\N	\N	\N	\N
70	2	\N	5	\N	\N	\N	\N	\N
71	2	\N	5	\N	\N	\N	\N	\N
72	2	\N	5	\N	\N	\N	\N	\N
73	2	\N	5	\N	\N	\N	\N	\N
74	2	\N	5	\N	\N	\N	\N	\N
75	2	\N	5	\N	\N	\N	\N	\N
76	2	\N	5	\N	\N	\N	\N	\N
77	2	\N	5	\N	\N	\N	\N	\N
78	2	\N	5	\N	\N	\N	\N	\N
79	2	\N	5	\N	\N	\N	\N	\N
80	2	\N	5	\N	\N	\N	\N	\N
81	2	\N	5	\N	\N	\N	\N	\N
82	2	\N	5	\N	\N	\N	\N	\N
83	2	\N	5	\N	\N	\N	\N	\N
84	2	\N	5	\N	\N	\N	\N	\N
85	2	\N	5	\N	\N	\N	\N	\N
86	2	\N	5	\N	\N	\N	\N	\N
87	2	\N	5	\N	\N	\N	\N	\N
88	2	\N	5	\N	\N	\N	\N	\N
89	2	\N	5	\N	\N	\N	\N	\N
90	2	\N	5	\N	\N	\N	\N	\N
91	2	\N	5	\N	\N	\N	\N	\N
93	2	\N	5	\N	\N	\N	\N	\N
95	2	\N	5	\N	\N	\N	\N	\N
96	2	\N	5	\N	\N	\N	\N	\N
97	2	\N	5	\N	\N	\N	\N	\N
98	2	\N	5	\N	\N	\N	\N	\N
99	2	\N	5	\N	\N	\N	\N	\N
100	2	\N	5	\N	\N	\N	\N	\N
101	2	\N	5	\N	\N	\N	\N	\N
102	2	\N	5	\N	\N	\N	\N	\N
103	2	\N	5	\N	\N	\N	\N	\N
104	2	\N	5	\N	\N	\N	\N	\N
105	2	\N	5	\N	\N	\N	\N	\N
106	2	\N	5	\N	\N	\N	\N	\N
107	2	\N	5	\N	\N	\N	\N	\N
108	2	\N	5	\N	\N	\N	\N	\N
109	2	\N	5	\N	\N	\N	\N	\N
110	2	\N	5	\N	\N	\N	\N	\N
111	2	\N	5	\N	\N	\N	\N	\N
112	2	\N	5	\N	\N	\N	\N	\N
113	2	\N	5	\N	\N	\N	\N	\N
114	2	\N	5	\N	\N	\N	\N	\N
115	2	\N	5	\N	\N	\N	\N	\N
116	2	\N	5	\N	\N	\N	\N	\N
117	2	\N	5	\N	\N	\N	\N	\N
118	2	\N	5	\N	\N	\N	\N	\N
119	2	\N	5	\N	\N	\N	\N	\N
120	2	\N	5	\N	\N	\N	\N	\N
121	2	\N	5	\N	\N	\N	\N	\N
122	2	\N	5	\N	\N	\N	\N	\N
123	2	\N	5	\N	\N	\N	\N	\N
124	2	\N	5	\N	\N	\N	\N	\N
125	2	\N	5	\N	\N	\N	\N	\N
126	2	\N	5	\N	\N	\N	\N	\N
127	2	\N	5	\N	\N	\N	\N	\N
128	2	\N	5	\N	\N	\N	\N	\N
129	2	\N	5	\N	\N	\N	\N	\N
132	2	\N	5	\N	\N	\N	\N	\N
133	2	\N	5	\N	\N	\N	\N	\N
131	2	\N	5	\N	\N	1	10	\N
135	2	\N	5	\N	\N	\N	\N	\N
167	13	\N	6	6	\N	\N	\N	\N
168	13	\N	6	6	\N	\N	\N	\N
169	13	\N	6	6	\N	\N	\N	\N
170	13	\N	6	6	\N	\N	\N	\N
171	13	\N	6	6	\N	\N	\N	\N
172	13	\N	6	6	\N	\N	\N	\N
173	13	\N	6	6	\N	\N	\N	\N
174	13	\N	6	6	\N	\N	\N	\N
175	13	\N	6	6	\N	\N	\N	\N
176	13	\N	6	6	\N	\N	\N	\N
177	13	\N	6	6	\N	\N	\N	\N
178	13	\N	6	6	\N	\N	\N	\N
179	13	\N	6	6	\N	\N	\N	\N
180	13	\N	6	6	\N	\N	\N	\N
181	13	\N	6	6	\N	\N	\N	\N
182	13	\N	6	6	\N	\N	\N	\N
183	13	\N	6	6	\N	\N	\N	\N
184	13	\N	6	6	\N	\N	\N	\N
185	13	\N	6	6	\N	\N	\N	\N
186	13	\N	6	6	\N	\N	\N	\N
187	13	\N	6	6	\N	\N	\N	\N
188	13	\N	6	6	\N	\N	\N	\N
189	13	\N	6	6	\N	\N	\N	\N
190	13	\N	6	6	\N	\N	\N	\N
191	13	\N	6	6	\N	\N	\N	\N
192	13	\N	6	6	\N	\N	\N	\N
193	13	\N	6	6	\N	\N	\N	\N
194	13	\N	6	6	\N	\N	\N	\N
195	13	\N	6	6	\N	\N	\N	\N
196	13	\N	6	6	\N	\N	\N	\N
197	13	\N	6	6	\N	\N	\N	\N
198	13	\N	6	6	\N	\N	\N	\N
199	13	\N	6	6	\N	\N	\N	\N
200	13	\N	6	6	\N	\N	\N	\N
201	13	\N	6	6	\N	\N	\N	\N
202	13	\N	6	6	\N	\N	\N	\N
203	13	\N	6	6	\N	\N	\N	\N
204	13	\N	6	6	\N	\N	\N	\N
205	13	\N	6	6	\N	\N	\N	\N
206	13	\N	6	6	\N	\N	\N	\N
207	13	\N	6	6	\N	\N	\N	\N
208	13	\N	6	6	\N	\N	\N	\N
209	13	\N	6	6	\N	\N	\N	\N
210	13	\N	6	6	\N	\N	\N	\N
211	13	\N	6	6	\N	\N	\N	\N
212	13	\N	6	6	\N	\N	\N	\N
213	13	\N	6	6	\N	\N	\N	\N
214	13	\N	6	6	\N	\N	\N	\N
215	13	\N	6	6	\N	\N	\N	\N
216	13	\N	6	6	\N	\N	\N	\N
217	13	\N	6	6	\N	\N	\N	\N
218	13	\N	6	6	\N	\N	\N	\N
219	13	\N	6	6	\N	\N	\N	\N
220	13	\N	6	6	\N	\N	\N	\N
221	13	\N	6	6	\N	\N	\N	\N
222	13	\N	6	6	\N	\N	\N	\N
223	13	\N	6	6	\N	\N	\N	\N
224	13	\N	6	6	\N	\N	\N	\N
225	13	\N	6	6	\N	\N	\N	\N
226	13	\N	6	6	\N	\N	\N	\N
227	13	\N	6	6	\N	\N	\N	\N
228	13	\N	6	6	\N	\N	\N	\N
229	13	\N	6	6	\N	\N	\N	\N
230	13	\N	6	6	\N	\N	\N	\N
231	13	\N	6	6	\N	\N	\N	\N
232	13	\N	6	6	\N	\N	\N	\N
233	13	\N	6	6	\N	\N	\N	\N
234	13	\N	6	6	\N	\N	\N	\N
235	13	\N	6	6	\N	\N	\N	\N
236	13	\N	6	6	\N	\N	\N	\N
237	13	\N	6	6	\N	\N	\N	\N
238	13	\N	6	6	\N	\N	\N	\N
239	13	\N	6	6	\N	\N	\N	\N
240	13	\N	6	6	\N	\N	\N	\N
241	13	\N	6	6	\N	\N	\N	\N
242	13	\N	6	6	\N	\N	\N	\N
243	13	\N	6	6	\N	\N	\N	\N
244	13	\N	6	6	\N	\N	\N	\N
245	13	\N	6	6	\N	\N	\N	\N
246	13	\N	6	6	\N	\N	\N	\N
247	13	\N	6	6	\N	\N	\N	\N
248	13	\N	6	6	\N	\N	\N	\N
249	13	\N	6	6	\N	\N	\N	\N
250	13	\N	6	6	\N	\N	\N	\N
251	13	\N	6	6	\N	\N	\N	\N
252	13	\N	6	6	\N	\N	\N	\N
253	13	\N	6	6	\N	\N	\N	\N
254	13	\N	6	6	\N	\N	\N	\N
255	13	\N	6	6	\N	\N	\N	\N
256	13	\N	6	6	\N	\N	\N	\N
257	13	\N	6	6	\N	\N	\N	\N
258	13	\N	6	6	\N	\N	\N	\N
292	14	\N	7	6	\N	1	8	\N
260	13	\N	6	6	\N	\N	\N	\N
261	13	\N	6	6	\N	\N	\N	\N
262	13	\N	6	6	\N	\N	\N	\N
263	13	\N	6	6	\N	\N	\N	\N
264	13	\N	6	6	\N	\N	\N	\N
265	13	\N	6	6	\N	\N	\N	\N
266	13	\N	6	6	\N	\N	\N	\N
268	14	\N	6	6	\N	\N	\N	\N
269	14	\N	6	6	\N	\N	\N	\N
270	14	\N	6	6	\N	\N	\N	\N
271	14	\N	6	6	\N	\N	\N	\N
272	14	\N	6	6	\N	\N	\N	\N
273	14	\N	6	6	\N	\N	\N	\N
274	14	\N	6	6	\N	\N	\N	\N
275	14	\N	6	6	\N	\N	\N	\N
276	14	\N	6	6	\N	\N	\N	\N
277	14	\N	6	6	\N	\N	\N	\N
278	14	\N	6	6	\N	\N	\N	\N
279	14	\N	6	6	\N	\N	\N	\N
280	14	\N	6	6	\N	\N	\N	\N
281	14	\N	6	6	\N	\N	\N	\N
282	14	\N	6	6	\N	\N	\N	\N
283	14	\N	6	6	\N	\N	\N	\N
284	14	\N	6	6	\N	\N	\N	\N
285	14	\N	6	6	\N	\N	\N	\N
286	14	\N	6	6	\N	\N	\N	\N
287	14	\N	6	6	\N	\N	\N	\N
288	14	\N	6	6	\N	\N	\N	\N
289	14	\N	6	6	\N	\N	\N	\N
290	14	\N	6	6	\N	\N	\N	\N
291	14	\N	6	6	\N	\N	\N	\N
293	14	\N	6	6	\N	\N	\N	\N
294	14	\N	6	6	\N	\N	\N	\N
295	14	\N	6	6	\N	\N	\N	\N
296	14	\N	6	6	\N	\N	\N	\N
297	14	\N	6	6	\N	\N	\N	\N
298	14	\N	6	6	\N	\N	\N	\N
299	14	\N	6	6	\N	\N	\N	\N
300	14	\N	6	6	\N	\N	\N	\N
301	14	\N	6	6	\N	\N	\N	\N
302	14	\N	6	6	\N	\N	\N	\N
303	14	\N	6	6	\N	\N	\N	\N
304	14	\N	6	6	\N	\N	\N	\N
305	14	\N	6	6	\N	\N	\N	\N
306	14	\N	6	6	\N	\N	\N	\N
307	14	\N	6	6	\N	\N	\N	\N
308	14	\N	6	6	\N	\N	\N	\N
309	14	\N	6	6	\N	\N	\N	\N
310	14	\N	6	6	\N	\N	\N	\N
311	14	\N	6	6	\N	\N	\N	\N
312	14	\N	6	6	\N	\N	\N	\N
313	14	\N	6	6	\N	\N	\N	\N
314	14	\N	6	6	\N	\N	\N	\N
315	14	\N	6	6	\N	\N	\N	\N
316	14	\N	6	6	\N	\N	\N	\N
317	14	\N	6	6	\N	\N	\N	\N
318	14	\N	6	6	\N	\N	\N	\N
319	14	\N	6	6	\N	\N	\N	\N
320	14	\N	6	6	\N	\N	\N	\N
321	14	\N	6	6	\N	\N	\N	\N
322	14	\N	6	6	\N	\N	\N	\N
323	14	\N	6	6	\N	\N	\N	\N
324	14	\N	6	6	\N	\N	\N	\N
325	14	\N	6	6	\N	\N	\N	\N
326	14	\N	6	6	\N	\N	\N	\N
327	14	\N	6	6	\N	\N	\N	\N
328	14	\N	6	6	\N	\N	\N	\N
329	14	\N	6	6	\N	\N	\N	\N
330	14	\N	6	6	\N	\N	\N	\N
331	14	\N	6	6	\N	\N	\N	\N
332	14	\N	6	6	\N	\N	\N	\N
333	14	\N	6	6	\N	\N	\N	\N
334	14	\N	6	6	\N	\N	\N	\N
335	14	\N	6	6	\N	\N	\N	\N
336	14	\N	6	6	\N	\N	\N	\N
337	14	\N	6	6	\N	\N	\N	\N
338	14	\N	6	6	\N	\N	\N	\N
339	14	\N	6	6	\N	\N	\N	\N
340	14	\N	6	6	\N	\N	\N	\N
341	14	\N	6	6	\N	\N	\N	\N
342	14	\N	6	6	\N	\N	\N	\N
343	14	\N	6	6	\N	\N	\N	\N
344	14	\N	6	6	\N	\N	\N	\N
345	14	\N	6	6	\N	\N	\N	\N
346	14	\N	6	6	\N	\N	\N	\N
347	14	\N	6	6	\N	\N	\N	\N
348	14	\N	6	6	\N	\N	\N	\N
349	14	\N	6	6	\N	\N	\N	\N
350	14	\N	6	6	\N	\N	\N	\N
351	14	\N	6	6	\N	\N	\N	\N
352	14	\N	6	6	\N	\N	\N	\N
353	14	\N	6	6	\N	\N	\N	\N
354	14	\N	6	6	\N	\N	\N	\N
355	14	\N	6	6	\N	\N	\N	\N
356	14	\N	6	6	\N	\N	\N	\N
357	14	\N	6	6	\N	\N	\N	\N
358	14	\N	6	6	\N	\N	\N	\N
359	14	\N	6	6	\N	\N	\N	\N
360	14	\N	6	6	\N	\N	\N	\N
361	14	\N	6	6	\N	\N	\N	\N
362	14	\N	6	6	\N	\N	\N	\N
363	14	\N	6	6	\N	\N	\N	\N
364	14	\N	6	6	\N	\N	\N	\N
365	14	\N	6	6	\N	\N	\N	\N
366	14	\N	6	6	\N	\N	\N	\N
158	3	\N	6	1	\N	\N	\N	\N
161	3	\N	6	1	\N	\N	\N	\N
165	3	\N	6	1	\N	\N	\N	\N
374	2	\N	6	1	\N	\N	\N	\N
152	2	\N	1	1	2006-07-20	\N	\N	\N
166	3	\N	7	1	2006-07-20	\N	\N	\N
416	2	\N	1	1	2006-07-24	\N	\N	\N
482	3	\N	6	17	\N	\N	\N	\N
542	3	\N	6	17	\N	\N	\N	\N
563	3	\N	1	17	2006-07-26	\N	\N	\N
560	3	\N	1	17	2006-07-26	\N	\N	\N
159	3	\N	1	1	2006-07-26	\N	\N	\N
571	3	\N	1	1	2006-07-26	\N	\N	\N
575	2	\N	1	1	2006-07-26	\N	\N	\N
578	2	\N	1	1	2006-07-26	\N	\N	\N
465	2	\N	5	1	2006-07-26	\N	\N	\N
447	2	\N	5	1	2006-07-26	\N	\N	\N
461	2	\N	5	1	2006-07-26	\N	\N	\N
464	2	\N	5	1	2006-07-26	\N	\N	\N
163	3	\N	5	1	2006-07-26	\N	\N	\N
162	3	\N	5	1	2006-07-26	\N	\N	\N
415	2	\N	5	1	2006-07-27	\N	\N	\N
425	2	\N	5	1	2006-07-27	\N	\N	\N
427	2	\N	5	1	2006-07-27	\N	\N	\N
565	3	\N	5	17	2006-07-28	\N	\N	\N
421	2	\N	5	1	2006-07-28	\N	\N	\N
420	2	\N	5	1	2006-07-28	\N	\N	\N
576	2	\N	5	1	2006-07-28	\N	\N	\N
164	3	\N	5	1	2006-07-28	\N	\N	\N
562	3	\N	5	17	2006-08-01	\N	\N	\N
432	2	\N	5	1	2006-08-01	\N	\N	\N
428	2	\N	5	1	2006-08-01	\N	\N	\N
566	3	\N	5	17	2006-08-01	\N	\N	\N
460	2	\N	5	1	2006-08-01	\N	\N	\N
440	2	\N	5	1	2006-08-01	\N	\N	\N
443	2	\N	5	1	2006-08-02	\N	\N	\N
413	2	\N	5	1	2006-08-02	\N	\N	\N
154	2	\N	5	1	2006-08-02	\N	\N	\N
153	2	\N	5	1	2006-08-02	\N	\N	\N
449	2	\N	5	1	2006-08-02	\N	\N	\N
435	2	\N	5	1	2006-08-02	\N	\N	\N
466	2	\N	5	1	2006-08-02	\N	\N	\N
452	2	\N	5	1	2006-08-02	\N	\N	\N
442	2	\N	5	1	2006-08-02	\N	\N	\N
144	2	\N	5	1	2006-08-09	\N	\N	\N
441	2	\N	5	1	2006-08-09	\N	\N	\N
136	2	\N	5	1	2006-08-11	\N	\N	\N
626	16	\N	6	1	\N	\N	\N	\N
631	3	\N	3	10	2006-08-14	\N	\N	\N
636	3	\N	3	10	2006-08-14	\N	\N	\N
637	3	\N	3	10	2006-08-14	\N	\N	\N
640	3	\N	3	10	2006-08-14	\N	\N	\N
642	3	\N	3	10	2006-08-14	\N	\N	\N
655	3	\N	3	10	2006-08-14	\N	\N	\N
656	3	\N	3	10	2006-08-14	\N	\N	\N
664	3	\N	3	10	2006-08-14	\N	\N	\N
691	3	\N	18	10	2006-08-14	\N	\N	\N
676	3	\N	18	10	2006-08-14	1	8	\N
669	3	\N	3	10	2006-08-14	\N	\N	\N
725	2	\N	18	10	2006-08-14	1	99999	\N
674	3	\N	3	10	2006-08-14	\N	\N	\N
675	3	\N	3	10	2006-08-14	\N	\N	\N
682	3	\N	3	10	2006-08-14	\N	\N	\N
683	3	\N	3	10	2006-08-14	\N	\N	\N
686	3	\N	3	10	2006-08-14	\N	\N	\N
688	3	\N	3	10	2006-08-14	\N	\N	\N
689	3	\N	3	10	2006-08-14	\N	\N	\N
690	3	\N	3	10	2006-08-14	\N	\N	\N
701	3	\N	3	10	2006-08-14	\N	\N	\N
699	3	\N	3	10	2006-08-14	\N	\N	\N
697	3	\N	3	10	2006-08-14	\N	\N	\N
694	3	\N	3	10	2006-08-14	\N	\N	\N
693	3	\N	3	10	2006-08-14	\N	\N	\N
692	3	\N	3	10	2006-08-14	\N	\N	\N
706	3	\N	3	10	2006-08-14	\N	\N	\N
711	3	\N	3	10	2006-08-14	\N	\N	\N
714	3	\N	3	10	2006-08-14	\N	\N	\N
715	3	\N	3	10	2006-08-14	\N	\N	\N
717	2	\N	3	10	2006-08-14	\N	\N	\N
720	2	\N	3	10	2006-08-14	\N	\N	\N
444	2	\N	5	1	2006-08-14	\N	\N	\N
580	2	\N	5	1	2006-08-14	\N	\N	\N
570	3	\N	5	1	2006-08-14	\N	\N	\N
267	14	\N	7	6	2006-08-17	\N	\N	\N
728	3	\N	1	8	2006-08-18	\N	\N	\N
155	2	\N	5	1	2006-08-18	\N	\N	\N
451	2	\N	5	1	2006-08-18	\N	\N	\N
567	3	\N	5	1	2006-08-18	\N	\N	\N
577	2	\N	5	1	2006-08-18	\N	\N	\N
739	2	\N	1	8	2006-08-21	\N	\N	\N
730	3	\N	1	8	2006-08-21	\N	\N	\N
617	2	\N	18	10	2006-08-10	\N	\N	\N
619	16	\N	18	1	2006-08-10	\N	\N	\N
603	2	\N	18	1	2006-08-10	\N	\N	\N
742	16	\N	6	10	\N	\N	\N	\N
741	16	\N	5	10	2006-08-24	\N	\N	\N
745	3	\N	6	10	\N	\N	\N	\N
747	2	\N	5	10	2006-08-25	\N	\N	\N
746	2	\N	5	10	2006-08-25	\N	\N	\N
386	2	\N	1	1	2006-08-28	\N	\N	\N
643	3	\N	1	10	2006-08-31	\N	\N	\N
773	1	\N	7	10	2006-08-31	\N	\N	\N
775	1	\N	7	10	2006-08-31	\N	\N	\N
777	1	\N	7	10	2006-08-31	\N	\N	\N
779	1	\N	7	10	2006-08-31	\N	\N	\N
781	1	\N	7	10	2006-08-31	\N	\N	\N
780	1	\N	7	10	2006-08-31	\N	\N	\N
778	1	\N	7	10	2006-08-31	\N	\N	\N
776	1	\N	7	10	2006-08-31	\N	\N	\N
774	1	\N	7	10	2006-08-31	\N	\N	\N
772	1	\N	7	10	2006-08-31	\N	\N	\N
783	1	\N	7	10	2006-08-31	\N	\N	\N
785	1	\N	7	10	2006-08-31	\N	\N	\N
787	1	\N	7	10	2006-08-31	\N	\N	\N
788	1	\N	7	10	2006-08-31	\N	\N	\N
791	1	\N	7	10	2006-08-31	\N	\N	\N
790	1	\N	7	10	2006-08-31	\N	\N	\N
789	1	\N	7	10	2006-08-31	\N	\N	\N
786	1	\N	7	10	2006-08-31	\N	\N	\N
784	1	\N	7	10	2006-08-31	\N	\N	\N
782	1	\N	7	10	2006-08-31	\N	\N	\N
800	1	\N	7	10	2006-08-31	\N	\N	\N
798	1	\N	7	10	2006-08-31	\N	\N	\N
796	1	\N	7	10	2006-08-31	\N	\N	\N
795	1	\N	7	10	2006-08-31	\N	\N	\N
794	1	\N	7	10	2006-08-31	\N	\N	\N
792	1	\N	7	10	2006-08-31	\N	\N	\N
793	1	\N	7	10	2006-08-31	\N	\N	\N
797	1	\N	7	10	2006-08-31	\N	\N	\N
799	1	\N	7	10	2006-08-31	\N	\N	\N
801	1	\N	7	10	2006-08-31	\N	\N	\N
802	1	\N	7	10	2006-08-31	\N	\N	\N
804	1	\N	7	10	2006-08-31	\N	\N	\N
806	1	\N	7	10	2006-08-31	\N	\N	\N
808	1	\N	7	10	2006-08-31	\N	\N	\N
810	1	\N	7	10	2006-08-31	\N	\N	\N
811	1	\N	7	10	2006-08-31	\N	\N	\N
809	1	\N	7	10	2006-08-31	\N	\N	\N
807	1	\N	7	10	2006-08-31	\N	\N	\N
805	1	\N	7	10	2006-08-31	\N	\N	\N
803	1	\N	7	10	2006-08-31	\N	\N	\N
812	1	\N	7	10	2006-08-31	\N	\N	\N
814	1	\N	7	10	2006-08-31	\N	\N	\N
816	1	\N	7	10	2006-08-31	\N	\N	\N
818	1	\N	7	10	2006-08-31	\N	\N	\N
820	1	\N	7	10	2006-08-31	\N	\N	\N
821	1	\N	7	10	2006-08-31	\N	\N	\N
819	1	\N	7	10	2006-08-31	\N	\N	\N
817	1	\N	7	10	2006-08-31	\N	\N	\N
815	1	\N	7	10	2006-08-31	\N	\N	\N
813	1	\N	7	10	2006-08-31	\N	\N	\N
830	1	\N	7	10	2006-08-31	\N	\N	\N
828	1	\N	7	10	2006-08-31	\N	\N	\N
826	1	\N	7	10	2006-08-31	\N	\N	\N
824	1	\N	7	10	2006-08-31	\N	\N	\N
822	1	\N	7	10	2006-08-31	\N	\N	\N
823	1	\N	7	10	2006-08-31	\N	\N	\N
825	1	\N	7	10	2006-08-31	\N	\N	\N
827	1	\N	7	10	2006-08-31	\N	\N	\N
829	1	\N	7	10	2006-08-31	\N	\N	\N
831	1	\N	7	10	2006-08-31	\N	\N	\N
841	1	\N	7	10	2006-08-31	\N	\N	\N
840	1	\N	7	10	2006-08-31	\N	\N	\N
837	1	\N	7	10	2006-08-31	\N	\N	\N
835	1	\N	7	10	2006-08-31	\N	\N	\N
833	1	\N	7	10	2006-08-31	\N	\N	\N
832	1	\N	7	10	2006-08-31	\N	\N	\N
834	1	\N	7	10	2006-08-31	\N	\N	\N
836	1	\N	7	10	2006-08-31	\N	\N	\N
838	1	\N	7	10	2006-08-31	\N	\N	\N
839	1	\N	7	10	2006-08-31	\N	\N	\N
847	1	\N	7	10	2006-08-31	\N	\N	\N
848	1	\N	7	10	2006-08-31	\N	\N	\N
849	1	\N	7	10	2006-08-31	\N	\N	\N
850	1	\N	7	10	2006-08-31	\N	\N	\N
751	1	\N	7	10	2006-08-31	\N	\N	\N
842	1	\N	7	10	2006-08-31	\N	\N	\N
843	1	\N	7	10	2006-08-31	\N	\N	\N
844	1	\N	7	10	2006-08-31	\N	\N	\N
845	1	\N	7	10	2006-08-31	\N	\N	\N
846	1	\N	7	10	2006-08-31	\N	\N	\N
422	2	\N	21	1	2006-07-24	\N	\N	\N
426	2	\N	21	1	2006-07-24	\N	\N	\N
459	2	\N	21	1	2006-07-24	\N	\N	\N
160	3	\N	21	1	2006-07-26	\N	\N	\N
572	3	\N	21	1	2006-07-26	\N	\N	\N
568	3	\N	21	1	2006-07-26	\N	\N	\N
569	3	\N	21	1	2006-07-26	\N	\N	\N
574	2	\N	21	1	2006-07-26	\N	\N	\N
579	2	\N	21	1	2006-07-26	\N	\N	\N
581	2	\N	21	1	2006-07-26	\N	\N	\N
582	2	\N	21	1	2006-07-26	\N	\N	\N
467	3	\N	21	17	2006-08-16	\N	\N	\N
476	3	\N	21	17	2006-08-16	\N	\N	\N
468	3	\N	21	17	2006-08-16	\N	\N	\N
469	3	\N	21	17	2006-08-16	\N	\N	\N
470	3	\N	21	17	2006-08-16	\N	\N	\N
398	2	\N	5	1	2006-08-25	1	10	\N
653	3	\N	21	10	2006-08-21	\N	\N	\N
638	3	\N	21	10	2006-08-21	\N	\N	\N
661	3	\N	21	10	2006-08-21	\N	\N	\N
654	3	\N	21	10	2006-08-21	\N	\N	\N
648	3	\N	21	10	2006-08-21	\N	\N	\N
671	3	\N	21	10	2006-08-21	\N	\N	\N
635	3	\N	21	10	2006-08-21	\N	\N	\N
670	3	\N	21	10	2006-08-21	\N	\N	\N
367	2	\N	21	1	2006-08-24	\N	\N	\N
397	2	\N	21	1	2006-08-25	\N	\N	\N
392	2	\N	21	1	2006-08-28	\N	\N	\N
385	2	\N	21	1	2006-08-28	\N	\N	\N
382	2	\N	21	1	2006-08-28	\N	\N	\N
381	2	\N	21	1	2006-08-28	\N	\N	\N
380	2	\N	21	1	2006-08-28	\N	\N	\N
379	2	\N	21	1	2006-08-28	\N	\N	\N
632	3	\N	21	10	2006-08-31	\N	\N	\N
477	3	\N	21	17	2006-08-31	\N	\N	\N
885	2	\N	21	10	2006-09-01	\N	\N	\N
865	2	\N	21	10	2006-09-01	\N	\N	\N
887	17	\N	21	\N	\N	\N	\N	\N
888	17	\N	21	\N	\N	\N	\N	\N
889	17	\N	21	\N	\N	\N	\N	\N
890	17	\N	21	\N	\N	\N	\N	\N
891	17	\N	21	\N	\N	\N	\N	\N
892	17	\N	21	\N	\N	\N	\N	\N
893	17	\N	21	\N	\N	\N	\N	\N
894	17	\N	21	\N	\N	\N	\N	\N
895	17	\N	21	\N	\N	\N	\N	\N
896	17	\N	21	\N	\N	\N	\N	\N
897	17	\N	21	\N	\N	\N	\N	\N
898	17	\N	21	\N	\N	\N	\N	\N
899	17	\N	21	\N	\N	\N	\N	\N
900	17	\N	21	\N	\N	\N	\N	\N
901	17	\N	21	\N	\N	\N	\N	\N
902	17	\N	21	\N	\N	\N	\N	\N
903	17	\N	21	\N	\N	\N	\N	\N
904	17	\N	21	\N	\N	\N	\N	\N
905	17	\N	21	\N	\N	\N	\N	\N
906	17	\N	21	\N	\N	\N	\N	\N
907	17	\N	21	\N	\N	\N	\N	\N
908	17	\N	21	\N	\N	\N	\N	\N
909	17	\N	21	\N	\N	\N	\N	\N
852	2	\N	1	10	2006-09-04	\N	\N	\N
953	2	\N	5	8	2006-09-05	1	8	\N
964	2	\N	5	8	2006-09-05	1	8	\N
954	2	\N	1	8	2006-09-05	\N	\N	\N
955	2	\N	1	8	2006-09-05	\N	\N	\N
961	2	\N	1	8	2006-09-05	\N	\N	\N
853	2	\N	1	10	2006-09-05	\N	\N	\N
965	2	\N	1	8	2006-09-05	\N	\N	\N
922	2	\N	1	8	2006-09-05	\N	\N	\N
863	2	\N	1	10	2006-09-05	\N	\N	\N
855	2	\N	1	10	2006-09-05	\N	\N	\N
876	2	\N	1	10	2006-09-05	\N	\N	\N
866	2	\N	1	10	2006-09-05	\N	\N	\N
967	2	\N	1	8	2006-09-05	\N	\N	\N
963	2	\N	1	8	2006-09-05	\N	\N	\N
977	2	\N	18	8	\N	\N	\N	\N
945	2	\N	18	8	2006-09-08	\N	\N	\N
879	2	\N	5	10	2006-09-08	1	8	\N
1005	2	\N	18	8	2006-09-08	\N	\N	\N
867	2	\N	5	10	2006-09-08	1	8	\N
942	2	\N	5	8	2006-09-08	1	8	\N
936	2	\N	18	8	2006-09-08	\N	\N	\N
968	2	\N	5	8	2006-09-05	1	8	\N
975	2	\N	6	8	\N	\N	\N	\N
976	2	\N	6	8	\N	\N	\N	\N
992	2	\N	1	8	2006-09-08	\N	\N	\N
951	2	\N	1	8	2006-09-08	\N	\N	\N
999	2	\N	1	8	2006-09-08	\N	\N	\N
952	2	\N	1	8	2006-09-08	\N	\N	\N
1010	2	\N	1	8	2006-09-08	\N	\N	\N
1008	2	\N	1	8	2006-09-08	\N	\N	\N
927	2	\N	1	8	2006-09-08	\N	\N	\N
970	2	\N	1	8	2006-09-08	\N	\N	\N
875	2	\N	1	10	2006-09-08	\N	\N	\N
946	2	\N	1	8	2006-09-08	\N	\N	\N
948	2	\N	1	8	2006-09-08	\N	\N	\N
930	2	\N	1	8	2006-09-08	\N	\N	\N
370	2	\N	18	1	2006-09-15	\N	\N	\N
971	2	\N	5	8	2006-09-18	1	8	\N
372	2	\N	18	1	2006-09-15	\N	\N	\N
979	2	\N	18	8	2006-09-19	\N	\N	\N
1275	2	\N	18	1	\N	\N	\N	\N
1274	3	\N	7	14	2006-09-19	\N	\N	\N
667	3	\N	5	10	2006-09-19	\N	\N	\N
1278	3	\N	6	5	\N	\N	\N	\N
1279	3	\N	6	5	\N	\N	\N	\N
1280	3	\N	6	5	\N	\N	\N	\N
1281	3	\N	6	5	\N	\N	\N	\N
1282	3	\N	6	5	\N	\N	\N	\N
1283	3	\N	6	5	\N	\N	\N	\N
1284	3	\N	6	5	\N	\N	\N	\N
1285	3	\N	6	5	\N	\N	\N	\N
1286	3	\N	6	5	\N	\N	\N	\N
1287	3	\N	6	5	\N	\N	\N	\N
1288	3	\N	6	5	\N	\N	\N	\N
1289	3	\N	6	5	\N	\N	\N	\N
1290	3	\N	6	5	\N	\N	\N	\N
1291	3	\N	6	5	\N	\N	\N	\N
1292	3	\N	6	5	\N	\N	\N	\N
1293	3	\N	6	5	\N	\N	\N	\N
1294	3	\N	6	5	\N	\N	\N	\N
1295	3	\N	6	5	\N	\N	\N	\N
1296	3	\N	6	5	\N	\N	\N	\N
1297	3	\N	6	5	\N	\N	\N	\N
1298	3	\N	6	5	\N	\N	\N	\N
1299	3	\N	6	5	\N	\N	\N	\N
1300	3	\N	6	5	\N	\N	\N	\N
1301	3	\N	6	5	\N	\N	\N	\N
1302	3	\N	6	5	\N	\N	\N	\N
1303	3	\N	6	5	\N	\N	\N	\N
1304	3	\N	6	5	\N	\N	\N	\N
1305	3	\N	6	5	\N	\N	\N	\N
1306	3	\N	6	5	\N	\N	\N	\N
1307	3	\N	6	5	\N	\N	\N	\N
1308	3	\N	6	5	\N	\N	\N	\N
1309	3	\N	6	5	\N	\N	\N	\N
1310	3	\N	6	5	\N	\N	\N	\N
1311	3	\N	6	5	\N	\N	\N	\N
1312	3	\N	6	5	\N	\N	\N	\N
1313	3	\N	6	5	\N	\N	\N	\N
1314	3	\N	6	5	\N	\N	\N	\N
1315	3	\N	6	5	\N	\N	\N	\N
1316	3	\N	6	5	\N	\N	\N	\N
1317	3	\N	6	5	\N	\N	\N	\N
1318	3	\N	6	5	\N	\N	\N	\N
1319	3	\N	6	5	\N	\N	\N	\N
1320	3	\N	6	5	\N	\N	\N	\N
1321	3	\N	6	5	\N	\N	\N	\N
1322	3	\N	6	5	\N	\N	\N	\N
1323	3	\N	6	5	\N	\N	\N	\N
1324	3	\N	6	5	\N	\N	\N	\N
1325	3	\N	6	5	\N	\N	\N	\N
1326	3	\N	6	5	\N	\N	\N	\N
1327	3	\N	6	5	\N	\N	\N	\N
1328	2	\N	6	5	\N	\N	\N	\N
1329	2	\N	6	5	\N	\N	\N	\N
1330	2	\N	6	5	\N	\N	\N	\N
1331	2	\N	6	5	\N	\N	\N	\N
1332	2	\N	6	5	\N	\N	\N	\N
1334	2	\N	6	5	\N	\N	\N	\N
1335	2	\N	6	5	\N	\N	\N	\N
1336	2	\N	6	5	\N	\N	\N	\N
1337	2	\N	6	5	\N	\N	\N	\N
1338	2	\N	6	5	\N	\N	\N	\N
1339	2	\N	6	5	\N	\N	\N	\N
1340	2	\N	6	5	\N	\N	\N	\N
1341	2	\N	6	5	\N	\N	\N	\N
1342	2	\N	6	5	\N	\N	\N	\N
1343	2	\N	6	5	\N	\N	\N	\N
1344	2	\N	6	5	\N	\N	\N	\N
1345	2	\N	6	5	\N	\N	\N	\N
1346	2	\N	6	5	\N	\N	\N	\N
1347	2	\N	6	5	\N	\N	\N	\N
1348	2	\N	6	5	\N	\N	\N	\N
1349	2	\N	6	5	\N	\N	\N	\N
1350	2	\N	6	5	\N	\N	\N	\N
1351	2	\N	6	5	\N	\N	\N	\N
1352	2	\N	6	5	\N	\N	\N	\N
1353	2	\N	6	5	\N	\N	\N	\N
1354	2	\N	6	5	\N	\N	\N	\N
1355	2	\N	6	5	\N	\N	\N	\N
1356	2	\N	6	5	\N	\N	\N	\N
1357	2	\N	6	5	\N	\N	\N	\N
1358	2	\N	6	5	\N	\N	\N	\N
1359	2	\N	6	5	\N	\N	\N	\N
1360	2	\N	6	5	\N	\N	\N	\N
1361	2	\N	6	5	\N	\N	\N	\N
1362	2	\N	6	5	\N	\N	\N	\N
1363	2	\N	6	5	\N	\N	\N	\N
1364	2	\N	6	5	\N	\N	\N	\N
1365	2	\N	6	5	\N	\N	\N	\N
1366	2	\N	6	5	\N	\N	\N	\N
1367	2	\N	6	5	\N	\N	\N	\N
1368	2	\N	6	5	\N	\N	\N	\N
1369	2	\N	6	5	\N	\N	\N	\N
1370	2	\N	6	5	\N	\N	\N	\N
1373	2	\N	6	5	\N	\N	\N	\N
1374	2	\N	6	5	\N	\N	\N	\N
1375	2	\N	6	5	\N	\N	\N	\N
1376	2	\N	6	5	\N	\N	\N	\N
1377	2	\N	6	5	\N	\N	\N	\N
1398	2	\N	6	1	\N	\N	\N	\N
1402	2	\N	5	1	2006-10-02	1	8	\N
940	2	\N	18	8	2006-09-27	\N	\N	\N
1395	2	\N	7	8	2006-09-26	\N	\N	\N
698	3	\N	21	10	2006-09-27	\N	\N	\N
703	3	\N	21	10	2006-09-27	\N	\N	\N
1418	17	\N	21	17	2006-09-27	\N	\N	\N
1419	17	\N	21	17	2006-09-27	\N	\N	\N
1271	3	\N	21	14	2006-10-02	\N	\N	\N
1422	17	\N	21	17	2006-10-02	\N	\N	\N
1423	17	\N	21	17	2006-10-02	\N	\N	\N
1420	18	\N	1	17	2006-10-02	\N	\N	\N
1424	18	\N	1	17	2006-10-02	\N	\N	\N
1425	2	\N	18	10	2006-10-03	\N	\N	\N
1427	2	\N	18	10	2006-10-03	\N	\N	\N
1433	2	\N	7	10	2006-10-03	\N	\N	\N
1431	2	\N	7	10	2006-10-03	\N	\N	\N
1687	1	\N	7	10	2006-10-03	\N	\N	\N
1682	1	\N	7	10	2006-10-03	\N	\N	\N
1681	1	\N	7	10	2006-10-03	\N	\N	\N
1686	1	\N	7	10	2006-10-03	\N	\N	\N
1685	1	\N	7	10	2006-10-03	\N	\N	\N
1680	1	\N	7	10	2006-10-03	\N	\N	\N
1679	1	\N	7	10	2006-10-03	\N	\N	\N
1684	1	\N	7	10	2006-10-03	\N	\N	\N
1683	1	\N	7	10	2006-10-03	\N	\N	\N
1678	1	\N	7	10	2006-10-03	\N	\N	\N
1676	1	\N	7	10	2006-10-03	\N	\N	\N
1677	1	\N	7	10	2006-10-03	\N	\N	\N
1674	1	\N	7	10	2006-10-03	\N	\N	\N
1675	1	\N	7	10	2006-10-03	\N	\N	\N
1673	1	\N	7	10	2006-10-03	\N	\N	\N
1672	1	\N	7	10	2006-10-03	\N	\N	\N
1670	1	\N	7	10	2006-10-03	\N	\N	\N
1671	1	\N	7	10	2006-10-03	\N	\N	\N
1669	1	\N	7	10	2006-10-03	\N	\N	\N
1668	1	\N	7	10	2006-10-03	\N	\N	\N
1666	1	\N	7	10	2006-10-03	\N	\N	\N
1664	1	\N	7	10	2006-10-03	\N	\N	\N
1662	1	\N	7	10	2006-10-03	\N	\N	\N
1660	1	\N	7	10	2006-10-03	\N	\N	\N
1658	1	\N	7	10	2006-10-03	\N	\N	\N
1659	1	\N	7	10	2006-10-03	\N	\N	\N
1661	1	\N	7	10	2006-10-03	\N	\N	\N
1663	1	\N	7	10	2006-10-03	\N	\N	\N
1665	1	\N	7	10	2006-10-03	\N	\N	\N
1667	1	\N	7	10	2006-10-03	\N	\N	\N
1657	1	\N	7	10	2006-10-03	\N	\N	\N
1656	1	\N	7	10	2006-10-03	\N	\N	\N
1655	1	\N	7	10	2006-10-03	\N	\N	\N
1654	1	\N	7	10	2006-10-03	\N	\N	\N
1653	1	\N	7	10	2006-10-03	\N	\N	\N
1648	1	\N	7	10	2006-10-03	\N	\N	\N
1649	1	\N	7	10	2006-10-03	\N	\N	\N
1650	1	\N	7	10	2006-10-03	\N	\N	\N
1651	1	\N	7	10	2006-10-03	\N	\N	\N
1652	1	\N	7	10	2006-10-03	\N	\N	\N
1629	1	\N	7	10	2006-10-03	\N	\N	\N
1630	1	\N	7	10	2006-10-03	\N	\N	\N
1633	1	\N	7	10	2006-10-03	\N	\N	\N
1636	1	\N	7	10	2006-10-03	\N	\N	\N
1637	1	\N	7	10	2006-10-03	\N	\N	\N
1635	1	\N	7	10	2006-10-03	\N	\N	\N
1634	1	\N	7	10	2006-10-03	\N	\N	\N
1632	1	\N	7	10	2006-10-03	\N	\N	\N
1631	1	\N	7	10	2006-10-03	\N	\N	\N
1628	1	\N	7	10	2006-10-03	\N	\N	\N
1626	1	\N	7	10	2006-10-03	\N	\N	\N
1625	1	\N	7	10	2006-10-03	\N	\N	\N
1623	1	\N	7	10	2006-10-03	\N	\N	\N
1620	1	\N	7	10	2006-10-03	\N	\N	\N
1619	1	\N	7	10	2006-10-03	\N	\N	\N
1618	1	\N	7	10	2006-10-03	\N	\N	\N
1621	1	\N	7	10	2006-10-03	\N	\N	\N
1622	1	\N	7	10	2006-10-03	\N	\N	\N
1624	1	\N	7	10	2006-10-03	\N	\N	\N
1627	1	\N	7	10	2006-10-03	\N	\N	\N
1615	1	\N	7	10	2006-10-03	\N	\N	\N
1614	1	\N	7	10	2006-10-03	\N	\N	\N
1612	1	\N	7	10	2006-10-03	\N	\N	\N
1611	1	\N	7	10	2006-10-03	\N	\N	\N
1608	1	\N	7	10	2006-10-03	\N	\N	\N
1609	1	\N	7	10	2006-10-03	\N	\N	\N
1610	1	\N	7	10	2006-10-03	\N	\N	\N
1613	1	\N	7	10	2006-10-03	\N	\N	\N
1616	1	\N	7	10	2006-10-03	\N	\N	\N
1617	1	\N	7	10	2006-10-03	\N	\N	\N
1598	1	\N	7	10	2006-10-03	\N	\N	\N
1599	1	\N	7	10	2006-10-03	\N	\N	\N
1601	1	\N	7	10	2006-10-03	\N	\N	\N
1600	1	\N	7	10	2006-10-03	\N	\N	\N
1602	1	\N	7	10	2006-10-03	\N	\N	\N
1603	1	\N	7	10	2006-10-03	\N	\N	\N
1605	1	\N	7	10	2006-10-03	\N	\N	\N
1604	1	\N	7	10	2006-10-03	\N	\N	\N
1606	1	\N	7	10	2006-10-03	\N	\N	\N
1607	1	\N	7	10	2006-10-03	\N	\N	\N
1468	1	\N	7	10	2006-10-03	\N	\N	\N
1469	1	\N	7	10	2006-10-03	\N	\N	\N
1470	1	\N	7	10	2006-10-03	\N	\N	\N
1471	1	\N	7	10	2006-10-03	\N	\N	\N
1472	1	\N	7	10	2006-10-03	\N	\N	\N
1473	1	\N	7	10	2006-10-03	\N	\N	\N
1474	1	\N	7	10	2006-10-03	\N	\N	\N
1475	1	\N	7	10	2006-10-03	\N	\N	\N
1476	1	\N	7	10	2006-10-03	\N	\N	\N
1477	1	\N	7	10	2006-10-03	\N	\N	\N
1463	1	\N	7	10	2006-10-03	\N	\N	\N
1458	1	\N	7	10	2006-10-03	\N	\N	\N
1459	1	\N	7	10	2006-10-03	\N	\N	\N
1464	1	\N	7	10	2006-10-03	\N	\N	\N
1465	1	\N	7	10	2006-10-03	\N	\N	\N
1460	1	\N	7	10	2006-10-03	\N	\N	\N
1461	1	\N	7	10	2006-10-03	\N	\N	\N
1466	1	\N	7	10	2006-10-03	\N	\N	\N
1467	1	\N	7	10	2006-10-03	\N	\N	\N
1462	1	\N	7	10	2006-10-03	\N	\N	\N
1478	1	\N	7	10	2006-10-03	\N	\N	\N
1479	1	\N	7	10	2006-10-03	\N	\N	\N
1566	1	\N	18	10	2006-10-03	\N	\N	\N
1481	1	\N	7	10	2006-10-03	\N	\N	\N
1482	1	\N	7	10	2006-10-03	\N	\N	\N
1483	1	\N	7	10	2006-10-03	\N	\N	\N
1484	1	\N	7	10	2006-10-03	\N	\N	\N
1485	1	\N	7	10	2006-10-03	\N	\N	\N
1486	1	\N	7	10	2006-10-03	\N	\N	\N
1487	1	\N	7	10	2006-10-03	\N	\N	\N
1492	1	\N	7	10	2006-10-03	\N	\N	\N
1491	1	\N	7	10	2006-10-03	\N	\N	\N
1490	1	\N	7	10	2006-10-03	\N	\N	\N
1489	1	\N	7	10	2006-10-03	\N	\N	\N
1488	1	\N	7	10	2006-10-03	\N	\N	\N
1493	1	\N	7	10	2006-10-03	\N	\N	\N
1494	1	\N	7	10	2006-10-03	\N	\N	\N
1495	1	\N	7	10	2006-10-03	\N	\N	\N
1496	1	\N	7	10	2006-10-03	\N	\N	\N
1497	1	\N	7	10	2006-10-03	\N	\N	\N
1502	1	\N	7	10	2006-10-03	\N	\N	\N
1501	1	\N	7	10	2006-10-03	\N	\N	\N
1500	1	\N	7	10	2006-10-03	\N	\N	\N
1499	1	\N	7	10	2006-10-03	\N	\N	\N
1498	1	\N	7	10	2006-10-03	\N	\N	\N
1507	1	\N	7	10	2006-10-03	\N	\N	\N
1506	1	\N	7	10	2006-10-03	\N	\N	\N
1505	1	\N	7	10	2006-10-03	\N	\N	\N
1504	1	\N	7	10	2006-10-03	\N	\N	\N
1503	1	\N	7	10	2006-10-03	\N	\N	\N
1647	1	\N	7	10	2006-10-03	\N	\N	\N
1641	1	\N	7	10	2006-10-03	\N	\N	\N
1640	1	\N	7	10	2006-10-03	\N	\N	\N
1639	1	\N	7	10	2006-10-03	\N	\N	\N
1638	1	\N	7	10	2006-10-03	\N	\N	\N
1642	1	\N	7	10	2006-10-03	\N	\N	\N
1643	1	\N	7	10	2006-10-03	\N	\N	\N
1644	1	\N	7	10	2006-10-03	\N	\N	\N
1645	1	\N	7	10	2006-10-03	\N	\N	\N
1646	1	\N	7	10	2006-10-03	\N	\N	\N
1588	1	\N	7	10	2006-10-03	\N	\N	\N
1597	1	\N	7	10	2006-10-03	\N	\N	\N
1596	1	\N	7	10	2006-10-03	\N	\N	\N
1589	1	\N	7	10	2006-10-03	\N	\N	\N
1590	1	\N	7	10	2006-10-03	\N	\N	\N
1595	1	\N	7	10	2006-10-03	\N	\N	\N
1594	1	\N	7	10	2006-10-03	\N	\N	\N
1591	1	\N	7	10	2006-10-03	\N	\N	\N
1592	1	\N	7	10	2006-10-03	\N	\N	\N
1593	1	\N	7	10	2006-10-03	\N	\N	\N
1582	1	\N	7	10	2006-10-03	\N	\N	\N
1581	1	\N	7	10	2006-10-03	\N	\N	\N
1580	1	\N	7	10	2006-10-03	\N	\N	\N
1579	1	\N	7	10	2006-10-03	\N	\N	\N
1587	1	\N	7	10	2006-10-03	\N	\N	\N
1586	1	\N	7	10	2006-10-03	\N	\N	\N
1585	1	\N	7	10	2006-10-03	\N	\N	\N
1584	1	\N	7	10	2006-10-03	\N	\N	\N
1583	1	\N	7	10	2006-10-03	\N	\N	\N
1574	1	\N	7	10	2006-10-03	\N	\N	\N
1573	1	\N	7	10	2006-10-03	\N	\N	\N
1570	1	\N	7	10	2006-10-03	\N	\N	\N
1569	1	\N	7	10	2006-10-03	\N	\N	\N
1568	1	\N	7	10	2006-10-03	\N	\N	\N
1571	1	\N	7	10	2006-10-03	\N	\N	\N
1572	1	\N	7	10	2006-10-03	\N	\N	\N
1575	1	\N	7	10	2006-10-03	\N	\N	\N
1576	1	\N	7	10	2006-10-03	\N	\N	\N
1577	1	\N	7	10	2006-10-03	\N	\N	\N
1558	1	\N	7	10	2006-10-03	\N	\N	\N
1559	1	\N	7	10	2006-10-03	\N	\N	\N
1560	1	\N	7	10	2006-10-03	\N	\N	\N
1561	1	\N	7	10	2006-10-03	\N	\N	\N
1562	1	\N	7	10	2006-10-03	\N	\N	\N
1563	1	\N	7	10	2006-10-03	\N	\N	\N
1564	1	\N	7	10	2006-10-03	\N	\N	\N
1565	1	\N	7	10	2006-10-03	\N	\N	\N
1567	1	\N	7	10	2006-10-03	\N	\N	\N
1552	1	\N	7	10	2006-10-03	\N	\N	\N
1551	1	\N	7	10	2006-10-03	\N	\N	\N
1550	1	\N	7	10	2006-10-03	\N	\N	\N
1549	1	\N	7	10	2006-10-03	\N	\N	\N
1548	1	\N	7	10	2006-10-03	\N	\N	\N
1553	1	\N	7	10	2006-10-03	\N	\N	\N
1554	1	\N	7	10	2006-10-03	\N	\N	\N
1555	1	\N	7	10	2006-10-03	\N	\N	\N
1556	1	\N	7	10	2006-10-03	\N	\N	\N
1557	1	\N	7	10	2006-10-03	\N	\N	\N
1538	1	\N	7	10	2006-10-03	\N	\N	\N
1539	1	\N	7	10	2006-10-03	\N	\N	\N
1540	1	\N	7	10	2006-10-03	\N	\N	\N
1541	1	\N	7	10	2006-10-03	\N	\N	\N
1542	1	\N	7	10	2006-10-03	\N	\N	\N
1543	1	\N	7	10	2006-10-03	\N	\N	\N
1544	1	\N	7	10	2006-10-03	\N	\N	\N
1545	1	\N	7	10	2006-10-03	\N	\N	\N
1546	1	\N	7	10	2006-10-03	\N	\N	\N
1547	1	\N	7	10	2006-10-03	\N	\N	\N
1532	1	\N	7	10	2006-10-03	\N	\N	\N
1531	1	\N	7	10	2006-10-03	\N	\N	\N
1530	1	\N	7	10	2006-10-03	\N	\N	\N
1529	1	\N	7	10	2006-10-03	\N	\N	\N
1528	1	\N	7	10	2006-10-03	\N	\N	\N
1533	1	\N	7	10	2006-10-03	\N	\N	\N
1534	1	\N	7	10	2006-10-03	\N	\N	\N
1535	1	\N	7	10	2006-10-03	\N	\N	\N
1536	1	\N	7	10	2006-10-03	\N	\N	\N
1537	1	\N	7	10	2006-10-03	\N	\N	\N
1522	1	\N	7	10	2006-10-03	\N	\N	\N
1521	1	\N	7	10	2006-10-03	\N	\N	\N
1520	1	\N	7	10	2006-10-03	\N	\N	\N
1519	1	\N	7	10	2006-10-03	\N	\N	\N
1518	1	\N	7	10	2006-10-03	\N	\N	\N
1523	1	\N	7	10	2006-10-03	\N	\N	\N
1524	1	\N	7	10	2006-10-03	\N	\N	\N
1525	1	\N	7	10	2006-10-03	\N	\N	\N
1526	1	\N	7	10	2006-10-03	\N	\N	\N
1527	1	\N	7	10	2006-10-03	\N	\N	\N
1508	1	\N	7	10	2006-10-03	\N	\N	\N
1509	1	\N	7	10	2006-10-03	\N	\N	\N
1510	1	\N	7	10	2006-10-03	\N	\N	\N
1511	1	\N	7	10	2006-10-03	\N	\N	\N
1512	1	\N	7	10	2006-10-03	\N	\N	\N
1517	1	\N	7	10	2006-10-03	\N	\N	\N
1516	1	\N	7	10	2006-10-03	\N	\N	\N
1515	1	\N	7	10	2006-10-03	\N	\N	\N
1514	1	\N	7	10	2006-10-03	\N	\N	\N
1513	1	\N	7	10	2006-10-03	\N	\N	\N
1697	2	\N	18	10	2006-10-04	\N	\N	\N
1698	2	\N	18	10	2006-10-04	\N	\N	\N
1699	2	\N	18	10	2006-10-04	\N	\N	\N
1706	3	\N	18	10	2006-10-04	\N	\N	\N
1705	3	\N	18	10	2006-10-04	\N	\N	\N
1711	18	\N	1	16	2006-10-05	\N	\N	\N
665	3	\N	21	10	2006-10-05	\N	\N	\N
1396	2	\N	21	8	2006-10-05	\N	\N	\N
658	3	\N	21	10	2006-10-05	\N	\N	\N
1372	2	\N	21	5	2006-10-05	\N	\N	\N
1710	17	\N	21	16	2006-10-05	\N	\N	\N
1709	17	\N	21	16	2006-10-05	\N	\N	\N
1818	13	\N	6	17	\N	\N	\N	\N
1819	13	\N	6	17	\N	\N	\N	\N
1820	13	\N	6	17	\N	\N	\N	\N
760	1	\N	7	10	2006-10-06	\N	\N	\N
761	1	\N	7	10	2006-10-06	\N	\N	\N
758	1	\N	7	10	2006-10-06	\N	\N	\N
756	1	\N	7	10	2006-10-06	\N	\N	\N
755	1	\N	7	10	2006-10-06	\N	\N	\N
753	1	\N	7	10	2006-10-06	\N	\N	\N
752	1	\N	7	10	2006-10-06	\N	\N	\N
754	1	\N	7	10	2006-10-06	\N	\N	\N
757	1	\N	7	10	2006-10-06	\N	\N	\N
759	1	\N	7	10	2006-10-06	\N	\N	\N
766	1	\N	7	10	2006-10-06	\N	\N	\N
765	1	\N	7	10	2006-10-06	\N	\N	\N
764	1	\N	7	10	2006-10-06	\N	\N	\N
763	1	\N	7	10	2006-10-06	\N	\N	\N
762	1	\N	7	10	2006-10-06	\N	\N	\N
771	1	\N	7	10	2006-10-06	\N	\N	\N
770	1	\N	7	10	2006-10-06	\N	\N	\N
769	1	\N	7	10	2006-10-06	\N	\N	\N
768	1	\N	7	10	2006-10-06	\N	\N	\N
767	1	\N	7	10	2006-10-06	\N	\N	\N
484	3	\N	7	17	2006-10-11	\N	\N	\N
448	2	\N	1	1	2006-10-11	\N	\N	\N
1988	14	\N	6	10	\N	\N	\N	\N
1989	14	\N	6	10	\N	\N	\N	\N
1987	14	\N	7	10	2006-10-16	\N	\N	\N
1986	14	\N	7	10	2006-10-16	\N	\N	\N
1985	14	\N	7	10	2006-10-16	\N	\N	\N
1984	14	\N	7	10	2006-10-16	\N	\N	\N
1983	14	\N	7	10	2006-10-16	\N	\N	\N
1982	14	\N	7	10	2006-10-16	\N	\N	\N
1981	14	\N	7	10	2006-10-16	\N	\N	\N
1980	14	\N	7	10	2006-10-16	\N	\N	\N
1979	14	\N	7	10	2006-10-16	\N	\N	\N
1978	14	\N	7	10	2006-10-16	\N	\N	\N
1977	14	\N	7	10	2006-10-16	\N	\N	\N
1976	14	\N	7	10	2006-10-16	\N	\N	\N
1975	14	\N	7	10	2006-10-16	\N	\N	\N
1974	14	\N	7	10	2006-10-16	\N	\N	\N
1973	14	\N	7	10	2006-10-16	\N	\N	\N
1972	14	\N	7	10	2006-10-16	\N	\N	\N
1971	14	\N	7	10	2006-10-16	\N	\N	\N
1970	14	\N	7	10	2006-10-16	\N	\N	\N
1969	14	\N	7	10	2006-10-16	\N	\N	\N
1968	14	\N	7	10	2006-10-16	\N	\N	\N
1967	14	\N	7	10	2006-10-16	\N	\N	\N
1966	14	\N	7	10	2006-10-16	\N	\N	\N
1965	14	\N	7	10	2006-10-16	\N	\N	\N
1964	14	\N	7	10	2006-10-16	\N	\N	\N
1963	14	\N	7	10	2006-10-16	\N	\N	\N
1962	14	\N	7	10	2006-10-16	\N	\N	\N
1961	14	\N	7	10	2006-10-16	\N	\N	\N
1960	14	\N	7	10	2006-10-16	\N	\N	\N
1959	14	\N	7	10	2006-10-16	\N	\N	\N
1958	14	\N	7	10	2006-10-16	\N	\N	\N
1957	14	\N	7	10	2006-10-16	\N	\N	\N
1956	14	\N	7	10	2006-10-16	\N	\N	\N
1955	14	\N	7	10	2006-10-16	\N	\N	\N
1954	14	\N	7	10	2006-10-16	\N	\N	\N
1953	14	\N	7	10	2006-10-16	\N	\N	\N
1952	14	\N	7	10	2006-10-16	\N	\N	\N
1951	14	\N	7	10	2006-10-16	\N	\N	\N
1950	14	\N	7	10	2006-10-16	\N	\N	\N
1949	14	\N	7	10	2006-10-16	\N	\N	\N
1948	14	\N	7	10	2006-10-16	\N	\N	\N
1947	14	\N	7	10	2006-10-16	\N	\N	\N
1946	14	\N	7	10	2006-10-16	\N	\N	\N
1990	13	\N	7	10	2006-10-16	\N	\N	\N
1991	13	\N	7	10	2006-10-16	\N	\N	\N
1992	13	\N	7	10	2006-10-16	\N	\N	\N
1993	13	\N	7	10	2006-10-16	\N	\N	\N
1994	13	\N	7	10	2006-10-16	\N	\N	\N
1995	13	\N	7	10	2006-10-16	\N	\N	\N
1996	13	\N	7	10	2006-10-16	\N	\N	\N
1997	13	\N	7	10	2006-10-16	\N	\N	\N
1998	13	\N	7	10	2006-10-16	\N	\N	\N
1999	13	\N	7	10	2006-10-16	\N	\N	\N
2000	13	\N	7	10	2006-10-16	\N	\N	\N
2001	13	\N	7	10	2006-10-16	\N	\N	\N
2002	13	\N	7	10	2006-10-16	\N	\N	\N
2003	13	\N	7	10	2006-10-16	\N	\N	\N
2004	13	\N	7	10	2006-10-16	\N	\N	\N
2005	13	\N	7	10	2006-10-16	\N	\N	\N
2006	13	\N	7	10	2006-10-16	\N	\N	\N
2007	13	\N	7	10	2006-10-16	\N	\N	\N
2008	13	\N	7	10	2006-10-16	\N	\N	\N
2009	13	\N	7	10	2006-10-16	\N	\N	\N
2010	13	\N	7	10	2006-10-16	\N	\N	\N
2011	13	\N	7	10	2006-10-16	\N	\N	\N
2012	13	\N	7	10	2006-10-16	\N	\N	\N
2013	13	\N	7	10	2006-10-16	\N	\N	\N
2014	13	\N	7	10	2006-10-16	\N	\N	\N
2015	13	\N	7	10	2006-10-16	\N	\N	\N
2016	13	\N	7	10	2006-10-16	\N	\N	\N
2017	13	\N	7	10	2006-10-16	\N	\N	\N
2018	13	\N	7	10	2006-10-16	\N	\N	\N
2019	13	\N	7	10	2006-10-16	\N	\N	\N
2020	13	\N	7	10	2006-10-16	\N	\N	\N
2021	13	\N	7	10	2006-10-16	\N	\N	\N
2022	13	\N	7	10	2006-10-16	\N	\N	\N
2023	13	\N	7	10	2006-10-16	\N	\N	\N
2024	13	\N	7	10	2006-10-16	\N	\N	\N
2025	13	\N	7	10	2006-10-16	\N	\N	\N
2026	13	\N	7	10	2006-10-16	\N	\N	\N
2027	13	\N	7	10	2006-10-16	\N	\N	\N
2028	13	\N	7	10	2006-10-16	\N	\N	\N
2029	13	\N	7	10	2006-10-16	\N	\N	\N
2030	13	\N	7	10	2006-10-16	\N	\N	\N
2031	13	\N	7	10	2006-10-16	\N	\N	\N
2032	13	\N	7	10	2006-10-16	\N	\N	\N
2033	13	\N	7	10	2006-10-16	\N	\N	\N
2034	13	\N	7	10	2006-10-16	\N	\N	\N
2035	13	\N	7	10	2006-10-16	\N	\N	\N
2036	13	\N	7	10	2006-10-16	\N	\N	\N
2037	13	\N	7	10	2006-10-16	\N	\N	\N
2069	13	\N	7	10	2006-10-16	\N	\N	\N
2068	13	\N	7	10	2006-10-16	\N	\N	\N
2067	13	\N	7	10	2006-10-16	\N	\N	\N
2066	13	\N	7	10	2006-10-16	\N	\N	\N
2065	13	\N	7	10	2006-10-16	\N	\N	\N
2064	13	\N	7	10	2006-10-16	\N	\N	\N
2063	13	\N	7	10	2006-10-16	\N	\N	\N
2062	13	\N	7	10	2006-10-16	\N	\N	\N
2061	13	\N	7	10	2006-10-16	\N	\N	\N
2060	13	\N	7	10	2006-10-16	\N	\N	\N
2059	13	\N	7	10	2006-10-16	\N	\N	\N
2058	13	\N	7	10	2006-10-16	\N	\N	\N
2057	13	\N	7	10	2006-10-16	\N	\N	\N
2056	13	\N	7	10	2006-10-16	\N	\N	\N
2055	13	\N	7	10	2006-10-16	\N	\N	\N
2054	13	\N	7	10	2006-10-16	\N	\N	\N
2053	13	\N	7	10	2006-10-16	\N	\N	\N
2052	13	\N	7	10	2006-10-16	\N	\N	\N
2051	13	\N	7	10	2006-10-16	\N	\N	\N
2050	13	\N	7	10	2006-10-16	\N	\N	\N
2049	13	\N	7	10	2006-10-16	\N	\N	\N
2048	13	\N	7	10	2006-10-16	\N	\N	\N
2047	13	\N	7	10	2006-10-16	\N	\N	\N
2046	13	\N	7	10	2006-10-16	\N	\N	\N
2045	13	\N	7	10	2006-10-16	\N	\N	\N
2044	13	\N	7	10	2006-10-16	\N	\N	\N
2043	13	\N	7	10	2006-10-16	\N	\N	\N
2042	13	\N	7	10	2006-10-16	\N	\N	\N
2041	13	\N	7	10	2006-10-16	\N	\N	\N
2040	13	\N	7	10	2006-10-16	\N	\N	\N
2039	13	\N	7	10	2006-10-16	\N	\N	\N
2038	13	\N	7	10	2006-10-16	\N	\N	\N
2117	13	\N	7	10	2006-10-16	\N	\N	\N
2116	13	\N	7	10	2006-10-16	\N	\N	\N
2115	13	\N	7	10	2006-10-16	\N	\N	\N
2114	13	\N	7	10	2006-10-16	\N	\N	\N
2113	13	\N	7	10	2006-10-16	\N	\N	\N
2112	13	\N	7	10	2006-10-16	\N	\N	\N
2111	13	\N	7	10	2006-10-16	\N	\N	\N
2110	13	\N	7	10	2006-10-16	\N	\N	\N
2109	13	\N	7	10	2006-10-16	\N	\N	\N
2108	13	\N	7	10	2006-10-16	\N	\N	\N
2107	13	\N	7	10	2006-10-16	\N	\N	\N
2106	13	\N	7	10	2006-10-16	\N	\N	\N
2105	13	\N	7	10	2006-10-16	\N	\N	\N
2104	13	\N	7	10	2006-10-16	\N	\N	\N
2103	13	\N	7	10	2006-10-16	\N	\N	\N
2102	13	\N	7	10	2006-10-16	\N	\N	\N
2101	13	\N	7	10	2006-10-16	\N	\N	\N
2100	13	\N	7	10	2006-10-16	\N	\N	\N
2099	13	\N	7	10	2006-10-16	\N	\N	\N
2098	13	\N	7	10	2006-10-16	\N	\N	\N
2097	13	\N	7	10	2006-10-16	\N	\N	\N
2096	13	\N	7	10	2006-10-16	\N	\N	\N
2095	13	\N	7	10	2006-10-16	\N	\N	\N
2094	13	\N	7	10	2006-10-16	\N	\N	\N
2093	13	\N	7	10	2006-10-16	\N	\N	\N
2092	13	\N	7	10	2006-10-16	\N	\N	\N
2091	13	\N	7	10	2006-10-16	\N	\N	\N
2090	13	\N	7	10	2006-10-16	\N	\N	\N
2089	13	\N	7	10	2006-10-16	\N	\N	\N
2088	13	\N	7	10	2006-10-16	\N	\N	\N
2087	13	\N	7	10	2006-10-16	\N	\N	\N
2086	13	\N	7	10	2006-10-16	\N	\N	\N
2085	13	\N	7	10	2006-10-16	\N	\N	\N
2084	13	\N	7	10	2006-10-16	\N	\N	\N
2083	13	\N	7	10	2006-10-16	\N	\N	\N
2082	13	\N	7	10	2006-10-16	\N	\N	\N
2081	13	\N	7	10	2006-10-16	\N	\N	\N
2080	13	\N	7	10	2006-10-16	\N	\N	\N
2079	13	\N	7	10	2006-10-16	\N	\N	\N
2078	13	\N	7	10	2006-10-16	\N	\N	\N
2077	13	\N	7	10	2006-10-16	\N	\N	\N
2076	13	\N	7	10	2006-10-16	\N	\N	\N
2075	13	\N	7	10	2006-10-16	\N	\N	\N
2074	13	\N	7	10	2006-10-16	\N	\N	\N
2073	13	\N	7	10	2006-10-16	\N	\N	\N
2072	13	\N	7	10	2006-10-16	\N	\N	\N
2071	13	\N	7	10	2006-10-16	\N	\N	\N
2070	13	\N	7	10	2006-10-16	\N	\N	\N
2118	13	\N	7	10	2006-10-16	\N	\N	\N
2119	13	\N	7	10	2006-10-16	\N	\N	\N
2120	13	\N	7	10	2006-10-16	\N	\N	\N
2121	13	\N	7	10	2006-10-16	\N	\N	\N
2122	13	\N	7	10	2006-10-16	\N	\N	\N
2123	13	\N	7	10	2006-10-16	\N	\N	\N
2124	13	\N	7	10	2006-10-16	\N	\N	\N
2125	13	\N	7	10	2006-10-16	\N	\N	\N
2126	13	\N	7	10	2006-10-16	\N	\N	\N
2127	13	\N	7	10	2006-10-16	\N	\N	\N
2128	13	\N	7	10	2006-10-16	\N	\N	\N
2129	13	\N	7	10	2006-10-16	\N	\N	\N
2130	13	\N	7	10	2006-10-16	\N	\N	\N
2131	13	\N	7	10	2006-10-16	\N	\N	\N
2132	13	\N	7	10	2006-10-16	\N	\N	\N
2133	13	\N	7	10	2006-10-16	\N	\N	\N
2134	13	\N	7	10	2006-10-16	\N	\N	\N
2135	13	\N	7	10	2006-10-16	\N	\N	\N
2136	13	\N	7	10	2006-10-16	\N	\N	\N
2137	13	\N	7	10	2006-10-16	\N	\N	\N
2138	13	\N	7	10	2006-10-16	\N	\N	\N
2139	13	\N	7	10	2006-10-16	\N	\N	\N
2140	13	\N	7	10	2006-10-16	\N	\N	\N
2141	13	\N	7	10	2006-10-16	\N	\N	\N
2142	13	\N	7	10	2006-10-16	\N	\N	\N
2143	13	\N	7	10	2006-10-16	\N	\N	\N
2144	13	\N	7	10	2006-10-16	\N	\N	\N
2145	13	\N	7	10	2006-10-16	\N	\N	\N
2146	13	\N	7	10	2006-10-16	\N	\N	\N
2147	13	\N	7	10	2006-10-16	\N	\N	\N
2148	13	\N	7	10	2006-10-16	\N	\N	\N
2149	13	\N	7	10	2006-10-16	\N	\N	\N
2158	13	\N	7	10	2006-10-16	\N	\N	\N
2159	13	\N	7	10	2006-10-16	\N	\N	\N
2160	13	\N	7	10	2006-10-16	\N	\N	\N
2161	13	\N	7	10	2006-10-16	\N	\N	\N
2162	13	\N	7	10	2006-10-16	\N	\N	\N
2163	13	\N	7	10	2006-10-16	\N	\N	\N
2164	13	\N	7	10	2006-10-16	\N	\N	\N
2165	13	\N	7	10	2006-10-16	\N	\N	\N
2166	13	\N	7	10	2006-10-16	\N	\N	\N
2167	13	\N	7	10	2006-10-16	\N	\N	\N
2168	13	\N	7	10	2006-10-16	\N	\N	\N
2169	13	\N	7	10	2006-10-16	\N	\N	\N
2170	13	\N	7	10	2006-10-16	\N	\N	\N
2171	13	\N	7	10	2006-10-16	\N	\N	\N
2172	13	\N	7	10	2006-10-16	\N	\N	\N
2173	13	\N	7	10	2006-10-16	\N	\N	\N
2174	13	\N	7	10	2006-10-16	\N	\N	\N
2175	13	\N	7	10	2006-10-16	\N	\N	\N
2176	13	\N	7	10	2006-10-16	\N	\N	\N
2177	13	\N	7	10	2006-10-16	\N	\N	\N
2178	13	\N	7	10	2006-10-16	\N	\N	\N
2179	13	\N	7	10	2006-10-16	\N	\N	\N
2180	13	\N	7	10	2006-10-16	\N	\N	\N
2181	13	\N	7	10	2006-10-16	\N	\N	\N
2182	13	\N	7	10	2006-10-16	\N	\N	\N
2183	13	\N	7	10	2006-10-16	\N	\N	\N
2184	13	\N	7	10	2006-10-16	\N	\N	\N
2185	13	\N	7	10	2006-10-16	\N	\N	\N
2186	13	\N	7	10	2006-10-16	\N	\N	\N
2187	13	\N	7	10	2006-10-16	\N	\N	\N
2188	13	\N	7	10	2006-10-16	\N	\N	\N
2189	13	\N	7	10	2006-10-16	\N	\N	\N
2190	13	\N	7	10	2006-10-16	\N	\N	\N
2191	13	\N	7	10	2006-10-16	\N	\N	\N
2192	13	\N	7	10	2006-10-16	\N	\N	\N
2193	13	\N	7	10	2006-10-16	\N	\N	\N
2194	13	\N	7	10	2006-10-16	\N	\N	\N
2195	13	\N	7	10	2006-10-16	\N	\N	\N
2196	13	\N	7	10	2006-10-16	\N	\N	\N
2197	13	\N	7	10	2006-10-16	\N	\N	\N
2198	13	\N	7	10	2006-10-16	\N	\N	\N
2199	13	\N	7	10	2006-10-16	\N	\N	\N
2200	13	\N	7	10	2006-10-16	\N	\N	\N
2201	13	\N	7	10	2006-10-16	\N	\N	\N
2202	13	\N	7	10	2006-10-16	\N	\N	\N
2203	13	\N	7	10	2006-10-16	\N	\N	\N
2204	13	\N	7	10	2006-10-16	\N	\N	\N
2205	13	\N	7	10	2006-10-16	\N	\N	\N
2206	13	\N	7	10	2006-10-16	\N	\N	\N
2207	13	\N	7	10	2006-10-16	\N	\N	\N
2208	13	\N	7	10	2006-10-16	\N	\N	\N
2209	13	\N	7	10	2006-10-16	\N	\N	\N
2210	13	\N	7	10	2006-10-16	\N	\N	\N
2211	13	\N	7	10	2006-10-16	\N	\N	\N
2212	13	\N	7	10	2006-10-16	\N	\N	\N
2213	13	\N	7	10	2006-10-16	\N	\N	\N
2214	13	\N	7	10	2006-10-16	\N	\N	\N
2215	13	\N	7	10	2006-10-16	\N	\N	\N
2216	13	\N	7	10	2006-10-16	\N	\N	\N
2217	13	\N	7	10	2006-10-16	\N	\N	\N
2218	13	\N	7	10	2006-10-16	\N	\N	\N
2219	13	\N	7	10	2006-10-16	\N	\N	\N
2220	13	\N	7	10	2006-10-16	\N	\N	\N
2221	13	\N	7	10	2006-10-16	\N	\N	\N
2230	18	\N	1	10000	2006-10-16	\N	\N	\N
2225	18	\N	1	10000	2006-10-16	\N	\N	\N
2226	18	\N	1	10000	2006-10-16	\N	\N	\N
974	2	\N	5	8	2006-10-16	1	8	\N
2227	18	\N	1	10000	2006-10-16	\N	\N	\N
2228	18	\N	1	10000	2006-10-16	\N	\N	\N
2229	18	\N	1	10000	2006-10-16	\N	\N	\N
2224	18	\N	1	10000	2006-10-16	\N	\N	\N
2223	17	\N	21	10000	2006-10-16	\N	\N	\N
2222	17	\N	21	10000	2006-10-16	\N	\N	\N
1273	3	\N	21	14	2006-10-16	\N	\N	\N
1272	3	\N	21	14	2006-10-16	\N	\N	\N
2232	17	\N	21	10000	2006-10-16	\N	\N	\N
2231	17	\N	21	10000	2006-10-16	\N	\N	\N
712	3	\N	21	10	2006-10-16	\N	\N	\N
723	2	\N	21	10	2006-10-16	\N	\N	\N
681	3	\N	21	10	2006-10-16	\N	\N	\N
716	2	\N	21	10	2006-10-16	\N	\N	\N
2234	17	\N	21	10000	2006-10-16	\N	\N	\N
2233	17	\N	21	10000	2006-10-16	\N	\N	\N
558	3	\N	21	17	2006-10-16	\N	\N	\N
998	2	\N	21	8	2006-10-16	\N	\N	\N
2236	17	\N	21	10000	2006-10-16	\N	\N	\N
2235	17	\N	21	10000	2006-10-16	\N	\N	\N
557	3	\N	21	17	2006-10-16	\N	\N	\N
2238	17	\N	21	10000	2006-10-16	\N	\N	\N
2237	17	\N	21	10000	2006-10-16	\N	\N	\N
1270	3	\N	21	14	2006-10-16	\N	\N	\N
1403	2	\N	21	1	2006-10-16	\N	\N	\N
2240	17	\N	21	10000	2006-10-16	\N	\N	\N
2239	17	\N	21	10000	2006-10-16	\N	\N	\N
564	3	\N	21	17	2006-10-16	\N	\N	\N
972	2	\N	21	8	2006-10-16	\N	\N	\N
2242	17	\N	21	10000	2006-10-16	\N	\N	\N
2241	17	\N	21	10000	2006-10-16	\N	\N	\N
2244	16	\N	1	10000	2006-10-16	\N	\N	\N
2243	16	\N	1	10000	2006-10-16	\N	\N	\N
2246	6	\N	1	10000	2006-10-16	\N	\N	\N
2245	19	\N	1	10000	2006-10-16	\N	\N	\N
486	3	\N	7	17	2006-10-17	\N	\N	\N
1408	2	\N	7	1	2006-10-17	\N	\N	\N
2249	2	\N	7	10	2006-10-17	\N	\N	\N
2260	18	\N	1	10000	2006-10-17	\N	\N	\N
2259	18	\N	1	10000	2006-10-17	\N	\N	\N
2258	18	\N	1	10000	2006-10-17	\N	\N	\N
2255	17	\N	1	10000	2006-10-17	\N	\N	\N
2254	17	\N	1	10000	2006-10-17	\N	\N	\N
962	2	\N	21	8	2006-10-17	\N	\N	\N
668	3	\N	21	10	2006-10-17	\N	\N	\N
2253	17	\N	21	10000	2006-10-17	\N	\N	\N
2252	17	\N	21	10000	2006-10-17	\N	\N	\N
666	3	\N	21	10	2006-10-17	\N	\N	\N
966	2	\N	21	8	2006-10-17	\N	\N	\N
2257	17	\N	21	10000	2006-10-17	\N	\N	\N
2256	17	\N	21	10000	2006-10-17	\N	\N	\N
633	3	\N	21	10	2006-10-17	\N	\N	\N
956	2	\N	21	8	2006-10-17	\N	\N	\N
663	3	\N	5	10	2006-10-17	1	10	\N
2262	18	\N	1	10000	2006-10-17	\N	\N	\N
2263	18	\N	1	10000	2006-10-17	\N	\N	\N
2267	17	\N	21	10000	2006-10-17	\N	\N	\N
2266	17	\N	21	10000	2006-10-17	\N	\N	\N
651	3	\N	21	10	2006-10-17	\N	\N	\N
388	2	\N	21	1	2006-10-17	\N	\N	\N
2261	18	\N	1	10000	2006-10-17	\N	\N	\N
634	3	\N	21	10	2006-10-17	\N	\N	\N
387	2	\N	21	1	2006-10-17	\N	\N	\N
2269	17	\N	21	10000	2006-10-17	\N	\N	\N
2268	17	\N	21	10000	2006-10-17	\N	\N	\N
647	3	\N	21	10	2006-10-17	\N	\N	\N
959	2	\N	21	8	2006-10-17	\N	\N	\N
2265	17	\N	21	10000	2006-10-17	\N	\N	\N
2264	17	\N	21	10000	2006-10-17	\N	\N	\N
475	3	\N	18	17	2006-08-16	\N	\N	\N
431	2	\N	18	1	2006-07-24	\N	\N	\N
709	3	\N	21	10	2006-10-20	\N	\N	\N
2275	18	\N	1	10000	2006-10-20	\N	\N	\N
2274	17	\N	21	10000	2006-10-20	\N	\N	\N
2273	17	\N	21	10000	2006-10-20	\N	\N	\N
2278	18	\N	1	10000	2006-10-20	\N	\N	\N
485	3	\N	21	17	2006-10-20	\N	\N	\N
2251	2	\N	21	10	2006-10-20	\N	\N	\N
488	3	\N	21	17	2006-10-20	\N	\N	\N
2277	17	\N	21	10000	2006-10-20	\N	\N	\N
2276	17	\N	21	10000	2006-10-20	\N	\N	\N
1708	2	\N	1	10	2006-10-23	\N	\N	\N
1703	3	\N	1	10	2006-10-23	\N	\N	\N
1704	3	\N	1	10	2006-10-23	\N	\N	\N
1702	2	\N	1	10	2006-10-23	\N	\N	\N
650	3	\N	1	10	2006-10-26	\N	\N	\N
707	3	\N	1	10	2006-10-26	\N	\N	\N
1277	3	\N	1	1	2006-10-26	\N	\N	\N
721	2	\N	1	10	2006-10-26	\N	\N	\N
708	3	\N	1	10	2006-10-26	\N	\N	\N
659	3	\N	1	10	2006-10-26	\N	\N	\N
1416	2	\N	1	1	2006-10-26	\N	\N	\N
1428	2	\N	1	10	2006-10-26	\N	\N	\N
2279	3	\N	7	10	2006-10-30	\N	\N	\N
2289	3	\N	18	10	\N	\N	\N	\N
2285	2	\N	7	10	2006-10-30	\N	\N	\N
2328	14	\N	7	10	2006-10-31	\N	\N	\N
2327	14	\N	7	10	2006-10-31	\N	\N	\N
2326	14	\N	7	10	2006-10-31	\N	\N	\N
2325	14	\N	7	10	2006-10-31	\N	\N	\N
2324	14	\N	7	10	2006-10-31	\N	\N	\N
2323	14	\N	7	10	2006-10-31	\N	\N	\N
2322	14	\N	7	10	2006-10-31	\N	\N	\N
2321	14	\N	7	10	2006-10-31	\N	\N	\N
2320	14	\N	7	10	2006-10-31	\N	\N	\N
2319	14	\N	7	10	2006-10-31	\N	\N	\N
2318	14	\N	7	10	2006-10-31	\N	\N	\N
2317	14	\N	7	10	2006-10-31	\N	\N	\N
2316	14	\N	7	10	2006-10-31	\N	\N	\N
2315	14	\N	7	10	2006-10-31	\N	\N	\N
2314	14	\N	7	10	2006-10-31	\N	\N	\N
2313	14	\N	7	10	2006-10-31	\N	\N	\N
2312	14	\N	7	10	2006-10-31	\N	\N	\N
2311	14	\N	7	10	2006-10-31	\N	\N	\N
2310	14	\N	7	10	2006-10-31	\N	\N	\N
2309	14	\N	7	10	2006-10-31	\N	\N	\N
2308	14	\N	7	10	2006-10-31	\N	\N	\N
2307	14	\N	7	10	2006-10-31	\N	\N	\N
2306	14	\N	7	10	2006-10-31	\N	\N	\N
2305	14	\N	7	10	2006-10-31	\N	\N	\N
2304	14	\N	7	10	2006-10-31	\N	\N	\N
2303	14	\N	7	10	2006-10-31	\N	\N	\N
2302	14	\N	7	10	2006-10-31	\N	\N	\N
2301	14	\N	7	10	2006-10-31	\N	\N	\N
2300	14	\N	7	10	2006-10-31	\N	\N	\N
2331	1	\N	7	10	2006-10-31	\N	\N	\N
2330	1	\N	7	10	2006-10-31	\N	\N	\N
2329	1	\N	7	10	2006-10-31	\N	\N	\N
2339	1	\N	7	10	2006-10-31	\N	\N	\N
2338	1	\N	7	10	2006-10-31	\N	\N	\N
2337	1	\N	7	10	2006-10-31	\N	\N	\N
2336	1	\N	7	10	2006-10-31	\N	\N	\N
2335	1	\N	7	10	2006-10-31	\N	\N	\N
2334	1	\N	7	10	2006-10-31	\N	\N	\N
2333	1	\N	7	10	2006-10-31	\N	\N	\N
2350	1	\N	7	10	2006-10-31	\N	\N	\N
2351	1	\N	7	10	2006-10-31	\N	\N	\N
2352	1	\N	7	10	2006-10-31	\N	\N	\N
2355	1	\N	7	10	2006-10-31	\N	\N	\N
2356	1	\N	7	10	2006-10-31	\N	\N	\N
2353	1	\N	7	10	2006-10-31	\N	\N	\N
2354	1	\N	7	10	2006-10-31	\N	\N	\N
2358	1	\N	7	10	2006-10-31	\N	\N	\N
2359	1	\N	7	10	2006-10-31	\N	\N	\N
2357	1	\N	7	10	2006-10-31	\N	\N	\N
2360	1	\N	7	10	2006-10-31	\N	\N	\N
2362	1	\N	7	10	2006-10-31	\N	\N	\N
2364	1	\N	7	10	2006-10-31	\N	\N	\N
2366	1	\N	7	10	2006-10-31	\N	\N	\N
2368	1	\N	7	10	2006-10-31	\N	\N	\N
2369	1	\N	7	10	2006-10-31	\N	\N	\N
2367	1	\N	7	10	2006-10-31	\N	\N	\N
2365	1	\N	7	10	2006-10-31	\N	\N	\N
2363	1	\N	7	10	2006-10-31	\N	\N	\N
2361	1	\N	7	10	2006-10-31	\N	\N	\N
2379	1	\N	7	10	2006-10-31	\N	\N	\N
2376	1	\N	7	10	2006-10-31	\N	\N	\N
2375	1	\N	7	10	2006-10-31	\N	\N	\N
2374	1	\N	7	10	2006-10-31	\N	\N	\N
2373	1	\N	7	10	2006-10-31	\N	\N	\N
2370	1	\N	7	10	2006-10-31	\N	\N	\N
2371	1	\N	7	10	2006-10-31	\N	\N	\N
2372	1	\N	7	10	2006-10-31	\N	\N	\N
2377	1	\N	7	10	2006-10-31	\N	\N	\N
2378	1	\N	7	10	2006-10-31	\N	\N	\N
2388	1	\N	7	10	2006-10-31	\N	\N	\N
2384	1	\N	7	10	2006-10-31	\N	\N	\N
2383	1	\N	7	10	2006-10-31	\N	\N	\N
2382	1	\N	7	10	2006-10-31	\N	\N	\N
2381	1	\N	7	10	2006-10-31	\N	\N	\N
2380	1	\N	7	10	2006-10-31	\N	\N	\N
2385	1	\N	7	10	2006-10-31	\N	\N	\N
2386	1	\N	7	10	2006-10-31	\N	\N	\N
2387	1	\N	7	10	2006-10-31	\N	\N	\N
2389	1	\N	7	10	2006-10-31	\N	\N	\N
2398	1	\N	7	10	2006-10-31	\N	\N	\N
2396	1	\N	7	10	2006-10-31	\N	\N	\N
2395	1	\N	7	10	2006-10-31	\N	\N	\N
2391	1	\N	7	10	2006-10-31	\N	\N	\N
2390	1	\N	7	10	2006-10-31	\N	\N	\N
2392	1	\N	7	10	2006-10-31	\N	\N	\N
2393	1	\N	7	10	2006-10-31	\N	\N	\N
2394	1	\N	7	10	2006-10-31	\N	\N	\N
2397	1	\N	7	10	2006-10-31	\N	\N	\N
2399	1	\N	7	10	2006-10-31	\N	\N	\N
2406	13	\N	7	10	2006-10-31	\N	\N	\N
2405	13	\N	7	10	2006-10-31	\N	\N	\N
2404	13	\N	7	10	2006-10-31	\N	\N	\N
2403	13	\N	7	10	2006-10-31	\N	\N	\N
2401	13	\N	7	10	2006-10-31	\N	\N	\N
2402	13	\N	7	10	2006-10-31	\N	\N	\N
2444	13	\N	6	10	\N	\N	\N	\N
2445	13	\N	6	10	\N	\N	\N	\N
2446	13	\N	6	10	\N	\N	\N	\N
2447	13	\N	6	10	\N	\N	\N	\N
2448	13	\N	6	10	\N	\N	\N	\N
2449	13	\N	6	10	\N	\N	\N	\N
2450	13	\N	6	10	\N	\N	\N	\N
2451	13	\N	6	10	\N	\N	\N	\N
2452	13	\N	6	10	\N	\N	\N	\N
2453	13	\N	6	10	\N	\N	\N	\N
2454	13	\N	6	10	\N	\N	\N	\N
2455	13	\N	6	10	\N	\N	\N	\N
2456	13	\N	6	10	\N	\N	\N	\N
2457	13	\N	6	10	\N	\N	\N	\N
2458	13	\N	6	10	\N	\N	\N	\N
2459	13	\N	6	10	\N	\N	\N	\N
2460	13	\N	6	10	\N	\N	\N	\N
2461	13	\N	6	10	\N	\N	\N	\N
2462	13	\N	6	10	\N	\N	\N	\N
2463	13	\N	6	10	\N	\N	\N	\N
2464	13	\N	6	10	\N	\N	\N	\N
2465	13	\N	6	10	\N	\N	\N	\N
2466	13	\N	6	10	\N	\N	\N	\N
2467	13	\N	6	10	\N	\N	\N	\N
2468	13	\N	6	10	\N	\N	\N	\N
2469	13	\N	6	10	\N	\N	\N	\N
2470	13	\N	6	10	\N	\N	\N	\N
2471	13	\N	6	10	\N	\N	\N	\N
2472	13	\N	6	10	\N	\N	\N	\N
2473	13	\N	6	10	\N	\N	\N	\N
2474	13	\N	6	10	\N	\N	\N	\N
2475	13	\N	6	10	\N	\N	\N	\N
2476	13	\N	6	10	\N	\N	\N	\N
2477	13	\N	6	10	\N	\N	\N	\N
2478	13	\N	6	10	\N	\N	\N	\N
2479	13	\N	6	10	\N	\N	\N	\N
2480	13	\N	6	10	\N	\N	\N	\N
2481	13	\N	6	10	\N	\N	\N	\N
2482	13	\N	6	10	\N	\N	\N	\N
2483	13	\N	6	10	\N	\N	\N	\N
2484	13	\N	6	10	\N	\N	\N	\N
2485	13	\N	6	10	\N	\N	\N	\N
2486	13	\N	6	10	\N	\N	\N	\N
2487	13	\N	6	10	\N	\N	\N	\N
2488	13	\N	6	10	\N	\N	\N	\N
2489	13	\N	6	10	\N	\N	\N	\N
2490	13	\N	6	10	\N	\N	\N	\N
2493	13	\N	6	10	\N	\N	\N	\N
2494	13	\N	6	10	\N	\N	\N	\N
2495	13	\N	6	10	\N	\N	\N	\N
2496	13	\N	6	10	\N	\N	\N	\N
2497	13	\N	6	10	\N	\N	\N	\N
2498	13	\N	6	10	\N	\N	\N	\N
2499	13	\N	6	10	\N	\N	\N	\N
2500	13	\N	6	10	\N	\N	\N	\N
2501	13	\N	6	10	\N	\N	\N	\N
2502	13	\N	6	10	\N	\N	\N	\N
2503	13	\N	6	10	\N	\N	\N	\N
2504	13	\N	6	10	\N	\N	\N	\N
2505	13	\N	6	10	\N	\N	\N	\N
2506	13	\N	6	10	\N	\N	\N	\N
2507	13	\N	6	10	\N	\N	\N	\N
2508	13	\N	6	10	\N	\N	\N	\N
2509	13	\N	6	10	\N	\N	\N	\N
2510	13	\N	6	10	\N	\N	\N	\N
2511	13	\N	6	10	\N	\N	\N	\N
2512	13	\N	6	10	\N	\N	\N	\N
2513	13	\N	6	10	\N	\N	\N	\N
2514	13	\N	6	10	\N	\N	\N	\N
2515	13	\N	6	10	\N	\N	\N	\N
2516	13	\N	6	10	\N	\N	\N	\N
2517	13	\N	6	10	\N	\N	\N	\N
2518	13	\N	6	10	\N	\N	\N	\N
2519	13	\N	6	10	\N	\N	\N	\N
2520	13	\N	6	10	\N	\N	\N	\N
2521	13	\N	6	10	\N	\N	\N	\N
2522	13	\N	6	10	\N	\N	\N	\N
2523	13	\N	6	10	\N	\N	\N	\N
2524	13	\N	6	10	\N	\N	\N	\N
2525	13	\N	6	10	\N	\N	\N	\N
2526	13	\N	6	10	\N	\N	\N	\N
2527	13	\N	6	10	\N	\N	\N	\N
2528	13	\N	6	10	\N	\N	\N	\N
2529	13	\N	6	10	\N	\N	\N	\N
2530	13	\N	6	10	\N	\N	\N	\N
2531	13	\N	6	10	\N	\N	\N	\N
2532	13	\N	6	10	\N	\N	\N	\N
2533	13	\N	6	10	\N	\N	\N	\N
2534	13	\N	6	10	\N	\N	\N	\N
2535	13	\N	6	10	\N	\N	\N	\N
2536	13	\N	6	10	\N	\N	\N	\N
2537	13	\N	6	10	\N	\N	\N	\N
2538	13	\N	6	10	\N	\N	\N	\N
2539	13	\N	6	10	\N	\N	\N	\N
2540	13	\N	6	10	\N	\N	\N	\N
2541	13	\N	6	10	\N	\N	\N	\N
2542	13	\N	6	10	\N	\N	\N	\N
2543	13	\N	6	10	\N	\N	\N	\N
2544	13	\N	6	10	\N	\N	\N	\N
2545	13	\N	6	10	\N	\N	\N	\N
2546	13	\N	6	10	\N	\N	\N	\N
2547	13	\N	6	10	\N	\N	\N	\N
2548	13	\N	6	10	\N	\N	\N	\N
2549	13	\N	6	10	\N	\N	\N	\N
2409	13	\N	7	10	2006-10-31	\N	\N	\N
2408	13	\N	7	10	2006-10-31	\N	\N	\N
2407	13	\N	7	10	2006-10-31	\N	\N	\N
2410	13	\N	7	10	2006-10-31	\N	\N	\N
2411	13	\N	7	10	2006-10-31	\N	\N	\N
2412	13	\N	7	10	2006-10-31	\N	\N	\N
2413	13	\N	7	10	2006-10-31	\N	\N	\N
2416	13	\N	7	10	2006-10-31	\N	\N	\N
2415	13	\N	7	10	2006-10-31	\N	\N	\N
2414	13	\N	7	10	2006-10-31	\N	\N	\N
2417	13	\N	7	10	2006-10-31	\N	\N	\N
2418	13	\N	7	10	2006-10-31	\N	\N	\N
2419	13	\N	7	10	2006-10-31	\N	\N	\N
2420	13	\N	7	10	2006-10-31	\N	\N	\N
2423	13	\N	7	10	2006-10-31	\N	\N	\N
2422	13	\N	7	10	2006-10-31	\N	\N	\N
2421	13	\N	7	10	2006-10-31	\N	\N	\N
2424	13	\N	7	10	2006-10-31	\N	\N	\N
2425	13	\N	7	10	2006-10-31	\N	\N	\N
2426	13	\N	7	10	2006-10-31	\N	\N	\N
2439	13	\N	7	10	2006-10-31	\N	\N	\N
2440	13	\N	7	10	2006-10-31	\N	\N	\N
2441	13	\N	7	10	2006-10-31	\N	\N	\N
2442	13	\N	7	10	2006-10-31	\N	\N	\N
2438	13	\N	7	10	2006-10-31	\N	\N	\N
2437	13	\N	7	10	2006-10-31	\N	\N	\N
2436	13	\N	7	10	2006-10-31	\N	\N	\N
2435	13	\N	7	10	2006-10-31	\N	\N	\N
2427	13	\N	7	10	2006-10-31	\N	\N	\N
2430	13	\N	7	10	2006-10-31	\N	\N	\N
2431	13	\N	7	10	2006-10-31	\N	\N	\N
2434	13	\N	7	10	2006-10-31	\N	\N	\N
2433	13	\N	7	10	2006-10-31	\N	\N	\N
2432	13	\N	7	10	2006-10-31	\N	\N	\N
2429	13	\N	7	10	2006-10-31	\N	\N	\N
2428	13	\N	7	10	2006-10-31	\N	\N	\N
2568	16	\N	6	11	\N	\N	\N	\N
2569	16	\N	6	11	\N	\N	\N	\N
2570	16	\N	6	11	\N	\N	\N	\N
2571	16	\N	6	11	\N	\N	\N	\N
2572	16	\N	6	11	\N	\N	\N	\N
2573	16	\N	6	11	\N	\N	\N	\N
2574	16	\N	6	11	\N	\N	\N	\N
2575	16	\N	6	11	\N	\N	\N	\N
2576	16	\N	6	11	\N	\N	\N	\N
2577	16	\N	6	11	\N	\N	\N	\N
2578	16	\N	6	11	\N	\N	\N	\N
2579	16	\N	6	11	\N	\N	\N	\N
2580	16	\N	6	11	\N	\N	\N	\N
2581	16	\N	6	11	\N	\N	\N	\N
2582	16	\N	6	11	\N	\N	\N	\N
2291	3	\N	1	8	2006-10-31	\N	\N	\N
2596	14	\N	18	14	2006-11-01	\N	\N	\N
2635	14	\N	7	14	2006-11-01	\N	\N	\N
2634	14	\N	7	14	2006-11-01	\N	\N	\N
2633	14	\N	7	14	2006-11-01	\N	\N	\N
2632	14	\N	7	14	2006-11-01	\N	\N	\N
2631	14	\N	7	14	2006-11-01	\N	\N	\N
2630	14	\N	7	14	2006-11-01	\N	\N	\N
2629	14	\N	7	14	2006-11-01	\N	\N	\N
2628	14	\N	7	14	2006-11-01	\N	\N	\N
2627	14	\N	7	14	2006-11-01	\N	\N	\N
2626	14	\N	7	14	2006-11-01	\N	\N	\N
2625	14	\N	7	14	2006-11-01	\N	\N	\N
2624	14	\N	7	14	2006-11-01	\N	\N	\N
2623	14	\N	7	14	2006-11-01	\N	\N	\N
2622	14	\N	7	14	2006-11-01	\N	\N	\N
2621	14	\N	7	14	2006-11-01	\N	\N	\N
2620	14	\N	7	14	2006-11-01	\N	\N	\N
2619	14	\N	7	14	2006-11-01	\N	\N	\N
2618	14	\N	7	14	2006-11-01	\N	\N	\N
2617	14	\N	7	14	2006-11-01	\N	\N	\N
2616	14	\N	7	14	2006-11-01	\N	\N	\N
2615	14	\N	7	14	2006-11-01	\N	\N	\N
2614	14	\N	7	14	2006-11-01	\N	\N	\N
2613	14	\N	7	14	2006-11-01	\N	\N	\N
2612	14	\N	7	14	2006-11-01	\N	\N	\N
2611	14	\N	7	14	2006-11-01	\N	\N	\N
2610	14	\N	7	14	2006-11-01	\N	\N	\N
2609	14	\N	7	14	2006-11-01	\N	\N	\N
2608	14	\N	7	14	2006-11-01	\N	\N	\N
2607	14	\N	7	14	2006-11-01	\N	\N	\N
2606	14	\N	7	14	2006-11-01	\N	\N	\N
2605	14	\N	7	14	2006-11-01	\N	\N	\N
2604	14	\N	7	14	2006-11-01	\N	\N	\N
2603	14	\N	7	14	2006-11-01	\N	\N	\N
2602	14	\N	7	14	2006-11-01	\N	\N	\N
2601	14	\N	7	14	2006-11-01	\N	\N	\N
2600	14	\N	7	14	2006-11-01	\N	\N	\N
2599	14	\N	7	14	2006-11-01	\N	\N	\N
2598	14	\N	7	14	2006-11-01	\N	\N	\N
2597	14	\N	7	14	2006-11-01	\N	\N	\N
2595	14	\N	7	14	2006-11-01	\N	\N	\N
2594	14	\N	7	14	2006-11-01	\N	\N	\N
2593	14	\N	7	14	2006-11-01	\N	\N	\N
2592	14	\N	7	14	2006-11-01	\N	\N	\N
2591	14	\N	7	14	2006-11-01	\N	\N	\N
2590	14	\N	7	14	2006-11-01	\N	\N	\N
2589	14	\N	7	14	2006-11-01	\N	\N	\N
2588	14	\N	7	14	2006-11-01	\N	\N	\N
2587	14	\N	7	14	2006-11-01	\N	\N	\N
2585	14	\N	7	14	2006-11-01	\N	\N	\N
2584	14	\N	7	14	2006-11-01	\N	\N	\N
2583	14	\N	7	14	2006-11-01	\N	\N	\N
2586	14	\N	7	14	2006-11-01	\N	\N	\N
2662	14	\N	6	17	\N	\N	\N	\N
2664	14	\N	6	17	\N	\N	\N	\N
2665	14	\N	6	17	\N	\N	\N	\N
2666	14	\N	6	17	\N	\N	\N	\N
2667	14	\N	6	17	\N	\N	\N	\N
2668	14	\N	6	17	\N	\N	\N	\N
2669	14	\N	6	17	\N	\N	\N	\N
2670	14	\N	6	17	\N	\N	\N	\N
2671	14	\N	6	17	\N	\N	\N	\N
2672	14	\N	6	17	\N	\N	\N	\N
2673	14	\N	6	17	\N	\N	\N	\N
2674	14	\N	6	17	\N	\N	\N	\N
2675	14	\N	6	17	\N	\N	\N	\N
2676	14	\N	6	17	\N	\N	\N	\N
2677	14	\N	6	17	\N	\N	\N	\N
2678	14	\N	6	17	\N	\N	\N	\N
2679	14	\N	6	17	\N	\N	\N	\N
2680	14	\N	6	17	\N	\N	\N	\N
2681	14	\N	6	17	\N	\N	\N	\N
2682	14	\N	6	17	\N	\N	\N	\N
2683	14	\N	6	17	\N	\N	\N	\N
2684	14	\N	6	17	\N	\N	\N	\N
2685	14	\N	6	17	\N	\N	\N	\N
2686	14	\N	6	17	\N	\N	\N	\N
2687	14	\N	6	17	\N	\N	\N	\N
2636	14	\N	7	14	2006-11-01	\N	\N	\N
2637	14	\N	7	14	2006-11-01	\N	\N	\N
2638	14	\N	7	14	2006-11-01	\N	\N	\N
2639	14	\N	7	14	2006-11-01	\N	\N	\N
2640	14	\N	7	14	2006-11-01	\N	\N	\N
2641	14	\N	7	14	2006-11-01	\N	\N	\N
2642	14	\N	7	14	2006-11-01	\N	\N	\N
2643	14	\N	7	14	2006-11-01	\N	\N	\N
2644	14	\N	7	14	2006-11-01	\N	\N	\N
2645	14	\N	7	14	2006-11-01	\N	\N	\N
2646	14	\N	7	14	2006-11-01	\N	\N	\N
2647	14	\N	7	14	2006-11-01	\N	\N	\N
2648	14	\N	7	14	2006-11-01	\N	\N	\N
2649	14	\N	7	14	2006-11-01	\N	\N	\N
2650	14	\N	7	14	2006-11-01	\N	\N	\N
2651	14	\N	7	14	2006-11-01	\N	\N	\N
2652	14	\N	7	14	2006-11-01	\N	\N	\N
2653	14	\N	7	14	2006-11-01	\N	\N	\N
2654	14	\N	7	14	2006-11-01	\N	\N	\N
2655	14	\N	7	14	2006-11-01	\N	\N	\N
2656	14	\N	7	14	2006-11-01	\N	\N	\N
2657	14	\N	7	14	2006-11-01	\N	\N	\N
2658	14	\N	7	14	2006-11-01	\N	\N	\N
2660	14	\N	7	14	2006-11-01	\N	\N	\N
2659	14	\N	7	14	2006-11-01	\N	\N	\N
2661	14	\N	7	14	2006-11-01	\N	\N	\N
491	3	\N	1	17	2006-11-02	\N	\N	\N
2694	16	\N	6	10	\N	\N	\N	\N
2693	18	\N	18	10	\N	\N	\N	\N
2695	16	\N	18	10	\N	\N	\N	\N
2709	2	\N	1	8	2006-11-08	\N	\N	\N
2711	17	\N	21	16	2006-11-09	\N	\N	\N
2712	17	\N	21	16	2006-11-09	\N	\N	\N
2297	3	\N	21	8	2006-11-09	\N	\N	\N
2689	17	\N	21	16	2006-11-09	\N	\N	\N
2688	17	\N	21	16	2006-11-09	\N	\N	\N
2713	17	\N	21	16	2006-11-09	\N	\N	\N
2714	17	\N	21	16	2006-11-09	\N	\N	\N
2715	17	\N	21	16	2006-11-09	\N	\N	\N
2716	17	\N	21	16	2006-11-09	\N	\N	\N
2293	3	\N	21	8	2006-11-09	\N	\N	\N
439	2	\N	21	1	2006-11-09	\N	\N	\N
490	3	\N	21	17	2006-11-09	\N	\N	\N
148	2	\N	21	1	2006-11-09	\N	\N	\N
2719	16	\N	6	16	\N	\N	\N	\N
2720	16	\N	6	16	\N	\N	\N	\N
498	3	\N	21	17	2006-11-10	1	1	\N
509	3	\N	21	17	2006-11-10	1	8	\N
499	3	\N	1	17	2006-11-10	\N	\N	\N
2732	2	\N	7	14	2006-11-10	\N	\N	\N
2730	2	\N	7	14	2006-11-10	\N	\N	\N
2724	2	\N	7	14	2006-11-10	\N	\N	\N
2723	2	\N	7	14	2006-11-10	\N	\N	\N
2744	2	\N	6	14	\N	\N	\N	\N
2746	2	\N	6	14	\N	\N	\N	\N
2748	2	\N	6	14	\N	\N	\N	\N
2756	13	\N	7	14	2006-11-10	\N	\N	\N
2755	13	\N	7	14	2006-11-10	\N	\N	\N
2754	13	\N	7	14	2006-11-10	\N	\N	\N
2753	13	\N	7	14	2006-11-10	\N	\N	\N
2752	13	\N	7	14	2006-11-10	\N	\N	\N
2751	13	\N	7	14	2006-11-10	\N	\N	\N
2750	13	\N	7	14	2006-11-10	\N	\N	\N
2749	13	\N	7	14	2006-11-10	\N	\N	\N
2690	18	\N	5	16	2006-11-10	\N	\N	\N
2718	18	\N	5	16	2006-11-10	\N	\N	\N
2717	18	\N	5	16	2006-11-10	\N	\N	\N
2710	18	\N	5	16	2006-11-10	\N	\N	\N
2758	17	\N	21	16	2006-11-15	\N	\N	\N
2759	17	\N	21	16	2006-11-15	\N	\N	\N
493	3	\N	21	17	2006-11-15	\N	\N	\N
2299	3	\N	21	8	2006-11-15	\N	\N	\N
2248	2	\N	21	10	2006-11-15	\N	\N	\N
2760	18	\N	5	16	2006-11-15	\N	\N	\N
2762	17	\N	21	16	2006-11-16	\N	\N	\N
2761	17	\N	21	16	2006-11-16	\N	\N	\N
497	3	\N	21	17	2006-11-16	\N	\N	\N
2765	16	\N	21	16	2006-11-16	\N	\N	\N
986	2	\N	21	8	2006-11-16	\N	\N	\N
2763	18	\N	5	16	2006-11-16	\N	\N	\N
368	2	\N	21	1	2006-11-16	\N	\N	\N
474	3	\N	21	17	2006-11-16	\N	\N	\N
473	3	\N	21	17	2006-11-16	\N	\N	\N
2764	16	\N	21	16	2006-11-16	\N	\N	\N
2802	2	\N	18	8	2006-11-22	\N	\N	\N
457	2	\N	5	1	2006-11-20	1	10	\N
492	3	\N	5	17	2006-11-20	1	10	\N
2290	3	\N	5	8	2006-11-20	1	10	\N
2770	16	\N	21	16	2006-11-20	\N	\N	\N
2769	16	\N	21	16	2006-11-20	\N	\N	\N
142	2	\N	5	1	2006-11-21	1	8	\N
2767	17	\N	21	16	2006-11-20	\N	\N	\N
2766	17	\N	21	16	2006-11-20	\N	\N	\N
2771	2	\N	6	10	\N	\N	\N	\N
2772	2	\N	6	10	\N	\N	\N	\N
2773	2	\N	6	10	\N	\N	\N	\N
2774	2	\N	6	10	\N	\N	\N	\N
2775	2	\N	6	10	\N	\N	\N	\N
2777	2	\N	5	10	2006-11-20	\N	\N	\N
2768	18	\N	5	16	2006-11-20	\N	\N	\N
500	3	\N	21	17	2006-11-21	\N	\N	\N
2783	16	\N	21	10	2006-11-21	\N	\N	\N
495	3	\N	21	17	2006-11-21	\N	\N	\N
2782	16	\N	21	10	2006-11-21	\N	\N	\N
923	2	\N	21	8	2006-11-21	\N	\N	\N
2781	17	\N	21	10	2006-11-21	\N	\N	\N
2780	17	\N	21	10	2006-11-21	\N	\N	\N
2779	18	\N	5	10	2006-11-21	\N	\N	\N
2787	3	\N	18	10	2006-11-21	\N	\N	\N
2786	3	\N	18	10	2006-11-21	\N	\N	\N
2796	16	\N	7	10	2006-11-21	\N	\N	\N
1411	2	\N	5	1	2006-11-22	\N	\N	\N
2803	2	\N	5	8	2006-11-22	\N	\N	\N
2804	2	\N	5	8	2006-11-23	1	8	\N
2699	2	\N	18	10	2006-11-28	\N	\N	\N
2807	2	\N	18	10	2006-11-22	\N	\N	\N
2812	3	\N	18	10	2006-11-22	\N	\N	\N
1700	2	\N	18	10	2006-10-23	\N	\N	\N
2808	2	\N	18	10	2006-11-22	\N	\N	\N
1451	2	\N	18	10	2006-10-20	\N	\N	\N
2810	2	\N	18	10	2006-11-22	\N	\N	\N
2809	2	\N	18	10	2006-11-22	\N	\N	\N
2778	2	\N	5	10	2006-11-23	\N	\N	\N
2805	2	\N	5	8	2006-11-23	\N	\N	\N
505	3	\N	21	17	2006-11-23	\N	\N	\N
2799	16	\N	21	10	2006-11-23	\N	\N	\N
989	2	\N	21	8	2006-11-23	\N	\N	\N
2817	17	\N	21	17	2006-11-23	\N	\N	\N
2818	17	\N	21	17	2006-11-23	\N	\N	\N
2816	18	\N	5	1	2006-11-23	\N	\N	\N
2821	17	\N	21	10	2006-11-24	\N	\N	\N
2820	17	\N	21	10	2006-11-24	\N	\N	\N
2819	18	\N	5	10	2006-11-24	\N	\N	\N
2822	2	\N	18	10	\N	\N	\N	\N
996	2	\N	5	8	2006-11-27	\N	\N	\N
2704	2	\N	5	10	2006-11-27	\N	\N	\N
2707	2	\N	5	10	2006-11-27	\N	\N	\N
2708	2	\N	5	10	2006-11-27	\N	\N	\N
1415	2	\N	5	1	2006-11-28	\N	\N	\N
2697	2	\N	5	10	2006-11-28	\N	\N	\N
2663	14	\N	1	17	2006-11-28	\N	\N	\N
1437	2	\N	18	10	2006-11-27	\N	\N	\N
2806	2	\N	5	10	2006-11-28	\N	\N	\N
433	2	\N	5	1	2006-11-28	\N	\N	\N
2700	2	\N	5	10	2006-11-28	\N	\N	\N
2703	2	\N	5	10	2006-11-28	\N	\N	\N
860	2	\N	5	10	2006-11-28	\N	\N	\N
2757	2	\N	5	14	2006-11-28	\N	\N	\N
2826	16	\N	7	10	2006-11-29	\N	\N	\N
456	2	\N	5	1	2006-11-29	\N	\N	\N
704	3	\N	5	10	2006-11-29	\N	\N	\N
2294	3	\N	21	8	2006-12-06	1	8	\N
2844	16	\N	21	10	2006-12-06	1	8	\N
932	2	\N	1	8	2006-11-29	1	8	\N
2837	17	\N	21	16	2006-11-29	\N	\N	\N
2836	17	\N	21	16	2006-11-29	\N	\N	\N
506	3	\N	18	17	2006-12-04	\N	\N	\N
2838	18	\N	5	16	2006-11-29	\N	\N	\N
2696	2	\N	5	10	2006-11-29	\N	\N	\N
2284	2	\N	18	10	2006-12-06	\N	\N	\N
2286	2	\N	5	10	2006-11-29	\N	\N	\N
2281	2	\N	5	10	2006-11-29	1	14	\N
2847	2	\N	6	10	\N	\N	\N	\N
259	13	\N	7	6	2006-12-01	\N	\N	\N
2855	17	\N	21	16	2006-12-04	\N	\N	\N
2854	17	\N	21	16	2006-12-04	\N	\N	\N
2853	17	\N	21	16	2006-12-04	\N	\N	\N
2852	17	\N	21	16	2006-12-04	\N	\N	\N
2856	18	\N	5	16	2006-12-04	\N	\N	\N
2857	18	\N	5	16	2006-12-04	\N	\N	\N
508	3	\N	21	17	2006-12-04	\N	\N	\N
2829	16	\N	21	10	2006-12-04	\N	\N	\N
1388	2	\N	21	8	2006-12-04	\N	\N	\N
2827	16	\N	21	10	2006-12-04	\N	\N	\N
1267	3	\N	21	14	2006-12-04	\N	\N	\N
2841	16	\N	21	10	2006-12-04	\N	\N	\N
1268	3	\N	21	14	2006-12-04	\N	\N	\N
2842	16	\N	21	10	2006-12-04	\N	\N	\N
438	2	\N	21	1	2006-12-04	\N	\N	\N
2859	17	\N	21	10	2006-12-06	\N	\N	\N
2858	17	\N	21	10	2006-12-06	\N	\N	\N
507	3	\N	21	17	2006-12-06	\N	\N	\N
2845	16	\N	21	10	2006-12-06	\N	\N	\N
2860	18	\N	5	10	2006-12-06	\N	\N	\N
2871	2	\N	5	16	2006-12-06	\N	\N	\N
2872	3	\N	5	16	2006-12-06	\N	\N	\N
2869	16	\N	5	16	2006-12-06	\N	\N	\N
2870	16	\N	5	16	2006-12-06	\N	\N	\N
710	3	\N	5	10	2006-12-11	\N	\N	\N
2920	16	\N	6	11	\N	\N	\N	\N
2921	16	\N	6	11	\N	\N	\N	\N
2922	16	\N	6	11	\N	\N	\N	\N
2923	16	\N	6	11	\N	\N	\N	\N
2924	16	\N	6	11	\N	\N	\N	\N
2925	16	\N	6	11	\N	\N	\N	\N
2926	16	\N	6	11	\N	\N	\N	\N
2927	16	\N	6	11	\N	\N	\N	\N
2928	16	\N	6	11	\N	\N	\N	\N
2929	16	\N	6	11	\N	\N	\N	\N
2930	16	\N	6	11	\N	\N	\N	\N
2931	16	\N	6	11	\N	\N	\N	\N
2932	16	\N	6	11	\N	\N	\N	\N
2933	16	\N	6	11	\N	\N	\N	\N
2934	16	\N	6	11	\N	\N	\N	\N
2935	16	\N	6	11	\N	\N	\N	\N
2936	16	\N	6	11	\N	\N	\N	\N
2937	16	\N	6	11	\N	\N	\N	\N
2938	16	\N	6	11	\N	\N	\N	\N
2939	16	\N	6	11	\N	\N	\N	\N
2940	16	\N	6	11	\N	\N	\N	\N
2941	16	\N	6	11	\N	\N	\N	\N
2942	16	\N	6	11	\N	\N	\N	\N
2943	16	\N	6	11	\N	\N	\N	\N
2944	16	\N	6	11	\N	\N	\N	\N
2945	16	\N	6	11	\N	\N	\N	\N
1258	3	\N	21	14	2006-12-13	\N	\N	\N
2867	16	\N	21	10	2006-12-13	\N	\N	\N
2877	2	\N	21	1	2006-12-13	\N	\N	\N
3045	17	\N	21	1	2006-12-13	\N	\N	\N
3044	17	\N	21	1	2006-12-13	\N	\N	\N
3046	18	\N	5	1	2006-12-13	\N	\N	\N
3025	2	\N	1	1	2006-12-13	\N	\N	\N
544	3	\N	1	17	2006-12-13	\N	\N	\N
2879	2	\N	5	1	2006-12-13	\N	\N	\N
2885	2	\N	5	1	2006-12-13	\N	\N	\N
1256	3	\N	5	14	2006-12-13	\N	\N	\N
2839	16	\N	5	10	2006-12-13	\N	\N	\N
2865	16	\N	5	10	2006-12-13	\N	\N	\N
2792	2	\N	5	10	2006-12-14	\N	\N	\N
3053	2	\N	5	16	2006-12-14	\N	\N	\N
3057	3	\N	21	14	2006-12-15	\N	\N	\N
2843	16	\N	21	10	2006-12-15	\N	\N	\N
3056	2	\N	21	14	2006-12-15	\N	\N	\N
3058	3	\N	21	14	2006-12-15	\N	\N	\N
3059	16	\N	21	14	2006-12-15	\N	\N	\N
3048	17	\N	21	10	2006-12-15	\N	\N	\N
3049	17	\N	21	10	2006-12-15	\N	\N	\N
3050	18	\N	5	10	2006-12-15	\N	\N	\N
3103	16	\N	5	1	2006-12-19	\N	\N	\N
3104	17	\N	21	8	2006-12-19	\N	\N	\N
3105	17	\N	21	8	2006-12-19	\N	\N	\N
3100	3	\N	21	17	2006-12-19	\N	\N	\N
2913	16	\N	21	11	2006-12-19	\N	\N	\N
3102	2	\N	21	17	2006-12-19	\N	\N	\N
3099	3	\N	21	17	2006-12-19	\N	\N	\N
2797	16	\N	21	10	2006-12-19	\N	\N	\N
3101	2	\N	21	17	2006-12-19	\N	\N	\N
3106	18	\N	5	8	2006-12-19	\N	\N	\N
3108	17	\N	21	8	2006-12-20	\N	\N	\N
3109	17	\N	21	8	2006-12-20	\N	\N	\N
549	3	\N	21	17	2006-12-20	\N	\N	\N
2894	16	\N	21	11	2006-12-20	\N	\N	\N
3107	18	\N	5	8	2006-12-20	\N	\N	\N
1439	2	\N	18	10	2006-10-04	\N	\N	\N
2840	16	\N	5	10	2007-01-04	1	10	\N
3112	18	\N	5	14	2006-12-20	\N	\N	\N
3116	17	\N	6	17	\N	\N	\N	\N
3117	17	\N	6	17	\N	\N	\N	\N
3114	17	\N	21	17	2006-12-22	\N	\N	\N
3115	17	\N	21	17	2006-12-22	\N	\N	\N
1263	3	\N	21	14	2006-12-22	\N	\N	\N
2912	16	\N	21	11	2006-12-22	\N	\N	\N
2887	2	\N	21	1	2006-12-22	\N	\N	\N
3113	18	\N	5	17	2006-12-22	\N	\N	\N
3118	2	\N	6	10	\N	\N	\N	\N
3119	2	\N	6	10	\N	\N	\N	\N
3120	2	\N	6	10	\N	\N	\N	\N
3121	2	\N	6	10	\N	\N	\N	\N
3122	2	\N	6	10	\N	\N	\N	\N
3123	2	\N	6	10	\N	\N	\N	\N
3124	17	\N	6	17	\N	\N	\N	\N
3125	17	\N	6	17	\N	\N	\N	\N
3126	17	\N	6	17	\N	\N	\N	\N
3130	17	\N	6	17	\N	\N	\N	\N
3131	17	\N	6	17	\N	\N	\N	\N
3132	18	\N	6	18	\N	\N	\N	\N
3133	18	\N	6	18	\N	\N	\N	\N
3134	18	\N	6	18	\N	\N	\N	\N
3135	18	\N	6	18	\N	\N	\N	\N
3136	18	\N	6	18	\N	\N	\N	\N
3137	18	\N	6	18	\N	\N	\N	\N
3138	17	\N	6	17	\N	\N	\N	\N
3139	18	\N	6	18	\N	\N	\N	\N
3140	18	\N	6	18	\N	\N	\N	\N
3141	18	\N	6	18	\N	\N	\N	\N
3142	18	\N	6	18	\N	\N	\N	\N
3143	18	\N	6	18	\N	\N	\N	\N
3144	18	\N	6	18	\N	\N	\N	\N
3145	18	\N	6	18	\N	\N	\N	\N
3146	18	\N	6	18	\N	\N	\N	\N
3147	18	\N	6	18	\N	\N	\N	\N
3148	18	\N	6	18	\N	\N	\N	\N
3149	18	\N	6	18	\N	\N	\N	\N
3150	18	\N	6	18	\N	\N	\N	\N
3151	18	\N	6	18	\N	\N	\N	\N
3152	18	\N	6	18	\N	\N	\N	\N
3153	18	\N	6	18	\N	\N	\N	\N
3154	18	\N	6	18	\N	\N	\N	\N
3155	18	\N	6	18	\N	\N	\N	\N
3156	18	\N	6	18	\N	\N	\N	\N
3157	18	\N	6	18	\N	\N	\N	\N
3158	18	\N	6	18	\N	\N	\N	\N
3159	18	\N	6	18	\N	\N	\N	\N
3160	18	\N	6	18	\N	\N	\N	\N
3161	18	\N	6	18	\N	\N	\N	\N
3162	18	\N	6	18	\N	\N	\N	\N
3163	18	\N	6	18	\N	\N	\N	\N
3164	18	\N	6	18	\N	\N	\N	\N
3165	18	\N	6	18	\N	\N	\N	\N
3166	18	\N	6	18	\N	\N	\N	\N
3167	18	\N	6	18	\N	\N	\N	\N
3168	18	\N	6	18	\N	\N	\N	\N
3169	18	\N	6	18	\N	\N	\N	\N
3170	18	\N	6	18	\N	\N	\N	\N
3043	2	\N	5	1	2007-01-05	\N	\N	\N
3177	16	\N	18	10	2007-01-04	\N	\N	\N
1413	2	\N	1	1	2007-01-05	\N	\N	\N
2851	2	\N	1	10	2007-01-05	\N	\N	\N
2850	2	\N	1	10	2007-01-05	\N	\N	\N
3176	16	\N	5	10	2007-01-05	1	10	\N
546	3	\N	18	17	2007-01-10	1	8	\N
2881	2	\N	5	1	2007-01-10	1	1	\N
2985	16	\N	7	11	2007-01-09	\N	\N	\N
2998	16	\N	7	11	2007-01-09	\N	\N	\N
2888	2	\N	5	1	2007-01-10	\N	\N	\N
554	3	\N	5	17	2007-01-10	\N	\N	\N
3172	16	\N	5	10	2007-01-10	\N	\N	\N
2834	16	\N	5	10	2007-01-10	\N	\N	\N
2692	2	\N	5	10	2007-01-11	\N	\N	\N
530	3	\N	5	17	2007-01-11	\N	\N	\N
3190	2	\N	5	10	2007-01-11	1	10	\N
541	3	\N	5	17	2007-01-12	1	10	\N
695	3	\N	5	10	2007-01-11	\N	\N	\N
2959	16	\N	5	11	2007-01-11	\N	\N	\N
3192	17	\N	21	8	2007-01-11	\N	\N	\N
3191	17	\N	21	8	2007-01-11	\N	\N	\N
553	3	\N	21	17	2007-01-11	\N	\N	\N
3068	16	\N	21	1	2007-01-11	\N	\N	\N
2880	2	\N	21	1	2007-01-11	\N	\N	\N
1265	3	\N	21	14	2007-01-11	\N	\N	\N
3066	16	\N	21	1	2007-01-11	\N	\N	\N
2882	2	\N	21	1	2007-01-11	\N	\N	\N
3193	18	\N	5	8	2007-01-11	\N	\N	\N
1266	3	\N	21	14	2007-01-12	\N	\N	\N
2908	16	\N	21	11	2007-01-12	\N	\N	\N
2884	2	\N	21	1	2007-01-12	\N	\N	\N
1261	3	\N	21	14	2007-01-12	\N	\N	\N
2800	16	\N	21	10	2007-01-12	\N	\N	\N
2886	2	\N	21	1	2007-01-12	\N	\N	\N
3194	17	\N	21	1	2007-01-12	\N	\N	\N
3195	17	\N	21	1	2007-01-12	\N	\N	\N
3196	18	\N	5	1	2007-01-12	\N	\N	\N
2794	16	\N	5	10	2007-01-12	\N	\N	\N
3038	2	\N	5	1	2007-01-12	\N	\N	\N
536	3	\N	21	17	2007-01-18	\N	\N	\N
3175	16	\N	21	10	2007-01-18	\N	\N	\N
3041	2	\N	21	1	2007-01-18	\N	\N	\N
2287	2	\N	18	10	2006-11-23	\N	\N	\N
538	3	\N	21	17	2007-01-18	\N	\N	\N
3174	16	\N	21	10	2007-01-18	\N	\N	\N
2727	2	\N	21	14	2007-01-18	\N	\N	\N
3228	17	\N	21	10	2007-01-18	\N	\N	\N
3227	17	\N	21	10	2007-01-18	\N	\N	\N
3229	18	\N	5	10	2007-01-18	\N	\N	\N
3223	2	\N	18	10	2007-01-18	\N	\N	\N
3224	2	\N	18	10	2007-01-18	\N	\N	\N
2283	2	\N	1	10	2007-01-23	\N	\N	\N
3226	3	\N	21	10	2007-02-13	1	10	\N
3287	16	\N	6	10	\N	\N	\N	\N
3200	2	\N	5	1	2007-01-19	1	10	\N
2340	1	\N	19	10	2007-01-22	\N	\N	\N
2341	1	\N	19	10	2007-01-22	\N	\N	\N
2342	1	\N	19	10	2007-01-22	\N	\N	\N
2343	1	\N	19	10	2007-01-22	\N	\N	\N
2344	1	\N	19	10	2007-01-22	\N	\N	\N
2345	1	\N	19	10	2007-01-22	\N	\N	\N
2346	1	\N	19	10	2007-01-22	\N	\N	\N
2347	1	\N	19	10	2007-01-22	\N	\N	\N
2348	1	\N	19	10	2007-01-22	\N	\N	\N
2349	1	\N	19	10	2007-01-22	\N	\N	\N
597	3	\N	5	1	2007-01-23	\N	\N	\N
3209	2	\N	18	1	2007-01-23	\N	\N	\N
3207	2	\N	18	1	2007-01-23	\N	\N	\N
3206	2	\N	18	1	2007-01-23	\N	\N	\N
3221	2	\N	18	10	2007-01-18	\N	\N	\N
3039	2	\N	5	1	2007-01-24	\N	\N	\N
540	3	\N	5	17	2007-01-24	\N	\N	\N
3060	16	\N	18	17	2007-01-24	\N	\N	\N
3309	17	\N	21	10	2007-01-24	\N	\N	\N
3310	17	\N	21	10	2007-01-24	\N	\N	\N
534	3	\N	21	17	2007-01-24	\N	\N	\N
2562	16	\N	21	11	2007-01-24	\N	\N	\N
2729	2	\N	21	14	2007-01-24	\N	\N	\N
539	3	\N	21	17	2007-01-24	\N	\N	\N
2561	16	\N	21	11	2007-01-24	\N	\N	\N
3034	2	\N	21	1	2007-01-24	\N	\N	\N
3375	18	\N	5	10	2007-01-24	\N	\N	\N
3307	17	\N	21	10	2007-01-24	\N	\N	\N
3308	17	\N	21	10	2007-01-24	\N	\N	\N
531	3	\N	21	17	2007-01-25	\N	\N	\N
2915	16	\N	21	11	2007-01-25	\N	\N	\N
2743	2	\N	21	14	2007-01-25	\N	\N	\N
532	3	\N	21	17	2007-01-25	\N	\N	\N
2830	16	\N	21	10	2007-01-25	\N	\N	\N
2731	2	\N	21	14	2007-01-25	\N	\N	\N
3381	3	\N	21	8	2007-01-29	\N	\N	\N
3380	3	\N	21	8	2007-01-29	\N	\N	\N
3384	3	\N	5	8	2007-03-16	\N	\N	\N
3383	3	\N	21	8	2007-03-19	\N	\N	\N
3385	3	\N	21	8	2007-03-19	\N	\N	\N
3387	3	\N	21	8	2007-03-19	\N	\N	\N
3382	3	\N	21	8	2007-03-23	\N	\N	\N
3386	3	\N	21	8	2007-03-23	\N	\N	\N
3371	2	\N	18	10	2007-01-24	\N	\N	\N
1404	2	\N	18	1	\N	\N	\N	\N
3391	18	\N	7	1	2007-01-26	\N	\N	\N
3295	17	\N	21	10	2007-01-26	\N	\N	\N
3296	17	\N	21	10	2007-01-26	\N	\N	\N
3297	17	\N	21	10	2007-01-26	\N	\N	\N
3298	17	\N	21	10	2007-01-26	\N	\N	\N
529	3	\N	21	17	2007-01-26	\N	\N	\N
2948	16	\N	21	11	2007-01-26	\N	\N	\N
3205	2	\N	21	1	2007-01-26	\N	\N	\N
528	3	\N	21	17	2007-01-26	\N	\N	\N
2947	16	\N	21	11	2007-01-26	\N	\N	\N
3204	2	\N	21	1	2007-01-26	\N	\N	\N
399	2	\N	5	1	2007-01-26	\N	\N	\N
3222	2	\N	5	10	2007-01-29	\N	\N	\N
3389	18	\N	5	1	2007-01-29	\N	\N	\N
3171	16	\N	21	10	2007-01-29	\N	\N	\N
1414	2	\N	21	1	2007-01-29	\N	\N	\N
2949	16	\N	21	11	2007-01-29	\N	\N	\N
3368	2	\N	21	8	2007-01-29	\N	\N	\N
3300	17	\N	21	10	2007-01-29	\N	\N	\N
3299	17	\N	21	10	2007-01-29	\N	\N	\N
3388	18	\N	5	1	2007-01-30	\N	\N	\N
3390	18	\N	5	1	2007-01-30	\N	\N	\N
3372	2	\N	18	10	2007-01-24	\N	\N	\N
3373	3	\N	18	10	2007-01-24	\N	\N	\N
3064	16	\N	18	1	2007-02-01	\N	\N	\N
3042	2	\N	5	1	2007-02-01	\N	\N	\N
2874	2	\N	5	1	2007-02-01	\N	\N	\N
550	3	\N	5	17	2007-02-01	\N	\N	\N
548	3	\N	5	17	2007-02-01	\N	\N	\N
3203	2	\N	18	1	2007-02-01	\N	\N	\N
3420	2	\N	5	10	2007-03-14	1	8	\N
3417	2	\N	5	10	2007-03-23	1	8	\N
3409	3	\N	21	1	2007-02-01	\N	\N	\N
3061	16	\N	21	1	2007-02-01	\N	\N	\N
3411	2	\N	21	1	2007-02-01	\N	\N	\N
3410	3	\N	21	1	2007-02-01	\N	\N	\N
3062	16	\N	21	1	2007-02-01	\N	\N	\N
3412	2	\N	21	1	2007-02-01	\N	\N	\N
3311	17	\N	21	10	2007-02-01	\N	\N	\N
3312	17	\N	21	10	2007-02-01	\N	\N	\N
3408	18	\N	5	1	2007-02-01	\N	\N	\N
3037	2	\N	5	1	2007-02-16	\N	\N	\N
3414	2	\N	5	10	2007-02-16	\N	\N	\N
615	3	\N	5	1	2007-02-16	\N	\N	\N
3413	3	\N	5	10	2007-02-16	\N	\N	\N
2953	16	\N	5	11	2007-02-16	\N	\N	\N
2835	16	\N	5	10	2007-02-16	\N	\N	\N
3415	2	\N	21	10	2007-02-26	\N	\N	\N
3424	2	\N	21	10	2007-03-06	\N	\N	\N
3423	2	\N	21	10	2007-03-06	\N	\N	\N
520	3	\N	21	17	2007-03-07	\N	\N	\N
523	3	\N	21	17	2007-03-07	\N	\N	\N
521	3	\N	21	17	2007-03-07	\N	\N	\N
524	3	\N	21	17	2007-03-16	\N	\N	\N
526	3	\N	21	17	2007-03-16	\N	\N	\N
525	3	\N	21	17	2007-03-16	\N	\N	\N
527	3	\N	21	17	2007-03-19	\N	\N	\N
3422	2	\N	5	10	2007-03-12	1	10	\N
522	3	\N	18	17	2007-03-15	\N	\N	\N
3421	2	\N	3	10	2007-03-14	1	10	\N
3419	2	\N	21	10	2007-03-14	1	14	\N
3401	3	\N	18	10	2007-01-31	\N	\N	\N
3402	2	\N	18	10	2007-01-31	\N	\N	\N
2846	16	\N	21	10	2007-02-07	1	8	\N
2876	2	\N	21	1	2007-02-07	1	8	\N
3215	2	\N	5	8	2007-02-05	\N	\N	\N
3426	2	\N	6	8	\N	\N	\N	\N
3427	2	\N	6	8	\N	\N	\N	\N
3428	2	\N	6	8	\N	\N	\N	\N
3429	2	\N	6	8	\N	\N	\N	\N
3430	2	\N	6	8	\N	\N	\N	\N
3431	2	\N	6	8	\N	\N	\N	\N
3432	2	\N	6	8	\N	\N	\N	\N
3433	2	\N	6	8	\N	\N	\N	\N
3434	2	\N	6	8	\N	\N	\N	\N
3435	2	\N	6	8	\N	\N	\N	\N
3436	2	\N	6	8	\N	\N	\N	\N
3437	2	\N	6	8	\N	\N	\N	\N
3438	2	\N	6	8	\N	\N	\N	\N
3439	2	\N	6	8	\N	\N	\N	\N
3440	2	\N	6	8	\N	\N	\N	\N
3441	2	\N	6	8	\N	\N	\N	\N
3442	2	\N	6	8	\N	\N	\N	\N
3443	2	\N	6	8	\N	\N	\N	\N
3444	2	\N	6	8	\N	\N	\N	\N
3445	2	\N	6	8	\N	\N	\N	\N
3301	17	\N	21	10	2007-02-07	\N	\N	\N
3302	17	\N	21	10	2007-02-07	\N	\N	\N
646	3	\N	21	10	2007-02-07	\N	\N	\N
2795	16	\N	21	10	2007-02-07	\N	\N	\N
3475	3	\N	7	10	2007-02-08	\N	\N	\N
3488	2	\N	5	8	2007-03-05	1	8	\N
3490	2	\N	5	8	2007-02-16	1	10	\N
3491	2	\N	5	8	2007-02-14	\N	\N	\N
3487	2	\N	21	8	2007-02-21	\N	\N	\N
3489	2	\N	21	8	2007-02-21	\N	\N	\N
3477	3	\N	21	10	2007-02-23	\N	\N	\N
3486	2	\N	21	8	2007-02-26	\N	\N	\N
3479	3	\N	21	10	2007-03-02	\N	\N	\N
3478	3	\N	21	10	2007-03-05	\N	\N	\N
3495	2	\N	5	8	2007-04-04	\N	\N	\N
3493	2	\N	5	8	2007-04-10	\N	\N	\N
3492	2	\N	21	8	2007-04-12	\N	\N	\N
3494	2	\N	5	8	2007-02-16	1	8	\N
724	2	\N	5	10	2007-02-14	1	10	\N
3305	17	\N	21	10	2007-02-12	\N	\N	\N
3306	17	\N	21	10	2007-02-12	\N	\N	\N
3481	3	\N	21	10	2007-02-12	\N	\N	\N
3063	16	\N	21	1	2007-02-12	\N	\N	\N
3202	2	\N	21	1	2007-02-12	\N	\N	\N
3480	3	\N	21	10	2007-02-12	\N	\N	\N
3230	16	\N	21	10	2007-02-12	\N	\N	\N
3201	2	\N	21	1	2007-02-12	\N	\N	\N
3393	18	\N	5	1	2007-02-12	\N	\N	\N
3304	17	\N	21	10	2007-02-13	\N	\N	\N
3303	17	\N	21	10	2007-02-13	\N	\N	\N
3468	3	\N	21	10	2007-02-13	\N	\N	\N
3241	16	\N	21	10	2007-02-13	\N	\N	\N
2566	16	\N	21	11	2007-02-13	\N	\N	\N
3392	18	\N	5	1	2007-02-13	\N	\N	\N
3498	18	\N	5	8	2007-02-13	\N	\N	\N
2823	2	\N	5	10	2007-02-14	\N	\N	\N
705	3	\N	5	10	2007-02-14	\N	\N	\N
3464	3	\N	5	10	2007-02-14	\N	\N	\N
3506	3	\N	18	10	2007-02-15	\N	\N	\N
1409	2	\N	5	1	2007-02-15	\N	\N	\N
3363	2	\N	5	8	2007-02-15	\N	\N	\N
3365	2	\N	5	8	2007-02-15	\N	\N	\N
641	3	\N	18	10	2007-01-11	\N	\N	\N
3502	2	\N	18	10	2007-02-15	\N	\N	\N
2811	3	\N	18	10	2006-11-22	\N	\N	\N
2567	16	\N	5	11	2007-02-16	\N	\N	\N
445	2	\N	5	1	2007-02-27	1	10	\N
3187	2	\N	5	10	2007-02-16	1	10	\N
3458	3	\N	5	10	2007-02-16	1	10	\N
3463	3	\N	5	10	2007-02-16	1	10	\N
3239	16	\N	5	10	2007-02-27	\N	\N	\N
3219	2	\N	5	8	2007-02-27	\N	\N	\N
3517	2	\N	5	10	2007-02-27	\N	\N	\N
2296	3	\N	5	8	2007-02-27	\N	\N	\N
3234	16	\N	21	10	\N	1	14	\N
3233	16	\N	21	10	\N	1	14	\N
561	3	\N	21	17	2007-02-27	1	10	\N
874	2	\N	21	10	2007-02-16	1	10	\N
3518	2	\N	5	10	2007-02-16	1	10	\N
3231	16	\N	21	10	2007-02-27	1	10	\N
3199	2	\N	5	10	2007-02-16	\N	\N	\N
3370	2	\N	5	10	2007-02-16	\N	\N	\N
3504	2	\N	18	10	2007-02-15	\N	\N	\N
1426	2	\N	18	10	2006-10-20	\N	\N	\N
3452	2	\N	5	8	2007-02-23	1	10	\N
3465	3	\N	21	10	2007-02-23	1	8	\N
3085	16	\N	21	1	2007-02-23	1	8	\N
3453	2	\N	5	8	2007-02-23	1	8	\N
3313	17	\N	21	10	2007-02-21	\N	\N	\N
3314	17	\N	21	10	2007-02-21	\N	\N	\N
3469	3	\N	21	10	2007-02-21	\N	\N	\N
2919	16	\N	21	11	2007-02-21	\N	\N	\N
3446	2	\N	21	8	2007-02-21	\N	\N	\N
3476	3	\N	21	10	2007-02-21	\N	\N	\N
3070	16	\N	21	1	2007-02-21	\N	\N	\N
3316	17	\N	21	10	2007-02-21	\N	\N	\N
3315	17	\N	21	10	2007-02-21	\N	\N	\N
489	3	\N	21	17	2007-02-21	\N	\N	\N
2868	16	\N	21	10	2007-02-21	\N	\N	\N
2741	2	\N	21	14	2007-02-21	\N	\N	\N
1260	3	\N	21	14	2007-02-21	\N	\N	\N
3098	16	\N	21	1	2007-02-21	\N	\N	\N
2883	2	\N	21	1	2007-02-21	\N	\N	\N
3521	18	\N	5	10	2007-02-21	\N	\N	\N
3317	17	\N	21	10	2007-02-21	\N	\N	\N
3318	17	\N	21	10	2007-02-21	\N	\N	\N
3457	3	\N	21	10	2007-02-21	\N	\N	\N
3235	16	\N	21	10	2007-02-21	\N	\N	\N
3321	17	\N	21	10	2007-02-21	\N	\N	\N
3322	17	\N	21	10	2007-02-21	\N	\N	\N
3520	18	\N	5	10	2007-02-21	\N	\N	\N
3522	18	\N	5	10	2007-02-21	\N	\N	\N
3213	2	\N	5	8	2007-02-23	\N	\N	\N
2866	16	\N	21	10	2007-02-23	\N	\N	\N
3323	17	\N	21	10	2007-02-23	\N	\N	\N
3324	17	\N	21	10	2007-02-23	\N	\N	\N
3524	18	\N	5	10	2007-02-23	\N	\N	\N
3325	17	\N	21	10	2007-02-26	\N	\N	\N
3326	17	\N	21	10	2007-02-26	\N	\N	\N
3470	3	\N	21	10	2007-02-26	\N	\N	\N
2798	16	\N	21	10	2007-02-26	\N	\N	\N
3447	2	\N	21	8	2007-02-26	\N	\N	\N
3456	3	\N	21	10	2007-02-26	\N	\N	\N
2917	16	\N	21	11	2007-02-26	\N	\N	\N
3525	18	\N	5	10	2007-02-26	\N	\N	\N
3237	16	\N	21	10	2007-02-26	\N	\N	\N
3523	18	\N	5	10	2007-02-26	\N	\N	\N
2956	16	\N	5	11	2007-02-26	\N	\N	\N
3533	3	\N	18	10	2007-02-26	\N	\N	\N
3029	2	\N	5	1	2007-02-27	\N	\N	\N
2893	2	\N	5	1	2007-02-27	\N	\N	\N
3548	2	\N	5	17	2007-02-27	1	8	\N
3545	2	\N	18	17	2007-02-27	\N	\N	\N
3547	2	\N	18	17	2007-02-27	\N	\N	\N
3554	2	\N	21	17	2007-02-27	1	10	\N
3553	2	\N	21	17	2007-02-27	1	14	\N
3550	2	\N	5	17	2007-02-27	1	8	\N
3552	2	\N	5	17	2007-03-12	1	10	\N
3549	2	\N	21	17	2007-02-27	1	1	\N
3551	2	\N	21	17	2007-02-27	1	8	\N
3245	16	\N	21	10	\N	1	8	\N
487	3	\N	21	17	2007-03-05	\N	\N	\N
140	2	\N	21	1	2007-03-05	\N	\N	\N
3244	16	\N	21	10	\N	1	1	\N
3555	3	\N	5	10	2007-03-12	1	10	\N
2790	2	\N	5	10	2007-03-06	1	8	\N
3032	2	\N	5	1	2007-02-28	\N	\N	\N
3534	17	\N	1	8	2007-02-28	\N	\N	\N
3543	17	\N	1	8	2007-02-28	\N	\N	\N
3027	2	\N	5	1	2007-03-01	\N	\N	\N
3031	2	\N	5	1	2007-03-01	\N	\N	\N
3336	17	\N	21	10	2007-03-01	\N	\N	\N
3335	17	\N	21	10	2007-03-01	\N	\N	\N
3572	3	\N	21	10	2007-03-01	\N	\N	\N
3247	16	\N	21	10	2007-03-01	\N	\N	\N
3571	3	\N	21	10	2007-03-01	\N	\N	\N
3246	16	\N	21	10	2007-03-01	\N	\N	\N
3218	2	\N	21	8	2007-03-01	\N	\N	\N
3573	18	\N	5	10	2007-03-01	\N	\N	\N
3253	16	\N	21	10	2007-03-02	\N	\N	\N
1389	2	\N	21	8	2007-03-02	\N	\N	\N
3328	17	\N	21	10	2007-03-05	\N	\N	\N
3327	17	\N	21	10	2007-03-05	\N	\N	\N
2863	16	\N	21	10	2007-03-05	\N	\N	\N
3526	18	\N	5	10	2007-03-05	\N	\N	\N
2564	16	\N	21	11	2007-03-05	\N	\N	\N
3466	3	\N	21	10	2007-03-05	\N	\N	\N
2910	16	\N	21	11	2007-03-05	\N	\N	\N
3329	17	\N	21	10	2007-03-05	\N	\N	\N
3330	17	\N	21	10	2007-03-05	\N	\N	\N
3527	18	\N	5	10	2007-03-05	\N	\N	\N
3331	17	\N	21	10	2007-03-06	\N	\N	\N
3332	17	\N	21	10	2007-03-06	\N	\N	\N
513	3	\N	21	17	2007-03-06	\N	\N	\N
3255	16	\N	21	10	2007-03-06	\N	\N	\N
512	3	\N	21	17	2007-03-06	\N	\N	\N
3254	16	\N	21	10	2007-03-06	\N	\N	\N
3528	18	\N	5	10	2007-03-06	\N	\N	\N
744	2	\N	5	10	2007-03-06	\N	\N	\N
3225	2	\N	5	10	2007-03-06	\N	\N	\N
3338	17	\N	21	10	2007-03-07	\N	\N	\N
3337	17	\N	21	10	2007-03-07	\N	\N	\N
3340	17	\N	21	10	2007-03-07	\N	\N	\N
3339	17	\N	21	10	2007-03-07	\N	\N	\N
3578	18	\N	5	10	2007-03-08	\N	\N	\N
3579	18	\N	5	10	2007-03-08	\N	\N	\N
3333	17	\N	21	10	2007-03-07	\N	\N	\N
3399	3	\N	21	8	2007-03-07	\N	\N	\N
3232	16	\N	21	10	2007-03-07	\N	\N	\N
3529	18	\N	5	10	2007-03-07	\N	\N	\N
3341	17	\N	21	10	2007-03-08	\N	\N	\N
3342	17	\N	21	10	2007-03-08	\N	\N	\N
3394	3	\N	21	8	2007-03-08	\N	\N	\N
2951	16	\N	21	11	2007-03-08	\N	\N	\N
3569	2	\N	21	8	2007-03-08	\N	\N	\N
3398	3	\N	21	8	2007-03-08	\N	\N	\N
2864	16	\N	21	10	2007-03-08	\N	\N	\N
3570	2	\N	21	8	2007-03-08	\N	\N	\N
3563	2	\N	21	8	2007-03-29	1	8	\N
3214	2	\N	5	8	2007-03-08	\N	\N	\N
3560	2	\N	21	8	2007-03-29	1	8	\N
3334	17	\N	21	10	2007-03-07	1	10	\N
3474	3	\N	21	10	2007-03-07	1	10	\N
2831	16	\N	21	10	2007-03-07	1	10	\N
3559	2	\N	5	8	2007-03-30	1	8	\N
3451	2	\N	5	8	2007-03-07	1	8	\N
2747	2	\N	5	14	2007-04-10	1	8	\N
430	2	\N	5	1	2007-03-12	\N	\N	\N
1394	2	\N	5	8	2007-03-13	\N	\N	\N
3054	2	\N	5	14	2007-03-13	\N	\N	\N
3354	17	\N	21	10	2007-03-15	\N	\N	\N
3353	17	\N	21	10	2007-03-15	\N	\N	\N
3270	16	\N	21	10	2007-03-15	\N	\N	\N
406	2	\N	21	1	2007-03-15	\N	\N	\N
3461	3	\N	21	10	2007-03-15	\N	\N	\N
3267	16	\N	21	10	2007-03-15	\N	\N	\N
3724	18	\N	5	17	2007-03-16	\N	\N	\N
3566	2	\N	21	8	2007-03-30	\N	\N	\N
3562	2	\N	5	8	2007-04-06	\N	\N	\N
3567	2	\N	5	8	2007-04-10	\N	\N	\N
3568	2	\N	5	8	2007-04-10	\N	\N	\N
3564	2	\N	5	8	2007-04-11	\N	\N	\N
3558	2	\N	21	8	2007-04-11	\N	\N	\N
3561	2	\N	21	8	2007-04-12	\N	\N	\N
3028	2	\N	21	1	2007-03-12	1	1	\N
3557	2	\N	18	8	2007-04-06	\N	\N	\N
3565	2	\N	18	8	2007-04-06	\N	\N	\N
3594	16	\N	21	10	\N	1	10	\N
3595	16	\N	21	10	\N	1	10	\N
3252	16	\N	21	10	2007-03-07	\N	\N	\N
401	2	\N	21	1	2007-03-07	\N	\N	\N
3484	3	\N	21	10	2007-03-07	\N	\N	\N
3249	16	\N	21	10	2007-03-07	\N	\N	\N
3361	2	\N	21	8	2007-03-07	\N	\N	\N
3599	16	\N	18	10	\N	4	10	\N
3250	16	\N	21	10	2007-03-07	\N	\N	\N
407	2	\N	21	1	2007-03-07	\N	\N	\N
3248	16	\N	21	10	2007-03-07	\N	\N	\N
403	2	\N	21	1	2007-03-07	\N	\N	\N
3600	16	\N	21	10	\N	1	10	\N
3580	18	\N	5	8	2007-03-08	\N	\N	\N
3601	16	\N	21	10	\N	1	10	\N
3604	16	\N	18	10	\N	\N	\N	\N
3472	3	\N	21	10	2007-03-08	\N	\N	\N
2862	16	\N	21	10	2007-03-08	\N	\N	\N
3051	2	\N	21	10	2007-03-08	\N	\N	\N
3473	3	\N	21	10	2007-03-08	\N	\N	\N
2909	16	\N	21	11	2007-03-08	\N	\N	\N
3344	17	\N	21	10	2007-03-08	\N	\N	\N
3343	17	\N	21	10	2007-03-08	\N	\N	\N
3581	18	\N	5	8	2007-03-08	\N	\N	\N
3602	16	\N	18	10	\N	4	1	\N
3450	2	\N	18	8	2007-03-08	\N	\N	\N
2735	2	\N	18	14	2007-03-01	\N	\N	\N
3597	16	\N	21	10	\N	1	8	\N
3585	3	\N	21	10	2007-03-12	\N	\N	\N
3278	16	\N	21	10	2007-03-12	\N	\N	\N
2745	2	\N	21	14	2007-03-12	\N	\N	\N
3348	17	\N	18	10	2007-03-12	\N	\N	\N
3251	16	\N	5	10	2007-03-12	\N	\N	\N
3256	16	\N	5	10	2007-03-12	\N	\N	\N
687	3	\N	5	10	2007-03-12	\N	\N	\N
3277	16	\N	5	10	2007-03-12	\N	\N	\N
3349	17	\N	21	10	2007-03-14	\N	\N	\N
3350	17	\N	21	10	2007-03-14	\N	\N	\N
3596	16	\N	6	10	\N	\N	\N	\N
3598	16	\N	6	10	\N	\N	\N	\N
3626	16	\N	6	10	\N	\N	\N	\N
3630	16	\N	6	10	\N	\N	\N	\N
3631	16	\N	6	10	\N	\N	\N	\N
3632	16	\N	6	10	\N	\N	\N	\N
3633	16	\N	6	10	\N	\N	\N	\N
3635	16	\N	6	10	\N	\N	\N	\N
3636	16	\N	6	10	\N	\N	\N	\N
3637	16	\N	6	10	\N	\N	\N	\N
3638	16	\N	6	10	\N	\N	\N	\N
3639	16	\N	6	10	\N	\N	\N	\N
3640	16	\N	6	10	\N	\N	\N	\N
3641	16	\N	6	10	\N	\N	\N	\N
3642	16	\N	6	10	\N	\N	\N	\N
3643	16	\N	6	10	\N	\N	\N	\N
3644	16	\N	6	10	\N	\N	\N	\N
3645	16	\N	6	10	\N	\N	\N	\N
3646	16	\N	6	10	\N	\N	\N	\N
3647	16	\N	6	10	\N	\N	\N	\N
3648	16	\N	6	10	\N	\N	\N	\N
3649	16	\N	6	10	\N	\N	\N	\N
3650	16	\N	6	10	\N	\N	\N	\N
3651	16	\N	6	10	\N	\N	\N	\N
3652	16	\N	6	10	\N	\N	\N	\N
3653	16	\N	6	10	\N	\N	\N	\N
3654	16	\N	6	10	\N	\N	\N	\N
3655	16	\N	6	10	\N	\N	\N	\N
3668	16	\N	6	10	\N	\N	\N	\N
3669	16	\N	6	10	\N	\N	\N	\N
3670	16	\N	6	10	\N	\N	\N	\N
3689	16	\N	21	10	\N	1	1	\N
3686	16	\N	21	10	\N	1	8	\N
3690	16	\N	21	10	\N	1	8	\N
3687	16	\N	1	10	\N	1	8	\N
3688	16	\N	1	10	\N	4	1	\N
3685	16	\N	21	10	\N	1	8	\N
3684	16	\N	5	10	\N	1	1	\N
3276	16	\N	18	10	2007-03-15	1	8	\N
3723	18	\N	5	17	2007-03-14	1	8	\N
3485	3	\N	21	10	2007-03-14	1	8	\N
2972	16	\N	21	11	2007-03-14	1	8	\N
2848	2	\N	21	10	2007-03-14	1	8	\N
1009	2	\N	5	8	2007-03-15	1	10	\N
696	3	\N	5	10	2007-03-14	\N	\N	\N
726	3	\N	5	8	2007-03-14	\N	\N	\N
2962	16	\N	5	11	2007-03-14	\N	\N	\N
2963	16	\N	5	11	2007-03-14	\N	\N	\N
3352	17	\N	21	10	2007-03-14	\N	\N	\N
3351	17	\N	21	10	2007-03-14	\N	\N	\N
3483	3	\N	21	10	2007-03-14	\N	\N	\N
3069	16	\N	21	1	2007-03-14	\N	\N	\N
3729	2	\N	1	10	2007-03-15	\N	\N	\N
3260	16	\N	5	10	2007-03-15	\N	\N	\N
3587	3	\N	5	1	2007-03-15	\N	\N	\N
3732	21	\N	5	10	2007-03-15	\N	\N	\N
537	3	\N	21	17	2007-03-15	\N	\N	\N
3065	16	\N	21	1	2007-03-15	\N	\N	\N
2726	2	\N	21	14	2007-03-15	\N	\N	\N
533	3	\N	21	17	2007-03-15	\N	\N	\N
2911	16	\N	21	11	2007-03-15	\N	\N	\N
2728	2	\N	21	14	2007-03-15	\N	\N	\N
3374	18	\N	5	10	2007-03-15	\N	\N	\N
3736	16	\N	5	10	2007-03-15	\N	\N	\N
3735	16	\N	5	10	2007-03-15	\N	\N	\N
502	3	\N	18	17	2007-03-23	\N	\N	\N
3556	2	\N	5	8	2007-03-23	1	8	\N
3018	16	\N	21	11	2007-03-23	1	8	\N
3874	18	\N	5	1	2007-03-23	1	8	\N
3592	2	\N	5	10	2007-03-21	1	8	\N
3273	16	\N	21	10	2007-03-16	\N	\N	\N
408	2	\N	21	1	2007-03-16	\N	\N	\N
3257	16	\N	21	10	2007-03-16	\N	\N	\N
400	2	\N	21	1	2007-03-16	\N	\N	\N
3356	17	\N	21	10	2007-03-16	\N	\N	\N
3355	17	\N	21	10	2007-03-16	\N	\N	\N
3725	18	\N	5	17	2007-03-16	\N	\N	\N
3024	2	\N	5	1	2007-03-16	\N	\N	\N
3737	17	\N	21	10	2007-03-16	\N	\N	\N
404	2	\N	21	1	2007-03-16	\N	\N	\N
3397	3	\N	21	8	2007-03-16	\N	\N	\N
3266	16	\N	21	10	2007-03-16	\N	\N	\N
3730	2	\N	21	10	2007-03-16	\N	\N	\N
3867	18	\N	5	8	2007-03-16	\N	\N	\N
3357	17	\N	21	10	2007-03-19	\N	\N	\N
3358	17	\N	21	10	2007-03-19	\N	\N	\N
3264	16	\N	21	10	2007-03-19	\N	\N	\N
410	2	\N	21	1	2007-03-19	\N	\N	\N
3261	16	\N	21	10	2007-03-19	\N	\N	\N
402	2	\N	21	1	2007-03-19	\N	\N	\N
3738	17	\N	21	10	2007-03-19	\N	\N	\N
3739	17	\N	21	10	2007-03-19	\N	\N	\N
496	3	\N	21	17	2007-03-19	\N	\N	\N
3262	16	\N	21	10	2007-03-19	\N	\N	\N
2288	2	\N	21	10	2007-03-19	\N	\N	\N
3868	3	\N	21	8	2007-03-19	\N	\N	\N
3259	16	\N	21	10	2007-03-19	\N	\N	\N
3728	2	\N	21	10	2007-03-19	\N	\N	\N
3870	18	\N	5	8	2007-03-19	\N	\N	\N
3726	18	\N	5	17	2007-03-19	\N	\N	\N
3345	17	\N	21	10	2007-03-19	\N	\N	\N
3346	17	\N	21	10	2007-03-19	\N	\N	\N
2969	16	\N	21	11	2007-03-19	\N	\N	\N
3036	2	\N	21	1	2007-03-19	\N	\N	\N
2968	16	\N	21	11	2007-03-19	\N	\N	\N
3035	2	\N	21	1	2007-03-19	\N	\N	\N
3871	18	\N	5	1	2007-03-19	\N	\N	\N
3589	3	\N	21	10	2007-03-21	\N	\N	\N
3281	16	\N	21	10	2007-03-21	\N	\N	\N
3590	3	\N	21	10	2007-03-21	\N	\N	\N
3282	16	\N	21	10	2007-03-21	\N	\N	\N
3588	18	\N	18	10	2007-03-14	\N	\N	\N
3740	17	\N	21	10	2007-03-23	\N	\N	\N
3741	17	\N	21	10	2007-03-23	\N	\N	\N
3012	16	\N	21	11	2007-03-23	\N	\N	\N
409	2	\N	21	1	2007-03-23	\N	\N	\N
3379	3	\N	21	8	2007-03-23	\N	\N	\N
3022	16	\N	21	11	2007-03-23	\N	\N	\N
3366	2	\N	21	8	2007-03-23	\N	\N	\N
3872	18	\N	5	1	2007-03-23	\N	\N	\N
3750	17	\N	21	10	2007-03-23	\N	\N	\N
3751	17	\N	21	10	2007-03-23	\N	\N	\N
3019	16	\N	21	11	2007-03-23	\N	\N	\N
1444	2	\N	21	10	2007-03-23	\N	\N	\N
3378	3	\N	21	8	2007-03-23	\N	\N	\N
3092	16	\N	21	1	2007-03-23	\N	\N	\N
3211	2	\N	21	8	2007-03-23	\N	\N	\N
3873	18	\N	5	1	2007-03-23	\N	\N	\N
3753	17	\N	21	10	2007-03-23	\N	\N	\N
3752	17	\N	21	10	2007-03-23	\N	\N	\N
3881	3	\N	21	14	2007-03-29	1	8	\N
3902	2	\N	6	14	\N	\N	\N	\N
3903	2	\N	6	14	\N	\N	\N	\N
2971	16	\N	21	11	2007-03-29	1	8	\N
3883	3	\N	21	14	2007-03-29	1	8	\N
2964	16	\N	21	11	2007-03-29	1	8	\N
3918	3	\N	18	10	\N	\N	\N	\N
3927	2	\N	5	10	2007-03-30	1	8	\N
3917	3	\N	18	10	\N	\N	\N	\N
3742	17	\N	21	10	2007-03-29	\N	\N	\N
3743	17	\N	21	10	2007-03-29	\N	\N	\N
3916	2	\N	18	10	\N	\N	\N	\N
3920	3	\N	18	10	\N	\N	\N	\N
3671	16	\N	18	10	\N	\N	\N	\N
3763	17	\N	18	10	2007-03-29	\N	\N	\N
3347	17	\N	5	10	2007-03-29	\N	\N	\N
3924	3	\N	18	10	\N	\N	\N	\N
3925	3	\N	18	10	\N	\N	\N	\N
3921	18	\N	5	8	2007-03-30	\N	\N	\N
3929	3	\N	18	10	2007-03-30	\N	\N	\N
3932	2	\N	18	10	2007-03-30	\N	\N	\N
3744	17	\N	21	10	2007-03-30	\N	\N	\N
3745	17	\N	21	10	2007-03-30	\N	\N	\N
3887	3	\N	21	14	2007-03-30	\N	\N	\N
2973	16	\N	21	11	2007-03-30	\N	\N	\N
3514	2	\N	21	8	2007-03-30	\N	\N	\N
3890	3	\N	21	14	2007-03-30	\N	\N	\N
3017	16	\N	21	11	2007-03-30	\N	\N	\N
3754	17	\N	21	10	2007-03-30	\N	\N	\N
3755	17	\N	21	10	2007-03-30	\N	\N	\N
3875	18	\N	5	1	2007-03-30	\N	\N	\N
3002	16	\N	21	11	2007-03-30	1	8	\N
3886	3	\N	21	14	2007-03-30	1	8	\N
3891	3	\N	21	14	2007-03-30	1	8	\N
2974	16	\N	21	11	2007-03-30	1	8	\N
3516	2	\N	21	8	2007-03-30	1	8	\N
3943	18	\N	5	8	2007-03-30	\N	\N	\N
4018	16	\N	5	10	\N	1	1	\N
3996	16	\N	6	10	\N	1	8	\N
3999	16	\N	6	10	\N	1	8	\N
3756	17	\N	21	10	2007-03-30	\N	\N	\N
3757	17	\N	21	10	2007-03-30	\N	\N	\N
514	3	\N	21	17	2007-03-30	\N	\N	\N
2965	16	\N	21	11	2007-03-30	\N	\N	\N
2733	2	\N	21	14	2007-03-30	\N	\N	\N
3998	16	\N	21	10	\N	1	1	\N
3995	16	\N	21	10	\N	1	1	\N
4019	16	\N	21	10	\N	1	14	\N
515	3	\N	21	17	2007-03-30	\N	\N	\N
3023	16	\N	21	11	2007-03-30	\N	\N	\N
2889	2	\N	21	1	2007-03-30	\N	\N	\N
3876	18	\N	5	1	2007-03-30	\N	\N	\N
3946	21	\N	5	1	2007-04-03	\N	\N	\N
3907	2	\N	5	14	2007-04-04	\N	\N	\N
3988	16	\N	6	10	\N	\N	\N	\N
3989	16	\N	6	10	\N	\N	\N	\N
3990	16	\N	6	10	\N	\N	\N	\N
3991	16	\N	6	10	\N	\N	\N	\N
3992	16	\N	6	10	\N	\N	\N	\N
3993	16	\N	6	10	\N	\N	\N	\N
3994	16	\N	6	10	\N	\N	\N	\N
3997	16	\N	6	10	\N	\N	\N	\N
4000	16	\N	6	10	\N	\N	\N	\N
4001	16	\N	6	10	\N	\N	\N	\N
4002	16	\N	6	10	\N	\N	\N	\N
4003	16	\N	6	10	\N	\N	\N	\N
4004	16	\N	6	10	\N	\N	\N	\N
4005	16	\N	6	10	\N	\N	\N	\N
4006	16	\N	6	10	\N	\N	\N	\N
4007	16	\N	6	10	\N	\N	\N	\N
4008	16	\N	6	10	\N	\N	\N	\N
4009	16	\N	6	10	\N	\N	\N	\N
4010	16	\N	6	10	\N	\N	\N	\N
4011	16	\N	6	10	\N	\N	\N	\N
4016	16	\N	6	10	\N	\N	\N	\N
4017	16	\N	6	10	\N	\N	\N	\N
4020	16	\N	6	10	\N	\N	\N	\N
4021	16	\N	6	10	\N	\N	\N	\N
4023	16	\N	6	10	\N	\N	\N	\N
4024	16	\N	6	10	\N	\N	\N	\N
4025	16	\N	6	10	\N	\N	\N	\N
4032	16	\N	6	10	\N	\N	\N	\N
4033	16	\N	6	10	\N	\N	\N	\N
4034	16	\N	6	10	\N	\N	\N	\N
4035	16	\N	6	10	\N	\N	\N	\N
4036	16	\N	6	10	\N	\N	\N	\N
4037	16	\N	6	10	\N	\N	\N	\N
4038	16	\N	6	10	\N	\N	\N	\N
4039	16	\N	6	10	\N	\N	\N	\N
4040	16	\N	6	10	\N	\N	\N	\N
4041	16	\N	6	10	\N	\N	\N	\N
4050	16	\N	6	10	\N	\N	\N	\N
4051	16	\N	6	10	\N	\N	\N	\N
4052	16	\N	6	10	\N	\N	\N	\N
4053	16	\N	6	10	\N	\N	\N	\N
4054	16	\N	6	10	\N	\N	\N	\N
4055	16	\N	6	10	\N	\N	\N	\N
4056	16	\N	6	10	\N	\N	\N	\N
4057	16	\N	6	10	\N	\N	\N	\N
4058	16	\N	6	10	\N	\N	\N	\N
4059	16	\N	6	10	\N	\N	\N	\N
4060	16	\N	6	10	\N	\N	\N	\N
4061	16	\N	6	10	\N	\N	\N	\N
4062	16	\N	6	10	\N	\N	\N	\N
4063	16	\N	6	10	\N	\N	\N	\N
4064	16	\N	6	10	\N	\N	\N	\N
4065	16	\N	6	10	\N	\N	\N	\N
4066	16	\N	6	10	\N	\N	\N	\N
4068	16	\N	6	10	\N	\N	\N	\N
4069	16	\N	6	10	\N	\N	\N	\N
4070	16	\N	6	10	\N	\N	\N	\N
4071	16	\N	6	10	\N	\N	\N	\N
4072	16	\N	6	10	\N	\N	\N	\N
4073	16	\N	6	10	\N	\N	\N	\N
4074	16	\N	6	10	\N	\N	\N	\N
4075	16	\N	6	10	\N	\N	\N	\N
4076	16	\N	6	10	\N	\N	\N	\N
4047	16	\N	21	10	\N	1	8	\N
4049	16	\N	21	10	\N	1	8	\N
4048	16	\N	1	10	\N	1	8	\N
4080	2	\N	18	10	2007-04-06	\N	\N	\N
3900	2	\N	5	14	2007-04-06	\N	\N	\N
3941	3	\N	5	14	2007-04-06	\N	\N	\N
2966	16	\N	5	11	2007-04-06	\N	\N	\N
2967	16	\N	5	11	2007-04-06	\N	\N	\N
3901	2	\N	18	14	2007-04-06	\N	\N	\N
3279	16	\N	5	10	2007-04-06	\N	\N	\N
4067	16	\N	7	10	2007-04-06	\N	\N	\N
2878	2	\N	18	1	2007-02-01	\N	\N	\N
2722	2	\N	18	14	2007-02-01	\N	\N	\N
4084	2	\N	18	10	2007-04-06	\N	\N	\N
4083	3	\N	18	10	2007-04-06	\N	\N	\N
518	3	\N	5	17	2007-04-06	\N	\N	\N
4085	1	\N	18	10	2007-04-06	\N	\N	\N
4088	3	\N	18	10	\N	\N	\N	\N
3885	3	\N	5	14	2007-04-06	\N	\N	\N
2849	2	\N	5	10	2007-04-10	\N	\N	\N
2740	2	\N	5	14	2007-04-10	\N	\N	\N
4097	3	\N	21	17	\N	1	8	\N
4100	3	\N	21	17	\N	1	8	\N
4103	3	\N	21	17	\N	1	8	\N
4090	3	\N	18	17	\N	1	1	\N
3899	2	\N	18	14	2007-04-10	1	10	\N
4092	3	\N	21	17	\N	1	8	\N
4111	2	\N	18	10	\N	\N	\N	\N
3945	21	\N	5	1	2007-04-11	\N	\N	\N
1000	2	\N	5	8	2007-04-11	\N	\N	\N
479	3	\N	5	17	2007-04-11	\N	\N	\N
480	3	\N	5	17	2007-04-11	\N	\N	\N
3911	2	\N	5	14	2007-04-11	\N	\N	\N
3935	3	\N	5	14	2007-04-11	\N	\N	\N
373	2	\N	5	1	2007-04-11	1	10000	\N
3933	3	\N	21	14	2007-04-10	1	10	\N
3934	3	\N	21	14	2007-04-10	1	10	\N
3910	2	\N	5	14	2007-04-10	1	10	\N
519	3	\N	5	17	2007-04-10	1	10	\N
702	3	\N	5	10	2007-04-10	1	10	\N
4108	3	\N	21	17	\N	1	1	\N
4107	3	\N	21	17	\N	1	10	\N
4091	3	\N	21	17	\N	1	10	\N
4093	3	\N	18	17	\N	\N	\N	\N
4106	3	\N	18	17	\N	\N	\N	\N
4105	3	\N	21	17	\N	1	8	\N
4095	3	\N	21	17	\N	1	1	\N
4098	3	\N	18	17	\N	\N	\N	\N
4099	3	\N	18	17	\N	\N	\N	\N
4104	3	\N	5	17	\N	1	10	\N
4102	3	\N	18	17	\N	\N	\N	\N
4101	3	\N	21	17	\N	1	8	\N
602	2	\N	5	1	2007-04-11	1	10	\N
4109	3	\N	5	17	2007-04-11	1	10	\N
3575	2	\N	5	10	2007-04-11	1	8	\N
3912	2	\N	5	10	2007-04-11	\N	\N	\N
4112	3	\N	18	10	\N	\N	\N	\N
4113	2	\N	18	10	\N	\N	\N	\N
3758	17	\N	21	10	2007-04-11	\N	\N	\N
3759	17	\N	21	10	2007-04-11	\N	\N	\N
3888	3	\N	21	14	2007-04-11	\N	\N	\N
2984	16	\N	21	11	2007-04-11	\N	\N	\N
2793	16	\N	21	10	2007-04-11	\N	\N	\N
503	3	\N	21	17	2007-04-11	\N	\N	\N
2979	16	\N	21	11	2007-04-11	\N	\N	\N
3515	2	\N	21	8	2007-04-11	\N	\N	\N
3877	18	\N	5	1	2007-04-11	\N	\N	\N
436	2	\N	5	1	2006-07-26	1	8	\N
600	2	\N	3	1	2006-08-04	\N	\N	\N
878	2	\N	4	10	2006-09-04	\N	\N	\N
937	2	\N	4	8	2006-09-04	\N	\N	\N
931	2	\N	4	8	2006-09-04	\N	\N	\N
933	2	\N	4	8	2006-09-04	\N	\N	\N
925	2	\N	4	8	2006-09-04	\N	\N	\N
1696	2	\N	4	10	2006-10-04	\N	\N	\N
1695	2	\N	4	10	2006-10-04	\N	\N	\N
1694	2	\N	4	10	2006-10-04	\N	\N	\N
1693	2	\N	4	10	2006-10-04	\N	\N	\N
1692	2	\N	4	10	2006-10-04	\N	\N	\N
1690	2	\N	4	10	2006-10-04	\N	\N	\N
1688	2	\N	4	10	2006-10-04	\N	\N	\N
1689	2	\N	4	10	2006-10-04	\N	\N	\N
1691	2	\N	4	10	2006-10-04	\N	\N	\N
1454	2	\N	4	10	2006-10-04	\N	\N	\N
1434	2	\N	4	10	2006-10-04	\N	\N	\N
1447	2	\N	4	10	2006-10-04	\N	\N	\N
1455	2	\N	4	10	2006-10-04	\N	\N	\N
1456	2	\N	4	10	2006-10-04	\N	\N	\N
1457	2	\N	4	10	2006-10-04	\N	\N	\N
1440	2	\N	4	10	2006-10-04	\N	\N	\N
1441	2	\N	4	10	2006-10-04	\N	\N	\N
1442	2	\N	4	10	2006-10-04	\N	\N	\N
1436	2	\N	4	10	2006-10-04	\N	\N	\N
1438	2	\N	4	10	2006-10-04	\N	\N	\N
1450	2	\N	4	10	2006-10-04	\N	\N	\N
1453	2	\N	4	10	2006-10-04	\N	\N	\N
1449	2	\N	4	10	2006-10-04	\N	\N	\N
1452	2	\N	4	10	2006-10-04	\N	\N	\N
1448	2	\N	4	10	2006-10-04	\N	\N	\N
1430	2	\N	4	10	2006-10-04	\N	\N	\N
1445	2	\N	4	10	2006-10-04	\N	\N	\N
1432	2	\N	4	10	2006-10-04	\N	\N	\N
1446	2	\N	4	10	2006-10-04	\N	\N	\N
396	2	\N	5	1	2006-08-28	\N	\N	\N
390	2	\N	5	1	2006-08-28	\N	\N	\N
389	2	\N	5	1	2006-08-28	\N	\N	\N
384	2	\N	5	1	2006-08-28	\N	\N	\N
911	18	\N	5	\N	\N	\N	\N	\N
912	18	\N	5	\N	\N	\N	\N	\N
913	18	\N	5	\N	\N	\N	\N	\N
914	18	\N	5	\N	\N	\N	\N	\N
915	18	\N	5	\N	\N	\N	\N	\N
916	18	\N	5	\N	\N	\N	\N	\N
917	18	\N	5	\N	\N	\N	\N	\N
918	18	\N	5	\N	\N	\N	\N	\N
919	18	\N	5	\N	\N	\N	\N	\N
920	18	\N	5	\N	\N	\N	\N	\N
921	18	\N	5	\N	\N	\N	\N	\N
446	2	\N	3	1	2006-07-26	\N	\N	\N
418	2	\N	3	1	2006-07-26	\N	\N	\N
463	2	\N	3	1	2006-07-26	\N	\N	\N
437	2	\N	3	1	2006-07-26	\N	\N	\N
462	2	\N	3	1	2006-07-26	\N	\N	\N
137	2	\N	3	1	2006-07-26	\N	\N	\N
156	2	\N	3	1	2006-07-26	\N	\N	\N
652	3	\N	3	10	2006-08-21	\N	\N	\N
378	2	\N	3	1	2006-08-31	\N	\N	\N
950	2	\N	3	8	2006-09-04	\N	\N	\N
882	2	\N	3	10	2006-09-04	\N	\N	\N
858	2	\N	3	10	2006-09-04	\N	\N	\N
884	2	\N	21	10	2006-09-04	1	10	\N
630	3	\N	3	10	2006-09-07	\N	\N	\N
657	3	\N	3	10	2006-09-07	\N	\N	\N
958	2	\N	3	8	2006-09-18	\N	\N	\N
990	2	\N	3	8	2006-09-25	\N	\N	\N
985	2	\N	3	8	2006-09-25	\N	\N	\N
982	2	\N	3	8	2006-09-25	\N	\N	\N
1391	2	\N	3	8	2006-09-25	\N	\N	\N
1392	2	\N	3	8	2006-09-25	\N	\N	\N
926	2	\N	3	8	2006-09-25	\N	\N	\N
2815	1	\N	3	10	2006-11-23	\N	\N	\N
1006	2	\N	5	8	2006-09-18	1	8	\N
734	2	\N	3	8	2006-08-18	\N	\N	\N
3760	17	\N	21	10	2007-04-12	\N	\N	\N
3761	17	\N	21	10	2007-04-12	\N	\N	\N
3889	3	\N	21	14	2007-04-12	\N	\N	\N
3258	16	\N	21	10	2007-04-12	\N	\N	\N
3882	3	\N	21	14	2007-04-12	\N	\N	\N
2970	16	\N	21	11	2007-04-12	\N	\N	\N
3878	18	\N	5	1	2007-04-12	\N	\N	\N
886	17	\N	21	\N	\N	1	10000	\N
18	14	\N	7	\N	\N	1	10	\N
3762	17	\N	21	10	\N	1	10000	\N
3814	17	\N	21	10	\N	1	10000	\N
3938	3	\N	21	14	2007-04-10	1	10000	\N
2960	16	\N	21	11	\N	1	10000	\N
3930	2	\N	21	10	2007-03-30	1	10000	\N
3937	3	\N	21	14	2007-04-10	1	10000	\N
2978	16	\N	21	11	\N	1	10000	\N
434	2	\N	21	1	2006-07-24	1	10000	\N
3530	2	\N	5	10	2007-04-12	1	8	\N
3802	17	\N	21	10	\N	1	10	\N
3803	17	\N	21	10	\N	1	10	\N
3021	16	\N	21	11	\N	1	10	\N
736	2	\N	21	8	2006-08-21	1	10	\N
3940	3	\N	21	14	2007-04-10	1	10	\N
3020	16	\N	21	11	\N	1	10	\N
3544	2	\N	21	10	2007-02-27	1	10	\N
4136	23	\N	6	16	\N	\N	\N	\N
3804	17	\N	21	10	\N	1	10	\N
3805	17	\N	21	10	\N	1	10	\N
2982	16	\N	21	11	\N	1	10	\N
3584	2	\N	21	10	2007-03-12	1	10	\N
4114	3	\N	21	8	2007-04-12	1	10	\N
2981	16	\N	21	11	\N	1	10	\N
4126	18	\N	5	10	\N	1	10	\N
3806	17	\N	21	10	\N	1	10	\N
3807	17	\N	21	10	\N	1	10	\N
4078	3	\N	21	10	2007-04-06	1	10	\N
3284	16	\N	21	10	2007-03-15	1	10	\N
4079	3	\N	21	10	2007-04-06	1	10	\N
3285	16	\N	21	10	2007-03-15	1	10	\N
4081	2	\N	21	10	2007-04-06	1	10	\N
4127	18	\N	5	10	\N	1	10	\N
3879	18	\N	5	1	\N	1	8	\N
3880	18	\N	5	1	\N	1	8	\N
3808	17	\N	21	10	\N	1	14	\N
3809	17	\N	21	10	\N	1	14	\N
4119	3	\N	21	8	2007-04-12	1	14	\N
2247	2	\N	21	10	2006-10-20	1	14	\N
4118	3	\N	21	8	2007-04-12	1	14	\N
3000	16	\N	21	11	\N	1	14	\N
16	14	\N	7	\N	\N	1	10	\N
4128	18	\N	5	10	\N	1	1	\N
3405	3	\N	21	10	2007-02-01	1	8	\N
3294	16	\N	21	10	2007-04-06	1	8	\N
4157	2	\N	18	10	\N	1	10	\N
4162	2	\N	18	10	\N	1	10	\N
4159	2	\N	18	10	\N	1	10	\N
4160	2	\N	18	10	\N	1	10	\N
4161	2	\N	18	10	\N	1	10	\N
662	3	\N	18	10	2006-10-16	\N	\N	\N
3733	3	\N	21	10	2007-03-15	1	1	\N
3734	2	\N	21	10	2007-03-15	1	1	\N
594	3	\N	21	8	2007-03-16	1	1	\N
3949	16	\N	21	10	\N	1	1	\N
2737	2	\N	21	14	2007-03-16	1	1	\N
3811	17	\N	21	10	\N	1	1	\N
3810	17	\N	21	10	\N	1	1	\N
4129	18	\N	5	10	\N	1	1	\N
3926	2	\N	5	10	2007-03-30	1	8	\N
4168	3	\N	18	10	\N	\N	\N	\N
3813	17	\N	21	10	\N	1	1	\N
3812	17	\N	21	10	\N	1	1	\N
3404	3	\N	21	10	2007-02-01	1	1	\N
3947	16	\N	21	10	2007-04-06	1	1	\N
3407	2	\N	21	10	2007-02-01	1	1	\N
4130	18	\N	5	10	\N	1	1	\N
4174	2	\N	18	10	\N	1	10	\N
4201	2	\N	18	10	\N	1	10	\N
4176	36	\N	5	\N	\N	1	10000	\N
4177	39	\N	5	\N	\N	1	10000	\N
4178	40	\N	5	\N	\N	1	10000	\N
4179	37	\N	5	\N	\N	1	10000	\N
4181	34	\N	5	\N	\N	1	10000	\N
4182	34	\N	5	\N	\N	1	10000	\N
4184	38	\N	26	\N	\N	\N	\N	\N
4185	37	\N	26	\N	\N	\N	\N	\N
4186	24	\N	26	\N	\N	\N	\N	\N
4187	44	\N	26	\N	\N	\N	\N	\N
4188	40	\N	26	\N	\N	\N	\N	\N
4183	51	\N	5	\N	\N	1	10000	\N
2282	2	\N	5	10	2006-10-30	1	1	\N
4169	3	\N	5	10	\N	1	1	\N
4190	38	\N	26	\N	\N	\N	\N	\N
4191	37	\N	26	\N	\N	\N	\N	\N
4192	24	\N	26	\N	\N	\N	\N	\N
4193	44	\N	26	\N	\N	\N	\N	\N
4194	40	\N	26	\N	\N	\N	\N	\N
4189	51	\N	5	\N	\N	1	10000	\N
4196	38	\N	26	\N	\N	\N	\N	\N
4197	37	\N	26	\N	\N	\N	\N	\N
4198	24	\N	26	\N	\N	\N	\N	\N
4199	44	\N	26	\N	\N	\N	\N	\N
4200	40	\N	26	\N	\N	\N	\N	\N
4195	51	\N	5	\N	\N	1	10000	\N
3095	16	\N	1	1	\N	3	5	\N
3094	16	\N	1	1	\N	3	5	\N
3376	3	\N	5	8	2007-01-31	1	10	\N
3367	2	\N	5	8	2007-01-31	1	10	\N
4123	3	\N	5	10	\N	1	10	\N
3576	2	\N	5	10	2007-03-29	1	10	\N
3955	16	\N	5	10	\N	1	10	\N
4205	2	\N	18	10	\N	1	10	\N
4203	3	\N	18	10	\N	1	10	\N
3746	17	\N	21	10	\N	1	8	\N
3747	17	\N	21	10	\N	1	8	\N
4204	3	\N	18	10	\N	1	10	\N
3897	2	\N	5	14	\N	1	8	\N
3956	16	\N	18	10	\N	3	5	\N
4207	83	\N	5	\N	\N	1	1	\N
3896	2	\N	18	14	\N	\N	\N	\N
4209	38	\N	26	\N	\N	\N	\N	\N
4210	37	\N	26	\N	\N	\N	\N	\N
4211	24	\N	26	\N	\N	\N	\N	\N
4212	44	\N	26	\N	\N	\N	\N	\N
4213	40	\N	26	\N	\N	\N	\N	\N
4208	51	\N	5	\N	\N	1	1	\N
4214	30	\N	5	\N	\N	1	1	\N
3898	2	\N	18	14	\N	1	1	\N
5	14	\N	7	\N	\N	1	10	\N
4220	2	\N	18	10	\N	1	10	\N
4219	2	\N	18	10	\N	1	10	\N
4218	2	\N	18	10	\N	1	10	\N
2738	2	\N	5	14	2007-01-23	1	10	\N
3727	2	\N	5	10	2007-03-15	1	10	\N
4206	2	\N	5	10	\N	1	10	\N
3748	17	\N	5	10	\N	1	8	\N
3789	17	\N	21	10	\N	1	14	\N
3790	17	\N	21	10	\N	1	14	\N
4170	3	\N	21	10	\N	1	14	\N
4171	3	\N	21	10	\N	1	14	\N
2958	16	\N	21	11	\N	1	14	\N
4172	2	\N	21	10	\N	1	14	\N
4271	18	\N	5	14	\N	1	14	\N
3791	17	\N	21	10	\N	1	14	\N
3792	17	\N	21	10	\N	1	14	\N
483	3	\N	21	17	2006-10-10	1	14	\N
3272	16	\N	21	10	\N	1	14	\N
4152	2	\N	21	10	\N	1	14	\N
4124	3	\N	21	10	\N	1	14	\N
2828	16	\N	21	10	2006-11-29	1	14	\N
3909	2	\N	21	14	\N	1	14	\N
4270	18	\N	5	14	\N	1	14	\N
4273	87	\N	5	\N	\N	1	14	\N
4259	2	\N	5	8	\N	1	8	\N
4256	2	\N	5	8	\N	1	8	\N
4258	2	\N	5	8	\N	1	8	\N
3531	2	\N	5	10	2007-02-22	1	1	\N
4281	3	\N	18	1	\N	1	1	\N
4283	87	\N	5	\N	\N	1	1	\N
4166	2	\N	18	10	\N	\N	\N	\N
4147	2	\N	5	8	\N	1	8	\N
3908	2	\N	5	14	\N	1	8	\N
3586	3	\N	18	1	2007-03-15	\N	\N	\N
3047	16	\N	21	1	2006-12-13	1	10	\N
984	2	\N	21	8	2006-12-13	1	10	\N
1421	3	\N	21	8	2006-10-16	1	10	\N
4286	2	\N	7	10	\N	1	10	\N
4285	2	\N	5	10	\N	1	10	\N
4284	2	\N	5	10	\N	1	10	\N
4146	2	\N	5	8	\N	1	8	\N
3425	2	\N	5	10	2007-02-05	1	8	\N
995	2	\N	5	8	2006-10-16	1	8	\N
4307	2	\N	18	10	\N	\N	\N	\N
3040	2	\N	18	1	2007-01-15	1	10	\N
991	2	\N	18	8	2006-09-18	1	10	\N
4163	2	\N	5	10	\N	1	10	\N
4137	2	\N	5	10	\N	1	8	\N
3959	16	\N	6	10	\N	3	5	\N
3960	16	\N	6	10	\N	3	5	\N
727	3	\N	5	8	2007-03-12	1	10	\N
3359	17	\N	5	10	2007-03-16	1	10	\N
4151	2	\N	5	10	\N	1	8	\N
4165	2	\N	5	10	\N	1	8	\N
4145	2	\N	5	8	\N	1	8	\N
743	2	\N	5	10	2006-08-25	1	8	\N
4290	87	\N	5	\N	\N	1	10	\N
609	2	\N	5	1	2006-08-10	1	8	\N
4077	2	\N	5	10	2007-04-06	1	8	\N
4138	2	\N	5	10	\N	1	8	\N
3793	17	\N	21	10	\N	1	10	\N
4153	3	\N	21	10	\N	1	10	\N
2785	16	\N	21	10	2006-11-21	1	10	\N
4291	76	\N	5	\N	\N	1	1	\N
3173	16	\N	21	10	2007-01-04	1	10	\N
4292	31	\N	5	\N	\N	1	1	\N
4293	31	\N	5	\N	\N	1	1	\N
4294	32	\N	5	\N	\N	1	1	\N
4295	32	\N	5	\N	\N	1	1	\N
4269	18	\N	5	14	\N	1	1	\N
4296	87	\N	5	\N	\N	1	1	\N
4297	87	\N	5	\N	\N	1	1	\N
3801	17	\N	21	10	\N	1	1	\N
3800	17	\N	21	10	\N	1	1	\N
4298	94	\N	5	\N	\N	1	1	\N
4215	22	\N	5	1	\N	1	1	\N
3952	16	\N	1	10	\N	1	10	\N
4299	61	\N	5	\N	\N	1	1	\N
4300	71	\N	5	\N	\N	1	1	\N
4301	71	\N	5	\N	\N	1	1	\N
4302	71	\N	5	\N	\N	1	1	\N
4303	87	\N	5	\N	\N	1	1	\N
4304	87	\N	5	\N	\N	1	1	\N
4305	84	\N	5	\N	\N	1	1	\N
749	2	\N	5	10	2006-08-31	1	10	\N
3503	2	\N	5	10	2007-02-15	1	10	\N
3500	2	\N	5	10	2007-02-15	1	10	\N
3217	2	\N	5	8	2007-02-28	1	10	\N
4306	2	\N	21	10	\N	1	8	\N
4309	2	\N	18	10	\N	\N	\N	\N
4313	2	\N	5	10	\N	1	10	\N
4257	2	\N	21	8	\N	1	8	\N
4087	3	\N	18	10	\N	1	1	\N
4314	2	\N	18	10	\N	\N	\N	\N
620	2	\N	5	10	2006-08-10	1	10	\N
3913	2	\N	5	10	\N	1	10	\N
3961	16	\N	18	10	\N	\N	\N	\N
4255	2	\N	18	8	\N	1	10	\N
3795	17	\N	21	10	\N	1	1	\N
3796	17	\N	21	10	\N	1	1	\N
3616	16	\N	21	10	\N	1	1	\N
3797	17	\N	21	10	\N	1	1	\N
3798	17	\N	21	10	\N	1	1	\N
4086	3	\N	21	10	\N	1	1	\N
3619	16	\N	21	10	\N	1	1	\N
3618	16	\N	21	10	\N	1	1	\N
3216	2	\N	21	8	2007-03-06	1	1	\N
4121	3	\N	21	8	2007-04-12	1	1	\N
3617	16	\N	21	10	\N	1	1	\N
3869	2	\N	21	8	2007-03-16	1	1	\N
639	3	\N	5	10	2006-08-14	1	1	\N
1264	3	\N	5	14	2006-12-13	1	1	\N
3396	3	\N	5	8	2007-01-30	1	1	\N
4321	56	\N	5	\N	\N	1	10000	\N
4322	61	\N	5	\N	\N	1	10000	\N
4323	44	\N	5	\N	\N	1	10000	\N
4324	44	\N	5	\N	\N	1	10000	\N
4325	43	\N	5	\N	\N	1	10000	\N
4326	44	\N	5	\N	\N	1	10000	\N
4327	44	\N	5	\N	\N	1	10000	\N
4328	43	\N	5	\N	\N	1	10000	\N
4329	52	\N	5	\N	\N	1	10000	\N
4330	52	\N	5	\N	\N	1	10000	\N
4331	54	\N	5	\N	\N	1	10000	\N
4332	56	\N	5	\N	\N	1	10000	\N
4333	54	\N	5	\N	\N	1	10000	\N
8	14	\N	1	\N	\N	1	8	\N
4141	2	\N	5	8	\N	1	8	\N
4143	2	\N	5	8	\N	1	8	\N
4155	3	\N	5	10	\N	1	10000	\N
598	2	\N	5	1	2006-08-03	1	1	\N
4140	2	\N	5	8	\N	1	8	\N
3220	2	\N	5	10	2007-01-17	1	8	\N
4334	71	\N	5	\N	\N	1	10	\N
4335	52	\N	5	\N	\N	1	10	\N
4336	55	\N	5	\N	\N	1	10	\N
4337	55	\N	5	\N	\N	1	10	\N
4338	58	\N	5	\N	\N	1	10	\N
4339	58	\N	5	\N	\N	1	10	\N
4340	61	\N	5	\N	\N	1	10	\N
4341	66	\N	5	\N	\N	1	10	\N
4342	58	\N	5	\N	\N	1	10	\N
4343	30	\N	5	\N	\N	1	10	\N
4344	32	\N	5	\N	\N	1	10	\N
4345	34	\N	5	\N	\N	1	10	\N
4346	34	\N	5	\N	\N	1	10	\N
4347	34	\N	5	\N	\N	1	10	\N
4348	27	\N	5	\N	\N	1	10	\N
4267	18	\N	5	14	\N	1	1	\N
4349	87	\N	5	\N	\N	1	1	\N
4350	87	\N	5	\N	\N	1	1	\N
4351	84	\N	5	\N	\N	1	1	\N
3944	21	\N	5	1	\N	1	1	\N
4180	38	\N	5	\N	\N	1	10	\N
3513	2	\N	1	8	2007-02-28	1	1	\N
1701	2	\N	18	10	2006-10-04	1	1	\N
516	3	\N	18	17	\N	1	1	\N
1007	2	\N	1	8	2007-02-01	1	8	\N
3198	2	\N	21	10	2007-01-15	1	1	\N
3501	2	\N	21	10	2007-02-15	1	1	\N
3377	3	\N	21	8	2007-01-31	1	1	\N
3072	16	\N	21	1	\N	1	1	\N
3364	2	\N	21	8	2007-01-31	1	1	\N
3289	16	\N	21	10	\N	1	1	\N
504	3	\N	18	17	2006-11-10	1	1	\N
3263	16	\N	5	10	\N	1	1	\N
3275	16	\N	21	10	\N	1	8	\N
2861	16	\N	21	10	2006-12-07	1	8	\N
3271	16	\N	5	10	\N	1	10	\N
3964	16	\N	1	10	\N	5	5	\N
3963	16	\N	1	10	\N	5	5	\N
3269	16	\N	21	10	\N	1	8	\N
3290	16	\N	21	10	\N	1	8	\N
2983	16	\N	21	11	\N	1	10	\N
2980	16	\N	21	11	\N	1	10	\N
3950	16	\N	21	10	\N	1	1	\N
2950	16	\N	21	11	2007-01-31	1	1	\N
3016	16	\N	21	11	\N	1	1	\N
3015	16	\N	21	11	\N	1	1	\N
3243	16	\N	18	10	\N	1	10	\N
3014	16	\N	21	11	\N	1	1	\N
3013	16	\N	21	11	\N	1	1	\N
2833	16	\N	21	10	2006-11-29	1	8	\N
3265	16	\N	5	10	\N	1	1	\N
3268	16	\N	5	10	\N	1	1	\N
3274	16	\N	21	10	\N	1	14	\N
3071	16	\N	21	1	2007-01-09	1	10	\N
3240	16	\N	21	10	\N	1	8	\N
3953	16	\N	5	10	\N	1	10	\N
2563	16	\N	1	11	\N	1	14	\N
2954	16	\N	6	11	\N	1	14	\N
3242	16	\N	6	10	\N	1	14	\N
3292	16	\N	6	10	\N	1	14	\N
2784	16	\N	5	10	2006-11-21	1	10	\N
4226	2	\N	18	8	\N	1	8	\N
4310	2	\N	18	10	\N	1	8	\N
1262	3	\N	21	14	2007-02-07	1	8	\N
3915	2	\N	19	10	\N	1	8	\N
481	3	\N	7	17	2006-10-10	1	8	\N
4175	2	\N	18	10	\N	1	8	\N
3510	2	\N	5	8	2007-04-05	1	8	\N
3400	3	\N	18	8	\N	1	8	\N
680	3	\N	1	10	2006-08-14	1	8	\N
2776	2	\N	18	10	2006-11-28	1	10	\N
4280	2	\N	5	1	\N	1	10	\N
3591	2	\N	5	10	2007-03-21	1	10	\N
583	2	\N	3	16	2006-08-10	1	8	\N
614	3	\N	18	1	2006-08-10	1	8	\N
2292	3	\N	1	8	2006-10-31	1	8	\N
604	2	\N	18	1	2006-08-10	1	8	\N
3764	17	\N	21	10	\N	1	1	\N
3799	17	\N	21	10	\N	1	1	\N
3097	16	\N	21	1	\N	1	1	\N
3096	16	\N	21	1	\N	1	1	\N
4262	18	\N	5	14	\N	1	1	\N
4355	87	\N	5	\N	\N	1	1	\N
4266	18	\N	5	14	\N	1	1	\N
4356	87	\N	5	\N	\N	1	1	\N
4357	87	\N	5	\N	\N	1	1	\N
4358	71	\N	5	\N	\N	1	1	\N
4359	44	\N	5	\N	\N	1	10	\N
4360	45	\N	5	\N	\N	1	10	\N
4361	60	\N	5	\N	\N	1	10	\N
4362	32	\N	5	\N	\N	1	10	\N
3957	16	\N	5	10	\N	1	10	\N
3958	16	\N	5	10	\N	1	10	\N
3766	17	\N	21	10	\N	1	1	\N
3765	17	\N	21	10	\N	1	1	\N
4363	87	\N	5	\N	\N	1	1	\N
4364	87	\N	5	\N	\N	1	1	\N
4263	18	\N	5	14	\N	1	1	\N
4316	2	\N	1	10	\N	1	14	\N
4279	2	\N	1	1	\N	1	14	\N
4365	2	\N	18	1	\N	1	1	\N
4089	2	\N	5	10	\N	1	1	\N
478	3	\N	21	17	2006-08-31	1	1	\N
4125	2	\N	21	10	\N	1	1	\N
4366	61	\N	5	\N	\N	1	10	\N
4082	2	\N	21	10	2007-04-06	1	8	\N
138	2	\N	5	1	2006-09-14	1	8	\N
2698	2	\N	5	10	2006-11-28	1	8	\N
4250	2	\N	19	8	\N	1	8	\N
4377	2	\N	18	10	\N	\N	\N	\N
3499	2	\N	5	10	2007-02-14	1	10	\N
4144	2	\N	5	8	\N	1	10	\N
4371	61	\N	5	\N	\N	1	10	\N
4372	96	\N	5	\N	\N	1	10	\N
4373	61	\N	5	\N	\N	1	10	\N
4374	40	\N	5	\N	\N	1	10	\N
4375	96	\N	5	\N	\N	1	10	\N
4376	99	\N	5	\N	\N	1	10	\N
3768	17	\N	21	10	\N	1	8	\N
3767	17	\N	21	10	\N	1	8	\N
4312	3	\N	21	10	\N	1	8	\N
3212	2	\N	21	8	2007-04-06	1	8	\N
3936	3	\N	21	14	2007-04-10	1	8	\N
616	2	\N	21	8	2006-08-10	1	8	\N
4369	2	\N	1	10	\N	1	8	\N
4264	18	\N	5	14	\N	1	1	\N
4378	87	\N	5	\N	\N	1	1	\N
2992	16	\N	6	11	\N	1	10	\N
2995	16	\N	6	11	\N	1	10	\N
4379	3	\N	18	10	\N	1	10	\N
3769	17	\N	21	10	\N	1	10	\N
3770	17	\N	21	10	\N	1	10	\N
2993	16	\N	5	11	\N	1	10	\N
4278	3	\N	5	1	\N	1	10	\N
4383	76	\N	5	\N	\N	1	10	\N
4385	2	\N	18	10	\N	1	10	\N
1397	2	\N	18	8	2006-10-26	1	8	\N
3519	2	\N	18	8	\N	1	8	\N
4318	2	\N	5	10	\N	1	10	\N
4320	2	\N	5	10	\N	1	8	\N
4390	84	\N	5	\N	\N	1	1	\N
4249	2	\N	18	8	\N	1	1	\N
4239	2	\N	18	8	\N	\N	\N	\N
4237	2	\N	18	8	\N	\N	\N	\N
4238	2	\N	18	8	\N	\N	\N	\N
3895	2	\N	18	14	\N	1	10	\N
4395	2	\N	18	16	\N	1	8	\N
3395	3	\N	18	8	2007-03-23	1	10	\N
3280	16	\N	21	10	2007-03-23	1	8	\N
4393	2	\N	5	16	\N	1	8	\N
405	2	\N	5	1	2006-08-15	1	10	\N
4231	2	\N	5	8	\N	1	10	\N
3454	2	\N	18	8	2007-03-05	\N	\N	\N
4232	2	\N	5	8	\N	1	10	\N
4228	2	\N	5	8	\N	1	10	\N
4394	2	\N	5	16	\N	1	8	\N
4391	2	\N	5	16	\N	1	8	\N
625	2	\N	18	10	2006-08-15	1	10	\N
4392	2	\N	18	16	\N	1	8	\N
593	3	\N	1	8	2006-08-10	1	8	\N
595	3	\N	1	8	2006-08-10	1	8	\N
4243	2	\N	21	8	\N	1	8	\N
4242	2	\N	5	8	\N	1	8	\N
4244	2	\N	5	8	\N	1	8	\N
3052	2	\N	5	10	2006-12-15	1	10	\N
3771	17	\N	21	10	\N	1	10	\N
3772	17	\N	21	10	\N	1	10	\N
3467	3	\N	21	10	2007-02-08	1	10	\N
141	2	\N	21	1	2006-10-11	1	10	\N
678	3	\N	21	10	2006-08-14	1	10	\N
722	2	\N	21	10	2006-08-14	1	10	\N
4265	18	\N	5	14	\N	1	1	\N
4397	87	\N	5	\N	\N	1	1	\N
4398	87	\N	5	\N	\N	1	1	\N
4223	2	\N	5	8	\N	1	1	\N
4132	18	\N	5	10	\N	1	10	\N
2994	16	\N	5	11	\N	1	10	\N
584	2	\N	18	16	2006-08-10	1	8	\N
679	3	\N	18	10	2006-11-28	\N	\N	\N
4399	3	\N	18	10	\N	1	10	\N
2742	2	\N	18	14	2006-12-11	\N	\N	\N
4233	2	\N	5	8	\N	1	10	\N
4235	2	\N	5	8	\N	1	10	\N
3210	2	\N	18	8	2007-02-23	\N	\N	\N
4400	3	\N	18	16	\N	\N	\N	\N
644	3	\N	18	10	2006-08-31	\N	\N	\N
4401	28	\N	5	\N	\N	1	1	\N
4402	36	\N	5	\N	\N	1	1	\N
4403	37	\N	5	\N	\N	1	1	\N
4404	37	\N	5	\N	\N	1	1	\N
4405	84	\N	5	\N	\N	1	1	\N
4406	61	\N	5	\N	\N	1	10	\N
4407	61	\N	5	\N	\N	1	10	\N
3074	16	\N	5	1	\N	1	1	\N
4408	99	\N	5	\N	\N	1	10	\N
4409	100	\N	5	\N	\N	1	10	\N
4410	61	\N	5	\N	\N	1	10	\N
4411	40	\N	5	\N	\N	1	10	\N
4412	99	\N	5	\N	\N	1	10	\N
4413	61	\N	5	\N	\N	1	10	\N
4414	40	\N	5	\N	\N	1	10	\N
4415	96	\N	5	\N	\N	1	10	\N
4416	99	\N	5	\N	\N	1	10	\N
4448	2	\N	21	8	\N	1	8	\N
4450	2	\N	5	8	\N	1	8	\N
4449	2	\N	5	8	\N	1	10	\N
4451	2	\N	5	8	\N	1	10	\N
4445	3	\N	21	14	\N	1	1	\N
4421	2	\N	1	14	\N	1	8	\N
4439	3	\N	7	14	\N	1	14	\N
4435	2	\N	5	14	\N	1	8	\N
4433	2	\N	1	14	\N	1	8	\N
4424	2	\N	21	14	\N	1	1	\N
4441	3	\N	21	14	\N	1	1	\N
4425	2	\N	21	14	\N	1	1	\N
4430	2	\N	5	14	\N	1	8	\N
4434	2	\N	5	14	\N	1	1	\N
4428	2	\N	5	14	\N	1	1	\N
4429	2	\N	5	14	\N	1	8	\N
4443	3	\N	21	14	\N	1	1	\N
4423	2	\N	21	14	\N	1	1	\N
4440	3	\N	21	14	\N	1	1	\N
4442	3	\N	21	14	\N	1	1	\N
4419	2	\N	21	14	\N	1	1	\N
4432	2	\N	5	14	\N	1	1	\N
4427	2	\N	5	14	\N	1	1	\N
4418	2	\N	5	14	\N	1	8	\N
4431	2	\N	5	14	\N	1	10	\N
4426	2	\N	5	14	\N	1	1	\N
4438	3	\N	5	14	\N	1	1	\N
4446	3	\N	5	14	\N	1	1	\N
592	3	\N	1	8	2006-08-10	1	8	\N
4437	3	\N	21	14	\N	1	1	\N
4452	34	\N	5	\N	\N	1	1	\N
4453	38	\N	5	\N	\N	1	1	\N
621	2	\N	21	10	2006-08-15	1	1	\N
4454	84	\N	5	\N	\N	1	1	\N
4455	36	\N	5	\N	\N	1	1	\N
4456	40	\N	5	\N	\N	1	1	\N
4386	2	\N	5	10	\N	1	8	\N
4457	45	\N	5	\N	\N	1	1	\N
4458	26	\N	5	\N	\N	1	1	\N
3773	17	\N	21	10	\N	1	1	\N
3774	17	\N	21	10	\N	1	1	\N
4352	3	\N	21	10	\N	1	1	\N
4236	2	\N	21	8	\N	1	1	\N
3970	16	\N	21	10	\N	1	1	\N
4417	2	\N	21	14	\N	1	1	\N
4133	18	\N	5	10	\N	1	1	\N
3775	17	\N	21	10	\N	1	10	\N
3776	17	\N	21	10	\N	1	10	\N
3008	16	\N	21	11	\N	1	10	\N
4248	2	\N	21	8	\N	1	10	\N
3007	16	\N	21	11	\N	1	10	\N
3319	17	\N	21	10	2007-02-13	1	10	\N
3320	17	\N	21	10	2007-02-13	1	10	\N
2957	16	\N	21	11	2007-02-13	1	10	\N
3197	2	\N	21	1	2007-02-13	1	10	\N
501	3	\N	21	17	2007-02-13	1	10	\N
2955	16	\N	21	11	2007-02-13	1	10	\N
3185	2	\N	21	10	2007-02-13	1	10	\N
4459	41	\N	5	\N	\N	1	10	\N
4460	41	\N	5	\N	\N	1	10	\N
4135	18	\N	5	10	\N	1	10	\N
4134	18	\N	5	10	\N	1	10	\N
4461	41	\N	5	\N	\N	1	10	\N
147	2	\N	1	1	2006-08-18	1	10	\N
3749	17	\N	21	10	\N	1	8	\N
3815	17	\N	21	10	\N	1	8	\N
471	3	\N	21	17	2006-08-18	1	8	\N
3971	16	\N	21	10	\N	1	8	\N
2739	2	\N	21	14	2007-01-11	1	8	\N
472	3	\N	21	17	2006-08-18	1	8	\N
3972	16	\N	21	10	\N	1	8	\N
411	2	\N	21	1	2006-08-18	1	8	\N
4462	18	\N	5	8	\N	1	8	\N
4463	87	\N	5	\N	\N	1	8	\N
4370	2	\N	5	10	\N	1	10	\N
4464	87	\N	5	\N	\N	1	10	\N
4465	87	\N	5	\N	\N	1	10	\N
3919	3	\N	18	10	\N	\N	\N	\N
4473	61	\N	5	\N	\N	1	1	\N
2813	3	\N	18	10	2007-02-14	\N	\N	\N
4470	2	\N	18	10	\N	1	10	\N
4474	101	\N	5	\N	\N	1	1	\N
4475	101	\N	5	\N	\N	1	1	\N
4476	3	\N	18	10	\N	1	10	\N
4277	3	\N	5	1	\N	1	10	\N
4222	2	\N	5	8	\N	1	10	\N
4241	2	\N	5	8	\N	1	8	\N
3067	16	\N	5	1	2007-01-23	1	10	\N
4478	87	\N	5	\N	\N	1	10	\N
4367	2	\N	5	10	\N	1	8	\N
4122	2	\N	5	10	\N	1	8	\N
4491	3	\N	18	10	\N	1	10	\N
910	18	\N	1	\N	\N	5	5	\N
4492	3	\N	18	10	\N	1	10	\N
4496	3	\N	18	10	\N	1	10	\N
4497	3	\N	18	10	\N	1	10	\N
3777	17	\N	21	10	\N	1	1	\N
3778	17	\N	21	10	\N	1	1	\N
4500	18	\N	5	1	\N	1	1	\N
4510	87	\N	5	\N	\N	1	1	\N
4480	3	\N	21	14	\N	1	1	\N
3893	2	\N	21	14	\N	1	1	\N
4368	2	\N	5	10	\N	1	8	\N
4479	3	\N	21	14	\N	1	1	\N
3073	16	\N	21	1	\N	1	1	\N
4225	2	\N	21	8	\N	1	1	\N
3779	17	\N	21	10	\N	1	1	\N
3780	17	\N	21	10	\N	1	1	\N
4488	3	\N	21	14	\N	1	1	\N
3009	16	\N	21	11	\N	1	1	\N
2997	16	\N	21	11	\N	1	1	\N
4501	18	\N	5	1	\N	1	1	\N
4523	1	\N	5	10	\N	1	10	\N
4520	2	\N	18	1	\N	\N	\N	\N
4526	1	\N	5	10	\N	1	1	\N
4525	1	\N	5	10	\N	1	1	\N
4521	45	\N	5	\N	\N	1	1	\N
4224	2	\N	5	8	\N	1	8	\N
4247	2	\N	5	8	\N	1	8	\N
731	2	\N	5	8	2006-08-18	1	8	\N
4482	3	\N	5	14	\N	1	1	\N
613	2	\N	5	1	2006-08-10	1	1	\N
622	2	\N	5	10	2006-08-15	1	1	\N
4527	1	\N	7	10	\N	1	10	\N
3079	16	\N	1	1	\N	1	10	\N
3080	16	\N	1	1	\N	1	10	\N
3969	16	\N	1	10	\N	1	10	\N
3976	16	\N	1	10	\N	1	10	\N
3974	16	\N	1	10	\N	1	10	\N
3781	17	\N	21	10	\N	1	1	\N
3782	17	\N	21	10	\N	1	1	\N
3625	16	\N	21	10	\N	1	1	\N
3622	16	\N	21	10	\N	1	1	\N
607	2	\N	21	1	2006-08-10	1	1	\N
3783	17	\N	21	10	\N	1	1	\N
3784	17	\N	21	10	\N	1	1	\N
4481	3	\N	21	14	\N	1	1	\N
4420	2	\N	21	14	\N	1	1	\N
3975	16	\N	1	10	\N	1	10	\N
4251	2	\N	18	8	\N	1	1	\N
4528	45	\N	5	\N	\N	1	10	\N
4490	2	\N	5	10	\N	1	8	\N
4489	2	\N	5	10	\N	1	8	\N
369	2	\N	1	1	2006-09-15	1	8	\N
4534	45	\N	5	\N	\N	1	10	\N
4531	2	\N	18	8	\N	1	8	\N
4529	2	\N	18	8	\N	1	8	\N
3623	16	\N	5	10	\N	1	10	\N
4515	2	\N	21	1	\N	1	8	\N
4502	18	\N	5	1	\N	1	1	\N
4503	18	\N	5	1	\N	1	1	\N
4514	2	\N	5	1	\N	1	1	\N
4535	84	\N	5	\N	\N	1	1	\N
510	3	\N	18	17	2006-11-28	\N	\N	\N
4538	2	\N	18	10	\N	1	10	\N
4536	3	\N	18	10	\N	1	10	\N
4537	2	\N	18	10	\N	1	10	\N
3954	16	\N	21	10	\N	1	10	\N
535	3	\N	21	17	2007-01-22	1	10	\N
4149	2	\N	21	8	\N	1	10	\N
1257	3	\N	21	14	2007-01-09	1	10	\N
4150	2	\N	21	8	\N	1	10	\N
4532	2	\N	1	8	\N	1	14	\N
4530	2	\N	1	8	\N	1	14	\N
4533	2	\N	1	8	\N	1	14	\N
4541	2	\N	18	10	\N	\N	\N	\N
4540	2	\N	18	10	\N	\N	\N	\N
4216	22	\N	5	1	\N	1	1	\N
4466	2	\N	5	10	\N	1	1	\N
4516	2	\N	18	1	\N	\N	\N	\N
4517	2	\N	18	1	\N	\N	\N	\N
4511	2	\N	5	1	\N	1	1	\N
4522	1	\N	5	10	\N	1	1	\N
4467	2	\N	5	10	\N	1	1	\N
4246	2	\N	5	8	\N	1	1	\N
4131	18	\N	5	10	\N	1	1	\N
3620	16	\N	5	10	\N	1	1	\N
3621	16	\N	5	10	\N	1	1	\N
4543	25	\N	5	\N	\N	1	1	\N
4544	28	\N	5	\N	\N	1	1	\N
4545	37	\N	5	\N	\N	1	1	\N
4546	26	\N	5	\N	\N	1	1	\N
606	2	\N	5	1	2006-08-10	1	8	\N
4539	2	\N	5	10	\N	1	8	\N
3786	17	\N	21	10	\N	1	1	\N
3785	17	\N	21	10	\N	1	1	\N
4504	18	\N	5	1	\N	1	1	\N
4547	25	\N	5	\N	\N	1	1	\N
4548	28	\N	5	\N	\N	1	1	\N
4549	37	\N	5	\N	\N	1	1	\N
4550	37	\N	5	\N	\N	1	1	\N
4551	26	\N	5	\N	\N	1	1	\N
4518	2	\N	18	1	\N	1	8	\N
4570	2	\N	18	1	\N	\N	\N	\N
4553	87	\N	5	\N	\N	1	1	\N
3362	2	\N	19	8	2007-01-23	1	8	\N
4552	84	\N	5	\N	\N	3	5	\N
4513	2	\N	5	1	\N	1	8	\N
4554	22	\N	6	1	\N	\N	\N	\N
3892	2	\N	5	14	\N	1	1	\N
2788	2	\N	18	10	2007-03-01	\N	\N	\N
4568	2	\N	18	1	\N	\N	\N	\N
4542	1	\N	1	10	\N	1	1	\N
4495	2	\N	5	10	\N	1	8	\N
601	2	\N	5	1	2006-08-10	1	8	\N
4487	3	\N	5	14	\N	1	8	\N
3816	17	\N	21	10	\N	1	8	\N
3817	17	\N	21	10	\N	1	8	\N
3675	16	\N	21	10	\N	1	8	\N
4586	2	\N	21	1	\N	1	8	\N
3676	16	\N	21	10	\N	1	8	\N
4585	2	\N	21	1	\N	1	8	\N
4154	3	\N	5	10	\N	1	10	\N
3188	2	\N	5	10	2007-01-24	1	10	\N
4604	3	\N	7	10	\N	1	10	\N
4603	3	\N	7	10	\N	1	10	\N
4602	3	\N	7	10	\N	1	10	\N
4601	3	\N	7	10	\N	1	10	\N
4600	3	\N	7	10	\N	1	10	\N
4599	3	\N	7	10	\N	1	10	\N
4598	3	\N	7	10	\N	1	10	\N
4597	3	\N	7	10	\N	1	10	\N
4596	3	\N	7	10	\N	1	10	\N
4595	3	\N	7	10	\N	1	10	\N
4594	3	\N	7	10	\N	1	10	\N
4593	3	\N	7	10	\N	1	10	\N
4592	3	\N	7	10	\N	1	10	\N
4591	3	\N	7	10	\N	1	10	\N
4590	3	\N	7	10	\N	1	10	\N
4605	25	\N	5	\N	\N	1	1	\N
3236	16	\N	21	10	2007-02-21	1	8	\N
4606	28	\N	5	\N	\N	1	1	\N
2789	2	\N	21	10	2007-02-21	1	8	\N
4607	37	\N	5	\N	\N	1	1	\N
4608	30	\N	5	\N	\N	1	1	\N
3460	3	\N	21	10	2007-02-21	1	8	\N
4609	36	\N	5	\N	\N	1	1	\N
4610	40	\N	5	\N	\N	1	1	\N
4611	95	\N	5	\N	\N	1	1	\N
4612	99	\N	5	\N	\N	1	1	\N
4509	18	\N	5	1	\N	1	1	\N
4625	3	\N	5	8	\N	1	1	\N
4628	3	\N	5	8	\N	1	1	\N
4614	3	\N	5	8	\N	1	8	\N
4623	3	\N	21	8	\N	1	1	\N
4613	3	\N	21	8	\N	1	1	\N
4616	3	\N	21	8	\N	1	1	\N
4622	3	\N	21	8	\N	1	1	\N
4627	3	\N	21	8	\N	1	14	\N
4619	3	\N	21	8	\N	1	8	\N
4621	3	\N	21	8	\N	1	8	\N
4618	3	\N	21	8	\N	1	8	\N
4620	3	\N	21	8	\N	1	8	\N
4589	3	\N	19	10	\N	1	8	\N
4629	2	\N	19	10	\N	1	8	\N
4587	3	\N	18	10	\N	1	10	\N
3679	16	\N	5	10	\N	1	8	\N
4624	3	\N	5	8	\N	1	10	\N
4617	3	\N	21	8	\N	1	8	\N
861	2	\N	18	10	2006-09-04	1	1	\N
4630	87	\N	5	\N	\N	1	8	\N
4575	2	\N	5	1	\N	1	1	\N
4576	2	\N	18	1	\N	\N	\N	\N
4577	2	\N	18	1	\N	\N	\N	\N
4578	2	\N	18	1	\N	\N	\N	\N
3532	2	\N	18	10	2007-02-22	1	8	\N
2270	2	\N	18	10	\N	\N	\N	\N
4579	2	\N	18	1	\N	\N	\N	\N
4580	2	\N	18	1	\N	\N	\N	\N
960	2	\N	18	8	2006-10-17	\N	\N	\N
376	2	\N	5	1	2006-08-31	1	1	\N
871	2	\N	5	10	2006-09-04	1	1	\N
4583	2	\N	18	1	\N	\N	\N	\N
4631	2	\N	18	1	\N	\N	\N	\N
4632	2	\N	18	1	\N	\N	\N	\N
4633	2	\N	18	1	\N	\N	\N	\N
4634	2	\N	18	1	\N	\N	\N	\N
4635	2	\N	18	1	\N	\N	\N	\N
997	2	\N	18	8	2006-09-27	\N	\N	\N
4636	2	\N	18	1	\N	\N	\N	\N
869	2	\N	5	10	2006-09-04	1	1	\N
3794	17	\N	18	10	\N	1	10	\N
4652	2	\N	18	10	\N	1	10	\N
2801	2	\N	18	10	2006-11-21	\N	\N	\N
4651	2	\N	18	10	\N	1	10	\N
4637	2	\N	5	1	\N	1	8	\N
4654	60	\N	5	\N	\N	1	8	\N
4655	61	\N	5	\N	\N	1	8	\N
4656	60	\N	5	\N	\N	1	8	\N
4657	61	\N	5	\N	\N	1	8	\N
4658	61	\N	5	\N	\N	1	8	\N
4659	96	\N	5	\N	\N	1	8	\N
4660	99	\N	5	\N	\N	1	8	\N
4661	61	\N	5	\N	\N	1	8	\N
4662	96	\N	5	\N	\N	1	8	\N
4663	99	\N	5	\N	\N	1	8	\N
3818	17	\N	21	10	\N	1	1	\N
3819	17	\N	21	10	\N	1	1	\N
3681	16	\N	21	10	\N	1	1	\N
993	2	\N	21	8	2006-09-08	1	1	\N
4505	18	\N	5	1	\N	1	1	\N
4664	28	\N	5	\N	\N	1	1	\N
4665	30	\N	5	\N	\N	1	1	\N
4666	25	\N	5	\N	\N	1	1	\N
4667	36	\N	5	\N	\N	1	1	\N
4668	37	\N	5	\N	\N	1	1	\N
4669	38	\N	5	\N	\N	1	1	\N
4670	40	\N	5	\N	\N	1	1	\N
4671	95	\N	5	\N	\N	1	1	\N
4672	99	\N	5	\N	\N	1	1	\N
4647	2	\N	21	1	\N	1	8	\N
4148	2	\N	18	8	\N	1	8	\N
555	3	\N	18	17	2006-12-13	\N	\N	\N
4581	2	\N	5	1	\N	1	8	\N
4643	2	\N	18	1	\N	\N	\N	\N
4382	2	\N	18	10	\N	1	1	\N
4673	42	\N	5	\N	\N	1	1	\N
4674	42	\N	5	\N	\N	1	1	\N
4644	2	\N	18	1	\N	\N	\N	\N
4645	2	\N	18	1	\N	\N	\N	\N
4646	2	\N	18	1	\N	\N	\N	\N
4649	2	\N	18	1	\N	\N	\N	\N
4650	2	\N	18	1	\N	\N	\N	\N
3823	17	\N	7	10	\N	1	1	\N
3822	17	\N	7	10	\N	1	1	\N
4571	2	\N	21	1	\N	1	1	\N
4158	2	\N	21	10	\N	1	1	\N
3824	17	\N	21	10	\N	1	1	\N
3825	17	\N	21	10	\N	1	1	\N
4675	2	\N	6	14	\N	\N	\N	\N
4676	2	\N	6	14	\N	\N	\N	\N
4677	2	\N	6	14	\N	\N	\N	\N
4678	2	\N	6	14	\N	\N	\N	\N
4679	2	\N	6	14	\N	\N	\N	\N
4680	2	\N	6	14	\N	\N	\N	\N
4681	2	\N	6	14	\N	\N	\N	\N
4682	2	\N	6	14	\N	\N	\N	\N
4683	2	\N	6	14	\N	\N	\N	\N
4684	2	\N	6	14	\N	\N	\N	\N
4685	2	\N	6	14	\N	\N	\N	\N
4686	2	\N	6	14	\N	\N	\N	\N
4687	2	\N	6	14	\N	\N	\N	\N
4688	2	\N	6	14	\N	\N	\N	\N
4689	2	\N	6	14	\N	\N	\N	\N
4690	2	\N	6	14	\N	\N	\N	\N
4691	2	\N	6	14	\N	\N	\N	\N
4692	2	\N	6	14	\N	\N	\N	\N
4693	2	\N	6	14	\N	\N	\N	\N
4694	2	\N	6	14	\N	\N	\N	\N
4695	2	\N	6	14	\N	\N	\N	\N
4696	2	\N	6	14	\N	\N	\N	\N
4697	2	\N	6	14	\N	\N	\N	\N
4698	2	\N	6	14	\N	\N	\N	\N
4699	2	\N	6	14	\N	\N	\N	\N
4700	2	\N	6	14	\N	\N	\N	\N
4701	2	\N	6	14	\N	\N	\N	\N
4702	2	\N	6	14	\N	\N	\N	\N
4703	2	\N	6	14	\N	\N	\N	\N
4704	2	\N	6	14	\N	\N	\N	\N
4705	2	\N	6	14	\N	\N	\N	\N
4706	2	\N	6	14	\N	\N	\N	\N
4707	2	\N	6	14	\N	\N	\N	\N
4708	2	\N	6	14	\N	\N	\N	\N
4709	2	\N	6	14	\N	\N	\N	\N
4710	2	\N	6	14	\N	\N	\N	\N
4711	2	\N	6	14	\N	\N	\N	\N
4712	2	\N	6	14	\N	\N	\N	\N
4713	2	\N	6	14	\N	\N	\N	\N
4714	2	\N	6	14	\N	\N	\N	\N
2987	16	\N	21	11	\N	1	1	\N
2986	16	\N	21	11	\N	1	1	\N
4508	18	\N	5	1	\N	1	1	\N
4715	97	\N	5	\N	\N	1	1	\N
4716	40	\N	5	\N	\N	1	1	\N
4717	96	\N	5	\N	\N	1	1	\N
4718	99	\N	5	\N	\N	1	1	\N
4719	97	\N	5	\N	\N	1	1	\N
4720	40	\N	5	\N	\N	1	1	\N
4721	96	\N	5	\N	\N	1	1	\N
4722	99	\N	5	\N	\N	1	1	\N
4723	2	\N	6	14	\N	\N	\N	\N
4724	2	\N	6	14	\N	\N	\N	\N
4725	2	\N	6	14	\N	\N	\N	\N
4726	2	\N	6	14	\N	\N	\N	\N
4727	2	\N	6	14	\N	\N	\N	\N
4728	2	\N	6	14	\N	\N	\N	\N
4729	2	\N	6	14	\N	\N	\N	\N
4730	2	\N	6	14	\N	\N	\N	\N
4731	2	\N	6	14	\N	\N	\N	\N
4732	2	\N	6	14	\N	\N	\N	\N
4733	2	\N	6	14	\N	\N	\N	\N
4734	2	\N	6	14	\N	\N	\N	\N
4735	2	\N	6	14	\N	\N	\N	\N
4736	2	\N	6	14	\N	\N	\N	\N
4737	2	\N	6	14	\N	\N	\N	\N
4738	2	\N	6	14	\N	\N	\N	\N
4739	2	\N	6	14	\N	\N	\N	\N
4740	2	\N	6	14	\N	\N	\N	\N
4741	2	\N	6	14	\N	\N	\N	\N
4742	2	\N	6	14	\N	\N	\N	\N
4507	18	\N	5	1	\N	1	10	\N
4572	2	\N	5	1	\N	1	10	\N
4648	2	\N	3	1	\N	1	10	\N
2295	3	\N	21	8	2007-02-27	1	10	\N
4569	2	\N	5	1	\N	1	10	\N
4469	2	\N	5	10	\N	1	10	\N
4477	2	\N	5	10	\N	1	10	\N
4759	2	\N	18	1	\N	\N	\N	\N
2824	2	\N	1	14	2006-11-29	1	8	\N
450	2	\N	5	1	2006-07-26	1	10	\N
4120	3	\N	21	8	2007-04-12	1	1	\N
2918	16	\N	21	11	\N	1	1	\N
3577	2	\N	21	10	2007-03-06	1	1	\N
4116	3	\N	21	8	2007-04-12	1	1	\N
3288	16	\N	21	10	\N	1	1	\N
3574	2	\N	21	10	2007-03-06	1	1	\N
4268	18	\N	5	14	\N	1	1	\N
3839	17	\N	21	10	\N	1	14	\N
3840	17	\N	21	10	\N	1	14	\N
4319	2	\N	21	10	\N	1	14	\N
3838	17	\N	21	10	\N	1	14	\N
3837	17	\N	21	10	\N	1	14	\N
4584	2	\N	5	1	\N	1	10	\N
2250	2	\N	19	10	2006-11-29	1	10	\N
3829	17	\N	21	10	\N	1	14	\N
3828	17	\N	21	10	\N	1	14	\N
4486	3	\N	21	14	\N	1	14	\N
2988	16	\N	21	11	\N	1	14	\N
3076	16	\N	21	1	\N	1	14	\N
929	2	\N	21	8	2006-09-08	1	14	\N
4773	18	\N	5	14	\N	1	14	\N
2990	16	\N	5	11	\N	1	10	\N
4749	2	\N	1	14	\N	1	10	\N
4748	2	\N	1	14	\N	1	10	\N
4779	2	\N	21	14	\N	1	8	\N
4793	2	\N	5	14	\N	1	8	\N
4795	2	\N	5	14	\N	1	8	\N
4783	2	\N	21	14	\N	1	8	\N
4797	22	\N	5	10	\N	1	10	\N
4792	2	\N	5	14	\N	1	8	\N
4785	2	\N	21	14	\N	1	8	\N
4802	22	\N	1	10	\N	1	10	\N
4801	22	\N	1	10	\N	1	10	\N
4800	22	\N	1	10	\N	1	10	\N
4799	22	\N	1	10	\N	1	10	\N
4798	22	\N	1	10	\N	1	10	\N
4796	22	\N	1	10	\N	1	10	\N
3977	16	\N	18	10	\N	1	10	\N
3075	16	\N	5	1	\N	1	10	\N
4788	2	\N	1	14	\N	1	8	\N
4786	2	\N	5	14	\N	1	8	\N
4790	2	\N	18	14	\N	\N	\N	\N
4791	2	\N	18	14	\N	1	8	\N
4789	2	\N	5	14	\N	1	8	\N
4787	2	\N	21	14	\N	1	8	\N
4794	2	\N	5	14	\N	1	1	\N
2902	16	\N	21	11	\N	1	8	\N
4778	2	\N	5	14	\N	1	8	\N
4776	2	\N	5	14	\N	1	10	\N
4784	2	\N	21	14	\N	1	14	\N
4781	2	\N	5	14	\N	1	8	\N
4782	2	\N	5	14	\N	1	10	\N
4841	3	\N	5	10	\N	1	1	\N
4830	3	\N	5	10	\N	1	1	\N
969	2	\N	1	8	2006-09-08	1	10	\N
4311	3	\N	21	10	\N	1	8	\N
4835	3	\N	21	10	\N	1	10	\N
4836	3	\N	21	10	\N	1	10	\N
4839	3	\N	21	10	\N	1	8	\N
591	3	\N	1	8	2006-08-10	1	10	\N
750	2	\N	3	10	2006-08-31	1	10	\N
4837	3	\N	21	10	\N	1	8	\N
4838	3	\N	21	10	\N	1	8	\N
4743	2	\N	5	14	\N	1	8	\N
4803	76	\N	5	\N	\N	1	1	\N
2157	13	\N	19	10	2006-10-16	1	10	\N
2156	13	\N	19	10	2006-10-16	1	10	\N
2153	13	\N	19	10	2006-10-16	1	10	\N
2152	13	\N	19	10	2006-10-16	1	10	\N
2150	13	\N	19	10	2006-10-16	1	10	\N
2151	13	\N	19	10	2006-10-16	1	10	\N
2154	13	\N	19	10	2006-10-16	1	10	\N
2155	13	\N	19	10	2006-10-16	1	10	\N
4582	2	\N	5	1	\N	1	1	\N
4752	2	\N	5	14	\N	1	1	\N
4804	76	\N	5	\N	\N	1	1	\N
4775	18	\N	5	14	\N	1	10	\N
4774	18	\N	5	14	\N	1	10	\N
4805	97	\N	5	\N	\N	1	10	\N
4806	96	\N	5	\N	\N	1	10	\N
4807	99	\N	5	\N	\N	1	10	\N
4808	40	\N	5	\N	\N	1	10	\N
4809	97	\N	5	\N	\N	1	10	\N
4810	40	\N	5	\N	\N	1	10	\N
4811	96	\N	5	\N	\N	1	10	\N
4812	99	\N	5	\N	\N	1	10	\N
4512	2	\N	5	1	\N	1	8	\N
4813	40	\N	5	\N	\N	1	10	\N
4814	99	\N	5	\N	\N	1	10	\N
4815	87	\N	5	\N	\N	1	1	\N
4816	2	\N	6	10	\N	\N	\N	\N
4817	2	\N	6	10	\N	\N	\N	\N
4818	2	\N	6	10	\N	\N	\N	\N
4819	3	\N	6	10	\N	\N	\N	\N
4820	3	\N	6	10	\N	\N	\N	\N
4821	3	\N	6	10	\N	\N	\N	\N
4822	3	\N	6	10	\N	\N	\N	\N
4826	87	\N	5	\N	\N	1	8	\N
4827	3	\N	6	10	\N	\N	\N	\N
4828	3	\N	6	10	\N	\N	\N	\N
4829	3	\N	6	10	\N	\N	\N	\N
3731	3	\N	5	10	2007-03-15	1	10	\N
1276	2	\N	5	1	2006-10-26	1	10	\N
4498	3	\N	1	10	\N	1	8	\N
4499	2	\N	1	10	\N	1	8	\N
4944	17	\N	21	10	\N	1	1	\N
4943	17	\N	21	10	\N	1	1	\N
4976	18	\N	5	10	\N	1	1	\N
4931	17	\N	21	10	\N	1	8	\N
4932	17	\N	21	10	\N	1	8	\N
5012	18	\N	5	10	\N	1	1	\N
4930	17	\N	21	10	\N	1	1	\N
4929	17	\N	21	10	\N	1	1	\N
4977	18	\N	5	10	\N	1	1	\N
4904	17	\N	21	10	\N	1	8	\N
4905	17	\N	21	10	\N	1	8	\N
4918	17	\N	21	10	\N	1	1	\N
4917	17	\N	21	10	\N	1	1	\N
4906	17	\N	21	10	\N	1	8	\N
4907	17	\N	21	10	\N	1	8	\N
4908	17	\N	21	10	\N	1	8	\N
4909	17	\N	21	10	\N	1	8	\N
4978	18	\N	5	10	\N	1	1	\N
4920	17	\N	21	10	\N	1	10	\N
4919	17	\N	21	10	\N	1	10	\N
4979	18	\N	18	10	\N	1	10	\N
4921	17	\N	18	10	\N	\N	\N	\N
4910	17	\N	21	10	\N	1	1	\N
4911	17	\N	21	10	\N	1	1	\N
4927	17	\N	21	10	\N	1	8	\N
4928	17	\N	21	10	\N	1	8	\N
4987	18	\N	5	10	\N	1	1	\N
4922	17	\N	21	10	\N	1	8	\N
4923	17	\N	21	10	\N	1	8	\N
4924	17	\N	21	10	\N	1	8	\N
4925	17	\N	21	10	\N	1	8	\N
4981	18	\N	5	10	\N	1	8	\N
4980	18	\N	5	10	\N	1	8	\N
4926	17	\N	21	10	\N	1	8	\N
4903	17	\N	21	10	\N	1	8	\N
4995	18	\N	5	10	\N	1	1	\N
4997	18	\N	5	10	\N	1	1	\N
4998	18	\N	5	10	\N	1	8	\N
5001	18	\N	5	10	\N	1	8	\N
5000	18	\N	5	10	\N	1	8	\N
5007	18	\N	5	10	\N	1	8	\N
4999	18	\N	5	10	\N	1	1	\N
5002	18	\N	5	10	\N	1	1	\N
5004	18	\N	5	10	\N	1	8	\N
5005	18	\N	5	10	\N	1	8	\N
5006	18	\N	5	10	\N	1	8	\N
5003	18	\N	5	10	\N	1	8	\N
4913	17	\N	21	10	\N	1	10	\N
4912	17	\N	21	10	\N	1	10	\N
4914	17	\N	21	10	\N	1	8	\N
4915	17	\N	21	10	\N	1	8	\N
4967	18	\N	6	10	\N	\N	\N	\N
4996	18	\N	6	10	\N	\N	\N	\N
5021	16	\N	6	10	\N	\N	\N	\N
5022	16	\N	6	10	\N	\N	\N	\N
5023	16	\N	6	10	\N	\N	\N	\N
5024	16	\N	6	10	\N	\N	\N	\N
5025	16	\N	6	10	\N	\N	\N	\N
5026	16	\N	6	10	\N	\N	\N	\N
5027	16	\N	6	10	\N	\N	\N	\N
5028	16	\N	6	10	\N	\N	\N	\N
5029	16	\N	6	10	\N	\N	\N	\N
5030	16	\N	6	10	\N	\N	\N	\N
5031	16	\N	6	10	\N	\N	\N	\N
5032	16	\N	6	10	\N	\N	\N	\N
5033	16	\N	6	10	\N	\N	\N	\N
5034	16	\N	6	10	\N	\N	\N	\N
5035	16	\N	6	10	\N	\N	\N	\N
5036	16	\N	6	10	\N	\N	\N	\N
5037	16	\N	6	10	\N	\N	\N	\N
5038	16	\N	6	10	\N	\N	\N	\N
5039	16	\N	6	10	\N	\N	\N	\N
5040	16	\N	6	10	\N	\N	\N	\N
5041	16	\N	6	10	\N	\N	\N	\N
5042	16	\N	6	10	\N	\N	\N	\N
5043	16	\N	6	10	\N	\N	\N	\N
5044	16	\N	6	10	\N	\N	\N	\N
5045	16	\N	6	10	\N	\N	\N	\N
5046	16	\N	6	10	\N	\N	\N	\N
5047	16	\N	6	10	\N	\N	\N	\N
5048	16	\N	6	10	\N	\N	\N	\N
5049	16	\N	6	10	\N	\N	\N	\N
5050	16	\N	6	10	\N	\N	\N	\N
5051	16	\N	6	10	\N	\N	\N	\N
5052	16	\N	6	10	\N	\N	\N	\N
5053	16	\N	6	10	\N	\N	\N	\N
5054	16	\N	6	10	\N	\N	\N	\N
5055	16	\N	6	10	\N	\N	\N	\N
5056	16	\N	6	10	\N	\N	\N	\N
5057	16	\N	6	10	\N	\N	\N	\N
5058	16	\N	6	10	\N	\N	\N	\N
5059	16	\N	6	10	\N	\N	\N	\N
5060	16	\N	6	10	\N	\N	\N	\N
5061	16	\N	6	10	\N	\N	\N	\N
5062	16	\N	6	10	\N	\N	\N	\N
5063	16	\N	6	10	\N	\N	\N	\N
5064	16	\N	6	10	\N	\N	\N	\N
5065	16	\N	6	10	\N	\N	\N	\N
5066	16	\N	6	10	\N	\N	\N	\N
5067	16	\N	6	10	\N	\N	\N	\N
5068	16	\N	6	10	\N	\N	\N	\N
5069	16	\N	6	10	\N	\N	\N	\N
5070	16	\N	6	10	\N	\N	\N	\N
5071	16	\N	6	10	\N	\N	\N	\N
5072	16	\N	6	10	\N	\N	\N	\N
5073	16	\N	6	10	\N	\N	\N	\N
5074	16	\N	6	10	\N	\N	\N	\N
5075	16	\N	6	10	\N	\N	\N	\N
5076	16	\N	6	10	\N	\N	\N	\N
5077	16	\N	6	10	\N	\N	\N	\N
5078	16	\N	6	10	\N	\N	\N	\N
5079	16	\N	6	10	\N	\N	\N	\N
5080	16	\N	6	10	\N	\N	\N	\N
5081	16	\N	6	10	\N	\N	\N	\N
5082	16	\N	6	10	\N	\N	\N	\N
5083	16	\N	6	10	\N	\N	\N	\N
5084	16	\N	6	10	\N	\N	\N	\N
5085	16	\N	6	10	\N	\N	\N	\N
5086	16	\N	6	10	\N	\N	\N	\N
5087	16	\N	6	10	\N	\N	\N	\N
5088	16	\N	6	10	\N	\N	\N	\N
5089	16	\N	6	10	\N	\N	\N	\N
5090	16	\N	6	10	\N	\N	\N	\N
5091	16	\N	6	10	\N	\N	\N	\N
5092	16	\N	6	10	\N	\N	\N	\N
5093	16	\N	6	10	\N	\N	\N	\N
5094	16	\N	6	10	\N	\N	\N	\N
5095	16	\N	6	10	\N	\N	\N	\N
5096	16	\N	6	10	\N	\N	\N	\N
5097	16	\N	6	10	\N	\N	\N	\N
5098	16	\N	6	10	\N	\N	\N	\N
5099	16	\N	6	10	\N	\N	\N	\N
5100	16	\N	6	10	\N	\N	\N	\N
5101	16	\N	6	10	\N	\N	\N	\N
5102	16	\N	6	10	\N	\N	\N	\N
5103	16	\N	6	10	\N	\N	\N	\N
5104	16	\N	6	10	\N	\N	\N	\N
5105	16	\N	6	10	\N	\N	\N	\N
5106	16	\N	6	10	\N	\N	\N	\N
5107	16	\N	6	10	\N	\N	\N	\N
5108	16	\N	6	10	\N	\N	\N	\N
5109	16	\N	6	10	\N	\N	\N	\N
5110	16	\N	6	10	\N	\N	\N	\N
5111	16	\N	6	10	\N	\N	\N	\N
5112	16	\N	6	10	\N	\N	\N	\N
5113	16	\N	6	10	\N	\N	\N	\N
5114	16	\N	6	10	\N	\N	\N	\N
5115	16	\N	6	10	\N	\N	\N	\N
5116	16	\N	6	10	\N	\N	\N	\N
5117	16	\N	6	10	\N	\N	\N	\N
5118	16	\N	6	10	\N	\N	\N	\N
5119	16	\N	6	10	\N	\N	\N	\N
5120	16	\N	6	10	\N	\N	\N	\N
5121	16	\N	6	10	\N	\N	\N	\N
5122	16	\N	6	10	\N	\N	\N	\N
5123	16	\N	6	10	\N	\N	\N	\N
5124	16	\N	6	10	\N	\N	\N	\N
5140	16	\N	6	10	\N	\N	\N	\N
5141	16	\N	6	10	\N	\N	\N	\N
5142	16	\N	6	10	\N	\N	\N	\N
5143	16	\N	6	10	\N	\N	\N	\N
5144	16	\N	6	10	\N	\N	\N	\N
5145	16	\N	6	10	\N	\N	\N	\N
5146	16	\N	6	10	\N	\N	\N	\N
5147	16	\N	6	10	\N	\N	\N	\N
5148	16	\N	6	10	\N	\N	\N	\N
5149	16	\N	6	10	\N	\N	\N	\N
5150	16	\N	6	10	\N	\N	\N	\N
5013	18	\N	5	10	\N	1	8	\N
3677	16	\N	21	10	\N	1	8	\N
5014	18	\N	5	10	\N	1	1	\N
5137	16	\N	18	10	\N	\N	\N	\N
4639	2	\N	21	1	\N	1	10	\N
4640	2	\N	5	1	\N	1	10	\N
3854	17	\N	21	10	\N	1	8	\N
3855	17	\N	21	10	\N	1	8	\N
4832	3	\N	21	10	\N	1	8	\N
3694	16	\N	21	10	\N	1	8	\N
4862	2	\N	21	10	\N	1	8	\N
4831	3	\N	21	10	\N	1	8	\N
3695	16	\N	21	10	\N	1	8	\N
4861	2	\N	21	10	\N	1	8	\N
4956	18	\N	5	10	\N	1	1	\N
5151	36	\N	5	\N	\N	1	1	\N
5152	37	\N	5	\N	\N	1	1	\N
5153	38	\N	5	\N	\N	1	1	\N
5154	84	\N	5	\N	\N	1	1	\N
5155	26	\N	5	\N	\N	1	1	\N
3856	17	\N	21	10	\N	1	8	\N
3857	17	\N	21	10	\N	1	8	\N
4565	3	\N	21	1	\N	1	8	\N
3010	16	\N	21	11	\N	1	8	\N
4468	2	\N	21	10	\N	1	8	\N
4566	3	\N	21	1	\N	1	8	\N
2977	16	\N	21	11	\N	1	8	\N
4760	2	\N	21	1	\N	1	8	\N
924	2	\N	5	8	2006-09-08	1	8	\N
371	2	\N	18	1	2006-09-15	\N	\N	\N
3841	17	\N	21	10	\N	1	14	\N
3842	17	\N	21	10	\N	1	14	\N
4557	3	\N	21	1	\N	1	14	\N
2999	16	\N	21	11	\N	1	14	\N
4564	3	\N	21	1	\N	1	14	\N
3003	16	\N	21	11	\N	1	14	\N
957	2	\N	7	8	2006-09-05	1	10	\N
3858	17	\N	21	10	\N	1	8	\N
3859	17	\N	21	10	\N	1	8	\N
4875	2	\N	18	10	\N	1	10	\N
2989	16	\N	21	11	\N	1	8	\N
146	2	\N	21	1	2006-08-11	1	8	\N
4873	2	\N	18	10	\N	1	10	\N
2996	16	\N	21	11	\N	1	8	\N
4957	18	\N	5	10	\N	1	1	\N
5156	87	\N	5	\N	\N	1	1	\N
4872	2	\N	18	10	\N	1	10	\N
4845	3	\N	18	10	\N	\N	\N	\N
4958	18	\N	5	10	\N	1	1	\N
429	2	\N	5	1	2006-07-26	1	10	\N
949	2	\N	5	8	2006-09-04	1	10	\N
4959	18	\N	5	10	\N	1	1	\N
5157	87	\N	5	\N	\N	1	1	\N
3860	17	\N	21	10	\N	1	8	\N
3861	17	\N	21	10	\N	1	8	\N
2961	16	\N	21	11	\N	1	8	\N
4746	2	\N	21	14	\N	1	8	\N
3004	16	\N	21	11	\N	1	8	\N
4744	2	\N	5	14	\N	1	1	\N
4960	18	\N	5	10	\N	1	8	\N
5158	40	\N	5	\N	\N	1	8	\N
5159	87	\N	5	\N	\N	1	8	\N
5160	96	\N	5	\N	\N	1	8	\N
5161	99	\N	5	\N	\N	1	8	\N
3864	17	\N	21	10	\N	1	8	\N
3865	17	\N	21	10	\N	1	8	\N
3862	17	\N	21	10	\N	1	8	\N
3863	17	\N	21	10	\N	1	8	\N
5163	30	\N	5	\N	\N	1	8	\N
3696	16	\N	21	10	\N	1	8	\N
5164	36	\N	5	\N	\N	1	8	\N
4962	18	\N	5	10	\N	1	1	\N
5162	25	\N	5	\N	\N	1	1	\N
4763	3	\N	5	1	\N	1	8	\N
2975	16	\N	21	11	\N	1	8	\N
5165	61	\N	5	\N	\N	1	1	\N
5166	32	\N	5	\N	\N	1	1	\N
3965	16	\N	21	10	\N	1	8	\N
5167	96	\N	5	\N	\N	1	1	\N
5168	26	\N	5	\N	\N	1	1	\N
4753	2	\N	5	1	\N	1	8	\N
5169	37	\N	5	\N	\N	1	1	\N
5170	40	\N	5	\N	\N	1	1	\N
5171	99	\N	5	\N	\N	1	1	\N
4961	18	\N	5	10	\N	1	1	\N
713	3	\N	21	10	2006-08-14	1	8	\N
4863	2	\N	21	10	\N	1	8	\N
4874	2	\N	1	10	\N	1	14	\N
4866	2	\N	1	10	\N	1	14	\N
3821	17	\N	21	10	\N	1	10	\N
3820	17	\N	21	10	\N	1	10	\N
4963	18	\N	18	10	\N	1	10	\N
1417	2	\N	18	1	2006-10-16	\N	\N	\N
4762	2	\N	5	1	\N	1	8	\N
5172	87	\N	5	\N	\N	1	1	\N
5173	87	\N	5	\N	\N	1	8	\N
3208	2	\N	18	1	2007-01-23	\N	\N	\N
4758	2	\N	18	1	\N	\N	\N	\N
4757	2	\N	18	1	\N	\N	\N	\N
4387	2	\N	18	10	\N	\N	\N	\N
4396	3	\N	18	10	\N	\N	\N	\N
4825	3	\N	18	10	\N	\N	\N	\N
4202	3	\N	5	10	\N	1	10	\N
981	2	\N	5	8	2006-11-15	1	1	\N
3843	17	\N	21	10	\N	1	8	\N
3844	17	\N	21	10	\N	1	8	\N
3845	17	\N	21	10	\N	1	8	\N
3846	17	\N	21	10	\N	1	8	\N
4843	3	\N	21	10	\N	1	8	\N
4867	2	\N	21	10	\N	1	8	\N
4877	2	\N	5	10	\N	1	8	\N
4842	3	\N	21	10	\N	1	8	\N
3005	16	\N	21	11	\N	1	8	\N
938	2	\N	21	8	2006-09-04	1	8	\N
4965	18	\N	5	10	\N	1	8	\N
5174	97	\N	5	\N	\N	1	8	\N
4884	2	\N	5	10	\N	1	8	\N
5175	40	\N	5	\N	\N	1	8	\N
5176	87	\N	5	\N	\N	1	8	\N
5177	96	\N	5	\N	\N	1	8	\N
5178	99	\N	5	\N	\N	1	8	\N
4889	2	\N	21	10	\N	1	8	\N
5179	46	\N	5	\N	\N	1	1	\N
5180	49	\N	5	\N	\N	1	1	\N
5181	76	\N	5	\N	\N	1	1	\N
139	2	\N	5	1	2006-07-26	1	1	\N
4755	2	\N	18	1	\N	\N	\N	\N
864	2	\N	18	10	2006-09-08	\N	\N	\N
862	2	\N	18	10	2006-09-08	\N	\N	\N
3884	3	\N	18	14	2007-04-06	\N	\N	\N
4756	2	\N	21	1	\N	1	8	\N
4754	2	\N	5	1	\N	1	14	\N
4164	2	\N	5	10	\N	1	10	\N
4253	2	\N	18	8	\N	1	8	\N
4876	2	\N	18	10	\N	\N	\N	\N
4878	2	\N	18	10	\N	\N	\N	\N
4888	2	\N	5	10	\N	1	8	\N
5182	99	\N	5	\N	\N	1	1	\N
5184	49	\N	5	\N	\N	1	1	\N
5185	46	\N	5	\N	\N	1	1	\N
3787	17	\N	21	10	\N	1	10	\N
3788	17	\N	21	10	\N	1	10	\N
4870	2	\N	5	10	\N	1	1	\N
4964	18	\N	5	10	\N	1	1	\N
5186	87	\N	5	\N	\N	1	1	\N
5187	61	\N	5	\N	\N	1	1	\N
4871	2	\N	5	10	\N	1	8	\N
872	2	\N	18	10	2006-09-08	\N	\N	\N
1001	2	\N	5	8	2006-10-16	1	1	\N
4880	2	\N	18	10	\N	\N	\N	\N
5188	71	\N	5	\N	\N	1	1	\N
4883	2	\N	1	10	\N	1	14	\N
3847	17	\N	21	10	\N	1	8	\N
3848	17	\N	21	10	\N	1	8	\N
4560	3	\N	21	1	\N	1	8	\N
3509	2	\N	21	8	2007-03-14	1	8	\N
4555	3	\N	21	1	\N	1	8	\N
2901	16	\N	21	11	\N	1	8	\N
5183	100	\N	1	\N	\N	1	8	\N
1410	2	\N	21	1	2006-11-28	1	8	\N
4966	18	\N	5	10	\N	1	8	\N
5189	87	\N	5	\N	\N	1	8	\N
877	2	\N	7	10	2006-09-05	1	10	\N
5190	99	\N	5	\N	\N	1	8	\N
5191	100	\N	5	\N	\N	1	8	\N
5251	3	\N	5	10	\N	1	1	\N
5252	3	\N	18	10	\N	\N	\N	\N
5254	3	\N	21	10	\N	1	1	\N
5256	3	\N	21	10	\N	1	10	\N
5257	3	\N	21	10	\N	1	1	\N
5222	2	\N	6	1	\N	\N	\N	\N
5264	3	\N	21	10	\N	1	8	\N
5265	3	\N	21	10	\N	1	8	\N
5262	3	\N	18	10	\N	\N	\N	\N
5259	3	\N	18	10	\N	\N	\N	\N
5260	3	\N	18	10	\N	\N	\N	\N
5236	25	\N	5	\N	\N	1	1	\N
5261	3	\N	18	10	\N	\N	\N	\N
5237	25	\N	5	\N	\N	1	1	\N
5258	3	\N	18	10	\N	1	8	\N
5238	26	\N	5	\N	\N	1	1	\N
5239	44	\N	5	\N	\N	1	1	\N
5240	44	\N	5	\N	\N	1	1	\N
4887	2	\N	21	10	\N	1	8	\N
423	2	\N	5	1	2006-07-26	1	10	\N
4765	3	\N	18	1	\N	1	10	\N
3700	16	\N	5	10	\N	1	1	\N
5241	88	\N	5	\N	\N	1	1	\N
3184	2	\N	18	10	2007-01-11	\N	\N	\N
4881	2	\N	5	10	\N	1	8	\N
4885	2	\N	3	10	\N	1	8	\N
3078	16	\N	5	1	\N	1	8	\N
3083	16	\N	5	1	\N	1	8	\N
624	2	\N	5	10	2006-08-15	1	10	\N
4751	2	\N	5	14	\N	1	10	\N
5282	2	\N	18	10	\N	\N	\N	\N
4823	3	\N	21	10	\N	1	1	\N
3084	16	\N	21	1	\N	1	1	\N
2702	2	\N	21	10	2006-11-28	1	1	\N
3081	16	\N	21	1	\N	1	1	\N
3894	2	\N	21	14	\N	1	1	\N
3830	17	\N	21	10	\N	1	14	\N
4217	2	\N	5	10	\N	1	10	\N
5266	3	\N	21	10	\N	1	14	\N
5233	2	\N	21	1	\N	1	14	\N
3672	16	\N	21	10	2007-03-29	1	14	\N
3826	17	\N	21	10	\N	1	14	\N
3827	17	\N	21	10	\N	1	14	\N
3832	17	\N	21	10	\N	1	14	\N
3833	17	\N	21	10	\N	1	14	\N
4484	3	\N	21	14	\N	1	14	\N
3082	16	\N	21	1	\N	1	14	\N
4834	3	\N	21	10	\N	1	14	\N
4869	2	\N	21	10	\N	1	14	\N
4968	18	\N	5	10	\N	1	1	\N
5287	87	\N	5	\N	\N	1	1	\N
4824	2	\N	5	10	\N	1	10	\N
3055	2	\N	5	14	2007-03-01	1	10	\N
5288	87	\N	5	\N	\N	1	1	\N
3582	2	\N	5	10	\N	1	10	\N
5289	87	\N	5	\N	\N	1	1	\N
5290	97	\N	5	\N	\N	1	1	\N
5291	96	\N	5	\N	\N	1	1	\N
5292	99	\N	5	\N	\N	1	1	\N
3906	2	\N	18	14	2007-04-02	1	1	\N
5280	2	\N	18	10	\N	\N	\N	\N
5278	2	\N	18	10	\N	\N	\N	\N
3835	17	\N	21	10	\N	1	10	\N
3834	17	\N	21	10	\N	1	10	\N
4970	18	\N	5	10	\N	1	10	\N
5293	87	\N	5	\N	\N	1	10	\N
5294	97	\N	5	\N	\N	1	10	\N
5295	40	\N	5	\N	\N	1	10	\N
5296	96	\N	5	\N	\N	1	10	\N
5297	99	\N	5	\N	\N	1	10	\N
543	3	\N	21	17	2007-01-04	1	10	\N
4745	2	\N	21	14	\N	1	10	\N
4444	3	\N	21	14	\N	1	10	\N
5196	2	\N	21	1	\N	1	10	\N
4971	18	\N	5	10	\N	1	10	\N
5298	87	\N	5	\N	\N	1	10	\N
5299	36	\N	5	\N	\N	1	1	\N
5300	38	\N	5	\N	\N	1	1	\N
5301	84	\N	5	\N	\N	1	1	\N
4229	2	\N	5	8	\N	1	1	\N
4274	2	\N	5	1	\N	1	10	\N
143	2	\N	21	1	2006-11-09	1	8	\N
737	2	\N	21	8	2006-10-17	1	10	\N
645	3	\N	21	10	2006-10-17	1	10	\N
3701	16	\N	21	10	\N	1	10	\N
851	2	\N	21	10	2006-09-01	1	10	\N
660	3	\N	21	10	2006-10-17	1	10	\N
3702	16	\N	21	10	\N	1	10	\N
868	2	\N	21	10	2006-10-17	1	10	\N
3849	17	\N	21	10	\N	1	10	\N
3850	17	\N	21	10	\N	1	10	\N
2298	3	\N	21	8	2006-11-09	1	8	\N
4556	3	\N	21	1	\N	1	8	\N
3001	16	\N	21	11	\N	1	8	\N
4471	2	\N	21	10	\N	1	8	\N
4954	17	\N	21	10	\N	1	8	\N
4955	17	\N	21	10	\N	1	8	\N
4972	18	\N	5	10	\N	1	1	\N
5302	87	\N	5	\N	\N	1	1	\N
5303	97	\N	5	\N	\N	1	1	\N
5304	40	\N	5	\N	\N	1	1	\N
5305	96	\N	5	\N	\N	1	1	\N
5306	99	\N	5	\N	\N	1	1	\N
4982	18	\N	5	10	\N	1	1	\N
5307	87	\N	5	\N	\N	1	1	\N
4969	18	\N	5	10	\N	1	1	\N
5308	25	\N	5	\N	\N	1	1	\N
5309	28	\N	5	\N	\N	1	1	\N
5310	29	\N	5	\N	\N	1	1	\N
5311	37	\N	5	\N	\N	1	1	\N
5312	38	\N	5	\N	\N	1	1	\N
5313	40	\N	5	\N	\N	1	1	\N
5314	96	\N	5	\N	\N	1	1	\N
5315	99	\N	5	\N	\N	1	1	\N
5316	61	\N	5	\N	\N	1	10	\N
5317	40	\N	5	\N	\N	1	10	\N
2898	16	\N	21	11	\N	1	8	\N
5318	96	\N	5	\N	\N	1	10	\N
5319	99	\N	5	\N	\N	1	10	\N
5320	71	\N	5	\N	\N	1	10	\N
5267	2	\N	18	10	\N	1	10	\N
4022	16	\N	6	10	\N	1	1	\N
4886	2	\N	18	10	\N	\N	\N	\N
5255	3	\N	21	10	\N	1	10	\N
150	2	\N	5	1	2006-11-09	1	8	\N
5285	2	\N	5	10	\N	1	10	\N
719	2	\N	5	10	2006-08-14	1	8	\N
5203	2	\N	21	1	\N	1	14	\N
5204	2	\N	5	1	\N	1	10	\N
3505	2	\N	21	10	2007-02-15	1	8	\N
4891	17	\N	21	10	\N	1	8	\N
4892	17	\N	21	10	\N	1	8	\N
5200	2	\N	5	1	\N	1	8	\N
4156	3	\N	5	10	\N	1	10	\N
5281	2	\N	18	10	\N	1	8	\N
732	2	\N	18	8	2006-08-18	1	8	\N
5284	2	\N	5	10	\N	1	8	\N
5202	2	\N	21	1	\N	1	8	\N
5193	3	\N	21	1	\N	1	8	\N
3704	16	\N	21	10	\N	1	8	\N
5269	2	\N	21	10	\N	1	8	\N
5194	3	\N	21	1	\N	1	8	\N
3703	16	\N	21	10	\N	1	8	\N
5270	2	\N	21	10	\N	1	8	\N
5192	3	\N	5	1	\N	1	10	\N
5206	2	\N	18	1	\N	\N	\N	\N
4983	18	\N	5	10	\N	1	8	\N
5321	25	\N	5	\N	\N	1	8	\N
5322	36	\N	5	\N	\N	1	8	\N
5323	38	\N	5	\N	\N	1	8	\N
5324	37	\N	5	\N	\N	1	8	\N
5325	26	\N	5	\N	\N	1	8	\N
4254	2	\N	5	8	\N	1	10	\N
3110	17	\N	21	14	2006-12-20	1	10	\N
3111	17	\N	21	14	2006-12-20	1	10	\N
4953	17	\N	21	10	\N	1	1	\N
4952	17	\N	21	10	\N	1	1	\N
5245	3	\N	21	10	\N	1	1	\N
3006	16	\N	21	11	\N	1	1	\N
605	2	\N	21	1	2006-08-10	1	1	\N
3951	16	\N	21	10	\N	1	1	\N
4142	2	\N	21	8	\N	1	1	\N
4990	18	\N	5	10	\N	1	1	\N
5326	25	\N	5	\N	\N	1	1	\N
5327	28	\N	5	\N	\N	1	1	\N
5328	29	\N	5	\N	\N	1	1	\N
5329	37	\N	5	\N	\N	1	1	\N
5207	2	\N	5	1	\N	1	1	\N
5273	2	\N	5	10	\N	1	1	\N
4767	3	\N	5	1	\N	1	1	\N
4768	3	\N	5	1	\N	1	1	\N
3698	16	\N	5	10	\N	1	1	\N
3699	16	\N	5	10	\N	1	1	\N
5330	25	\N	5	\N	\N	1	1	\N
5331	29	\N	5	\N	\N	1	1	\N
5332	45	\N	5	\N	\N	1	1	\N
5333	87	\N	5	\N	\N	1	8	\N
3852	17	\N	21	10	\N	1	8	\N
5243	3	\N	21	10	\N	1	8	\N
3605	16	\N	21	10	\N	1	8	\N
5220	2	\N	21	1	\N	1	8	\N
5244	3	\N	21	10	\N	1	8	\N
3291	16	\N	21	10	\N	1	8	\N
881	2	\N	21	10	2006-09-04	1	8	\N
3851	17	\N	1	10	\N	1	10	\N
393	2	\N	18	1	2006-12-04	\N	\N	\N
5210	2	\N	18	1	\N	\N	\N	\N
4973	18	\N	5	10	\N	1	1	\N
5334	87	\N	5	\N	\N	1	1	\N
4951	17	\N	21	10	\N	1	1	\N
4950	17	\N	21	10	\N	1	1	\N
4846	3	\N	21	10	\N	1	1	\N
3286	16	\N	21	10	2007-03-15	1	1	\N
5276	2	\N	21	10	\N	1	1	\N
5195	3	\N	21	1	\N	1	1	\N
3722	16	\N	21	10	\N	1	1	\N
5277	2	\N	21	10	\N	1	1	\N
4991	18	\N	5	10	\N	1	1	\N
3182	2	\N	3	1	2007-01-05	1	8	\N
5335	87	\N	5	\N	\N	1	1	\N
3511	2	\N	18	8	2007-03-15	1	10	\N
623	2	\N	5	10	2006-08-15	1	8	\N
5272	2	\N	18	10	\N	\N	\N	\N
5205	2	\N	5	1	\N	1	8	\N
735	2	\N	5	8	2006-08-18	1	8	\N
5212	2	\N	18	1	\N	\N	\N	\N
4769	3	\N	18	1	\N	\N	\N	\N
4770	3	\N	18	1	\N	\N	\N	\N
5209	2	\N	18	1	\N	\N	\N	\N
5274	2	\N	1	10	\N	1	8	\N
5275	2	\N	5	10	\N	1	10	\N
5211	2	\N	18	1	\N	\N	\N	\N
677	3	\N	18	10	2007-01-05	1	10	\N
684	3	\N	18	10	2006-08-14	1	8	\N
2791	2	\N	18	10	2006-11-21	1	8	\N
5286	2	\N	5	10	\N	1	10	\N
4949	17	\N	21	10	\N	1	8	\N
4948	17	\N	21	10	\N	1	8	\N
700	3	\N	21	10	2007-01-11	1	8	\N
3706	16	\N	21	10	\N	1	8	\N
4447	2	\N	21	8	\N	1	8	\N
3705	16	\N	21	10	\N	1	8	\N
4992	18	\N	5	10	\N	1	8	\N
5336	87	\N	5	\N	\N	1	8	\N
3183	3	\N	5	1	2007-01-05	1	10	\N
5216	2	\N	18	1	\N	\N	\N	\N
5215	2	\N	18	1	\N	\N	\N	\N
5337	1	\N	6	1	\N	\N	\N	\N
5338	1	\N	6	1	\N	\N	\N	\N
5339	1	\N	6	1	\N	\N	\N	\N
5340	1	\N	6	1	\N	\N	\N	\N
5341	1	\N	6	1	\N	\N	\N	\N
5342	1	\N	6	1	\N	\N	\N	\N
5343	1	\N	6	1	\N	\N	\N	\N
5344	1	\N	6	1	\N	\N	\N	\N
5345	1	\N	6	1	\N	\N	\N	\N
5346	1	\N	6	1	\N	\N	\N	\N
5347	1	\N	6	1	\N	\N	\N	\N
5348	1	\N	6	1	\N	\N	\N	\N
5349	1	\N	6	1	\N	\N	\N	\N
5350	1	\N	6	1	\N	\N	\N	\N
5351	1	\N	6	1	\N	\N	\N	\N
5352	1	\N	6	1	\N	\N	\N	\N
5353	1	\N	6	1	\N	\N	\N	\N
5354	1	\N	6	1	\N	\N	\N	\N
5355	1	\N	6	1	\N	\N	\N	\N
5356	1	\N	6	1	\N	\N	\N	\N
5357	1	\N	6	1	\N	\N	\N	\N
5358	1	\N	6	1	\N	\N	\N	\N
5359	1	\N	6	1	\N	\N	\N	\N
5360	1	\N	6	1	\N	\N	\N	\N
5361	1	\N	6	1	\N	\N	\N	\N
5362	1	\N	6	1	\N	\N	\N	\N
5363	1	\N	6	1	\N	\N	\N	\N
5364	1	\N	6	1	\N	\N	\N	\N
5365	1	\N	6	1	\N	\N	\N	\N
5366	1	\N	6	1	\N	\N	\N	\N
5367	1	\N	6	1	\N	\N	\N	\N
5368	1	\N	6	1	\N	\N	\N	\N
5369	1	\N	6	1	\N	\N	\N	\N
5370	1	\N	6	1	\N	\N	\N	\N
5371	1	\N	6	1	\N	\N	\N	\N
5372	1	\N	6	1	\N	\N	\N	\N
5373	1	\N	6	1	\N	\N	\N	\N
5374	1	\N	6	1	\N	\N	\N	\N
5375	1	\N	6	1	\N	\N	\N	\N
5376	1	\N	6	1	\N	\N	\N	\N
5377	1	\N	6	1	\N	\N	\N	\N
5378	1	\N	6	1	\N	\N	\N	\N
5379	1	\N	6	1	\N	\N	\N	\N
5380	1	\N	6	1	\N	\N	\N	\N
5381	1	\N	6	1	\N	\N	\N	\N
5382	1	\N	6	1	\N	\N	\N	\N
5383	1	\N	6	1	\N	\N	\N	\N
5384	1	\N	6	1	\N	\N	\N	\N
5385	1	\N	6	1	\N	\N	\N	\N
5386	1	\N	6	1	\N	\N	\N	\N
5387	1	\N	6	1	\N	\N	\N	\N
5388	1	\N	6	1	\N	\N	\N	\N
5389	1	\N	6	1	\N	\N	\N	\N
5390	1	\N	6	1	\N	\N	\N	\N
5391	1	\N	6	1	\N	\N	\N	\N
5392	1	\N	6	1	\N	\N	\N	\N
5393	1	\N	6	1	\N	\N	\N	\N
5394	1	\N	6	1	\N	\N	\N	\N
5395	1	\N	6	1	\N	\N	\N	\N
5396	1	\N	6	1	\N	\N	\N	\N
5397	1	\N	6	1	\N	\N	\N	\N
5398	1	\N	6	1	\N	\N	\N	\N
5399	1	\N	6	1	\N	\N	\N	\N
5400	1	\N	6	1	\N	\N	\N	\N
5401	1	\N	6	1	\N	\N	\N	\N
5402	1	\N	6	1	\N	\N	\N	\N
5403	1	\N	6	1	\N	\N	\N	\N
5404	1	\N	6	1	\N	\N	\N	\N
5405	1	\N	6	1	\N	\N	\N	\N
5406	1	\N	6	1	\N	\N	\N	\N
5407	1	\N	6	1	\N	\N	\N	\N
5408	1	\N	6	1	\N	\N	\N	\N
5409	1	\N	6	1	\N	\N	\N	\N
5410	1	\N	6	1	\N	\N	\N	\N
5411	1	\N	6	1	\N	\N	\N	\N
5412	1	\N	6	1	\N	\N	\N	\N
5413	1	\N	6	1	\N	\N	\N	\N
5414	1	\N	6	1	\N	\N	\N	\N
5415	1	\N	6	1	\N	\N	\N	\N
5416	1	\N	6	1	\N	\N	\N	\N
5417	1	\N	6	1	\N	\N	\N	\N
5418	1	\N	6	1	\N	\N	\N	\N
5419	1	\N	6	1	\N	\N	\N	\N
5420	1	\N	6	1	\N	\N	\N	\N
5421	1	\N	6	1	\N	\N	\N	\N
5422	1	\N	6	1	\N	\N	\N	\N
5423	1	\N	6	1	\N	\N	\N	\N
5424	1	\N	6	1	\N	\N	\N	\N
5425	1	\N	6	1	\N	\N	\N	\N
5426	1	\N	6	1	\N	\N	\N	\N
5427	1	\N	6	1	\N	\N	\N	\N
5428	1	\N	6	1	\N	\N	\N	\N
5429	1	\N	6	1	\N	\N	\N	\N
5430	1	\N	6	1	\N	\N	\N	\N
5431	1	\N	6	1	\N	\N	\N	\N
5432	1	\N	6	1	\N	\N	\N	\N
5433	1	\N	6	1	\N	\N	\N	\N
5434	1	\N	6	1	\N	\N	\N	\N
5435	1	\N	6	1	\N	\N	\N	\N
5436	1	\N	6	1	\N	\N	\N	\N
5437	1	\N	6	1	\N	\N	\N	\N
5438	1	\N	6	1	\N	\N	\N	\N
5439	1	\N	6	1	\N	\N	\N	\N
5440	1	\N	6	1	\N	\N	\N	\N
5441	1	\N	6	1	\N	\N	\N	\N
5442	1	\N	6	1	\N	\N	\N	\N
5443	1	\N	6	1	\N	\N	\N	\N
5444	1	\N	6	1	\N	\N	\N	\N
5445	1	\N	6	1	\N	\N	\N	\N
5446	1	\N	6	1	\N	\N	\N	\N
5447	1	\N	6	1	\N	\N	\N	\N
5448	1	\N	6	1	\N	\N	\N	\N
5449	1	\N	6	1	\N	\N	\N	\N
5450	1	\N	6	1	\N	\N	\N	\N
5451	1	\N	6	1	\N	\N	\N	\N
5452	1	\N	6	1	\N	\N	\N	\N
5453	1	\N	6	1	\N	\N	\N	\N
5454	1	\N	6	1	\N	\N	\N	\N
5455	1	\N	6	1	\N	\N	\N	\N
5456	1	\N	6	1	\N	\N	\N	\N
5467	1	\N	6	1	\N	\N	\N	\N
5468	1	\N	6	1	\N	\N	\N	\N
5469	1	\N	6	1	\N	\N	\N	\N
5470	1	\N	6	1	\N	\N	\N	\N
5471	1	\N	6	1	\N	\N	\N	\N
5472	1	\N	6	1	\N	\N	\N	\N
5473	1	\N	6	1	\N	\N	\N	\N
5474	1	\N	6	1	\N	\N	\N	\N
5475	1	\N	6	1	\N	\N	\N	\N
5476	1	\N	6	1	\N	\N	\N	\N
5477	1	\N	6	1	\N	\N	\N	\N
5478	1	\N	6	1	\N	\N	\N	\N
5479	1	\N	6	1	\N	\N	\N	\N
5480	1	\N	6	1	\N	\N	\N	\N
5481	1	\N	6	1	\N	\N	\N	\N
5482	1	\N	6	1	\N	\N	\N	\N
5483	1	\N	6	1	\N	\N	\N	\N
5484	1	\N	6	1	\N	\N	\N	\N
5485	1	\N	6	1	\N	\N	\N	\N
5486	1	\N	6	1	\N	\N	\N	\N
5487	1	\N	6	1	\N	\N	\N	\N
5488	1	\N	6	1	\N	\N	\N	\N
5489	1	\N	6	1	\N	\N	\N	\N
5490	1	\N	6	1	\N	\N	\N	\N
5491	1	\N	6	1	\N	\N	\N	\N
5492	1	\N	6	1	\N	\N	\N	\N
5493	1	\N	6	1	\N	\N	\N	\N
5494	1	\N	6	1	\N	\N	\N	\N
5495	1	\N	6	1	\N	\N	\N	\N
5496	1	\N	6	1	\N	\N	\N	\N
5497	1	\N	6	1	\N	\N	\N	\N
5498	1	\N	6	1	\N	\N	\N	\N
5499	1	\N	6	1	\N	\N	\N	\N
5500	1	\N	6	1	\N	\N	\N	\N
5501	1	\N	6	1	\N	\N	\N	\N
5502	1	\N	6	1	\N	\N	\N	\N
5503	1	\N	6	1	\N	\N	\N	\N
5504	1	\N	6	1	\N	\N	\N	\N
5505	1	\N	6	1	\N	\N	\N	\N
5506	1	\N	6	1	\N	\N	\N	\N
5507	1	\N	6	1	\N	\N	\N	\N
5508	1	\N	6	1	\N	\N	\N	\N
5509	1	\N	6	1	\N	\N	\N	\N
5510	1	\N	6	1	\N	\N	\N	\N
5511	1	\N	6	1	\N	\N	\N	\N
5512	1	\N	6	1	\N	\N	\N	\N
5513	1	\N	6	1	\N	\N	\N	\N
5514	1	\N	6	1	\N	\N	\N	\N
5515	1	\N	6	1	\N	\N	\N	\N
5516	1	\N	6	1	\N	\N	\N	\N
5517	1	\N	6	1	\N	\N	\N	\N
5518	1	\N	6	1	\N	\N	\N	\N
5519	1	\N	6	1	\N	\N	\N	\N
5520	1	\N	6	1	\N	\N	\N	\N
5521	1	\N	6	1	\N	\N	\N	\N
5522	1	\N	6	1	\N	\N	\N	\N
5523	1	\N	6	1	\N	\N	\N	\N
5524	1	\N	6	1	\N	\N	\N	\N
5525	1	\N	6	1	\N	\N	\N	\N
5526	1	\N	6	1	\N	\N	\N	\N
5527	1	\N	6	1	\N	\N	\N	\N
5528	1	\N	6	1	\N	\N	\N	\N
5529	1	\N	6	1	\N	\N	\N	\N
5530	1	\N	6	1	\N	\N	\N	\N
5531	1	\N	6	1	\N	\N	\N	\N
5532	1	\N	6	1	\N	\N	\N	\N
5533	1	\N	6	1	\N	\N	\N	\N
5534	1	\N	6	1	\N	\N	\N	\N
5535	1	\N	6	1	\N	\N	\N	\N
5536	1	\N	6	1	\N	\N	\N	\N
5537	1	\N	6	1	\N	\N	\N	\N
5538	1	\N	6	1	\N	\N	\N	\N
5539	1	\N	6	1	\N	\N	\N	\N
5540	1	\N	6	1	\N	\N	\N	\N
5541	1	\N	6	1	\N	\N	\N	\N
5542	1	\N	6	1	\N	\N	\N	\N
5543	1	\N	6	1	\N	\N	\N	\N
5544	1	\N	6	1	\N	\N	\N	\N
5545	1	\N	6	1	\N	\N	\N	\N
5546	1	\N	6	1	\N	\N	\N	\N
5547	1	\N	6	1	\N	\N	\N	\N
5548	1	\N	6	1	\N	\N	\N	\N
5549	1	\N	6	1	\N	\N	\N	\N
5550	1	\N	6	1	\N	\N	\N	\N
5551	1	\N	6	1	\N	\N	\N	\N
5552	1	\N	6	1	\N	\N	\N	\N
5553	1	\N	6	1	\N	\N	\N	\N
5554	1	\N	6	1	\N	\N	\N	\N
5555	1	\N	6	1	\N	\N	\N	\N
5556	1	\N	6	1	\N	\N	\N	\N
5557	1	\N	6	1	\N	\N	\N	\N
5558	1	\N	6	1	\N	\N	\N	\N
5559	1	\N	6	1	\N	\N	\N	\N
5560	1	\N	6	1	\N	\N	\N	\N
5561	1	\N	6	1	\N	\N	\N	\N
5562	1	\N	6	1	\N	\N	\N	\N
5563	1	\N	6	1	\N	\N	\N	\N
5564	1	\N	6	1	\N	\N	\N	\N
5565	1	\N	6	1	\N	\N	\N	\N
5566	1	\N	6	1	\N	\N	\N	\N
5567	1	\N	6	1	\N	\N	\N	\N
5568	1	\N	6	1	\N	\N	\N	\N
5569	1	\N	6	1	\N	\N	\N	\N
5570	1	\N	6	1	\N	\N	\N	\N
5571	1	\N	6	1	\N	\N	\N	\N
5572	1	\N	6	1	\N	\N	\N	\N
5573	1	\N	6	1	\N	\N	\N	\N
5574	1	\N	6	1	\N	\N	\N	\N
5575	1	\N	6	1	\N	\N	\N	\N
5576	1	\N	6	1	\N	\N	\N	\N
5577	1	\N	6	1	\N	\N	\N	\N
5578	1	\N	6	1	\N	\N	\N	\N
5579	1	\N	6	1	\N	\N	\N	\N
5580	1	\N	6	1	\N	\N	\N	\N
5581	1	\N	6	1	\N	\N	\N	\N
5582	1	\N	6	1	\N	\N	\N	\N
5583	1	\N	6	1	\N	\N	\N	\N
5584	1	\N	6	1	\N	\N	\N	\N
5585	1	\N	6	1	\N	\N	\N	\N
5586	1	\N	6	1	\N	\N	\N	\N
5587	1	\N	6	1	\N	\N	\N	\N
5588	1	\N	6	1	\N	\N	\N	\N
5589	1	\N	6	1	\N	\N	\N	\N
5590	1	\N	6	1	\N	\N	\N	\N
5591	1	\N	6	1	\N	\N	\N	\N
5592	1	\N	6	1	\N	\N	\N	\N
5593	1	\N	6	1	\N	\N	\N	\N
5594	1	\N	6	1	\N	\N	\N	\N
5595	1	\N	6	1	\N	\N	\N	\N
5596	1	\N	6	1	\N	\N	\N	\N
5597	1	\N	6	1	\N	\N	\N	\N
5598	1	\N	6	1	\N	\N	\N	\N
5599	1	\N	6	1	\N	\N	\N	\N
5600	1	\N	6	1	\N	\N	\N	\N
5601	1	\N	6	1	\N	\N	\N	\N
5602	1	\N	6	1	\N	\N	\N	\N
5603	1	\N	6	1	\N	\N	\N	\N
5604	1	\N	6	1	\N	\N	\N	\N
5605	1	\N	6	1	\N	\N	\N	\N
5606	1	\N	6	1	\N	\N	\N	\N
5607	1	\N	6	1	\N	\N	\N	\N
5608	1	\N	6	1	\N	\N	\N	\N
5609	1	\N	6	1	\N	\N	\N	\N
5610	1	\N	6	1	\N	\N	\N	\N
5611	1	\N	6	1	\N	\N	\N	\N
5612	1	\N	6	1	\N	\N	\N	\N
5613	1	\N	6	1	\N	\N	\N	\N
5614	1	\N	6	1	\N	\N	\N	\N
5615	1	\N	6	1	\N	\N	\N	\N
5616	1	\N	6	1	\N	\N	\N	\N
5617	1	\N	6	1	\N	\N	\N	\N
5618	1	\N	6	1	\N	\N	\N	\N
5619	1	\N	6	1	\N	\N	\N	\N
5620	1	\N	6	1	\N	\N	\N	\N
5621	1	\N	6	1	\N	\N	\N	\N
5622	1	\N	6	1	\N	\N	\N	\N
5623	1	\N	6	1	\N	\N	\N	\N
5624	1	\N	6	1	\N	\N	\N	\N
5625	1	\N	6	1	\N	\N	\N	\N
5626	1	\N	6	1	\N	\N	\N	\N
5627	1	\N	6	1	\N	\N	\N	\N
5628	1	\N	6	1	\N	\N	\N	\N
5629	1	\N	6	1	\N	\N	\N	\N
5630	1	\N	6	1	\N	\N	\N	\N
5631	1	\N	6	1	\N	\N	\N	\N
5632	1	\N	6	1	\N	\N	\N	\N
5633	1	\N	6	1	\N	\N	\N	\N
5634	1	\N	6	1	\N	\N	\N	\N
5635	1	\N	6	1	\N	\N	\N	\N
5636	1	\N	6	1	\N	\N	\N	\N
5637	1	\N	6	1	\N	\N	\N	\N
5638	1	\N	6	1	\N	\N	\N	\N
5639	1	\N	6	1	\N	\N	\N	\N
5640	1	\N	6	1	\N	\N	\N	\N
5641	1	\N	6	1	\N	\N	\N	\N
5642	1	\N	6	1	\N	\N	\N	\N
5643	1	\N	6	1	\N	\N	\N	\N
5644	1	\N	6	1	\N	\N	\N	\N
5645	1	\N	6	1	\N	\N	\N	\N
5646	1	\N	6	1	\N	\N	\N	\N
5647	1	\N	6	1	\N	\N	\N	\N
5648	1	\N	6	1	\N	\N	\N	\N
5649	1	\N	6	1	\N	\N	\N	\N
5650	1	\N	6	1	\N	\N	\N	\N
5651	1	\N	6	1	\N	\N	\N	\N
5652	1	\N	6	1	\N	\N	\N	\N
5653	1	\N	6	1	\N	\N	\N	\N
5654	1	\N	6	1	\N	\N	\N	\N
5655	1	\N	6	1	\N	\N	\N	\N
5656	1	\N	6	1	\N	\N	\N	\N
5657	1	\N	6	1	\N	\N	\N	\N
5658	1	\N	6	1	\N	\N	\N	\N
5659	1	\N	6	1	\N	\N	\N	\N
5660	1	\N	6	1	\N	\N	\N	\N
5661	1	\N	6	1	\N	\N	\N	\N
5662	1	\N	6	1	\N	\N	\N	\N
5663	1	\N	6	1	\N	\N	\N	\N
5664	1	\N	6	1	\N	\N	\N	\N
5665	1	\N	6	1	\N	\N	\N	\N
5666	1	\N	6	1	\N	\N	\N	\N
5667	1	\N	6	1	\N	\N	\N	\N
5668	1	\N	6	1	\N	\N	\N	\N
5669	1	\N	6	1	\N	\N	\N	\N
5670	1	\N	6	1	\N	\N	\N	\N
5671	1	\N	6	1	\N	\N	\N	\N
5672	1	\N	6	1	\N	\N	\N	\N
5673	1	\N	6	1	\N	\N	\N	\N
5674	1	\N	6	1	\N	\N	\N	\N
5675	1	\N	6	1	\N	\N	\N	\N
5676	1	\N	6	1	\N	\N	\N	\N
5677	1	\N	6	1	\N	\N	\N	\N
5678	1	\N	6	1	\N	\N	\N	\N
5679	1	\N	6	1	\N	\N	\N	\N
5680	1	\N	6	1	\N	\N	\N	\N
5681	1	\N	6	1	\N	\N	\N	\N
5682	1	\N	6	1	\N	\N	\N	\N
5683	1	\N	6	1	\N	\N	\N	\N
5684	1	\N	6	1	\N	\N	\N	\N
5685	1	\N	6	1	\N	\N	\N	\N
5686	1	\N	6	1	\N	\N	\N	\N
5687	1	\N	6	1	\N	\N	\N	\N
5688	1	\N	6	1	\N	\N	\N	\N
5689	1	\N	6	1	\N	\N	\N	\N
5690	1	\N	6	1	\N	\N	\N	\N
5691	1	\N	6	1	\N	\N	\N	\N
5692	1	\N	6	1	\N	\N	\N	\N
5693	1	\N	6	1	\N	\N	\N	\N
5694	1	\N	6	1	\N	\N	\N	\N
5695	1	\N	6	1	\N	\N	\N	\N
5696	1	\N	6	1	\N	\N	\N	\N
5697	1	\N	6	1	\N	\N	\N	\N
5698	1	\N	6	1	\N	\N	\N	\N
5699	1	\N	6	1	\N	\N	\N	\N
5700	1	\N	6	1	\N	\N	\N	\N
5701	1	\N	6	1	\N	\N	\N	\N
5702	1	\N	6	1	\N	\N	\N	\N
5703	1	\N	6	1	\N	\N	\N	\N
5704	1	\N	6	1	\N	\N	\N	\N
5705	1	\N	6	1	\N	\N	\N	\N
5706	1	\N	6	1	\N	\N	\N	\N
5707	1	\N	6	1	\N	\N	\N	\N
5708	1	\N	6	1	\N	\N	\N	\N
5709	1	\N	6	1	\N	\N	\N	\N
5710	1	\N	6	1	\N	\N	\N	\N
5711	1	\N	6	1	\N	\N	\N	\N
5712	1	\N	6	1	\N	\N	\N	\N
5713	1	\N	6	1	\N	\N	\N	\N
5714	1	\N	6	1	\N	\N	\N	\N
5715	1	\N	6	1	\N	\N	\N	\N
5716	1	\N	6	1	\N	\N	\N	\N
5717	1	\N	6	1	\N	\N	\N	\N
5718	1	\N	6	1	\N	\N	\N	\N
5719	1	\N	6	1	\N	\N	\N	\N
5720	1	\N	6	1	\N	\N	\N	\N
5721	1	\N	6	1	\N	\N	\N	\N
5722	1	\N	6	1	\N	\N	\N	\N
5723	1	\N	6	1	\N	\N	\N	\N
5724	1	\N	6	1	\N	\N	\N	\N
5725	1	\N	6	1	\N	\N	\N	\N
5726	1	\N	6	1	\N	\N	\N	\N
5727	1	\N	6	1	\N	\N	\N	\N
5728	1	\N	6	1	\N	\N	\N	\N
5729	1	\N	6	1	\N	\N	\N	\N
5730	1	\N	6	1	\N	\N	\N	\N
5731	1	\N	6	1	\N	\N	\N	\N
5732	1	\N	6	1	\N	\N	\N	\N
5733	1	\N	6	1	\N	\N	\N	\N
5734	1	\N	6	1	\N	\N	\N	\N
5735	1	\N	6	1	\N	\N	\N	\N
5736	1	\N	6	1	\N	\N	\N	\N
5737	1	\N	6	1	\N	\N	\N	\N
5738	1	\N	6	1	\N	\N	\N	\N
5739	1	\N	6	1	\N	\N	\N	\N
5740	1	\N	6	1	\N	\N	\N	\N
5741	1	\N	6	1	\N	\N	\N	\N
5742	1	\N	6	1	\N	\N	\N	\N
5743	1	\N	6	1	\N	\N	\N	\N
5744	1	\N	6	1	\N	\N	\N	\N
5745	1	\N	6	1	\N	\N	\N	\N
5746	1	\N	6	1	\N	\N	\N	\N
5747	1	\N	6	1	\N	\N	\N	\N
5748	1	\N	6	1	\N	\N	\N	\N
5749	1	\N	6	1	\N	\N	\N	\N
5750	1	\N	6	1	\N	\N	\N	\N
5751	1	\N	6	1	\N	\N	\N	\N
5752	1	\N	6	1	\N	\N	\N	\N
5753	1	\N	6	1	\N	\N	\N	\N
5754	1	\N	6	1	\N	\N	\N	\N
5755	1	\N	6	1	\N	\N	\N	\N
5756	1	\N	6	1	\N	\N	\N	\N
5757	1	\N	6	1	\N	\N	\N	\N
5758	1	\N	6	1	\N	\N	\N	\N
5759	1	\N	6	1	\N	\N	\N	\N
5760	1	\N	6	1	\N	\N	\N	\N
5761	1	\N	6	1	\N	\N	\N	\N
5762	1	\N	6	1	\N	\N	\N	\N
5763	1	\N	6	1	\N	\N	\N	\N
5764	1	\N	6	1	\N	\N	\N	\N
5765	1	\N	6	1	\N	\N	\N	\N
5766	1	\N	6	1	\N	\N	\N	\N
5767	1	\N	6	1	\N	\N	\N	\N
5768	1	\N	6	1	\N	\N	\N	\N
5769	1	\N	6	1	\N	\N	\N	\N
5770	1	\N	6	1	\N	\N	\N	\N
5771	1	\N	6	1	\N	\N	\N	\N
5772	1	\N	6	1	\N	\N	\N	\N
5773	1	\N	6	1	\N	\N	\N	\N
5774	1	\N	6	1	\N	\N	\N	\N
5775	1	\N	6	1	\N	\N	\N	\N
5776	1	\N	6	1	\N	\N	\N	\N
5777	1	\N	6	1	\N	\N	\N	\N
5778	1	\N	6	1	\N	\N	\N	\N
5779	1	\N	6	1	\N	\N	\N	\N
5780	1	\N	6	1	\N	\N	\N	\N
5781	1	\N	6	1	\N	\N	\N	\N
5782	1	\N	6	1	\N	\N	\N	\N
5783	1	\N	6	1	\N	\N	\N	\N
5784	1	\N	6	1	\N	\N	\N	\N
5785	1	\N	6	1	\N	\N	\N	\N
5786	1	\N	6	1	\N	\N	\N	\N
5787	1	\N	6	1	\N	\N	\N	\N
5788	1	\N	6	1	\N	\N	\N	\N
5789	1	\N	6	1	\N	\N	\N	\N
5790	1	\N	6	1	\N	\N	\N	\N
5791	1	\N	6	1	\N	\N	\N	\N
5792	1	\N	6	1	\N	\N	\N	\N
5793	1	\N	6	1	\N	\N	\N	\N
5794	1	\N	6	1	\N	\N	\N	\N
5795	1	\N	6	1	\N	\N	\N	\N
5796	1	\N	6	1	\N	\N	\N	\N
5797	1	\N	6	1	\N	\N	\N	\N
5798	1	\N	6	1	\N	\N	\N	\N
5799	1	\N	6	1	\N	\N	\N	\N
5800	1	\N	6	1	\N	\N	\N	\N
5801	1	\N	6	1	\N	\N	\N	\N
5802	1	\N	6	1	\N	\N	\N	\N
5803	1	\N	6	1	\N	\N	\N	\N
5804	1	\N	6	1	\N	\N	\N	\N
5805	1	\N	6	1	\N	\N	\N	\N
5806	1	\N	6	1	\N	\N	\N	\N
5807	103	\N	6	1	\N	\N	\N	\N
5808	103	\N	6	1	\N	\N	\N	\N
5809	103	\N	6	1	\N	\N	\N	\N
5810	103	\N	6	1	\N	\N	\N	\N
5811	103	\N	6	1	\N	\N	\N	\N
5812	103	\N	6	1	\N	\N	\N	\N
5813	103	\N	6	1	\N	\N	\N	\N
5814	103	\N	6	1	\N	\N	\N	\N
5815	103	\N	6	1	\N	\N	\N	\N
5816	103	\N	6	1	\N	\N	\N	\N
5817	103	\N	6	1	\N	\N	\N	\N
5818	103	\N	6	1	\N	\N	\N	\N
5819	103	\N	6	1	\N	\N	\N	\N
5820	103	\N	6	1	\N	\N	\N	\N
5821	103	\N	6	1	\N	\N	\N	\N
5822	103	\N	6	1	\N	\N	\N	\N
5823	103	\N	6	1	\N	\N	\N	\N
5824	103	\N	6	1	\N	\N	\N	\N
5825	103	\N	6	1	\N	\N	\N	\N
5826	103	\N	6	1	\N	\N	\N	\N
5827	103	\N	6	1	\N	\N	\N	\N
5828	103	\N	6	1	\N	\N	\N	\N
5829	103	\N	6	1	\N	\N	\N	\N
5830	103	\N	6	1	\N	\N	\N	\N
5831	103	\N	6	1	\N	\N	\N	\N
5832	103	\N	6	1	\N	\N	\N	\N
5833	103	\N	6	1	\N	\N	\N	\N
5834	103	\N	6	1	\N	\N	\N	\N
5835	103	\N	6	1	\N	\N	\N	\N
5836	103	\N	6	1	\N	\N	\N	\N
5837	103	\N	6	1	\N	\N	\N	\N
5838	103	\N	6	1	\N	\N	\N	\N
5839	103	\N	6	1	\N	\N	\N	\N
5840	103	\N	6	1	\N	\N	\N	\N
5841	103	\N	6	1	\N	\N	\N	\N
5842	103	\N	6	1	\N	\N	\N	\N
5843	103	\N	6	1	\N	\N	\N	\N
5844	103	\N	6	1	\N	\N	\N	\N
5845	103	\N	6	1	\N	\N	\N	\N
5846	103	\N	6	1	\N	\N	\N	\N
5847	103	\N	6	1	\N	\N	\N	\N
5848	103	\N	6	1	\N	\N	\N	\N
5849	103	\N	6	1	\N	\N	\N	\N
5850	103	\N	6	1	\N	\N	\N	\N
5851	103	\N	6	1	\N	\N	\N	\N
5852	103	\N	6	1	\N	\N	\N	\N
5853	103	\N	6	1	\N	\N	\N	\N
5854	103	\N	6	1	\N	\N	\N	\N
5855	103	\N	6	1	\N	\N	\N	\N
5856	103	\N	6	1	\N	\N	\N	\N
5857	104	\N	6	1	\N	\N	\N	\N
5858	104	\N	6	1	\N	\N	\N	\N
5859	104	\N	6	1	\N	\N	\N	\N
5860	104	\N	6	1	\N	\N	\N	\N
5861	104	\N	6	1	\N	\N	\N	\N
5862	104	\N	6	1	\N	\N	\N	\N
5863	104	\N	6	1	\N	\N	\N	\N
5864	104	\N	6	1	\N	\N	\N	\N
5865	104	\N	6	1	\N	\N	\N	\N
5866	104	\N	6	1	\N	\N	\N	\N
5867	104	\N	6	1	\N	\N	\N	\N
5868	104	\N	6	1	\N	\N	\N	\N
5869	104	\N	6	1	\N	\N	\N	\N
5870	104	\N	6	1	\N	\N	\N	\N
5871	104	\N	6	1	\N	\N	\N	\N
5872	104	\N	6	1	\N	\N	\N	\N
5873	104	\N	6	1	\N	\N	\N	\N
5874	104	\N	6	1	\N	\N	\N	\N
5875	104	\N	6	1	\N	\N	\N	\N
5876	104	\N	6	1	\N	\N	\N	\N
5877	104	\N	6	1	\N	\N	\N	\N
5878	104	\N	6	1	\N	\N	\N	\N
5879	104	\N	6	1	\N	\N	\N	\N
5880	104	\N	6	1	\N	\N	\N	\N
5881	104	\N	6	1	\N	\N	\N	\N
5882	104	\N	6	1	\N	\N	\N	\N
5883	104	\N	6	1	\N	\N	\N	\N
5884	104	\N	6	1	\N	\N	\N	\N
5885	104	\N	6	1	\N	\N	\N	\N
5886	104	\N	6	1	\N	\N	\N	\N
5887	104	\N	6	1	\N	\N	\N	\N
5888	104	\N	6	1	\N	\N	\N	\N
5889	104	\N	6	1	\N	\N	\N	\N
5890	104	\N	6	1	\N	\N	\N	\N
5891	104	\N	6	1	\N	\N	\N	\N
5892	104	\N	6	1	\N	\N	\N	\N
5893	104	\N	6	1	\N	\N	\N	\N
5894	104	\N	6	1	\N	\N	\N	\N
5895	104	\N	6	1	\N	\N	\N	\N
5896	104	\N	6	1	\N	\N	\N	\N
5897	104	\N	6	1	\N	\N	\N	\N
5898	104	\N	6	1	\N	\N	\N	\N
5899	104	\N	6	1	\N	\N	\N	\N
5900	104	\N	6	1	\N	\N	\N	\N
5901	104	\N	6	1	\N	\N	\N	\N
5902	104	\N	6	1	\N	\N	\N	\N
5903	104	\N	6	1	\N	\N	\N	\N
5904	104	\N	6	1	\N	\N	\N	\N
5905	104	\N	6	1	\N	\N	\N	\N
5906	104	\N	6	1	\N	\N	\N	\N
5907	1	\N	6	1	\N	\N	\N	\N
5908	1	\N	6	1	\N	\N	\N	\N
5909	1	\N	6	1	\N	\N	\N	\N
5910	1	\N	6	1	\N	\N	\N	\N
5911	1	\N	6	1	\N	\N	\N	\N
5912	1	\N	6	1	\N	\N	\N	\N
5913	1	\N	6	1	\N	\N	\N	\N
5914	1	\N	6	1	\N	\N	\N	\N
5915	1	\N	6	1	\N	\N	\N	\N
5916	1	\N	6	1	\N	\N	\N	\N
5917	1	\N	6	1	\N	\N	\N	\N
5918	1	\N	6	1	\N	\N	\N	\N
5919	1	\N	6	1	\N	\N	\N	\N
5920	1	\N	6	1	\N	\N	\N	\N
5921	1	\N	6	1	\N	\N	\N	\N
5922	1	\N	6	1	\N	\N	\N	\N
5923	1	\N	6	1	\N	\N	\N	\N
5924	1	\N	6	1	\N	\N	\N	\N
5925	1	\N	6	1	\N	\N	\N	\N
5926	1	\N	6	1	\N	\N	\N	\N
5927	103	\N	6	1	\N	\N	\N	\N
5928	103	\N	6	1	\N	\N	\N	\N
5929	103	\N	6	1	\N	\N	\N	\N
5930	103	\N	6	1	\N	\N	\N	\N
5931	103	\N	6	1	\N	\N	\N	\N
5932	103	\N	6	1	\N	\N	\N	\N
5933	103	\N	6	1	\N	\N	\N	\N
5934	103	\N	6	1	\N	\N	\N	\N
5935	103	\N	6	1	\N	\N	\N	\N
5936	103	\N	6	1	\N	\N	\N	\N
5937	103	\N	6	1	\N	\N	\N	\N
5938	103	\N	6	1	\N	\N	\N	\N
5939	103	\N	6	1	\N	\N	\N	\N
5940	103	\N	6	1	\N	\N	\N	\N
5941	103	\N	6	1	\N	\N	\N	\N
5942	103	\N	6	1	\N	\N	\N	\N
5943	103	\N	6	1	\N	\N	\N	\N
5944	103	\N	6	1	\N	\N	\N	\N
5945	103	\N	6	1	\N	\N	\N	\N
5946	103	\N	6	1	\N	\N	\N	\N
5947	103	\N	6	1	\N	\N	\N	\N
5948	103	\N	6	1	\N	\N	\N	\N
5949	103	\N	6	1	\N	\N	\N	\N
5950	103	\N	6	1	\N	\N	\N	\N
5951	103	\N	6	1	\N	\N	\N	\N
5952	103	\N	6	1	\N	\N	\N	\N
5953	103	\N	6	1	\N	\N	\N	\N
5954	103	\N	6	1	\N	\N	\N	\N
5955	103	\N	6	1	\N	\N	\N	\N
5956	103	\N	6	1	\N	\N	\N	\N
5957	103	\N	6	1	\N	\N	\N	\N
5958	103	\N	6	1	\N	\N	\N	\N
5959	103	\N	6	1	\N	\N	\N	\N
5960	103	\N	6	1	\N	\N	\N	\N
5961	103	\N	6	1	\N	\N	\N	\N
5962	103	\N	6	1	\N	\N	\N	\N
5963	103	\N	6	1	\N	\N	\N	\N
5964	103	\N	6	1	\N	\N	\N	\N
5965	103	\N	6	1	\N	\N	\N	\N
5966	103	\N	6	1	\N	\N	\N	\N
5967	103	\N	6	1	\N	\N	\N	\N
5968	103	\N	6	1	\N	\N	\N	\N
5969	103	\N	6	1	\N	\N	\N	\N
5970	103	\N	6	1	\N	\N	\N	\N
5971	103	\N	6	1	\N	\N	\N	\N
5972	103	\N	6	1	\N	\N	\N	\N
5973	104	\N	6	1	\N	\N	\N	\N
5974	104	\N	6	1	\N	\N	\N	\N
5975	104	\N	6	1	\N	\N	\N	\N
5976	104	\N	6	1	\N	\N	\N	\N
5977	104	\N	6	1	\N	\N	\N	\N
5978	104	\N	6	1	\N	\N	\N	\N
5979	104	\N	6	1	\N	\N	\N	\N
5980	104	\N	6	1	\N	\N	\N	\N
5981	104	\N	6	1	\N	\N	\N	\N
5982	104	\N	6	1	\N	\N	\N	\N
5983	103	\N	6	1	\N	\N	\N	\N
5984	103	\N	6	1	\N	\N	\N	\N
5985	103	\N	6	1	\N	\N	\N	\N
5986	103	\N	6	1	\N	\N	\N	\N
5987	103	\N	6	1	\N	\N	\N	\N
5988	103	\N	6	1	\N	\N	\N	\N
5989	103	\N	6	1	\N	\N	\N	\N
5990	103	\N	6	1	\N	\N	\N	\N
5991	104	\N	6	1	\N	\N	\N	\N
5992	104	\N	6	1	\N	\N	\N	\N
5993	104	\N	6	1	\N	\N	\N	\N
5994	104	\N	6	1	\N	\N	\N	\N
5995	104	\N	6	1	\N	\N	\N	\N
5996	104	\N	6	1	\N	\N	\N	\N
5997	104	\N	6	1	\N	\N	\N	\N
5998	104	\N	6	1	\N	\N	\N	\N
5999	104	\N	6	1	\N	\N	\N	\N
6000	104	\N	6	1	\N	\N	\N	\N
6001	104	\N	6	1	\N	\N	\N	\N
6002	104	\N	6	1	\N	\N	\N	\N
92	2	\N	5	\N	\N	1	10	\N
5199	2	\N	5	1	\N	1	10	\N
4641	2	\N	5	1	\N	1	8	\N
2271	2	\N	18	10	\N	\N	\N	\N
3497	2	\N	18	10	2007-02-13	\N	\N	\N
3496	3	\N	18	10	2007-02-13	\N	\N	\N
6003	103	\N	6	10	\N	\N	\N	\N
6004	103	\N	6	10	\N	\N	\N	\N
6005	103	\N	6	10	\N	\N	\N	\N
6006	103	\N	6	10	\N	\N	\N	\N
6007	103	\N	6	10	\N	\N	\N	\N
6008	103	\N	6	10	\N	\N	\N	\N
6009	103	\N	6	10	\N	\N	\N	\N
6010	103	\N	6	10	\N	\N	\N	\N
6011	103	\N	6	10	\N	\N	\N	\N
6012	103	\N	6	10	\N	\N	\N	\N
6013	103	\N	6	10	\N	\N	\N	\N
6014	103	\N	6	10	\N	\N	\N	\N
6015	103	\N	6	10	\N	\N	\N	\N
6016	103	\N	6	10	\N	\N	\N	\N
6017	103	\N	6	10	\N	\N	\N	\N
6018	103	\N	6	10	\N	\N	\N	\N
6019	103	\N	6	10	\N	\N	\N	\N
6020	103	\N	6	10	\N	\N	\N	\N
6021	103	\N	6	10	\N	\N	\N	\N
6022	103	\N	6	10	\N	\N	\N	\N
6023	103	\N	6	10	\N	\N	\N	\N
6024	103	\N	6	10	\N	\N	\N	\N
6025	103	\N	6	10	\N	\N	\N	\N
6026	103	\N	6	10	\N	\N	\N	\N
6027	103	\N	6	10	\N	\N	\N	\N
6028	103	\N	6	10	\N	\N	\N	\N
6029	103	\N	6	10	\N	\N	\N	\N
6030	103	\N	6	10	\N	\N	\N	\N
6031	103	\N	6	10	\N	\N	\N	\N
6032	103	\N	6	10	\N	\N	\N	\N
6033	103	\N	6	10	\N	\N	\N	\N
6034	103	\N	6	10	\N	\N	\N	\N
6035	103	\N	6	10	\N	\N	\N	\N
6036	103	\N	6	10	\N	\N	\N	\N
6037	103	\N	6	10	\N	\N	\N	\N
6038	103	\N	6	10	\N	\N	\N	\N
6039	103	\N	6	10	\N	\N	\N	\N
6040	103	\N	6	10	\N	\N	\N	\N
6041	103	\N	6	10	\N	\N	\N	\N
6042	103	\N	6	10	\N	\N	\N	\N
6043	103	\N	6	10	\N	\N	\N	\N
6044	103	\N	6	10	\N	\N	\N	\N
6045	103	\N	6	10	\N	\N	\N	\N
6046	103	\N	6	10	\N	\N	\N	\N
6047	103	\N	6	10	\N	\N	\N	\N
6048	103	\N	6	10	\N	\N	\N	\N
6049	103	\N	6	10	\N	\N	\N	\N
6050	103	\N	6	10	\N	\N	\N	\N
6051	103	\N	6	10	\N	\N	\N	\N
6052	103	\N	6	10	\N	\N	\N	\N
6053	103	\N	6	10	\N	\N	\N	\N
6054	103	\N	6	10	\N	\N	\N	\N
6055	103	\N	6	10	\N	\N	\N	\N
6056	103	\N	6	10	\N	\N	\N	\N
6057	103	\N	6	10	\N	\N	\N	\N
6058	103	\N	6	10	\N	\N	\N	\N
1400	2	\N	5	1	\N	1	1	\N
4761	2	\N	5	1	\N	1	1	\N
6059	85	\N	5	\N	\N	1	1	\N
6060	86	\N	5	\N	\N	1	1	\N
6070	2	\N	5	14	\N	1	8	\N
5226	2	\N	21	1	\N	1	8	\N
6061	85	\N	5	\N	\N	1	1	\N
5217	2	\N	18	1	\N	\N	\N	\N
6062	71	\N	5	\N	\N	1	1	\N
6063	87	\N	5	\N	\N	1	1	\N
4559	3	\N	5	1	\N	1	8	\N
1412	2	\N	21	1	2006-12-06	1	8	\N
610	2	\N	19	1	2006-08-10	1	8	\N
4245	2	\N	5	8	\N	1	1	\N
1443	2	\N	5	10	2006-11-20	1	1	\N
748	2	\N	5	10	2006-08-31	1	1	\N
5235	2	\N	5	1	\N	1	1	\N
3928	2	\N	19	10	2007-03-30	1	8	\N
6071	2	\N	21	14	\N	1	14	\N
5225	2	\N	5	1	\N	1	8	\N
4173	2	\N	5	10	\N	1	8	\N
870	2	\N	5	10	2006-09-04	1	10	\N
5208	2	\N	21	1	\N	1	8	\N
3967	16	\N	21	10	\N	1	8	\N
3710	16	\N	21	10	\N	1	8	\N
3711	16	\N	21	10	\N	1	8	\N
6073	2	\N	21	14	\N	1	8	\N
3866	17	\N	21	10	\N	1	1	\N
3853	17	\N	21	10	\N	1	1	\N
3939	3	\N	21	14	2007-04-10	1	1	\N
3962	16	\N	21	10	\N	1	1	\N
4864	2	\N	21	10	\N	1	1	\N
4974	18	\N	5	10	\N	1	1	\N
4993	18	\N	5	10	\N	1	8	\N
6074	36	\N	5	\N	\N	1	8	\N
4840	3	\N	21	10	\N	1	8	\N
947	2	\N	21	8	2006-09-04	1	8	\N
3583	2	\N	18	10	2007-03-12	1	8	\N
5224	2	\N	5	1	\N	1	8	\N
414	2	\N	5	1	2006-07-26	1	8	\N
4844	3	\N	18	10	\N	1	8	\N
935	2	\N	5	8	2006-09-04	1	8	\N
453	2	\N	19	1	2006-07-26	1	8	\N
618	2	\N	18	10	2006-08-10	1	10	\N
4252	2	\N	5	8	\N	1	1	\N
4847	3	\N	21	10	\N	1	8	\N
4115	3	\N	5	8	2007-04-12	1	1	\N
4947	17	\N	21	10	\N	1	8	\N
4946	17	\N	21	10	\N	1	8	\N
6072	2	\N	21	14	\N	1	8	\N
3406	2	\N	5	10	2007-02-01	1	8	\N
3508	2	\N	5	8	2007-02-28	1	8	\N
4289	3	\N	5	10	\N	1	8	\N
4865	2	\N	5	10	\N	1	8	\N
6075	84	\N	5	\N	\N	1	1	\N
3836	17	\N	21	10	\N	1	1	\N
4945	17	\N	21	10	\N	1	1	\N
4766	3	\N	21	1	\N	1	1	\N
6069	2	\N	21	14	\N	1	1	\N
2914	16	\N	21	11	2007-03-30	1	1	\N
3030	2	\N	21	1	2007-03-30	1	1	\N
552	3	\N	21	17	2007-03-30	1	1	\N
3714	16	\N	21	10	\N	1	1	\N
3189	2	\N	21	10	2007-01-11	1	1	\N
4975	18	\N	5	10	\N	1	1	\N
6076	87	\N	5	\N	\N	1	1	\N
941	2	\N	1	8	2006-09-04	1	8	\N
2565	16	\N	7	11	\N	1	10	\N
4436	2	\N	18	14	\N	1	8	\N
383	2	\N	18	1	2006-08-28	1	8	\N
2897	16	\N	21	11	\N	1	1	\N
5279	2	\N	5	10	\N	1	10	\N
4288	3	\N	5	10	\N	1	10	\N
4764	3	\N	5	1	\N	1	10	\N
3717	16	\N	5	10	\N	1	10	\N
6077	25	\N	5	\N	\N	1	10	\N
2906	16	\N	21	11	\N	1	1	\N
6078	99	\N	5	\N	\N	1	10	\N
6079	100	\N	5	\N	\N	1	10	\N
6080	26	\N	5	\N	\N	1	10	\N
3665	16	\N	21	10	\N	1	8	\N
3978	16	\N	21	10	\N	1	8	\N
3691	16	\N	5	10	\N	1	1	\N
6081	104	\N	6	10	\N	\N	\N	\N
6082	104	\N	6	10	\N	\N	\N	\N
6083	104	\N	6	10	\N	\N	\N	\N
6084	104	\N	6	10	\N	\N	\N	\N
6085	104	\N	6	10	\N	\N	\N	\N
6086	104	\N	6	10	\N	\N	\N	\N
6087	104	\N	6	10	\N	\N	\N	\N
6088	104	\N	6	10	\N	\N	\N	\N
6089	104	\N	6	10	\N	\N	\N	\N
6090	104	\N	6	10	\N	\N	\N	\N
6091	104	\N	6	10	\N	\N	\N	\N
6092	104	\N	6	10	\N	\N	\N	\N
6093	99	\N	5	\N	\N	1	1	\N
6094	100	\N	5	\N	\N	1	1	\N
3663	16	\N	21	10	\N	1	8	\N
6095	42	\N	5	\N	\N	1	1	\N
6096	26	\N	5	\N	\N	1	1	\N
3088	16	\N	21	1	\N	1	8	\N
3693	16	\N	5	10	\N	1	1	\N
3613	16	\N	21	10	\N	1	8	\N
134	2	\N	5	\N	\N	1	14	\N
2903	16	\N	6	11	\N	1	14	\N
3091	16	\N	6	1	\N	1	14	\N
2904	16	\N	6	11	\N	1	14	\N
3634	16	\N	1	10	\N	1	14	\N
3627	16	\N	1	10	\N	1	14	\N
3966	16	\N	1	10	\N	1	14	\N
3629	16	\N	1	10	\N	1	14	\N
3612	16	\N	21	10	\N	1	8	\N
3682	16	\N	7	10	\N	4	1	\N
3666	16	\N	5	10	\N	1	1	\N
3086	16	\N	5	1	\N	1	10	\N
3661	16	\N	5	10	\N	1	10	\N
6068	2	\N	1	14	\N	1	8	\N
3664	16	\N	5	10	\N	1	1	\N
3673	16	\N	5	10	\N	1	1	\N
3674	16	\N	5	10	\N	1	1	\N
3948	16	\N	5	10	\N	1	1	\N
3692	16	\N	1	10	\N	1	14	\N
3608	16	\N	6	10	\N	1	14	\N
3658	16	\N	21	10	\N	1	14	\N
3607	16	\N	5	10	\N	1	10	\N
3089	16	\N	21	1	\N	1	8	\N
4939	17	\N	21	10	\N	1	8	\N
4940	17	\N	21	10	\N	1	8	\N
6121	2	\N	18	10	\N	\N	\N	\N
672	3	\N	21	10	2006-10-26	1	8	\N
3716	16	\N	21	10	\N	1	8	\N
4388	2	\N	21	10	\N	1	8	\N
4563	3	\N	21	1	\N	1	8	\N
3715	16	\N	21	10	\N	1	8	\N
5201	2	\N	21	1	\N	1	8	\N
4879	2	\N	5	10	\N	1	10	\N
4483	3	\N	5	14	\N	1	10	\N
4942	17	\N	21	10	\N	1	8	\N
4941	17	\N	21	10	\N	1	8	\N
6122	2	\N	18	10	\N	\N	\N	\N
4573	2	\N	5	1	\N	1	1	\N
6124	2	\N	21	10	\N	1	10	\N
685	3	\N	21	10	2006-10-16	1	8	\N
4027	16	\N	21	10	\N	1	8	\N
718	2	\N	21	10	2006-10-16	1	8	\N
5247	3	\N	21	10	\N	1	1	\N
740	3	\N	21	8	2006-08-21	1	8	\N
4026	16	\N	21	10	\N	1	8	\N
4260	2	\N	21	8	\N	1	8	\N
6130	2	\N	21	10	\N	1	10	\N
6097	87	\N	5	\N	\N	1	1	\N
6098	97	\N	5	\N	\N	1	1	\N
6099	99	\N	5	\N	\N	1	1	\N
5008	18	\N	5	10	\N	1	10	\N
5009	18	\N	5	10	\N	1	10	\N
6127	2	\N	5	10	\N	1	8	\N
3707	16	\N	1	10	\N	1	10	\N
3712	16	\N	1	10	\N	1	10	\N
3708	16	\N	5	10	\N	1	10	\N
1406	2	\N	18	1	2006-09-27	\N	\N	\N
6128	2	\N	21	10	\N	1	8	\N
6100	32	\N	5	\N	\N	1	1	\N
6125	2	\N	5	10	\N	1	8	\N
6101	45	\N	5	\N	\N	1	1	\N
6102	32	\N	5	\N	\N	1	1	\N
5232	2	\N	18	1	\N	\N	\N	\N
6126	2	\N	21	10	\N	1	8	\N
4493	2	\N	5	10	\N	1	10	\N
6123	2	\N	18	10	\N	1	8	\N
4012	16	\N	5	10	\N	1	10	\N
4013	16	\N	5	10	\N	1	10	\N
6103	99	\N	5	\N	\N	1	10	\N
6104	100	\N	5	\N	\N	1	10	\N
4045	16	\N	6	10	\N	1	1	\N
4044	16	\N	6	10	\N	1	1	\N
4042	16	\N	6	10	\N	1	1	\N
4043	16	\N	6	10	\N	1	1	\N
4893	17	\N	21	10	\N	1	10	\N
4894	17	\N	21	10	\N	1	10	\N
4485	3	\N	21	14	\N	1	10	\N
2976	16	\N	21	11	\N	1	10	\N
6105	2	\N	21	10	\N	1	10	\N
4562	3	\N	21	1	\N	1	10	\N
3593	16	\N	21	10	\N	1	10	\N
6106	2	\N	21	10	\N	1	10	\N
4984	18	\N	5	10	\N	1	10	\N
6135	87	\N	5	\N	\N	1	10	\N
6136	97	\N	5	\N	\N	1	1	\N
6137	99	\N	5	\N	\N	1	1	\N
6138	87	\N	5	\N	\N	1	1	\N
6139	61	\N	5	\N	\N	1	1	\N
6140	99	\N	5	\N	\N	1	1	\N
6141	96	\N	5	\N	\N	1	1	\N
6142	61	\N	5	\N	\N	1	1	\N
6143	96	\N	5	\N	\N	1	1	\N
6144	99	\N	5	\N	\N	1	1	\N
4895	17	\N	21	10	\N	1	10	\N
4896	17	\N	21	10	\N	1	10	\N
4851	3	\N	21	10	\N	1	10	\N
3983	16	\N	21	10	\N	1	10	\N
6107	2	\N	21	10	\N	1	10	\N
4558	3	\N	21	1	\N	1	10	\N
3986	16	\N	21	10	\N	1	10	\N
4519	2	\N	21	1	\N	1	10	\N
4985	18	\N	5	10	\N	1	10	\N
6145	87	\N	5	\N	\N	1	10	\N
4230	2	\N	5	8	\N	1	1	\N
5248	3	\N	3	10	\N	1	1	\N
6146	104	\N	6	10	\N	\N	\N	\N
6147	104	\N	6	10	\N	\N	\N	\N
6148	104	\N	6	10	\N	\N	\N	\N
6149	104	\N	6	10	\N	\N	\N	\N
6150	104	\N	6	10	\N	\N	\N	\N
6151	104	\N	6	10	\N	\N	\N	\N
6152	104	\N	6	10	\N	\N	\N	\N
6153	104	\N	6	10	\N	\N	\N	\N
6154	104	\N	6	10	\N	\N	\N	\N
6155	104	\N	6	10	\N	\N	\N	\N
6156	104	\N	6	10	\N	\N	\N	\N
6157	104	\N	6	10	\N	\N	\N	\N
6158	104	\N	6	10	\N	\N	\N	\N
6159	104	\N	6	10	\N	\N	\N	\N
6160	104	\N	6	10	\N	\N	\N	\N
6161	104	\N	6	10	\N	\N	\N	\N
6162	104	\N	6	10	\N	\N	\N	\N
6163	104	\N	6	10	\N	\N	\N	\N
6164	104	\N	6	10	\N	\N	\N	\N
6165	104	\N	6	10	\N	\N	\N	\N
6166	104	\N	6	10	\N	\N	\N	\N
6167	104	\N	6	10	\N	\N	\N	\N
6168	104	\N	6	10	\N	\N	\N	\N
6169	104	\N	6	10	\N	\N	\N	\N
6170	104	\N	6	10	\N	\N	\N	\N
6171	104	\N	6	10	\N	\N	\N	\N
6172	104	\N	6	10	\N	\N	\N	\N
6173	104	\N	6	10	\N	\N	\N	\N
6174	104	\N	6	10	\N	\N	\N	\N
6175	104	\N	6	10	\N	\N	\N	\N
6176	104	\N	6	10	\N	\N	\N	\N
6177	104	\N	6	10	\N	\N	\N	\N
6178	104	\N	6	10	\N	\N	\N	\N
6179	104	\N	6	10	\N	\N	\N	\N
6180	104	\N	6	10	\N	\N	\N	\N
6181	104	\N	6	10	\N	\N	\N	\N
6182	104	\N	6	10	\N	\N	\N	\N
6183	104	\N	6	10	\N	\N	\N	\N
6184	104	\N	6	10	\N	\N	\N	\N
6185	104	\N	6	10	\N	\N	\N	\N
6186	104	\N	6	10	\N	\N	\N	\N
6187	104	\N	6	10	\N	\N	\N	\N
6188	104	\N	6	10	\N	\N	\N	\N
6189	104	\N	6	10	\N	\N	\N	\N
6190	104	\N	6	10	\N	\N	\N	\N
6191	104	\N	6	10	\N	\N	\N	\N
6192	104	\N	6	10	\N	\N	\N	\N
6193	104	\N	6	10	\N	\N	\N	\N
6194	104	\N	6	10	\N	\N	\N	\N
6195	104	\N	6	10	\N	\N	\N	\N
6196	104	\N	6	10	\N	\N	\N	\N
6197	104	\N	6	10	\N	\N	\N	\N
6198	104	\N	6	10	\N	\N	\N	\N
6199	104	\N	6	10	\N	\N	\N	\N
6200	104	\N	6	10	\N	\N	\N	\N
6201	104	\N	6	10	\N	\N	\N	\N
6202	104	\N	6	10	\N	\N	\N	\N
6203	104	\N	6	10	\N	\N	\N	\N
6204	104	\N	6	10	\N	\N	\N	\N
6205	104	\N	6	10	\N	\N	\N	\N
6206	104	\N	6	10	\N	\N	\N	\N
6207	104	\N	6	10	\N	\N	\N	\N
6208	104	\N	6	10	\N	\N	\N	\N
6209	104	\N	6	10	\N	\N	\N	\N
6210	104	\N	6	10	\N	\N	\N	\N
6211	104	\N	6	10	\N	\N	\N	\N
6212	104	\N	6	10	\N	\N	\N	\N
6213	104	\N	6	10	\N	\N	\N	\N
6214	104	\N	6	10	\N	\N	\N	\N
6215	104	\N	6	10	\N	\N	\N	\N
6216	104	\N	6	10	\N	\N	\N	\N
6217	104	\N	6	10	\N	\N	\N	\N
6218	103	\N	6	10	\N	\N	\N	\N
6219	103	\N	6	10	\N	\N	\N	\N
6220	103	\N	6	10	\N	\N	\N	\N
6221	103	\N	6	10	\N	\N	\N	\N
6222	103	\N	6	10	\N	\N	\N	\N
6223	103	\N	6	10	\N	\N	\N	\N
6224	103	\N	6	10	\N	\N	\N	\N
6225	103	\N	6	10	\N	\N	\N	\N
6226	103	\N	6	10	\N	\N	\N	\N
6227	103	\N	6	10	\N	\N	\N	\N
6228	103	\N	6	10	\N	\N	\N	\N
6229	103	\N	6	10	\N	\N	\N	\N
6230	103	\N	6	10	\N	\N	\N	\N
6231	103	\N	6	10	\N	\N	\N	\N
6232	103	\N	6	10	\N	\N	\N	\N
6233	103	\N	6	10	\N	\N	\N	\N
6234	103	\N	6	10	\N	\N	\N	\N
6235	103	\N	6	10	\N	\N	\N	\N
6236	103	\N	6	10	\N	\N	\N	\N
6237	103	\N	6	10	\N	\N	\N	\N
6238	103	\N	6	10	\N	\N	\N	\N
6239	103	\N	6	10	\N	\N	\N	\N
6240	103	\N	6	10	\N	\N	\N	\N
6241	103	\N	6	10	\N	\N	\N	\N
6242	103	\N	6	10	\N	\N	\N	\N
6243	103	\N	6	10	\N	\N	\N	\N
6244	103	\N	6	10	\N	\N	\N	\N
6245	103	\N	6	10	\N	\N	\N	\N
6246	103	\N	6	10	\N	\N	\N	\N
6247	103	\N	6	10	\N	\N	\N	\N
6248	103	\N	6	10	\N	\N	\N	\N
6249	103	\N	6	10	\N	\N	\N	\N
6250	103	\N	6	10	\N	\N	\N	\N
6251	103	\N	6	10	\N	\N	\N	\N
6252	103	\N	6	10	\N	\N	\N	\N
6253	103	\N	6	10	\N	\N	\N	\N
6254	103	\N	6	10	\N	\N	\N	\N
6255	103	\N	6	10	\N	\N	\N	\N
6256	103	\N	6	10	\N	\N	\N	\N
6257	103	\N	6	10	\N	\N	\N	\N
6258	103	\N	6	10	\N	\N	\N	\N
6259	103	\N	6	10	\N	\N	\N	\N
6260	103	\N	6	10	\N	\N	\N	\N
6261	103	\N	6	10	\N	\N	\N	\N
6262	103	\N	6	10	\N	\N	\N	\N
6263	103	\N	6	10	\N	\N	\N	\N
6264	103	\N	6	10	\N	\N	\N	\N
6265	103	\N	6	10	\N	\N	\N	\N
6266	103	\N	6	10	\N	\N	\N	\N
6267	103	\N	6	10	\N	\N	\N	\N
6268	103	\N	6	10	\N	\N	\N	\N
6269	103	\N	6	10	\N	\N	\N	\N
6270	103	\N	6	10	\N	\N	\N	\N
6271	103	\N	6	10	\N	\N	\N	\N
6272	103	\N	6	10	\N	\N	\N	\N
6273	103	\N	6	10	\N	\N	\N	\N
6274	103	\N	6	10	\N	\N	\N	\N
6275	103	\N	6	10	\N	\N	\N	\N
6276	103	\N	6	10	\N	\N	\N	\N
6277	103	\N	6	10	\N	\N	\N	\N
6278	103	\N	6	10	\N	\N	\N	\N
6279	103	\N	6	10	\N	\N	\N	\N
6280	103	\N	6	10	\N	\N	\N	\N
6281	103	\N	6	10	\N	\N	\N	\N
6282	103	\N	6	10	\N	\N	\N	\N
6283	103	\N	6	10	\N	\N	\N	\N
6284	103	\N	6	10	\N	\N	\N	\N
6285	103	\N	6	10	\N	\N	\N	\N
6286	103	\N	6	10	\N	\N	\N	\N
6287	103	\N	6	10	\N	\N	\N	\N
6288	103	\N	6	10	\N	\N	\N	\N
6289	103	\N	6	10	\N	\N	\N	\N
4524	1	\N	5	10	\N	1	1	\N
6290	99	\N	5	\N	\N	1	1	\N
6291	100	\N	5	\N	\N	1	1	\N
6132	2	\N	18	10	\N	\N	\N	\N
6133	2	\N	18	10	\N	\N	\N	\N
3283	16	\N	21	10	\N	1	1	\N
5223	2	\N	18	1	\N	1	1	\N
412	2	\N	5	1	2006-08-10	1	1	\N
458	2	\N	5	1	2006-07-26	1	1	\N
5198	2	\N	5	1	\N	1	1	\N
649	3	\N	18	10	2006-08-14	\N	\N	\N
944	2	\N	18	8	2006-09-05	\N	\N	\N
4031	16	\N	1	10	\N	1	1	\N
4030	16	\N	1	10	\N	1	1	\N
4028	16	\N	1	10	\N	1	1	\N
4029	16	\N	1	10	\N	1	1	\N
3973	16	\N	1	10	\N	1	1	\N
6292	58	\N	5	\N	\N	1	1	\N
6293	98	\N	5	\N	\N	1	1	\N
6294	59	\N	5	\N	\N	1	1	\N
983	2	\N	5	8	2007-04-11	1	10	\N
857	2	\N	5	10	2006-10-17	1	10	\N
4287	2	\N	5	10	\N	1	10	\N
4852	3	\N	18	10	\N	\N	\N	\N
6109	2	\N	18	10	\N	\N	\N	\N
6131	2	\N	18	10	\N	\N	\N	\N
4937	17	\N	21	10	\N	1	8	\N
4938	17	\N	21	10	\N	1	8	\N
4854	3	\N	21	10	\N	1	8	\N
3721	16	\N	21	10	\N	1	8	\N
4853	3	\N	21	10	\N	1	8	\N
3720	16	\N	21	10	\N	1	8	\N
4353	2	\N	21	10	\N	1	8	\N
5010	18	\N	5	10	\N	1	8	\N
6295	87	\N	5	\N	\N	1	8	\N
4935	17	\N	21	10	\N	1	8	\N
4936	17	\N	21	10	\N	1	8	\N
6296	84	\N	5	\N	\N	1	8	\N
551	3	\N	21	17	2006-12-20	1	8	\N
3011	16	\N	21	11	2006-12-20	1	8	\N
4857	3	\N	18	10	\N	\N	\N	\N
5246	3	\N	21	10	\N	1	8	\N
3611	16	\N	21	10	\N	1	8	\N
2891	2	\N	21	1	2006-12-20	1	8	\N
6111	2	\N	18	10	\N	\N	\N	\N
4117	3	\N	21	8	2007-04-12	1	8	\N
3683	16	\N	21	10	\N	1	8	\N
939	2	\N	21	8	2006-09-04	1	8	\N
5011	18	\N	5	10	\N	1	10	\N
6297	87	\N	5	\N	\N	1	10	\N
5218	2	\N	5	1	\N	1	10	\N
4850	3	\N	5	10	\N	1	10	\N
2907	16	\N	5	11	2006-12-20	1	10	\N
2916	16	\N	5	11	2006-12-20	1	10	\N
6298	25	\N	5	\N	\N	1	10	\N
4772	3	\N	21	1	\N	1	14	\N
3987	16	\N	21	10	\N	1	14	\N
6299	87	\N	5	\N	\N	1	14	\N
4989	18	\N	5	10	\N	1	10	\N
6300	100	\N	5	\N	\N	1	10	\N
6301	99	\N	5	\N	\N	1	10	\N
3455	2	\N	18	8	2007-02-13	\N	\N	\N
4899	17	\N	21	10	\N	1	8	\N
4900	17	\N	21	10	\N	1	8	\N
3982	16	\N	18	10	\N	1	14	\N
6108	2	\N	5	10	\N	1	8	\N
4848	3	\N	21	10	\N	1	8	\N
3610	16	\N	21	10	\N	1	8	\N
4849	3	\N	21	10	\N	1	8	\N
3981	16	\N	21	10	\N	1	8	\N
6064	2	\N	21	14	\N	1	8	\N
4898	17	\N	21	10	\N	1	8	\N
4897	17	\N	21	10	\N	1	8	\N
4986	18	\N	5	10	\N	1	8	\N
6134	2	\N	5	10	\N	1	10	\N
2892	2	\N	18	1	2006-12-20	\N	\N	\N
854	2	\N	5	10	2006-09-08	1	8	\N
5197	2	\N	5	1	\N	1	8	\N
588	3	\N	5	8	2006-08-10	1	8	\N
590	3	\N	5	8	2006-08-10	1	8	\N
3718	16	\N	5	10	\N	1	8	\N
3719	16	\N	5	10	\N	1	8	\N
3179	2	\N	5	1	2007-01-09	1	1	\N
6302	25	\N	5	\N	\N	1	8	\N
6120	2	\N	5	10	\N	1	8	\N
6303	29	\N	5	\N	\N	1	8	\N
6304	32	\N	5	\N	\N	1	8	\N
5214	2	\N	5	1	\N	1	1	\N
6305	61	\N	5	\N	\N	1	8	\N
6306	36	\N	5	\N	\N	1	8	\N
6307	96	\N	5	\N	\N	1	8	\N
6308	99	\N	5	\N	\N	1	8	\N
6309	40	\N	5	\N	\N	1	8	\N
6310	25	\N	5	\N	\N	1	8	\N
6321	3	\N	18	10	\N	\N	\N	\N
6311	29	\N	5	\N	\N	1	8	\N
417	2	\N	5	1	2006-07-26	1	8	\N
6312	32	\N	5	\N	\N	1	8	\N
6313	61	\N	5	\N	\N	1	8	\N
6314	36	\N	5	\N	\N	1	8	\N
738	2	\N	21	8	2006-08-21	1	8	\N
6315	40	\N	5	\N	\N	1	8	\N
6316	96	\N	5	\N	\N	1	8	\N
6322	3	\N	21	10	\N	1	8	\N
6317	99	\N	5	\N	\N	1	8	\N
856	2	\N	19	10	2006-09-08	1	8	\N
5230	2	\N	19	1	\N	1	8	\N
2734	2	\N	5	14	\N	1	8	\N
4494	2	\N	5	10	\N	1	8	\N
4855	3	\N	18	10	\N	1	10	\N
1259	3	\N	18	14	2006-12-20	1	8	\N
4856	3	\N	18	10	\N	\N	\N	\N
6110	2	\N	18	10	\N	\N	\N	\N
6112	2	\N	18	10	\N	\N	\N	\N
4860	3	\N	18	10	\N	\N	\N	\N
4902	17	\N	21	10	\N	1	8	\N
4901	17	\N	21	10	\N	1	8	\N
5249	3	\N	21	10	\N	1	8	\N
3660	16	\N	21	10	\N	1	8	\N
6117	2	\N	21	10	\N	1	8	\N
4859	3	\N	21	10	\N	1	8	\N
3615	16	\N	21	10	\N	1	8	\N
5231	2	\N	21	1	\N	1	8	\N
4988	18	\N	5	10	\N	1	8	\N
6318	87	\N	5	\N	\N	1	8	\N
6118	2	\N	18	10	\N	\N	\N	\N
6323	3	\N	6	10	\N	\N	\N	\N
6339	84	\N	5	\N	\N	1	1	\N
6340	87	\N	5	\N	\N	1	1	\N
6320	3	\N	18	10	\N	\N	\N	\N
6115	2	\N	18	10	\N	\N	\N	\N
4933	17	\N	21	10	\N	1	8	\N
4934	17	\N	21	10	\N	1	8	\N
1371	2	\N	18	5	\N	\N	\N	\N
4858	3	\N	21	10	\N	1	8	\N
4015	16	\N	21	10	\N	1	8	\N
6119	2	\N	21	10	\N	1	8	\N
5228	2	\N	5	1	\N	1	10	\N
6319	3	\N	5	10	\N	1	10	\N
4014	16	\N	21	10	\N	1	8	\N
612	2	\N	21	1	2006-08-10	1	8	\N
4994	18	\N	5	10	\N	1	8	\N
6341	87	\N	5	\N	\N	1	8	\N
6344	13	\N	6	16	\N	\N	\N	\N
6345	13	\N	6	16	\N	\N	\N	\N
6346	13	\N	6	16	\N	\N	\N	\N
6347	13	\N	6	16	\N	\N	\N	\N
6348	13	\N	6	16	\N	\N	\N	\N
6349	13	\N	6	16	\N	\N	\N	\N
6116	2	\N	18	10	\N	\N	\N	\N
3709	16	\N	5	10	\N	1	10	\N
6350	30	\N	5	\N	\N	1	10	\N
6351	99	\N	5	\N	\N	1	10	\N
6342	21	\N	5	10	\N	1	10	\N
6352	99	\N	5	\N	\N	1	10	\N
4227	2	\N	5	8	\N	1	1	\N
5242	3	\N	5	10	\N	1	1	\N
6354	99	\N	5	\N	\N	1	1	\N
6353	1	\N	6	10	\N	3	5	\N
3657	16	\N	1	10	\N	1	1	\N
6338	2	\N	18	10	\N	\N	\N	\N
6337	2	\N	18	10	\N	\N	\N	\N
1578	1	\N	18	10	2006-10-03	4	10	\N
3471	3	\N	18	10	2007-02-13	\N	\N	\N
2725	2	\N	18	14	2006-11-10	\N	\N	\N
4389	2	\N	18	10	\N	1	10	\N
6375	108	\N	6	10000	\N	\N	\N	\N
6376	108	\N	6	10000	\N	\N	\N	\N
6377	108	\N	6	10000	\N	\N	\N	\N
6113	2	\N	5	10	\N	1	8	\N
1480	1	\N	18	10	2006-10-03	\N	\N	\N
6363	3	\N	21	8	\N	1	1	\N
3033	2	\N	21	1	2007-01-15	1	1	\N
1333	2	\N	18	5	\N	\N	\N	\N
6343	21	\N	5	10	\N	1	1	\N
377	2	\N	5	1	2006-08-31	1	1	\N
6359	3	\N	21	8	\N	1	1	\N
6360	3	\N	21	8	\N	1	1	\N
6365	2	\N	21	8	\N	1	1	\N
6385	87	\N	5	\N	\N	1	1	\N
6386	86	\N	5	\N	\N	1	8	\N
6387	86	\N	5	\N	\N	1	8	\N
6388	86	\N	5	\N	\N	1	8	\N
6389	86	\N	5	\N	\N	1	8	\N
6390	86	\N	5	\N	\N	1	8	\N
6336	2	\N	18	10	\N	\N	\N	\N
3360	2	\N	18	8	2007-02-13	\N	\N	\N
6391	25	\N	5	\N	\N	1	8	\N
6392	30	\N	5	\N	\N	1	8	\N
6393	36	\N	5	\N	\N	1	8	\N
6394	38	\N	5	\N	\N	1	8	\N
6395	95	\N	5	\N	\N	1	8	\N
6396	99	\N	5	\N	\N	1	8	\N
6397	26	\N	5	\N	\N	1	8	\N
6370	2	\N	5	8	\N	1	1	\N
6355	3	\N	18	8	\N	\N	\N	\N
5221	2	\N	21	1	\N	1	1	\N
6362	3	\N	21	8	\N	1	8	\N
6374	2	\N	21	8	\N	1	8	\N
6358	3	\N	21	8	\N	1	8	\N
6328	3	\N	18	10	\N	\N	\N	\N
6418	25	\N	5	\N	\N	1	1	\N
6419	84	\N	5	\N	\N	1	1	\N
6420	36	\N	5	\N	\N	1	1	\N
6421	38	\N	5	\N	\N	1	1	\N
6422	40	\N	5	\N	\N	1	1	\N
6423	101	\N	5	\N	\N	1	1	\N
6424	101	\N	5	\N	\N	1	1	\N
6425	25	\N	5	\N	\N	1	1	\N
6426	38	\N	5	\N	\N	1	1	\N
6427	84	\N	5	\N	\N	1	1	\N
6428	36	\N	5	\N	\N	1	1	\N
6429	37	\N	5	\N	\N	1	1	\N
6430	26	\N	5	\N	\N	1	1	\N
5015	18	\N	5	10	\N	1	8	\N
6398	2	\N	18	10	\N	\N	\N	\N
6431	61	\N	5	\N	\N	1	1	\N
6432	40	\N	5	\N	\N	1	8	\N
6433	99	\N	5	\N	\N	1	8	\N
6434	26	\N	5	\N	\N	1	8	\N
6435	40	\N	5	\N	\N	1	8	\N
6436	99	\N	5	\N	\N	1	8	\N
6437	26	\N	5	\N	\N	1	8	\N
6400	2	\N	18	10	\N	\N	\N	\N
6372	2	\N	21	8	\N	1	1	\N
5016	18	\N	5	10	\N	1	1	\N
4282	3	\N	21	1	\N	1	8	\N
6373	2	\N	21	8	\N	1	8	\N
5138	16	\N	7	10	\N	1	8	\N
6464	17	\N	6	10	\N	\N	\N	\N
6465	17	\N	6	10	\N	\N	\N	\N
6466	17	\N	6	10	\N	\N	\N	\N
6467	17	\N	6	10	\N	\N	\N	\N
6468	17	\N	6	10	\N	\N	\N	\N
6469	17	\N	6	10	\N	\N	\N	\N
6470	17	\N	6	10	\N	\N	\N	\N
6471	17	\N	6	10	\N	\N	\N	\N
6472	17	\N	6	10	\N	\N	\N	\N
6473	17	\N	6	10	\N	\N	\N	\N
6474	17	\N	6	10	\N	\N	\N	\N
6475	17	\N	6	10	\N	\N	\N	\N
6476	17	\N	6	10	\N	\N	\N	\N
6477	17	\N	6	10	\N	\N	\N	\N
6478	17	\N	6	10	\N	\N	\N	\N
6479	17	\N	6	10	\N	\N	\N	\N
6480	17	\N	6	10	\N	\N	\N	\N
6481	17	\N	6	10	\N	\N	\N	\N
6482	17	\N	6	10	\N	\N	\N	\N
6483	17	\N	6	10	\N	\N	\N	\N
6484	17	\N	6	10	\N	\N	\N	\N
6485	17	\N	6	10	\N	\N	\N	\N
6486	17	\N	6	10	\N	\N	\N	\N
6487	17	\N	6	10	\N	\N	\N	\N
6488	17	\N	6	10	\N	\N	\N	\N
6489	17	\N	6	10	\N	\N	\N	\N
6524	17	\N	6	10	\N	\N	\N	\N
6525	17	\N	6	10	\N	\N	\N	\N
6526	17	\N	6	10	\N	\N	\N	\N
6527	17	\N	6	10	\N	\N	\N	\N
6528	17	\N	6	10	\N	\N	\N	\N
6555	16	\N	18	10	\N	4	1	\N
6451	17	\N	21	10	\N	1	8	\N
6452	17	\N	21	10	\N	1	8	\N
6453	17	\N	21	10	\N	1	8	\N
6454	17	\N	21	10	\N	1	8	\N
6455	17	\N	21	10	\N	1	8	\N
6456	17	\N	21	10	\N	1	8	\N
6545	16	\N	7	10	\N	1	10	\N
6544	16	\N	7	10	\N	1	10	\N
6543	16	\N	7	10	\N	1	10	\N
880	2	\N	5	10	2006-09-08	1	8	\N
6402	2	\N	18	10	\N	\N	\N	\N
6568	25	\N	5	\N	\N	1	1	\N
6569	36	\N	5	\N	\N	1	1	\N
6570	38	\N	5	\N	\N	1	1	\N
6571	40	\N	5	\N	\N	1	1	\N
6572	25	\N	5	\N	\N	1	1	\N
6573	29	\N	5	\N	\N	1	1	\N
6574	36	\N	5	\N	\N	1	1	\N
6575	38	\N	5	\N	\N	1	1	\N
6576	40	\N	5	\N	\N	1	1	\N
6334	2	\N	21	10	\N	1	8	\N
6333	2	\N	21	10	\N	1	8	\N
6403	2	\N	18	10	\N	\N	\N	\N
5139	16	\N	7	10	\N	4	8	\N
6335	2	\N	21	10	\N	1	8	\N
6577	87	\N	5	\N	\N	1	8	\N
6578	87	\N	5	\N	\N	1	8	\N
6404	2	\N	18	10	\N	\N	\N	\N
6579	61	\N	5	\N	\N	1	1	\N
5126	16	\N	18	10	\N	4	10	\N
6580	87	\N	5	\N	\N	1	8	\N
5017	18	\N	5	10	\N	1	8	\N
6581	87	\N	5	\N	\N	1	8	\N
6582	61	\N	5	\N	\N	1	10	\N
6407	2	\N	18	10	\N	\N	\N	\N
6364	3	\N	21	8	\N	1	8	\N
5129	16	\N	21	10	\N	1	8	\N
5130	16	\N	21	10	\N	1	8	\N
5018	18	\N	5	10	\N	1	8	\N
6583	3	\N	6	10	\N	\N	\N	\N
6584	3	\N	6	10	\N	\N	\N	\N
5128	16	\N	21	10	\N	1	8	\N
6405	2	\N	21	10	\N	1	8	\N
6613	25	\N	5	\N	\N	1	1	\N
6614	28	\N	5	\N	\N	1	1	\N
6615	32	\N	5	\N	\N	1	1	\N
6616	37	\N	5	\N	\N	1	1	\N
6617	26	\N	5	\N	\N	1	1	\N
6411	2	\N	5	10	\N	1	1	\N
6410	2	\N	18	10	\N	\N	\N	\N
6409	2	\N	18	10	\N	\N	\N	\N
6618	24	\N	5	\N	\N	1	1	\N
6619	99	\N	5	\N	\N	1	1	\N
6408	2	\N	1	10	\N	1	8	\N
6620	100	\N	5	\N	\N	1	1	\N
6621	99	\N	5	\N	\N	1	1	\N
6970	116	\N	6	1	\N	\N	\N	\N
6971	116	\N	6	1	\N	\N	\N	\N
6972	116	\N	6	1	\N	\N	\N	\N
6973	116	\N	6	1	\N	\N	\N	\N
6974	116	\N	6	1	\N	\N	\N	\N
6975	116	\N	6	1	\N	\N	\N	\N
6976	116	\N	6	1	\N	\N	\N	\N
6977	116	\N	6	1	\N	\N	\N	\N
6978	116	\N	6	1	\N	\N	\N	\N
6979	116	\N	6	1	\N	\N	\N	\N
6980	116	\N	6	1	\N	\N	\N	\N
6981	116	\N	6	1	\N	\N	\N	\N
6622	110	\N	5	1	\N	1	1	\N
6624	110	\N	5	1	\N	1	1	\N
6623	111	\N	5	1	\N	1	1	\N
6683	111	\N	5	1	\N	1	1	\N
6742	112	\N	5	1	\N	1	1	\N
6743	112	\N	5	1	\N	1	1	\N
6802	113	\N	5	1	\N	1	1	\N
6803	113	\N	5	1	\N	1	1	\N
6862	114	\N	5	1	\N	1	1	\N
6863	114	\N	5	1	\N	1	1	\N
6943	116	\N	5	1	\N	1	1	\N
6944	116	\N	5	1	\N	1	1	\N
6625	110	\N	5	1	\N	1	1	\N
6626	110	\N	5	1	\N	1	1	\N
6684	111	\N	5	1	\N	1	1	\N
6685	111	\N	5	1	\N	1	1	\N
6744	112	\N	5	1	\N	1	1	\N
6745	112	\N	5	1	\N	1	1	\N
6804	113	\N	5	1	\N	1	1	\N
6805	113	\N	5	1	\N	1	1	\N
6864	114	\N	5	1	\N	1	1	\N
6865	114	\N	5	1	\N	1	1	\N
6922	115	\N	5	1	\N	1	1	\N
6945	116	\N	5	1	\N	1	1	\N
6627	110	\N	5	1	\N	1	1	\N
6628	110	\N	5	1	\N	1	1	\N
6686	111	\N	5	1	\N	1	1	\N
6687	111	\N	5	1	\N	1	1	\N
6746	112	\N	5	1	\N	1	1	\N
6747	112	\N	5	1	\N	1	1	\N
6806	113	\N	5	1	\N	1	1	\N
6807	113	\N	5	1	\N	1	1	\N
6866	114	\N	5	1	\N	1	1	\N
6867	114	\N	5	1	\N	1	1	\N
6923	115	\N	5	1	\N	1	1	\N
6924	115	\N	5	1	\N	1	1	\N
6629	110	\N	5	1	\N	1	1	\N
6630	110	\N	5	1	\N	1	1	\N
6688	111	\N	5	1	\N	1	1	\N
6689	111	\N	5	1	\N	1	1	\N
6748	112	\N	5	1	\N	1	1	\N
6749	112	\N	5	1	\N	1	1	\N
6808	113	\N	5	1	\N	1	1	\N
6809	113	\N	5	1	\N	1	1	\N
6868	114	\N	5	1	\N	1	1	\N
6869	114	\N	5	1	\N	1	1	\N
6925	115	\N	5	1	\N	1	1	\N
6946	116	\N	5	1	\N	1	1	\N
6631	110	\N	5	1	\N	1	1	\N
6632	110	\N	5	1	\N	1	1	\N
6690	111	\N	5	1	\N	1	1	\N
6691	111	\N	5	1	\N	1	1	\N
6750	112	\N	5	1	\N	1	1	\N
6751	112	\N	5	1	\N	1	1	\N
6810	113	\N	5	1	\N	1	1	\N
6811	113	\N	5	1	\N	1	1	\N
6870	114	\N	5	1	\N	1	1	\N
6871	114	\N	5	1	\N	1	1	\N
6947	116	\N	5	1	\N	1	1	\N
6948	116	\N	5	1	\N	1	1	\N
6634	110	\N	5	1	\N	1	1	\N
6633	110	\N	5	1	\N	1	1	\N
6692	111	\N	5	1	\N	1	1	\N
6693	111	\N	5	1	\N	1	1	\N
6752	112	\N	5	1	\N	1	1	\N
6753	112	\N	5	1	\N	1	1	\N
6812	113	\N	5	1	\N	1	1	\N
6813	113	\N	5	1	\N	1	1	\N
6872	114	\N	5	1	\N	1	1	\N
6873	114	\N	5	1	\N	1	1	\N
6926	115	\N	5	1	\N	1	1	\N
6982	117	\N	5	1	\N	1	1	\N
6635	110	\N	5	1	\N	1	1	\N
6636	110	\N	5	1	\N	1	1	\N
6694	111	\N	5	1	\N	1	1	\N
6695	111	\N	5	1	\N	1	1	\N
6754	112	\N	5	1	\N	1	1	\N
6755	112	\N	5	1	\N	1	1	\N
6814	113	\N	5	1	\N	1	1	\N
6815	113	\N	5	1	\N	1	1	\N
6874	114	\N	5	1	\N	1	1	\N
6875	114	\N	5	1	\N	1	1	\N
6927	115	\N	5	1	\N	1	1	\N
6983	117	\N	5	1	\N	1	1	\N
6637	110	\N	5	1	\N	1	1	\N
6638	110	\N	5	1	\N	1	1	\N
6639	110	\N	5	1	\N	1	1	\N
6640	110	\N	5	1	\N	1	1	\N
6641	110	\N	5	1	\N	1	1	\N
6642	110	\N	5	1	\N	1	1	\N
6643	110	\N	5	1	\N	1	1	\N
6644	110	\N	5	1	\N	1	1	\N
6645	110	\N	5	1	\N	1	1	\N
6646	110	\N	5	1	\N	1	1	\N
6647	110	\N	5	1	\N	1	1	\N
6648	110	\N	5	1	\N	1	1	\N
6649	110	\N	5	1	\N	1	1	\N
6650	110	\N	5	1	\N	1	1	\N
6706	111	\N	5	1	\N	1	1	\N
6707	111	\N	5	1	\N	1	1	\N
6708	111	\N	5	1	\N	1	1	\N
6709	111	\N	5	1	\N	1	1	\N
6696	111	\N	5	1	\N	1	1	\N
6697	111	\N	5	1	\N	1	1	\N
6698	111	\N	5	1	\N	1	1	\N
6699	111	\N	5	1	\N	1	1	\N
6700	111	\N	5	1	\N	1	1	\N
6701	111	\N	5	1	\N	1	1	\N
6702	111	\N	5	1	\N	1	1	\N
6703	111	\N	5	1	\N	1	1	\N
6704	111	\N	5	1	\N	1	1	\N
6705	111	\N	5	1	\N	1	1	\N
6756	112	\N	5	1	\N	1	1	\N
6757	112	\N	5	1	\N	1	1	\N
6758	112	\N	5	1	\N	1	1	\N
6759	112	\N	5	1	\N	1	1	\N
6760	112	\N	5	1	\N	1	1	\N
6761	112	\N	5	1	\N	1	1	\N
6762	112	\N	5	1	\N	1	1	\N
6763	112	\N	5	1	\N	1	1	\N
6764	112	\N	5	1	\N	1	1	\N
6765	112	\N	5	1	\N	1	1	\N
6766	112	\N	5	1	\N	1	1	\N
6767	112	\N	5	1	\N	1	1	\N
6768	112	\N	5	1	\N	1	1	\N
6769	112	\N	5	1	\N	1	1	\N
6816	113	\N	5	1	\N	1	1	\N
6817	113	\N	5	1	\N	1	1	\N
6818	113	\N	5	1	\N	1	1	\N
6819	113	\N	5	1	\N	1	1	\N
6820	113	\N	5	1	\N	1	1	\N
6821	113	\N	5	1	\N	1	1	\N
6822	113	\N	5	1	\N	1	1	\N
6823	113	\N	5	1	\N	1	1	\N
6824	113	\N	5	1	\N	1	1	\N
6825	113	\N	5	1	\N	1	1	\N
6826	113	\N	5	1	\N	1	1	\N
6827	113	\N	5	1	\N	1	1	\N
6828	113	\N	5	1	\N	1	1	\N
6829	113	\N	5	1	\N	1	1	\N
6886	114	\N	5	1	\N	1	1	\N
6887	114	\N	5	1	\N	1	1	\N
6888	114	\N	5	1	\N	1	1	\N
6889	114	\N	5	1	\N	1	1	\N
6876	114	\N	5	1	\N	1	1	\N
6877	114	\N	5	1	\N	1	1	\N
6878	114	\N	5	1	\N	1	1	\N
6879	114	\N	5	1	\N	1	1	\N
6880	114	\N	5	1	\N	1	1	\N
6881	114	\N	5	1	\N	1	1	\N
6882	114	\N	5	1	\N	1	1	\N
6883	114	\N	5	1	\N	1	1	\N
6884	114	\N	5	1	\N	1	1	\N
6885	114	\N	5	1	\N	1	1	\N
6958	116	\N	5	1	\N	1	1	\N
6959	116	\N	5	1	\N	1	1	\N
6960	116	\N	5	1	\N	1	1	\N
6961	116	\N	5	1	\N	1	1	\N
6949	116	\N	5	1	\N	1	1	\N
6950	116	\N	5	1	\N	1	1	\N
6951	116	\N	5	1	\N	1	1	\N
6952	116	\N	5	1	\N	1	1	\N
6953	116	\N	5	1	\N	1	1	\N
6954	116	\N	5	1	\N	1	1	\N
6955	116	\N	5	1	\N	1	1	\N
6956	116	\N	5	1	\N	1	1	\N
6957	116	\N	5	1	\N	1	1	\N
6655	110	\N	5	1	\N	1	1	\N
6656	110	\N	5	1	\N	1	1	\N
6657	110	\N	5	1	\N	1	1	\N
6658	110	\N	5	1	\N	1	1	\N
6659	110	\N	5	1	\N	1	1	\N
6660	110	\N	5	1	\N	1	1	\N
6661	110	\N	5	1	\N	1	1	\N
6662	110	\N	5	1	\N	1	1	\N
6663	110	\N	5	1	\N	1	1	\N
6664	110	\N	5	1	\N	1	1	\N
6713	111	\N	5	1	\N	1	1	\N
6714	111	\N	5	1	\N	1	1	\N
6715	111	\N	5	1	\N	1	1	\N
6716	111	\N	5	1	\N	1	1	\N
6717	111	\N	5	1	\N	1	1	\N
6718	111	\N	5	1	\N	1	1	\N
6719	111	\N	5	1	\N	1	1	\N
6720	111	\N	5	1	\N	1	1	\N
6721	111	\N	5	1	\N	1	1	\N
6722	111	\N	5	1	\N	1	1	\N
6778	112	\N	5	1	\N	1	1	\N
6779	112	\N	5	1	\N	1	1	\N
6780	112	\N	5	1	\N	1	1	\N
6781	112	\N	5	1	\N	1	1	\N
6782	112	\N	5	1	\N	1	1	\N
6783	112	\N	5	1	\N	1	1	\N
6774	112	\N	5	1	\N	1	1	\N
6776	112	\N	5	1	\N	1	1	\N
6777	112	\N	5	1	\N	1	1	\N
6840	113	\N	5	1	\N	1	1	\N
6841	113	\N	5	1	\N	1	1	\N
6842	113	\N	5	1	\N	1	1	\N
6843	113	\N	5	1	\N	1	1	\N
6834	113	\N	5	1	\N	1	1	\N
6835	113	\N	5	1	\N	1	1	\N
6836	113	\N	5	1	\N	1	1	\N
6837	113	\N	5	1	\N	1	1	\N
6838	113	\N	5	1	\N	1	1	\N
6839	113	\N	5	1	\N	1	1	\N
6893	114	\N	5	1	\N	1	1	\N
6894	114	\N	5	1	\N	1	1	\N
6895	114	\N	5	1	\N	1	1	\N
6896	114	\N	5	1	\N	1	1	\N
6897	114	\N	5	1	\N	1	1	\N
6898	114	\N	5	1	\N	1	1	\N
6899	114	\N	5	1	\N	1	1	\N
6900	114	\N	5	1	\N	1	1	\N
6901	114	\N	5	1	\N	1	1	\N
6902	114	\N	5	1	\N	1	1	\N
6928	115	\N	5	1	\N	1	1	\N
6930	115	\N	5	1	\N	1	1	\N
6931	115	\N	5	1	\N	1	1	\N
6932	115	\N	5	1	\N	1	1	\N
6929	115	\N	5	1	\N	1	1	\N
6963	116	\N	5	1	\N	1	1	\N
6964	116	\N	5	1	\N	1	1	\N
6965	116	\N	5	1	\N	1	1	\N
6966	116	\N	5	1	\N	1	1	\N
6967	116	\N	5	1	\N	1	1	\N
6670	110	\N	5	1	\N	1	1	\N
6671	110	\N	5	1	\N	1	1	\N
6672	110	\N	5	1	\N	1	1	\N
6665	110	\N	5	1	\N	1	1	\N
6666	110	\N	5	1	\N	1	1	\N
6667	110	\N	5	1	\N	1	1	\N
6668	110	\N	5	1	\N	1	1	\N
6669	110	\N	5	1	\N	1	1	\N
6723	111	\N	5	1	\N	1	1	\N
6724	111	\N	5	1	\N	1	1	\N
6725	111	\N	5	1	\N	1	1	\N
6726	111	\N	5	1	\N	1	1	\N
6727	111	\N	5	1	\N	1	1	\N
6728	111	\N	5	1	\N	1	1	\N
6729	111	\N	5	1	\N	1	1	\N
6730	111	\N	5	1	\N	1	1	\N
6784	112	\N	5	1	\N	1	1	\N
6785	112	\N	5	1	\N	1	1	\N
6786	112	\N	5	1	\N	1	1	\N
6787	112	\N	5	1	\N	1	1	\N
6788	112	\N	5	1	\N	1	1	\N
6789	112	\N	5	1	\N	1	1	\N
6790	112	\N	5	1	\N	1	1	\N
6791	112	\N	5	1	\N	1	1	\N
6850	113	\N	5	1	\N	1	1	\N
6851	113	\N	5	1	\N	1	1	\N
6844	113	\N	5	1	\N	1	1	\N
6845	113	\N	5	1	\N	1	1	\N
6846	113	\N	5	1	\N	1	1	\N
6847	113	\N	5	1	\N	1	1	\N
6848	113	\N	5	1	\N	1	1	\N
6849	113	\N	5	1	\N	1	1	\N
6903	114	\N	5	1	\N	1	1	\N
6904	114	\N	5	1	\N	1	1	\N
6905	114	\N	5	1	\N	1	1	\N
6906	114	\N	5	1	\N	1	1	\N
6907	114	\N	5	1	\N	1	1	\N
6908	114	\N	5	1	\N	1	1	\N
6909	114	\N	5	1	\N	1	1	\N
6910	114	\N	5	1	\N	1	1	\N
6933	115	\N	5	1	\N	1	1	\N
6934	115	\N	5	1	\N	1	1	\N
6935	115	\N	5	1	\N	1	1	\N
6936	115	\N	5	1	\N	1	1	\N
6937	115	\N	5	1	\N	1	1	\N
6938	115	\N	5	1	\N	1	1	\N
6939	115	\N	5	1	\N	1	1	\N
6940	115	\N	5	1	\N	1	1	\N
6651	110	\N	5	1	\N	1	1	\N
6652	110	\N	5	1	\N	1	1	\N
6653	110	\N	5	1	\N	1	1	\N
6654	110	\N	5	1	\N	1	1	\N
6710	111	\N	5	1	\N	1	1	\N
6711	111	\N	5	1	\N	1	1	\N
6712	111	\N	5	1	\N	1	1	\N
6731	111	\N	5	1	\N	1	1	\N
6770	112	\N	5	1	\N	1	1	\N
6771	112	\N	5	1	\N	1	1	\N
6772	112	\N	5	1	\N	1	1	\N
6773	112	\N	5	1	\N	1	1	\N
6830	113	\N	5	1	\N	1	1	\N
6831	113	\N	5	1	\N	1	1	\N
6832	113	\N	5	1	\N	1	1	\N
6833	113	\N	5	1	\N	1	1	\N
6890	114	\N	5	1	\N	1	1	\N
6891	114	\N	5	1	\N	1	1	\N
6892	114	\N	5	1	\N	1	1	\N
6911	114	\N	5	1	\N	1	1	\N
6941	115	\N	5	1	\N	1	1	\N
6942	115	\N	5	1	\N	1	1	\N
6984	117	\N	5	1	\N	1	1	\N
6985	117	\N	5	1	\N	1	1	\N
6673	110	\N	5	1	\N	1	1	\N
6674	110	\N	5	1	\N	1	1	\N
6675	110	\N	5	1	\N	1	1	\N
6676	110	\N	5	1	\N	1	1	\N
6677	110	\N	5	1	\N	1	1	\N
6678	110	\N	5	1	\N	1	1	\N
6732	111	\N	5	1	\N	1	1	\N
6733	111	\N	5	1	\N	1	1	\N
6734	111	\N	5	1	\N	1	1	\N
6735	111	\N	5	1	\N	1	1	\N
6736	111	\N	5	1	\N	1	1	\N
6737	111	\N	5	1	\N	1	1	\N
6792	112	\N	5	1	\N	1	1	\N
6793	112	\N	5	1	\N	1	1	\N
6794	112	\N	5	1	\N	1	1	\N
6795	112	\N	5	1	\N	1	1	\N
6796	112	\N	5	1	\N	1	1	\N
6797	112	\N	5	1	\N	1	1	\N
6852	113	\N	5	1	\N	1	1	\N
6853	113	\N	5	1	\N	1	1	\N
6854	113	\N	5	1	\N	1	1	\N
6855	113	\N	5	1	\N	1	1	\N
6856	113	\N	5	1	\N	1	1	\N
6857	113	\N	5	1	\N	1	1	\N
6912	114	\N	5	1	\N	1	1	\N
6913	114	\N	5	1	\N	1	1	\N
6914	114	\N	5	1	\N	1	1	\N
6915	114	\N	5	1	\N	1	1	\N
6916	114	\N	5	1	\N	1	1	\N
6917	114	\N	5	1	\N	1	1	\N
6986	117	\N	5	1	\N	1	1	\N
6987	117	\N	5	1	\N	1	1	\N
6988	117	\N	5	1	\N	1	1	\N
6989	117	\N	5	1	\N	1	1	\N
6990	117	\N	5	1	\N	1	1	\N
6991	117	\N	5	1	\N	1	1	\N
6679	110	\N	5	1	\N	1	1	\N
6680	110	\N	5	1	\N	1	1	\N
6681	110	\N	5	1	\N	1	1	\N
6682	110	\N	5	1	\N	1	1	\N
6738	111	\N	5	1	\N	1	1	\N
6739	111	\N	5	1	\N	1	1	\N
6741	111	\N	5	1	\N	1	1	\N
6798	112	\N	5	1	\N	1	1	\N
6799	112	\N	5	1	\N	1	1	\N
6800	112	\N	5	1	\N	1	1	\N
6801	112	\N	5	1	\N	1	1	\N
6858	113	\N	5	1	\N	1	1	\N
6859	113	\N	5	1	\N	1	1	\N
6860	113	\N	5	1	\N	1	1	\N
6861	113	\N	5	1	\N	1	1	\N
6918	114	\N	5	1	\N	1	1	\N
6919	114	\N	5	1	\N	1	1	\N
6920	114	\N	5	1	\N	1	1	\N
6921	114	\N	5	1	\N	1	1	\N
6968	116	\N	5	1	\N	1	1	\N
6969	116	\N	5	1	\N	1	1	\N
6992	117	\N	5	1	\N	1	1	\N
6993	117	\N	5	1	\N	1	1	\N
6740	111	\N	18	1	\N	1	1	\N
6594	2	\N	5	10	\N	1	8	\N
6417	2	\N	5	10	\N	1	8	\N
6998	21	\N	6	8	\N	\N	\N	\N
6995	21	\N	1	8	\N	1	8	\N
6996	21	\N	1	8	\N	1	8	\N
6595	2	\N	5	10	\N	1	10	\N
6999	86	\N	5	\N	\N	1	1	\N
6416	2	\N	5	10	\N	1	10	\N
7000	86	\N	5	\N	\N	1	1	\N
6997	21	\N	18	8	\N	\N	\N	\N
6605	2	\N	18	10	\N	\N	\N	\N
6606	2	\N	18	10	\N	\N	\N	\N
6607	2	\N	18	10	\N	\N	\N	\N
6608	2	\N	18	10	\N	\N	\N	\N
6609	2	\N	18	10	\N	\N	\N	\N
6611	2	\N	18	10	\N	\N	\N	\N
6599	2	\N	18	10	\N	\N	\N	\N
6600	2	\N	18	10	\N	\N	\N	\N
7001	21	\N	6	8	\N	\N	\N	\N
7010	21	\N	1	8	\N	1	8	\N
7009	21	\N	1	8	\N	1	8	\N
7006	21	\N	1	8	\N	1	8	\N
7007	21	\N	1	8	\N	1	8	\N
7002	21	\N	1	8	\N	1	8	\N
7003	21	\N	1	8	\N	1	8	\N
7011	99	\N	5	\N	\N	1	1	\N
7012	100	\N	5	\N	\N	1	1	\N
7013	32	\N	5	\N	\N	1	1	\N
7014	85	\N	5	\N	\N	1	1	\N
7019	99	\N	5	\N	\N	1	1	\N
7020	100	\N	5	\N	\N	1	1	\N
6586	3	\N	18	10	\N	\N	\N	\N
7021	61	\N	5	\N	\N	1	1	\N
5125	16	\N	19	10	\N	1	10	\N
6612	2	\N	18	10	\N	\N	\N	\N
6602	2	\N	18	10	\N	\N	\N	\N
6603	2	\N	5	10	\N	1	8	\N
6598	2	\N	5	10	\N	1	1	\N
7023	2	\N	18	1	\N	\N	\N	\N
7022	2	\N	18	1	\N	\N	\N	\N
6585	3	\N	18	10	\N	\N	\N	\N
6597	2	\N	5	10	\N	1	8	\N
7025	2	\N	18	1	\N	\N	\N	\N
7027	2	\N	18	1	\N	\N	\N	\N
7026	2	\N	18	1	\N	\N	\N	\N
6324	3	\N	18	10	\N	\N	\N	\N
7028	2	\N	18	1	\N	\N	\N	\N
5219	2	\N	18	1	\N	1	8	\N
6542	16	\N	7	10	\N	1	10	\N
6604	2	\N	1	10	\N	1	8	\N
7052	103	\N	6	9	\N	\N	\N	\N
7053	103	\N	6	9	\N	\N	\N	\N
7054	103	\N	6	9	\N	\N	\N	\N
7055	103	\N	6	9	\N	\N	\N	\N
7056	103	\N	6	9	\N	\N	\N	\N
7057	103	\N	6	9	\N	\N	\N	\N
7058	103	\N	6	9	\N	\N	\N	\N
7059	103	\N	6	9	\N	\N	\N	\N
7060	103	\N	6	9	\N	\N	\N	\N
7061	103	\N	6	9	\N	\N	\N	\N
7062	103	\N	6	9	\N	\N	\N	\N
7063	103	\N	6	9	\N	\N	\N	\N
7064	103	\N	6	9	\N	\N	\N	\N
7065	103	\N	6	9	\N	\N	\N	\N
7066	103	\N	6	9	\N	\N	\N	\N
7067	103	\N	6	9	\N	\N	\N	\N
7068	103	\N	6	9	\N	\N	\N	\N
7069	103	\N	6	9	\N	\N	\N	\N
7070	103	\N	6	9	\N	\N	\N	\N
7071	103	\N	6	9	\N	\N	\N	\N
7072	103	\N	6	9	\N	\N	\N	\N
7073	103	\N	6	9	\N	\N	\N	\N
7074	103	\N	6	9	\N	\N	\N	\N
7075	103	\N	6	9	\N	\N	\N	\N
7076	103	\N	6	9	\N	\N	\N	\N
7077	103	\N	6	9	\N	\N	\N	\N
7078	103	\N	6	9	\N	\N	\N	\N
7079	103	\N	6	9	\N	\N	\N	\N
7080	103	\N	6	9	\N	\N	\N	\N
7081	103	\N	6	9	\N	\N	\N	\N
7082	103	\N	6	9	\N	\N	\N	\N
7083	103	\N	6	9	\N	\N	\N	\N
7084	103	\N	6	9	\N	\N	\N	\N
7085	103	\N	6	9	\N	\N	\N	\N
7086	103	\N	6	9	\N	\N	\N	\N
7087	103	\N	6	9	\N	\N	\N	\N
7088	103	\N	6	9	\N	\N	\N	\N
7089	103	\N	6	9	\N	\N	\N	\N
7090	103	\N	6	9	\N	\N	\N	\N
7091	103	\N	6	9	\N	\N	\N	\N
7092	103	\N	6	9	\N	\N	\N	\N
7093	103	\N	6	9	\N	\N	\N	\N
7094	103	\N	6	9	\N	\N	\N	\N
7095	103	\N	6	9	\N	\N	\N	\N
7096	103	\N	6	9	\N	\N	\N	\N
7097	103	\N	6	9	\N	\N	\N	\N
7098	103	\N	6	9	\N	\N	\N	\N
7099	103	\N	6	9	\N	\N	\N	\N
7100	103	\N	6	9	\N	\N	\N	\N
7101	103	\N	6	9	\N	\N	\N	\N
7102	103	\N	6	9	\N	\N	\N	\N
7103	103	\N	6	9	\N	\N	\N	\N
7104	103	\N	6	9	\N	\N	\N	\N
7105	103	\N	6	9	\N	\N	\N	\N
7106	103	\N	6	9	\N	\N	\N	\N
7107	103	\N	6	9	\N	\N	\N	\N
7108	103	\N	6	9	\N	\N	\N	\N
7109	103	\N	6	9	\N	\N	\N	\N
7110	103	\N	6	9	\N	\N	\N	\N
7111	103	\N	6	9	\N	\N	\N	\N
7112	103	\N	6	9	\N	\N	\N	\N
7113	103	\N	6	9	\N	\N	\N	\N
7114	103	\N	6	9	\N	\N	\N	\N
7115	103	\N	6	9	\N	\N	\N	\N
7116	103	\N	6	9	\N	\N	\N	\N
7117	103	\N	6	9	\N	\N	\N	\N
7118	103	\N	6	9	\N	\N	\N	\N
7119	103	\N	6	9	\N	\N	\N	\N
7120	103	\N	6	9	\N	\N	\N	\N
7121	103	\N	6	9	\N	\N	\N	\N
7122	103	\N	6	9	\N	\N	\N	\N
7123	103	\N	6	9	\N	\N	\N	\N
7124	103	\N	6	9	\N	\N	\N	\N
7125	103	\N	6	9	\N	\N	\N	\N
7126	103	\N	6	9	\N	\N	\N	\N
7127	103	\N	6	9	\N	\N	\N	\N
7128	103	\N	6	9	\N	\N	\N	\N
7129	103	\N	6	9	\N	\N	\N	\N
7130	103	\N	6	9	\N	\N	\N	\N
7131	103	\N	6	9	\N	\N	\N	\N
7132	103	\N	6	9	\N	\N	\N	\N
7133	103	\N	6	9	\N	\N	\N	\N
7134	103	\N	6	9	\N	\N	\N	\N
7135	103	\N	6	9	\N	\N	\N	\N
7136	103	\N	6	9	\N	\N	\N	\N
7137	103	\N	6	9	\N	\N	\N	\N
7138	103	\N	6	9	\N	\N	\N	\N
7139	103	\N	6	9	\N	\N	\N	\N
7140	103	\N	6	9	\N	\N	\N	\N
7141	103	\N	6	9	\N	\N	\N	\N
7142	103	\N	6	9	\N	\N	\N	\N
7143	103	\N	6	9	\N	\N	\N	\N
7144	103	\N	6	9	\N	\N	\N	\N
7145	103	\N	6	9	\N	\N	\N	\N
7146	103	\N	6	9	\N	\N	\N	\N
7147	103	\N	6	9	\N	\N	\N	\N
7148	103	\N	6	9	\N	\N	\N	\N
7149	103	\N	6	9	\N	\N	\N	\N
7150	103	\N	6	9	\N	\N	\N	\N
7151	103	\N	6	9	\N	\N	\N	\N
7152	103	\N	6	9	\N	\N	\N	\N
7153	103	\N	6	9	\N	\N	\N	\N
7154	103	\N	6	9	\N	\N	\N	\N
7155	103	\N	6	9	\N	\N	\N	\N
7156	103	\N	6	9	\N	\N	\N	\N
7157	103	\N	6	9	\N	\N	\N	\N
7158	103	\N	6	9	\N	\N	\N	\N
7159	103	\N	6	9	\N	\N	\N	\N
7160	103	\N	6	9	\N	\N	\N	\N
7161	103	\N	6	9	\N	\N	\N	\N
7162	103	\N	6	9	\N	\N	\N	\N
7163	103	\N	6	9	\N	\N	\N	\N
7164	103	\N	6	9	\N	\N	\N	\N
7165	103	\N	6	9	\N	\N	\N	\N
7166	103	\N	6	9	\N	\N	\N	\N
7167	103	\N	6	9	\N	\N	\N	\N
7168	103	\N	6	9	\N	\N	\N	\N
7169	103	\N	6	9	\N	\N	\N	\N
7170	103	\N	6	9	\N	\N	\N	\N
7171	103	\N	6	9	\N	\N	\N	\N
7172	103	\N	6	9	\N	\N	\N	\N
7173	103	\N	6	9	\N	\N	\N	\N
7174	103	\N	6	9	\N	\N	\N	\N
7175	103	\N	6	9	\N	\N	\N	\N
7176	103	\N	6	9	\N	\N	\N	\N
7177	103	\N	6	9	\N	\N	\N	\N
7178	103	\N	6	9	\N	\N	\N	\N
7179	103	\N	6	9	\N	\N	\N	\N
7180	103	\N	6	9	\N	\N	\N	\N
7181	103	\N	6	9	\N	\N	\N	\N
7182	103	\N	6	9	\N	\N	\N	\N
7183	103	\N	6	9	\N	\N	\N	\N
7184	103	\N	6	9	\N	\N	\N	\N
7185	103	\N	6	9	\N	\N	\N	\N
7186	103	\N	6	9	\N	\N	\N	\N
7187	103	\N	6	9	\N	\N	\N	\N
7188	103	\N	6	9	\N	\N	\N	\N
7189	103	\N	6	9	\N	\N	\N	\N
7190	103	\N	6	9	\N	\N	\N	\N
7191	103	\N	6	9	\N	\N	\N	\N
7192	103	\N	6	9	\N	\N	\N	\N
7193	103	\N	6	9	\N	\N	\N	\N
7194	103	\N	6	9	\N	\N	\N	\N
7195	103	\N	6	9	\N	\N	\N	\N
7196	103	\N	6	9	\N	\N	\N	\N
7197	103	\N	6	9	\N	\N	\N	\N
7198	103	\N	6	9	\N	\N	\N	\N
7199	103	\N	6	9	\N	\N	\N	\N
7200	103	\N	6	9	\N	\N	\N	\N
7201	103	\N	6	9	\N	\N	\N	\N
6457	17	\N	21	10	\N	1	14	\N
6458	17	\N	21	10	\N	1	14	\N
6326	3	\N	21	10	\N	1	14	\N
6325	3	\N	21	10	\N	1	14	\N
7031	2	\N	18	1	\N	\N	\N	\N
7202	87	\N	5	\N	\N	1	1	\N
7030	2	\N	18	1	\N	\N	\N	\N
7029	2	\N	18	1	\N	\N	\N	\N
5133	16	\N	5	10	\N	1	10	\N
5134	16	\N	5	10	\N	1	10	\N
7203	99	\N	5	\N	\N	1	10	\N
7204	100	\N	5	\N	\N	1	10	\N
7205	33	\N	5	\N	\N	1	10	\N
6587	3	\N	18	10	\N	\N	\N	\N
7206	42	\N	5	\N	\N	1	8	\N
6588	3	\N	18	10	\N	\N	\N	\N
7049	2	\N	5	1	\N	1	10	\N
6459	17	\N	21	10	\N	1	8	\N
6460	17	\N	21	10	\N	1	8	\N
6590	3	\N	21	10	\N	1	8	\N
7048	2	\N	21	1	\N	1	8	\N
6589	3	\N	21	10	\N	1	8	\N
6567	16	\N	21	10	\N	1	8	\N
7047	2	\N	21	1	\N	1	8	\N
6560	16	\N	18	10	\N	4	10	\N
6558	16	\N	5	10	\N	1	8	\N
7208	45	\N	5	\N	\N	1	1	\N
7209	76	\N	5	\N	\N	1	1	\N
6461	17	\N	21	10	\N	1	8	\N
6462	17	\N	21	10	\N	1	8	\N
7229	3	\N	21	10	\N	1	8	\N
6546	16	\N	21	10	\N	1	8	\N
7228	3	\N	21	10	\N	1	8	\N
6547	16	\N	21	10	\N	1	8	\N
7250	25	\N	5	\N	\N	1	8	\N
7251	44	\N	5	\N	\N	1	8	\N
7252	30	\N	5	\N	\N	1	8	\N
6463	17	\N	21	10	\N	1	8	\N
6502	17	\N	21	10	\N	1	8	\N
7227	3	\N	21	10	\N	1	8	\N
6562	16	\N	21	10	\N	1	8	\N
7249	2	\N	21	10	\N	1	8	\N
7226	3	\N	21	10	\N	1	8	\N
6561	16	\N	21	10	\N	1	8	\N
7248	2	\N	21	10	\N	1	8	\N
6500	17	\N	21	10	\N	1	8	\N
6501	17	\N	21	10	\N	1	8	\N
7225	3	\N	21	10	\N	1	8	\N
6557	16	\N	21	10	\N	1	8	\N
7253	87	\N	5	\N	\N	1	8	\N
7254	45	\N	5	\N	\N	1	8	\N
6498	17	\N	21	10	\N	1	8	\N
6499	17	\N	21	10	\N	1	8	\N
7224	3	\N	21	10	\N	1	8	\N
3656	16	\N	21	10	\N	1	8	\N
3659	16	\N	21	10	\N	1	8	\N
7255	87	\N	5	\N	\N	1	8	\N
7222	3	\N	21	10	\N	1	8	\N
6549	16	\N	21	10	\N	1	8	\N
7050	2	\N	21	1	\N	1	8	\N
7223	3	\N	21	10	\N	1	8	\N
6550	16	\N	21	10	\N	1	8	\N
7221	3	\N	18	10	\N	1	8	\N
7220	3	\N	18	10	\N	1	8	\N
7218	3	\N	5	10	\N	1	8	\N
7217	3	\N	5	10	\N	1	8	\N
7214	3	\N	21	10	\N	1	8	\N
7215	3	\N	21	10	\N	1	8	\N
6496	17	\N	21	10	\N	1	8	\N
6497	17	\N	21	10	\N	1	8	\N
7213	3	\N	21	10	\N	1	8	\N
7211	3	\N	1	10	\N	1	8	\N
7212	3	\N	21	10	\N	1	8	\N
5132	16	\N	18	10	\N	1	10	\N
6556	16	\N	18	10	\N	1	10	\N
7207	1	\N	7	10	\N	1	10	\N
5131	16	\N	18	10	\N	1	8	\N
7036	2	\N	18	1	\N	\N	\N	\N
7037	2	\N	18	1	\N	\N	\N	\N
7256	42	\N	5	\N	\N	1	8	\N
7247	2	\N	18	10	\N	1	8	\N
7246	2	\N	18	10	\N	1	8	\N
7243	2	\N	18	10	\N	1	8	\N
7038	2	\N	18	1	\N	\N	\N	\N
7244	2	\N	18	10	\N	1	8	\N
6067	2	\N	5	14	\N	1	8	\N
7017	21	\N	5	8	\N	1	8	\N
7245	2	\N	3	10	\N	1	8	\N
2899	16	\N	5	11	\N	1	10	\N
6552	16	\N	18	10	\N	4	10	\N
6554	16	\N	18	10	\N	4	10	\N
7039	2	\N	18	1	\N	\N	\N	\N
7040	2	\N	18	1	\N	\N	\N	\N
7041	2	\N	18	1	\N	\N	\N	\N
7239	2	\N	18	10	\N	1	8	\N
7240	2	\N	18	10	\N	1	8	\N
7257	96	\N	5	\N	\N	1	8	\N
7258	61	\N	5	\N	\N	1	8	\N
7259	99	\N	5	\N	\N	1	8	\N
7260	96	\N	5	\N	\N	1	8	\N
7261	61	\N	5	\N	\N	1	8	\N
7262	99	\N	5	\N	\N	1	8	\N
7263	40	\N	5	\N	\N	1	8	\N
7264	96	\N	5	\N	\N	1	8	\N
7265	61	\N	5	\N	\N	1	8	\N
7266	40	\N	5	\N	\N	1	8	\N
7267	99	\N	5	\N	\N	1	8	\N
6548	16	\N	18	10	\N	1	8	\N
7268	32	\N	5	\N	\N	1	8	\N
6563	16	\N	5	10	\N	1	8	\N
6564	16	\N	5	10	\N	1	8	\N
6565	16	\N	5	10	\N	1	8	\N
6566	16	\N	5	10	\N	1	8	\N
7042	2	\N	18	1	\N	\N	\N	\N
7231	2	\N	18	10	\N	1	8	\N
7033	2	\N	5	1	\N	1	8	\N
7230	2	\N	5	10	\N	1	8	\N
6494	17	\N	21	10	\N	1	8	\N
6495	17	\N	21	10	\N	1	8	\N
6529	16	\N	21	10	\N	1	8	\N
7233	2	\N	21	10	\N	1	8	\N
6530	16	\N	21	10	\N	1	8	\N
7232	2	\N	21	10	\N	1	8	\N
7269	25	\N	5	\N	\N	1	8	\N
7270	37	\N	5	\N	\N	1	8	\N
7271	38	\N	5	\N	\N	1	8	\N
7272	26	\N	5	\N	\N	1	8	\N
7234	2	\N	18	10	\N	1	8	\N
7273	99	\N	5	\N	\N	1	8	\N
7274	85	\N	5	\N	\N	1	8	\N
7005	21	\N	5	8	\N	1	8	\N
7275	71	\N	5	\N	\N	1	8	\N
7276	95	\N	5	\N	\N	1	8	\N
7277	99	\N	5	\N	\N	1	8	\N
7235	2	\N	5	10	\N	1	8	\N
6592	3	\N	18	10	\N	\N	\N	\N
7044	2	\N	18	1	\N	\N	\N	\N
7045	2	\N	18	1	\N	\N	\N	\N
7046	2	\N	18	1	\N	\N	\N	\N
6413	2	\N	18	10	\N	\N	\N	\N
7296	2	\N	6	99999	\N	\N	\N	\N
6412	2	\N	18	10	\N	\N	\N	\N
7310	3	\N	5	8	\N	1	8	\N
7312	3	\N	5	8	\N	1	8	\N
5135	16	\N	5	10	\N	1	8	\N
5136	16	\N	5	10	\N	1	8	\N
7319	25	\N	5	\N	\N	1	8	\N
7320	99	\N	5	\N	\N	1	8	\N
7321	26	\N	5	\N	\N	1	8	\N
7015	21	\N	5	8	\N	1	8	\N
7322	99	\N	5	\N	\N	1	8	\N
7323	76	\N	5	\N	\N	1	10	\N
7324	42	\N	5	\N	\N	1	8	\N
7325	49	\N	5	\N	\N	1	8	\N
7326	71	\N	5	\N	\N	1	8	\N
7327	95	\N	5	\N	\N	1	8	\N
7328	99	\N	5	\N	\N	1	8	\N
7329	71	\N	5	\N	\N	1	8	\N
7330	95	\N	5	\N	\N	1	8	\N
7331	99	\N	5	\N	\N	1	8	\N
7332	40	\N	5	\N	\N	1	8	\N
7333	71	\N	5	\N	\N	1	8	\N
7334	40	\N	5	\N	\N	1	8	\N
7335	95	\N	5	\N	\N	1	8	\N
7336	99	\N	5	\N	\N	1	8	\N
7337	25	\N	5	\N	\N	1	1	\N
7338	37	\N	5	\N	\N	1	1	\N
7339	30	\N	5	\N	\N	1	1	\N
7340	26	\N	5	\N	\N	1	1	\N
7343	16	\N	6	10	\N	\N	\N	\N
7344	16	\N	6	10	\N	\N	\N	\N
7345	16	\N	6	10	\N	\N	\N	\N
7359	16	\N	6	10	\N	\N	\N	\N
7360	16	\N	6	10	\N	\N	\N	\N
7361	16	\N	6	10	\N	\N	\N	\N
7362	16	\N	6	10	\N	\N	\N	\N
7363	16	\N	6	10	\N	\N	\N	\N
7364	16	\N	6	10	\N	\N	\N	\N
7376	16	\N	6	10	\N	\N	\N	\N
7390	16	\N	6	10	\N	\N	\N	\N
7391	16	\N	6	10	\N	\N	\N	\N
7392	16	\N	6	10	\N	\N	\N	\N
7393	16	\N	6	10	\N	\N	\N	\N
7394	16	\N	6	10	\N	\N	\N	\N
7395	16	\N	6	10	\N	\N	\N	\N
7396	16	\N	6	10	\N	\N	\N	\N
7397	16	\N	6	10	\N	\N	\N	\N
7398	16	\N	6	10	\N	\N	\N	\N
7399	16	\N	6	10	\N	\N	\N	\N
7400	16	\N	6	10	\N	\N	\N	\N
7401	16	\N	6	10	\N	\N	\N	\N
7402	16	\N	6	10	\N	\N	\N	\N
7403	16	\N	6	10	\N	\N	\N	\N
7404	16	\N	6	10	\N	\N	\N	\N
7405	16	\N	6	10	\N	\N	\N	\N
7409	16	\N	6	10	\N	\N	\N	\N
7414	16	\N	6	10	\N	\N	\N	\N
7417	16	\N	6	10	\N	\N	\N	\N
7419	16	\N	6	10	\N	\N	\N	\N
7420	16	\N	6	10	\N	\N	\N	\N
7421	16	\N	6	10	\N	\N	\N	\N
7422	16	\N	6	10	\N	\N	\N	\N
7423	16	\N	6	10	\N	\N	\N	\N
7424	16	\N	6	10	\N	\N	\N	\N
7425	16	\N	6	10	\N	\N	\N	\N
7426	16	\N	6	10	\N	\N	\N	\N
7427	16	\N	6	10	\N	\N	\N	\N
7428	16	\N	6	10	\N	\N	\N	\N
7429	16	\N	6	10	\N	\N	\N	\N
7430	16	\N	6	10	\N	\N	\N	\N
7431	16	\N	6	10	\N	\N	\N	\N
7432	16	\N	6	10	\N	\N	\N	\N
7433	16	\N	6	10	\N	\N	\N	\N
7434	16	\N	6	10	\N	\N	\N	\N
7435	16	\N	6	10	\N	\N	\N	\N
7436	16	\N	6	10	\N	\N	\N	\N
7437	16	\N	6	10	\N	\N	\N	\N
7438	16	\N	6	10	\N	\N	\N	\N
7439	16	\N	6	10	\N	\N	\N	\N
7440	16	\N	6	10	\N	\N	\N	\N
7441	16	\N	6	10	\N	\N	\N	\N
7442	16	\N	6	10	\N	\N	\N	\N
7443	16	\N	6	10	\N	\N	\N	\N
7444	16	\N	6	10	\N	\N	\N	\N
7445	16	\N	6	10	\N	\N	\N	\N
7446	16	\N	6	10	\N	\N	\N	\N
7447	16	\N	6	10	\N	\N	\N	\N
7448	16	\N	6	10	\N	\N	\N	\N
7449	16	\N	6	10	\N	\N	\N	\N
7450	16	\N	6	10	\N	\N	\N	\N
7451	16	\N	6	10	\N	\N	\N	\N
7452	16	\N	6	10	\N	\N	\N	\N
7453	16	\N	6	10	\N	\N	\N	\N
7454	16	\N	6	10	\N	\N	\N	\N
7455	16	\N	6	10	\N	\N	\N	\N
7456	16	\N	6	10	\N	\N	\N	\N
7457	16	\N	6	10	\N	\N	\N	\N
7458	16	\N	6	10	\N	\N	\N	\N
7459	16	\N	6	10	\N	\N	\N	\N
7460	16	\N	6	10	\N	\N	\N	\N
7461	16	\N	6	10	\N	\N	\N	\N
7462	16	\N	6	10	\N	\N	\N	\N
7463	16	\N	6	10	\N	\N	\N	\N
7464	16	\N	6	10	\N	\N	\N	\N
7465	16	\N	6	10	\N	\N	\N	\N
7466	16	\N	6	10	\N	\N	\N	\N
7467	16	\N	6	10	\N	\N	\N	\N
7468	16	\N	6	10	\N	\N	\N	\N
7469	16	\N	6	10	\N	\N	\N	\N
7470	16	\N	6	10	\N	\N	\N	\N
6492	17	\N	21	10	\N	1	8	\N
6493	17	\N	21	10	\N	1	8	\N
7471	84	\N	5	\N	\N	1	8	\N
7472	29	\N	5	\N	\N	1	8	\N
7473	26	\N	5	\N	\N	1	8	\N
7474	36	\N	5	\N	\N	1	8	\N
7475	29	\N	5	\N	\N	1	8	\N
7476	84	\N	5	\N	\N	1	8	\N
7477	36	\N	5	\N	\N	1	8	\N
7478	26	\N	5	\N	\N	1	8	\N
7479	84	\N	5	\N	\N	1	10	\N
7480	25	\N	5	\N	\N	1	10	\N
7481	26	\N	5	\N	\N	1	10	\N
7482	36	\N	5	\N	\N	1	10	\N
7237	2	\N	5	10	\N	1	8	\N
7314	2	\N	5	8	\N	1	8	\N
6490	17	\N	21	10	\N	1	8	\N
6491	17	\N	21	10	\N	1	8	\N
7308	3	\N	21	8	\N	1	8	\N
3609	16	\N	21	10	\N	1	8	\N
7309	3	\N	21	8	\N	1	8	\N
3980	16	\N	21	10	\N	1	8	\N
7288	2	\N	18	99999	\N	\N	\N	\N
7483	40	\N	5	\N	\N	1	10	\N
7487	40	\N	5	\N	\N	1	10	\N
7484	61	\N	5	\N	\N	1	10	\N
7485	99	\N	5	\N	\N	1	10	\N
7486	96	\N	5	\N	\N	1	10	\N
7307	3	\N	21	99999	\N	1	8	\N
7318	3	\N	5	8	\N	1	10	\N
7488	84	\N	5	\N	\N	1	10	\N
7489	25	\N	5	\N	\N	1	10	\N
6539	16	\N	5	10	\N	1	10	\N
7490	26	\N	5	\N	\N	1	10	\N
7491	36	\N	5	\N	\N	1	10	\N
7492	71	\N	5	\N	\N	1	8	\N
7493	95	\N	5	\N	\N	1	8	\N
7494	99	\N	5	\N	\N	1	8	\N
7495	87	\N	5	\N	\N	1	8	\N
7353	16	\N	5	10	\N	1	8	\N
7306	3	\N	1	99999	\N	1	8	\N
7289	2	\N	1	99999	\N	1	8	\N
7348	16	\N	1	10	\N	1	8	\N
7305	3	\N	21	99999	\N	1	8	\N
7349	16	\N	21	10	\N	1	8	\N
7290	2	\N	21	99999	\N	1	8	\N
7496	71	\N	5	\N	\N	1	8	\N
7497	95	\N	5	\N	\N	1	8	\N
7498	99	\N	5	\N	\N	1	8	\N
6775	112	\N	5	1	\N	1	10	\N
7499	61	\N	5	\N	\N	1	10	\N
7500	25	\N	5	\N	\N	1	10	\N
7501	37	\N	5	\N	\N	1	10	\N
7502	99	\N	5	\N	\N	1	10	\N
7503	30	\N	5	\N	\N	1	10	\N
7504	38	\N	5	\N	\N	1	10	\N
7505	100	\N	5	\N	\N	1	10	\N
2905	16	\N	18	11	\N	1	1	\N
6531	16	\N	18	10	\N	1	8	\N
7347	16	\N	21	10	\N	1	8	\N
7342	16	\N	21	10	\N	1	8	\N
6503	17	\N	21	10	\N	1	8	\N
7510	18	\N	5	8	\N	1	8	\N
6504	17	\N	21	10	\N	1	8	\N
6505	17	\N	21	10	\N	1	8	\N
7511	25	\N	5	\N	\N	1	1	\N
7512	37	\N	5	\N	\N	1	1	\N
7513	30	\N	5	\N	\N	1	1	\N
6541	16	\N	6	10	\N	1	8	\N
6551	16	\N	18	10	\N	1	8	\N
6553	16	\N	18	10	\N	1	8	\N
7236	2	\N	1	10	\N	1	8	\N
7311	3	\N	5	8	\N	1	8	\N
7514	87	\N	5	\N	\N	1	8	\N
7515	30	\N	5	\N	\N	1	8	\N
7516	30	\N	5	\N	\N	1	1	\N
7517	30	\N	5	\N	\N	1	8	\N
4275	2	\N	18	1	\N	1	1	\N
7518	61	\N	5	\N	\N	1	8	\N
7519	96	\N	5	\N	\N	1	8	\N
7520	99	\N	5	\N	\N	1	8	\N
7509	18	\N	5	8	\N	1	8	\N
7521	25	\N	5	\N	\N	1	8	\N
7522	36	\N	5	\N	\N	1	8	\N
7523	61	\N	5	\N	\N	1	8	\N
7524	30	\N	5	\N	\N	1	8	\N
7525	96	\N	5	\N	\N	1	8	\N
7526	99	\N	5	\N	\N	1	8	\N
7527	26	\N	5	\N	\N	1	8	\N
7538	25	\N	5	\N	\N	1	8	\N
7539	85	\N	5	\N	\N	1	8	\N
7540	26	\N	5	\N	\N	1	8	\N
7541	37	\N	5	\N	\N	1	8	\N
7542	87	\N	5	\N	\N	1	8	\N
6327	3	\N	18	10	\N	1	8	\N
6536	16	\N	18	10	\N	1	8	\N
7291	2	\N	5	99999	\N	1	8	\N
7553	61	\N	5	\N	\N	1	8	\N
7554	96	\N	5	\N	\N	1	8	\N
7555	99	\N	5	\N	\N	1	8	\N
7315	2	\N	5	8	\N	1	8	\N
3667	16	\N	5	10	\N	1	8	\N
6535	16	\N	1	10	\N	1	8	\N
6538	16	\N	1	10	\N	1	8	\N
6540	16	\N	1	10	\N	1	8	\N
6506	17	\N	21	10	\N	1	8	\N
6507	17	\N	21	10	\N	1	8	\N
589	3	\N	21	8	2006-08-10	1	8	\N
7551	3	\N	21	10	\N	1	8	\N
7292	2	\N	21	99999	\N	1	8	\N
6508	17	\N	21	10	\N	1	8	\N
6509	17	\N	21	10	\N	1	8	\N
7350	16	\N	21	10	\N	1	8	\N
7550	3	\N	21	10	\N	1	8	\N
7293	2	\N	21	99999	\N	1	8	\N
7508	18	\N	5	8	\N	1	8	\N
7507	18	\N	5	8	\N	1	8	\N
7556	84	\N	5	\N	\N	1	8	\N
7557	61	\N	5	\N	\N	1	8	\N
7558	87	\N	5	\N	\N	1	8	\N
7286	2	\N	1	99999	\N	1	8	\N
7536	2	\N	18	10	\N	\N	\N	\N
7238	2	\N	5	10	\N	1	8	\N
7549	3	\N	18	10	\N	\N	\N	\N
7559	71	\N	5	\N	\N	1	8	\N
7560	95	\N	5	\N	\N	1	8	\N
7561	99	\N	5	\N	\N	1	8	\N
6610	2	\N	1	10	\N	1	8	\N
7529	2	\N	18	10	\N	\N	\N	\N
7530	2	\N	18	10	\N	\N	\N	\N
7018	21	\N	5	8	\N	1	1	\N
7562	99	\N	5	\N	\N	1	1	\N
7563	100	\N	5	\N	\N	1	1	\N
7043	2	\N	5	1	\N	1	8	\N
7284	2	\N	5	99999	\N	1	8	\N
7564	99	\N	5	\N	\N	1	99999	\N
151	2	\N	5	1	2006-10-11	1	1	\N
7301	3	\N	18	99999	\N	\N	\N	\N
7302	3	\N	18	99999	\N	\N	\N	\N
7411	16	\N	18	10	\N	\N	\N	\N
7546	3	\N	5	10	\N	1	8	\N
7528	2	\N	5	10	\N	1	8	\N
7300	3	\N	18	99999	\N	\N	\N	\N
6401	2	\N	18	10	\N	1	8	\N
7582	2	\N	18	8	\N	\N	\N	\N
7584	2	\N	5	8	\N	1	8	\N
7565	3	\N	18	8	\N	\N	\N	\N
7583	2	\N	18	8	\N	\N	\N	\N
7580	2	\N	18	8	\N	\N	\N	\N
7531	2	\N	18	10	\N	\N	\N	\N
7585	87	\N	5	\N	\N	1	10	\N
7282	2	\N	5	99999	\N	1	8	\N
7313	2	\N	5	8	\N	1	8	\N
6601	2	\N	5	10	\N	1	8	\N
130	2	\N	5	\N	\N	1	8	\N
7537	2	\N	5	10	\N	1	8	\N
7278	2	\N	18	99999	\N	\N	\N	\N
7586	99	\N	5	\N	\N	2	1	\N
7299	3	\N	21	99999	\N	1	10	\N
7369	16	\N	21	10	\N	1	10	\N
7572	2	\N	21	8	\N	1	10	\N
7573	2	\N	21	8	\N	1	10	\N
7346	16	\N	18	10	\N	1	10	\N
7587	25	\N	5	\N	\N	1	8	\N
7588	30	\N	5	\N	\N	1	8	\N
7589	25	\N	5	\N	\N	1	10	\N
7590	37	\N	5	\N	\N	1	10	\N
7591	30	\N	5	\N	\N	1	10	\N
7592	26	\N	5	\N	\N	1	10	\N
5019	18	\N	5	10	\N	1	10	\N
7593	36	\N	5	\N	\N	1	8	\N
7594	71	\N	5	\N	\N	1	8	\N
7595	36	\N	5	\N	\N	1	8	\N
7596	61	\N	5	\N	\N	1	8	\N
3614	16	\N	6	10	\N	4	10	\N
7597	99	\N	5	\N	\N	1	10000	\N
7599	99	\N	5	\N	\N	1	10	\N
7379	16	\N	5	10	\N	1	10000	\N
7598	99	\N	5	\N	\N	1	10000	\N
7600	41	\N	5	\N	\N	1	10000	\N
3628	16	\N	5	10	\N	1	1	\N
7579	2	\N	5	8	\N	1	8	\N
7285	2	\N	5	99999	\N	1	8	\N
7578	2	\N	5	8	\N	1	8	\N
6962	116	\N	18	1	\N	1	1	\N
7603	117	\N	6	99999	\N	\N	\N	\N
7601	39	\N	5	\N	\N	1	99999	\N
7602	63	\N	5	\N	\N	1	99999	\N
7577	2	\N	18	8	\N	\N	\N	\N
7283	2	\N	5	99999	\N	1	8	\N
2900	16	\N	1	11	\N	1	8	\N
3090	16	\N	5	1	\N	1	10	\N
7576	2	\N	18	8	\N	\N	\N	\N
7614	99	\N	5	\N	\N	1	1	\N
7615	99	\N	5	\N	\N	1	1	\N
7621	100	\N	5	\N	\N	1	8	\N
7617	71	\N	5	\N	\N	1	8	\N
7616	61	\N	5	\N	\N	1	8	\N
7620	99	\N	5	\N	\N	1	8	\N
7618	95	\N	5	\N	\N	1	8	\N
7619	96	\N	5	\N	\N	1	8	\N
7622	84	\N	5	\N	\N	1	1	\N
7279	2	\N	1	99999	\N	1	8	\N
7532	2	\N	5	10	\N	1	8	\N
7280	2	\N	5	99999	\N	1	8	\N
7534	2	\N	5	10	\N	1	8	\N
7605	2	\N	5	10	\N	1	8	\N
7581	2	\N	5	8	\N	1	8	\N
6510	17	\N	21	10	\N	1	8	\N
6511	17	\N	21	10	\N	1	8	\N
7410	16	\N	21	10	\N	1	8	\N
7408	16	\N	21	10	\N	1	8	\N
5020	18	\N	5	10	\N	1	1	\N
7623	87	\N	5	\N	\N	1	1	\N
7370	16	\N	18	10	\N	\N	\N	\N
7545	3	\N	18	10	\N	\N	\N	\N
7608	2	\N	18	10	\N	\N	\N	\N
7544	3	\N	18	10	\N	\N	\N	\N
7629	49	\N	5	\N	\N	1	1	\N
7633	49	\N	5	\N	\N	1	1	\N
7630	47	\N	5	\N	\N	1	1	\N
7631	99	\N	5	\N	\N	1	1	\N
7632	95	\N	5	\N	\N	1	1	\N
7548	3	\N	5	10	\N	1	1	\N
7634	99	\N	5	\N	\N	1	1	\N
7609	2	\N	18	10	\N	\N	\N	\N
7626	3	\N	18	8	\N	\N	\N	\N
454	2	\N	18	1	2006-07-26	1	8	\N
7635	26	\N	5	\N	\N	1	10	\N
7636	37	\N	5	\N	\N	1	10	\N
7637	47	\N	5	\N	\N	1	10	\N
7418	16	\N	18	10	\N	\N	\N	\N
7624	3	\N	18	8	\N	\N	\N	\N
7378	16	\N	18	10	\N	\N	\N	\N
7352	16	\N	5	10	\N	1	8	\N
6593	2	\N	5	10	\N	1	8	\N
7241	2	\N	5	10	\N	1	8	\N
7574	2	\N	5	8	\N	1	8	\N
7575	2	\N	5	8	\N	1	8	\N
7367	16	\N	18	10	\N	1	10	\N
7368	16	\N	18	10	\N	1	10	\N
7374	16	\N	18	10	\N	\N	\N	\N
7375	16	\N	18	10	\N	4	10	\N
7373	16	\N	18	10	\N	4	10	\N
7372	16	\N	18	10	\N	4	10	\N
7371	16	\N	18	10	\N	4	10	\N
6534	16	\N	18	10	\N	4	10	\N
7641	2	\N	18	10	\N	\N	\N	\N
7566	3	\N	18	8	\N	\N	\N	\N
7567	3	\N	18	8	\N	\N	\N	\N
6533	16	\N	18	10	\N	4	10	\N
6532	16	\N	18	10	\N	4	10	\N
6512	17	\N	21	10	\N	1	8	\N
6513	17	\N	21	10	\N	1	8	\N
3985	16	\N	21	10	\N	1	8	\N
7547	3	\N	21	10	\N	1	8	\N
3606	16	\N	21	10	\N	1	8	\N
7665	18	\N	5	8	\N	1	1	\N
7666	117	\N	6	99999	\N	\N	\N	\N
7667	117	\N	6	99999	\N	\N	\N	\N
7668	117	\N	6	99999	\N	\N	\N	\N
7669	117	\N	6	99999	\N	\N	\N	\N
7670	117	\N	6	99999	\N	\N	\N	\N
7671	117	\N	6	99999	\N	\N	\N	\N
7672	117	\N	6	99999	\N	\N	\N	\N
7673	117	\N	6	99999	\N	\N	\N	\N
7674	117	\N	6	99999	\N	\N	\N	\N
7675	117	\N	6	99999	\N	\N	\N	\N
7676	117	\N	6	99999	\N	\N	\N	\N
7677	117	\N	6	99999	\N	\N	\N	\N
7678	117	\N	6	99999	\N	\N	\N	\N
7679	117	\N	6	99999	\N	\N	\N	\N
7680	117	\N	6	99999	\N	\N	\N	\N
7681	117	\N	6	99999	\N	\N	\N	\N
7682	117	\N	6	99999	\N	\N	\N	\N
7683	117	\N	6	99999	\N	\N	\N	\N
7684	117	\N	6	99999	\N	\N	\N	\N
7685	117	\N	6	99999	\N	\N	\N	\N
7686	117	\N	6	99999	\N	\N	\N	\N
7687	117	\N	6	99999	\N	\N	\N	\N
7688	117	\N	6	99999	\N	\N	\N	\N
7689	117	\N	6	99999	\N	\N	\N	\N
7690	117	\N	6	99999	\N	\N	\N	\N
7691	117	\N	6	99999	\N	\N	\N	\N
7692	117	\N	6	99999	\N	\N	\N	\N
7693	117	\N	6	99999	\N	\N	\N	\N
7694	117	\N	6	99999	\N	\N	\N	\N
7695	117	\N	6	99999	\N	\N	\N	\N
7696	117	\N	6	99999	\N	\N	\N	\N
7697	117	\N	6	99999	\N	\N	\N	\N
7698	117	\N	6	99999	\N	\N	\N	\N
7699	117	\N	6	99999	\N	\N	\N	\N
7700	117	\N	6	99999	\N	\N	\N	\N
7701	117	\N	6	99999	\N	\N	\N	\N
7702	117	\N	6	99999	\N	\N	\N	\N
7703	117	\N	6	99999	\N	\N	\N	\N
7704	117	\N	6	99999	\N	\N	\N	\N
7705	117	\N	6	99999	\N	\N	\N	\N
7706	117	\N	6	99999	\N	\N	\N	\N
7707	117	\N	6	99999	\N	\N	\N	\N
7708	117	\N	6	99999	\N	\N	\N	\N
7709	117	\N	6	99999	\N	\N	\N	\N
7710	117	\N	6	99999	\N	\N	\N	\N
7711	117	\N	6	99999	\N	\N	\N	\N
7712	117	\N	6	99999	\N	\N	\N	\N
7713	117	\N	6	99999	\N	\N	\N	\N
7714	117	\N	6	99999	\N	\N	\N	\N
7715	117	\N	6	99999	\N	\N	\N	\N
7716	117	\N	6	99999	\N	\N	\N	\N
7717	117	\N	6	99999	\N	\N	\N	\N
7718	117	\N	6	99999	\N	\N	\N	\N
7719	117	\N	6	99999	\N	\N	\N	\N
7720	117	\N	6	99999	\N	\N	\N	\N
7721	117	\N	6	99999	\N	\N	\N	\N
7722	117	\N	6	99999	\N	\N	\N	\N
7723	117	\N	6	99999	\N	\N	\N	\N
7724	117	\N	6	99999	\N	\N	\N	\N
7725	117	\N	6	99999	\N	\N	\N	\N
7726	117	\N	6	99999	\N	\N	\N	\N
7727	117	\N	6	99999	\N	\N	\N	\N
7728	117	\N	6	99999	\N	\N	\N	\N
7729	117	\N	6	99999	\N	\N	\N	\N
7730	117	\N	6	99999	\N	\N	\N	\N
7731	117	\N	6	99999	\N	\N	\N	\N
7732	117	\N	6	99999	\N	\N	\N	\N
7733	117	\N	6	99999	\N	\N	\N	\N
7734	117	\N	6	99999	\N	\N	\N	\N
7735	117	\N	6	99999	\N	\N	\N	\N
7736	117	\N	6	99999	\N	\N	\N	\N
7737	117	\N	6	99999	\N	\N	\N	\N
7738	117	\N	6	99999	\N	\N	\N	\N
7739	117	\N	6	99999	\N	\N	\N	\N
7740	117	\N	6	99999	\N	\N	\N	\N
7741	117	\N	6	99999	\N	\N	\N	\N
7642	2	\N	18	10	\N	\N	\N	\N
7643	2	\N	18	10	\N	\N	\N	\N
7644	2	\N	18	10	\N	\N	\N	\N
7647	2	\N	18	10	\N	\N	\N	\N
6514	17	\N	21	10	\N	1	8	\N
6515	17	\N	21	10	\N	1	8	\N
6591	3	\N	21	10	\N	1	8	\N
7664	18	\N	5	8	\N	1	10	\N
7742	61	\N	5	\N	\N	1	10	\N
7743	60	\N	5	\N	\N	1	10	\N
7744	76	\N	5	\N	\N	1	1	\N
7646	2	\N	18	10	\N	\N	\N	\N
5127	16	\N	1	10	\N	1	8	\N
6438	17	\N	21	10	\N	1	8	\N
6439	17	\N	21	10	\N	1	8	\N
7543	3	\N	21	10	\N	1	8	\N
3087	16	\N	21	1	\N	1	8	\N
3662	16	\N	21	10	\N	1	8	\N
7298	3	\N	21	99999	\N	1	8	\N
6415	2	\N	21	10	\N	1	8	\N
7663	18	\N	5	8	\N	1	8	\N
7745	87	\N	5	\N	\N	1	8	\N
7746	99	\N	5	\N	\N	1	8	\N
7568	3	\N	18	8	\N	\N	\N	\N
7747	2	\N	6	8	\N	\N	\N	\N
7748	2	\N	6	8	\N	\N	\N	\N
7749	2	\N	6	8	\N	\N	\N	\N
7750	2	\N	6	8	\N	\N	\N	\N
7751	2	\N	6	8	\N	\N	\N	\N
7752	2	\N	6	8	\N	\N	\N	\N
7753	2	\N	6	8	\N	\N	\N	\N
7754	2	\N	6	8	\N	\N	\N	\N
7755	2	\N	6	8	\N	\N	\N	\N
7756	2	\N	6	8	\N	\N	\N	\N
7757	2	\N	6	8	\N	\N	\N	\N
7758	2	\N	6	8	\N	\N	\N	\N
7759	2	\N	6	8	\N	\N	\N	\N
7760	2	\N	6	8	\N	\N	\N	\N
7761	2	\N	6	8	\N	\N	\N	\N
7762	2	\N	6	8	\N	\N	\N	\N
7763	2	\N	6	8	\N	\N	\N	\N
7764	2	\N	6	8	\N	\N	\N	\N
7765	2	\N	6	8	\N	\N	\N	\N
7766	2	\N	6	8	\N	\N	\N	\N
7767	2	\N	6	8	\N	\N	\N	\N
7768	2	\N	6	8	\N	\N	\N	\N
7769	2	\N	6	8	\N	\N	\N	\N
7770	2	\N	6	8	\N	\N	\N	\N
7771	2	\N	6	8	\N	\N	\N	\N
7772	2	\N	6	8	\N	\N	\N	\N
7773	2	\N	6	8	\N	\N	\N	\N
7774	2	\N	6	8	\N	\N	\N	\N
7775	2	\N	6	8	\N	\N	\N	\N
7776	2	\N	6	8	\N	\N	\N	\N
7777	2	\N	6	8	\N	\N	\N	\N
7778	2	\N	6	8	\N	\N	\N	\N
7779	2	\N	6	8	\N	\N	\N	\N
7780	2	\N	6	8	\N	\N	\N	\N
7781	2	\N	6	8	\N	\N	\N	\N
7782	2	\N	6	8	\N	\N	\N	\N
7783	2	\N	6	8	\N	\N	\N	\N
7784	2	\N	6	8	\N	\N	\N	\N
7785	2	\N	6	8	\N	\N	\N	\N
7786	2	\N	6	8	\N	\N	\N	\N
7787	2	\N	6	8	\N	\N	\N	\N
7788	2	\N	6	8	\N	\N	\N	\N
7789	2	\N	6	8	\N	\N	\N	\N
7790	2	\N	6	8	\N	\N	\N	\N
7791	2	\N	6	8	\N	\N	\N	\N
7792	2	\N	6	8	\N	\N	\N	\N
7793	2	\N	6	8	\N	\N	\N	\N
7794	2	\N	6	8	\N	\N	\N	\N
7795	2	\N	6	8	\N	\N	\N	\N
7796	2	\N	6	8	\N	\N	\N	\N
7797	2	\N	6	8	\N	\N	\N	\N
7798	2	\N	6	8	\N	\N	\N	\N
7799	2	\N	6	8	\N	\N	\N	\N
7800	2	\N	6	8	\N	\N	\N	\N
7801	2	\N	6	8	\N	\N	\N	\N
7802	2	\N	6	8	\N	\N	\N	\N
7803	2	\N	6	8	\N	\N	\N	\N
7804	2	\N	6	8	\N	\N	\N	\N
7805	2	\N	6	8	\N	\N	\N	\N
7806	2	\N	6	8	\N	\N	\N	\N
7825	2	\N	18	8	\N	\N	\N	\N
7660	3	\N	18	10	\N	\N	\N	\N
7659	3	\N	18	10	\N	\N	\N	\N
7807	2	\N	18	8	\N	\N	\N	\N
7808	2	\N	18	8	\N	\N	\N	\N
7809	2	\N	18	8	\N	\N	\N	\N
7810	2	\N	18	8	\N	\N	\N	\N
7827	42	\N	5	\N	\N	1	5	\N
7811	2	\N	18	8	\N	\N	\N	\N
7812	2	\N	18	8	\N	\N	\N	\N
7819	2	\N	18	8	\N	\N	\N	\N
7649	2	\N	18	10	\N	\N	\N	\N
7650	2	\N	18	10	\N	\N	\N	\N
7051	2	\N	18	1	\N	\N	\N	\N
7828	99	\N	5	\N	\N	1	8	\N
7829	87	\N	5	\N	\N	1	8	\N
7651	3	\N	18	10	\N	\N	\N	\N
7652	3	\N	18	10	\N	\N	\N	\N
7813	2	\N	18	8	\N	\N	\N	\N
7830	25	\N	5	\N	\N	1	10	\N
3979	16	\N	5	10	\N	1	10	\N
7281	2	\N	18	99999	\N	1	8	\N
7535	2	\N	18	10	\N	1	8	\N
6399	2	\N	18	10	\N	1	10	\N
7287	2	\N	5	99999	\N	1	10	\N
7831	83	\N	5	\N	\N	1	10	\N
6066	2	\N	5	14	\N	1	10	\N
7817	2	\N	5	8	\N	1	10	\N
6065	2	\N	5	14	\N	1	10	\N
7317	2	\N	5	8	\N	1	10	\N
7826	2	\N	5	8	\N	1	8	\N
7034	2	\N	5	1	\N	1	10	\N
7852	76	\N	5	\N	\N	1	10	\N
7853	76	\N	5	\N	\N	1	8	\N
7834	2	\N	5	10	\N	1	8	\N
7835	2	\N	5	10	\N	1	8	\N
7854	58	\N	5	\N	\N	1	8	\N
7855	47	\N	5	\N	\N	1	8	\N
7856	30	\N	5	\N	\N	1	8	\N
7857	99	\N	5	\N	\N	1	8	\N
7035	2	\N	5	1	\N	1	8	\N
7858	61	\N	5	\N	\N	1	10	\N
7859	99	\N	5	\N	\N	1	10	\N
7860	96	\N	5	\N	\N	1	10	\N
7552	3	\N	5	10	\N	1	10	\N
7032	2	\N	5	1	\N	1	8	\N
7610	2	\N	5	10	\N	1	8	\N
7607	2	\N	5	10	\N	1	8	\N
3448	2	\N	5	8	2007-03-15	1	8	\N
7836	2	\N	18	10	\N	\N	\N	\N
7863	41	\N	5	\N	\N	1	10	\N
6440	17	\N	21	10	\N	1	8	\N
6441	17	\N	21	10	\N	1	8	\N
4094	3	\N	21	17	\N	1	8	\N
3680	16	\N	21	10	\N	1	8	\N
3181	2	\N	21	1	2007-01-05	1	8	\N
2814	3	\N	21	10	2006-11-29	1	8	\N
3624	16	\N	21	10	\N	1	8	\N
6129	2	\N	21	10	\N	1	8	\N
6442	17	\N	21	10	\N	1	8	\N
6443	17	\N	21	10	\N	1	8	\N
7658	3	\N	21	10	\N	1	8	\N
7412	16	\N	21	10	\N	1	8	\N
4096	3	\N	21	17	\N	1	8	\N
7413	16	\N	21	10	\N	1	8	\N
7876	18	\N	6	10	\N	\N	\N	\N
7877	18	\N	6	10	\N	\N	\N	\N
6444	17	\N	21	10	\N	1	8	\N
6445	17	\N	21	10	\N	1	8	\N
2825	3	\N	21	10	2006-11-29	1	8	\N
7416	16	\N	21	10	\N	1	8	\N
4638	2	\N	21	1	\N	1	8	\N
7657	3	\N	21	10	\N	1	8	\N
3678	16	\N	21	10	\N	1	8	\N
7837	2	\N	21	10	\N	1	8	\N
6446	17	\N	21	10	\N	1	8	\N
6447	17	\N	21	10	\N	1	8	\N
6356	3	\N	21	8	\N	1	8	\N
7381	16	\N	21	10	\N	1	8	\N
6368	2	\N	21	8	\N	1	8	\N
7304	3	\N	21	99999	\N	1	8	\N
7407	16	\N	21	10	\N	1	8	\N
7864	3	\N	21	10	\N	1	8	\N
4046	16	\N	21	10	\N	1	8	\N
7294	2	\N	21	99999	\N	1	8	\N
6448	17	\N	21	10	\N	1	8	\N
6449	17	\N	21	10	\N	1	8	\N
7303	3	\N	21	99999	\N	1	8	\N
7406	16	\N	21	10	\N	1	8	\N
4833	3	\N	21	10	\N	1	8	\N
3697	16	\N	21	10	\N	1	8	\N
4588	3	\N	21	10	\N	1	8	\N
7382	16	\N	21	10	\N	1	8	\N
934	2	\N	21	8	2006-09-08	1	8	\N
7873	3	\N	21	10	\N	1	8	\N
7380	16	\N	21	10	\N	1	8	\N
7295	2	\N	21	99999	\N	1	8	\N
7894	28	\N	5	\N	\N	1	8	\N
7895	61	\N	5	\N	\N	1	8	\N
7896	25	\N	5	\N	\N	1	8	\N
7897	37	\N	5	\N	\N	1	8	\N
7898	30	\N	5	\N	\N	1	8	\N
7899	28	\N	5	\N	\N	1	8	\N
7900	61	\N	5	\N	\N	1	8	\N
7901	25	\N	5	\N	\N	1	8	\N
7902	37	\N	5	\N	\N	1	8	\N
7903	30	\N	5	\N	\N	1	8	\N
7904	28	\N	5	\N	\N	1	8	\N
7905	61	\N	5	\N	\N	1	8	\N
7906	25	\N	5	\N	\N	1	8	\N
7907	37	\N	5	\N	\N	1	8	\N
7908	30	\N	5	\N	\N	1	8	\N
7909	28	\N	5	\N	\N	1	8	\N
7910	25	\N	5	\N	\N	1	8	\N
7911	37	\N	5	\N	\N	1	8	\N
7912	30	\N	5	\N	\N	1	8	\N
7913	28	\N	5	\N	\N	1	8	\N
7914	25	\N	5	\N	\N	1	8	\N
7915	37	\N	5	\N	\N	1	8	\N
7916	30	\N	5	\N	\N	1	8	\N
7661	18	\N	5	8	\N	1	8	\N
7882	18	\N	5	10	\N	1	8	\N
7662	18	\N	5	8	\N	1	8	\N
7874	18	\N	5	10	\N	1	8	\N
7883	18	\N	5	10	\N	1	8	\N
4139	2	\N	5	8	\N	1	8	\N
973	2	\N	18	8	2006-12-04	\N	\N	\N
7838	2	\N	18	10	\N	\N	\N	\N
4276	2	\N	5	1	\N	1	8	\N
7839	2	\N	18	10	\N	\N	\N	\N
7383	16	\N	18	10	\N	\N	\N	\N
978	2	\N	18	8	2006-09-19	\N	\N	\N
7917	25	\N	5	\N	\N	1	8	\N
7918	30	\N	5	\N	\N	1	8	\N
7847	2	\N	18	10	\N	1	8	\N
7919	99	\N	5	\N	\N	1	8	\N
7920	96	\N	5	\N	\N	1	8	\N
7851	2	\N	5	10	\N	1	8	\N
7816	2	\N	5	8	\N	1	8	\N
7832	2	\N	5	10	\N	1	8	\N
7849	2	\N	5	10	\N	1	8	\N
7921	85	\N	5	\N	\N	1	10	\N
7922	99	\N	5	\N	\N	1	10	\N
7844	2	\N	18	10	\N	\N	\N	\N
6332	2	\N	1	10	\N	1	8	\N
6371	2	\N	18	8	\N	1	1	\N
4771	3	\N	18	1	\N	1	1	\N
7923	42	\N	5	\N	\N	1	10	\N
7638	42	\N	5	\N	\N	1	10	\N
7639	49	\N	5	\N	\N	1	10	\N
7640	99	\N	5	\N	\N	1	10	\N
455	2	\N	18	1	2006-10-11	\N	\N	\N
3178	2	\N	18	1	2007-01-09	1	1	\N
7613	2	\N	18	10	\N	1	8	\N
517	3	\N	18	17	\N	1	1	\N
1399	2	\N	18	1	2006-10-16	\N	\N	\N
547	3	\N	18	17	2006-12-20	\N	\N	\N
7386	16	\N	18	10	\N	\N	\N	\N
7385	16	\N	18	10	\N	\N	\N	\N
2946	16	\N	18	11	2007-01-11	\N	\N	\N
3462	3	\N	18	10	2007-02-16	1	10	\N
494	3	\N	18	17	2006-11-02	\N	\N	\N
7384	16	\N	18	10	\N	\N	\N	\N
7924	42	\N	5	\N	\N	1	10	\N
7925	25	\N	5	\N	\N	1	10	\N
7926	26	\N	5	\N	\N	1	10	\N
928	2	\N	5	8	2007-03-07	1	8	\N
7570	2	\N	5	8	\N	1	8	\N
2890	2	\N	5	1	2006-12-20	1	8	\N
7927	42	\N	5	\N	\N	1	8	\N
7930	42	\N	5	\N	\N	1	8	\N
7931	42	\N	5	\N	\N	1	8	\N
7932	25	\N	5	\N	\N	1	8	\N
7933	99	\N	5	\N	\N	1	8	\N
7934	26	\N	5	\N	\N	1	8	\N
7935	42	\N	5	\N	\N	1	8	\N
7928	25	\N	5	\N	\N	1	8	\N
7929	26	\N	5	\N	\N	1	8	\N
7842	2	\N	1	10	\N	1	8	\N
7645	2	\N	1	10	\N	1	8	\N
7884	2	\N	1	10	\N	1	8	\N
5213	2	\N	5	1	\N	1	8	\N
7865	3	\N	18	10	\N	\N	\N	\N
943	2	\N	19	8	2006-09-04	1	8	\N
7571	2	\N	19	8	\N	1	8	\N
611	2	\N	19	1	2007-03-12	1	8	\N
587	3	\N	19	1	2006-08-10	1	8	\N
7297	2	\N	19	99999	\N	1	8	\N
7822	2	\N	19	8	\N	1	8	\N
6406	2	\N	19	10	\N	1	8	\N
1002	2	\N	19	8	2006-09-18	1	8	\N
4890	2	\N	19	10	\N	1	8	\N
3369	2	\N	19	8	2007-02-15	1	8	\N
2736	2	\N	19	14	\N	1	8	\N
873	2	\N	19	10	2006-09-08	1	8	\N
1390	2	\N	19	8	2006-09-25	1	8	\N
4567	2	\N	19	1	\N	1	8	\N
994	2	\N	19	8	2006-09-08	1	8	\N
5283	2	\N	19	10	\N	1	8	\N
5253	3	\N	18	10	\N	\N	\N	\N
7936	42	\N	5	\N	\N	1	8	\N
7939	42	\N	5	\N	\N	1	8	\N
7937	99	\N	5	\N	\N	1	8	\N
7938	87	\N	5	\N	\N	1	8	\N
3968	16	\N	5	10	\N	1	8	\N
7940	25	\N	5	\N	\N	1	8	\N
7941	99	\N	5	\N	\N	1	8	\N
7891	2	\N	1	10	\N	1	8	\N
7841	2	\N	1	10	\N	1	8	\N
4882	2	\N	19	10	\N	1	8	\N
6450	17	\N	18	10	\N	\N	\N	\N
149	2	\N	5	1	2006-10-11	1	8	\N
6414	2	\N	18	10	\N	\N	\N	\N
4653	2	\N	18	10	\N	1	10	\N
7888	2	\N	5	10	\N	1	8	\N
7890	2	\N	5	10	\N	1	8	\N
7850	2	\N	5	10	\N	1	1	\N
7942	99	\N	5	\N	\N	1	1	\N
7943	70	\N	5	\N	\N	1	1	\N
7944	87	\N	5	\N	\N	1	1	\N
2280	2	\N	18	10	2006-11-29	\N	\N	\N
2691	2	\N	18	10	\N	\N	\N	\N
7628	3	\N	21	8	\N	1	8	\N
7341	16	\N	21	10	\N	1	8	\N
3180	2	\N	21	1	2007-01-09	1	8	\N
7627	3	\N	21	8	\N	1	8	\N
3293	16	\N	21	10	\N	1	8	\N
7612	2	\N	21	10	\N	1	8	\N
7814	2	\N	5	8	\N	1	8	\N
4381	2	\N	5	10	\N	1	8	\N
7892	2	\N	5	10	\N	1	8	\N
7889	2	\N	5	10	\N	1	8	\N
7656	3	\N	18	10	\N	\N	\N	\N
2875	2	\N	5	1	2006-12-20	1	8	\N
4384	2	\N	5	10	\N	1	8	\N
4916	17	\N	21	10	\N	1	8	\N
3831	17	\N	21	10	\N	1	8	\N
1707	3	\N	21	10	2006-10-23	1	8	\N
7840	2	\N	21	10	\N	1	8	\N
3922	3	\N	21	10	2007-03-29	1	8	\N
3923	2	\N	21	10	2007-03-29	1	8	\N
7875	18	\N	5	10	\N	1	1	\N
7846	2	\N	18	10	\N	\N	\N	\N
7845	2	\N	18	10	\N	\N	\N	\N
6516	17	\N	21	10	\N	1	8	\N
6517	17	\N	21	10	\N	1	8	\N
673	3	\N	21	10	2006-08-14	1	8	\N
2991	16	\N	21	11	\N	1	8	\N
7886	2	\N	21	10	\N	1	8	\N
4561	3	\N	21	1	\N	1	8	\N
3077	16	\N	21	1	\N	1	8	\N
7887	2	\N	21	10	\N	1	8	\N
6518	17	\N	21	10	\N	1	8	\N
6519	17	\N	21	10	\N	1	8	\N
4615	3	\N	21	8	\N	1	8	\N
2896	16	\N	21	11	\N	1	8	\N
3507	2	\N	21	8	2007-02-28	1	8	\N
4626	3	\N	21	8	\N	1	8	\N
2895	16	\N	21	11	\N	1	8	\N
7953	2	\N	21	8	\N	1	8	\N
7880	18	\N	5	10	\N	1	8	\N
7881	18	\N	5	10	\N	1	8	\N
7955	25	\N	5	\N	\N	1	8	\N
7956	38	\N	5	\N	\N	1	8	\N
7957	26	\N	5	\N	\N	1	8	\N
7958	36	\N	5	\N	\N	1	8	\N
7959	25	\N	5	\N	\N	1	8	\N
7960	38	\N	5	\N	\N	1	8	\N
7961	26	\N	5	\N	\N	1	8	\N
7962	36	\N	5	\N	\N	1	8	\N
7963	25	\N	5	\N	\N	1	8	\N
7964	26	\N	5	\N	\N	1	8	\N
7965	36	\N	5	\N	\N	1	8	\N
7866	3	\N	18	10	\N	\N	\N	\N
7506	18	\N	5	8	\N	1	8	\N
7966	71	\N	5	\N	\N	1	8	\N
4750	2	\N	18	14	\N	1	10	\N
7366	16	\N	18	10	\N	\N	\N	\N
3449	2	\N	5	8	2007-02-16	1	8	\N
6520	17	\N	21	10	\N	1	8	\N
6521	17	\N	21	10	\N	1	8	\N
7872	3	\N	21	10	\N	1	8	\N
7355	16	\N	21	10	\N	1	8	\N
7818	2	\N	21	8	\N	1	8	\N
7871	3	\N	21	10	\N	1	8	\N
7354	16	\N	21	10	\N	1	8	\N
3904	2	\N	21	14	\N	1	8	\N
7879	18	\N	5	10	\N	1	8	\N
7967	25	\N	5	\N	\N	1	8	\N
7968	99	\N	5	\N	\N	1	8	\N
7969	26	\N	5	\N	\N	1	8	\N
7970	25	\N	5	\N	\N	1	8	\N
7971	99	\N	5	\N	\N	1	8	\N
7972	26	\N	5	\N	\N	1	8	\N
7004	21	\N	5	8	\N	1	8	\N
7973	71	\N	5	\N	\N	1	1	\N
145	2	\N	1	1	2006-08-09	1	8	\N
394	2	\N	1	1	2006-08-28	1	8	\N
4167	2	\N	1	10	\N	1	8	\N
7833	2	\N	1	10	\N	1	8	\N
585	2	\N	1	16	2006-08-10	1	8	\N
4380	2	\N	1	10	\N	1	8	\N
7946	2	\N	1	8	\N	1	8	\N
1003	2	\N	1	8	2006-10-27	1	8	\N
7949	2	\N	1	8	\N	1	8	\N
7952	2	\N	1	8	\N	1	8	\N
7951	2	\N	1	8	\N	1	8	\N
7950	2	\N	1	8	\N	1	8	\N
7954	2	\N	1	8	\N	1	8	\N
7945	2	\N	1	8	\N	1	8	\N
2721	2	\N	1	14	2006-11-10	1	8	\N
3546	2	\N	1	17	2007-02-27	1	8	\N
4240	2	\N	1	8	\N	1	8	\N
1435	2	\N	1	10	2006-11-10	1	8	\N
988	2	\N	1	8	2007-03-12	1	8	\N
7947	2	\N	1	8	\N	1	8	\N
7974	2	\N	1	8	\N	1	8	\N
7975	2	\N	1	8	\N	1	8	\N
7976	2	\N	1	8	\N	1	8	\N
7948	2	\N	1	8	\N	1	8	\N
7387	16	\N	18	10	\N	\N	\N	\N
7388	16	\N	18	10	\N	\N	\N	\N
7984	1	\N	6	8	\N	\N	\N	\N
7985	1	\N	6	8	\N	\N	\N	\N
7986	88	\N	5	\N	\N	1	8	\N
5250	3	\N	18	10	\N	1	1	\N
6114	2	\N	18	10	\N	1	1	\N
6559	16	\N	5	10	\N	1	1	\N
7997	99	\N	5	\N	\N	1	1	\N
4747	2	\N	18	14	\N	1	8	\N
1407	2	\N	18	1	2006-10-26	\N	\N	\N
7977	2	\N	5	8	\N	1	8	\N
7024	2	\N	1	1	\N	1	8	\N
4422	2	\N	1	14	\N	1	8	\N
7316	2	\N	1	8	\N	1	8	\N
7987	2	\N	1	8	\N	1	8	\N
7389	16	\N	18	10	\N	\N	\N	\N
7993	2	\N	6	8	\N	6	8	\N
7862	101	\N	18	\N	\N	1	8	\N
3459	3	\N	18	10	2007-02-14	\N	\N	\N
4272	87	\N	18	\N	\N	1	14	\N
7994	2	\N	5	8	\N	1	8	\N
7870	3	\N	18	10	\N	\N	\N	\N
3093	16	\N	1	1	2007-03-16	1	8	\N
7998	71	\N	5	\N	\N	1	1	\N
7999	99	\N	5	\N	\N	1	1	\N
8000	95	\N	5	\N	\N	1	1	\N
7989	2	\N	5	8	\N	1	8	\N
7982	2	\N	1	8	\N	1	8	\N
4780	2	\N	1	14	\N	1	8	\N
7980	2	\N	1	8	\N	1	8	\N
7981	2	\N	1	8	\N	1	8	\N
7979	2	\N	1	8	\N	1	8	\N
8003	97	\N	5	\N	\N	1	1	\N
8001	61	\N	5	\N	\N	1	1	\N
8002	99	\N	5	\N	\N	1	1	\N
8004	96	\N	5	\N	\N	1	1	\N
7983	2	\N	5	8	\N	1	1	\N
7824	2	\N	18	8	\N	1	8	\N
7653	3	\N	18	10	\N	\N	\N	\N
2272	2	\N	5	10	\N	1	8	\N
7978	2	\N	1	8	\N	1	8	\N
1393	2	\N	1	8	2006-09-25	1	8	\N
733	2	\N	1	8	2006-08-18	1	8	\N
8014	2	\N	1	8	\N	1	8	\N
5268	2	\N	1	10	\N	1	8	\N
7604	2	\N	1	10	\N	1	8	\N
7823	2	\N	1	8	\N	1	8	\N
8013	2	\N	1	8	\N	1	8	\N
7996	2	\N	1	8	\N	1	8	\N
4317	2	\N	1	10	\N	1	8	\N
8012	2	\N	1	8	\N	1	8	\N
8015	87	\N	5	\N	\N	1	1	\N
4261	2	\N	5	8	\N	1	8	\N
8016	71	\N	5	\N	\N	1	1	\N
8017	95	\N	5	\N	\N	1	1	\N
8018	71	\N	5	\N	\N	1	1	\N
8019	99	\N	5	\N	\N	1	1	\N
8020	95	\N	5	\N	\N	1	1	\N
1401	2	\N	18	1	2006-10-16	\N	\N	\N
8008	2	\N	5	8	\N	1	8	\N
3186	2	\N	5	10	2007-01-25	1	8	\N
3026	2	\N	5	1	2007-02-07	1	8	\N
8007	2	\N	5	8	\N	1	8	\N
987	2	\N	18	8	2007-03-14	1	8	\N
7216	3	\N	18	10	\N	1	8	\N
391	2	\N	18	1	2006-08-28	1	8	\N
7815	2	\N	5	8	\N	1	8	\N
424	2	\N	5	1	2006-07-24	1	8	\N
8010	2	\N	5	8	\N	1	8	\N
4868	2	\N	5	10	\N	1	8	\N
8009	2	\N	5	8	\N	1	8	\N
8023	2	\N	1	8	\N	1	8	\N
8011	2	\N	5	8	\N	1	8	\N
8034	2	\N	18	8	\N	\N	\N	\N
8033	2	\N	18	8	\N	\N	\N	\N
8024	2	\N	18	8	\N	\N	\N	\N
8006	2	\N	1	8	\N	1	8	\N
8041	71	\N	5	\N	\N	7	99999	\N
8042	71	\N	5	\N	\N	7	99999	\N
8035	2	\N	18	8	\N	\N	\N	\N
3905	2	\N	5	14	2007-04-02	1	8	\N
157	3	\N	18	1	2006-07-28	\N	\N	\N
980	2	\N	18	8	2006-09-19	\N	\N	\N
7655	3	\N	18	10	\N	\N	\N	\N
7820	2	\N	18	8	\N	\N	\N	\N
8043	71	\N	5	\N	\N	7	99999	\N
8044	1008	\N	6	99999	\N	\N	\N	\N
8027	2	\N	5	8	\N	1	8	\N
8045	71	\N	5	\N	\N	1	8	\N
8046	99	\N	5	\N	\N	1	8	\N
8047	95	\N	5	\N	\N	1	8	\N
8048	71	\N	5	\N	\N	1	8	\N
8049	99	\N	5	\N	\N	1	8	\N
8050	95	\N	5	\N	\N	1	8	\N
8051	99	\N	5	\N	\N	1	8	\N
8036	2	\N	18	8	\N	\N	\N	\N
8052	30	\N	5	\N	\N	1	8	\N
8062	2	\N	18	8	\N	\N	\N	\N
729	3	\N	18	8	2006-08-18	\N	\N	\N
7242	2	\N	5	10	\N	1	8	\N
7219	3	\N	5	10	\N	1	8	\N
4777	2	\N	5	14	\N	1	8	\N
586	3	\N	5	1	2006-08-10	1	8	\N
8028	2	\N	5	8	\N	1	8	\N
8021	2	\N	5	8	\N	1	8	\N
7016	21	\N	5	8	\N	1	1	\N
8029	2	\N	5	8	\N	1	8	\N
883	2	\N	5	10	2006-09-25	1	8	\N
7533	2	\N	5	10	\N	1	8	\N
7611	2	\N	5	10	\N	1	8	\N
7843	2	\N	18	10	\N	1	8	\N
8005	2	\N	5	8	\N	1	8	\N
2701	2	\N	5	10	2006-11-28	1	8	\N
4574	2	\N	5	1	\N	1	8	\N
2705	2	\N	18	10	2006-11-27	\N	\N	\N
7654	3	\N	18	10	\N	\N	\N	\N
5234	2	\N	18	1	\N	1	10	\N
4110	3	\N	18	10	\N	1	8	\N
5229	2	\N	18	1	\N	1	1	\N
7356	16	\N	18	10	\N	\N	\N	\N
7377	16	\N	18	10	\N	1	10	\N
8063	67	\N	5	\N	\N	1	1	\N
7988	2	\N	5	8	\N	1	8	\N
8025	2	\N	5	8	\N	1	8	\N
1004	2	\N	5	8	2006-09-08	1	8	\N
7606	2	\N	5	10	\N	1	8	\N
3984	16	\N	1	10	\N	1	8	\N
8058	2	\N	18	8	\N	\N	\N	\N
1405	2	\N	5	1	2006-09-27	1	8	\N
4221	2	\N	5	10	\N	1	8	\N
7848	2	\N	18	10	\N	1	8	\N
7821	2	\N	18	8	\N	\N	\N	\N
7625	3	\N	5	8	\N	1	1	\N
7861	42	\N	18	\N	\N	1	10	\N
7869	3	\N	18	10	\N	\N	\N	\N
8057	2	\N	18	8	\N	\N	\N	\N
8032	2	\N	5	8	\N	1	8	\N
7415	16	\N	18	10	\N	\N	\N	\N
3512	2	\N	18	8	2007-03-14	1	8	\N
8037	2	\N	5	8	\N	1	8	\N
8056	2	\N	18	8	\N	\N	\N	\N
8031	2	\N	5	8	\N	1	8	\N
859	2	\N	5	10	2006-09-18	1	8	\N
4234	2	\N	18	8	\N	1	14	\N
4308	2	\N	5	10	\N	1	8	\N
8054	2	\N	18	8	\N	\N	\N	\N
4472	3	\N	18	10	\N	1	14	\N
3603	16	\N	18	10	\N	\N	\N	\N
6522	17	\N	21	10	\N	1	8	\N
6523	17	\N	21	10	\N	1	8	\N
7868	3	\N	21	10	\N	1	8	\N
7357	16	\N	21	10	\N	1	8	\N
2706	2	\N	21	10	2006-11-27	1	8	\N
7358	16	\N	21	10	\N	1	8	\N
6331	2	\N	21	10	\N	1	8	\N
7878	18	\N	5	10	\N	1	8	\N
8064	25	\N	5	\N	\N	1	8	\N
5263	3	\N	5	10	\N	1	1	\N
8065	99	\N	5	\N	\N	1	1	\N
7885	2	\N	18	10	\N	1	8	\N
5227	2	\N	18	1	\N	1	8	\N
8053	2	\N	18	8	\N	\N	\N	\N
4315	2	\N	5	10	\N	1	8	\N
5271	2	\N	5	10	\N	1	8	\N
8066	71	\N	5	\N	\N	1	1	\N
608	2	\N	5	1	2007-04-05	1	8	\N
8067	87	\N	5	\N	\N	1	8	\N
8068	3	\N	6	8	\N	\N	\N	\N
8070	3	\N	5	8	\N	1	8	\N
8069	3	\N	18	8	\N	\N	\N	\N
6596	2	\N	5	10	\N	1	8	\N
8022	2	\N	5	8	\N	1	8	\N
7867	3	\N	5	10	\N	1	8	\N
7351	16	\N	5	10	\N	1	8	\N
6537	16	\N	5	10	\N	1	8	\N
8072	2	\N	6	8	\N	\N	\N	\N
8073	2	\N	6	8	\N	\N	\N	\N
8074	2	\N	6	8	\N	\N	\N	\N
8075	2	\N	6	8	\N	\N	\N	\N
8076	2	\N	6	8	\N	\N	\N	\N
8077	2	\N	6	8	\N	\N	\N	\N
8078	2	\N	6	8	\N	\N	\N	\N
7569	3	\N	18	8	\N	\N	\N	\N
6994	21	\N	18	8	\N	1	10	\N
7992	2	\N	5	8	\N	1	8	\N
8079	2	\N	18	8	\N	\N	\N	\N
7991	2	\N	5	8	\N	1	8	\N
3713	16	\N	18	10	\N	4	8	\N
4354	2	\N	18	10	\N	1	8	\N
7210	3	\N	18	10	\N	1	8	\N
8081	87	\N	5	\N	\N	1	8	\N
8083	99	\N	5	\N	\N	1	8	\N
8082	87	\N	5	\N	\N	1	8	\N
7893	2	\N	5	10	\N	1	8	\N
7008	21	\N	5	8	\N	1	8	\N
8084	42	\N	5	\N	\N	1	8	\N
7995	2	\N	5	8	\N	1	8	\N
2952	16	\N	5	11	2007-01-31	1	8	\N
8026	2	\N	18	8	\N	1	8	\N
511	3	\N	18	17	2007-02-26	\N	\N	\N
7648	2	\N	18	10	\N	1	8	\N
3482	3	\N	18	10	2007-02-26	\N	\N	\N
3416	2	\N	18	10	2007-02-26	\N	\N	\N
7365	16	\N	18	10	\N	\N	\N	\N
3238	16	\N	18	10	2007-02-26	4	8	\N
4642	2	\N	5	1	\N	1	8	\N
1429	2	\N	5	10	2006-10-04	1	8	\N
8030	2	\N	1	8	\N	1	8	\N
375	2	\N	1	1	2006-08-31	1	8	\N
573	2	\N	1	1	2006-07-28	1	8	\N
8061	2	\N	1	8	\N	1	8	\N
3403	2	\N	1	10	2007-01-31	1	8	\N
8038	2	\N	1	8	\N	1	8	\N
8039	2	\N	1	8	\N	1	8	\N
8055	2	\N	1	8	\N	1	8	\N
419	2	\N	1	1	2006-08-28	1	8	\N
8080	2	\N	1	8	\N	1	8	\N
3914	2	\N	1	10	\N	1	8	\N
3931	2	\N	1	10	2007-03-30	1	8	\N
7990	2	\N	1	8	\N	1	8	\N
8059	2	\N	1	8	\N	1	8	\N
395	2	\N	1	1	2006-08-31	1	8	\N
8040	2	\N	1	8	\N	1	8	\N
8060	2	\N	1	8	\N	1	8	\N
8071	2	\N	1	8	\N	1	8	\N
3942	3	\N	18	14	2007-04-06	\N	\N	\N
3418	2	\N	18	10	2007-02-02	1	8	\N
\.


--
-- Data for Name: productsondeliverynote; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY productsondeliverynote (id, productid, deliverynoteid, productname, productquantity, unitprice, inevidence) FROM stdin;
717	\N	313	Tuner BSKE-110A 1000 Kc	1	\N	\N
40	\N	30	Trasformator 230V/18V	2	\N	\N
42	\N	31	Transformator 230V/21V (24V)	2	\N	\N
44	\N	32	Transformator 230V/21V (24V)	2	\N	\N
46	\N	33	Transformator	2	\N	\N
48	\N	34	Transformatoy 230V/21V	2	\N	\N
50	\N	35	Transformator 230V/21V	2	\N	\N
59	\N	40	Transformator 230V/21V	2	\N	\N
72	\N	43	vyrizeni RMA 644	1	\N	\N
73	\N	43	vyrizeni RMA 659	1	\N	\N
86	\N	48	test	1	\N	\N
87	\N	49	test	1	\N	\N
88	\N	50	sadfasdf	1	\N	\N
92	\N	52	Transformator 230V / 21V	2	\N	\N
97	\N	54	Transformator 21V/230V	2	\N	\N
107	\N	58	Transformator 230/21 V	2	\N	\N
120	\N	60	trafo 21V	2	\N	\N
121	\N	61	transformator 21/230	1	\N	\N
124	\N	62	transformator 21V/230V	2	\N	\N
126	\N	63	Transformator 230V/21V	2	\N	\N
127	\N	64	Transformator 21V/230V	1	\N	\N
138	\N	69	transformator 21V/230V	2	\N	\N
140	\N	70	TRANSFORMATOR 21V-230V	2	\N	\N
141	\N	71	Radom pr?m?r 	1	\N	\N
142	\N	72	radom prumer 120 cm	1	\N	\N
143	\N	73	Kabelaz k antene prumer 65cm	1	\N	\N
144	\N	73	Mechanika prumer 65cm	1	\N	\N
145	\N	73	Radom prumer 65cm	1	\N	\N
146	\N	74	kabelaz k parabole 35	2	\N	\N
147	\N	74	parabola prumer 35 cm	2	\N	\N
148	\N	74	radom prumer 35 cm	2	\N	\N
149	\N	75	radom prumer 65	1	\N	\N
720	\N	314	Stínící přípravek	1	\N	\N
151	\N	76	TRANSFORMATOR 21V-230V	2	\N	\N
152	\N	77	Kompletace	1	\N	\N
153	\N	77	Premereni	1	\N	\N
154	\N	77	Testovani	1	\N	\N
155	\N	77	Vymena mechaniky 65 - 35	1	\N	\N
157	\N	77	Vymena mikrovlnne jednotky	2	\N	\N
160	\N	78	TRANSFORMATOR 21V-230V	4	\N	\N
161	\N	79	Velke klestiny	2	\N	\N
162	\N	80	prezkouseni spoje	1	\N	\N
164	\N	80	vymena mechanik z 30 na 60 /stare za nove/	2	\N	\N
165	\N	81	TRANSFORMATOR 21V-230V	2	\N	\N
166	\N	82	radom prumer 120 cm	3	\N	\N
167	\N	83	PREZKOUSENI SPOJE	1	\N	\N
169	\N	83	ZMENA MECHANIKY Z 35 NA 65	2	\N	\N
170	\N	84	antena 65 leva	1	\N	\N
171	\N	84	antena 65 prava	1	\N	\N
172	\N	84	kabelaz k antene 65	2	\N	\N
173	\N	84	radom prumer 65	3	\N	\N
175	\N	85	TRANSFORMATOR 21V-230V	2	\N	\N
177	\N	86	TRANSFORMATOR 21V-230V	2	\N	\N
178	\N	87	Antena prumer 60	1	\N	\N
179	\N	87	Kabelaz na prumer 60	1	\N	\N
180	\N	87	Radom prumer 60	1	\N	\N
182	\N	88	TRANSFORMATOR 21V-230V	2	\N	\N
184	\N	89	TRANSFORMATOR 21V-230V	2	\N	\N
185	\N	90	Kabelaz na prumer 120	1	\N	\N
187	\N	90	TRANSFORMATOR 21V-230V	2	\N	\N
189	\N	91	TRANSFORMATOR 21V-230V	2	\N	\N
191	\N	92	TRANSFORMATOR 21V-230V	2	\N	\N
194	\N	94	TRANSFORMATOR 21V-230V	2	\N	\N
196	\N	95	TRANSFORMATOR 21V-230V	2	\N	\N
197	\N	96	preladeni spoje	1	\N	\N
198	\N	96	prezkouseni spoje	1	\N	\N
199	\N	96	zmena mechaniky ze 120cm do 60cm	1	\N	\N
200	\N	96	zmena mechaniky z 60 cm na 30 cm	1	\N	\N
203	\N	97	TRANSFORMATOR 21V-230V	2	\N	\N
205	\N	98	TRANSFORMATOR 21V-230V	2	\N	\N
207	\N	99	TRANSFORMATOR 21V-230V	2	\N	\N
209	\N	100	TRANSFORMATOR 21V-230V	2	\N	\N
211	\N	101	transformator 21v-230v	2	\N	\N
212	\N	102	kabelaz	2	\N	\N
213	\N	102	mechanika 65	2	\N	\N
214	\N	102	radom 65	2	\N	\N
221	\N	104	vymena mechaniky 120 za 65 vcetne radomu a kabelu	1	\N	\N
226	\N	105	TRANSFORMATOR 21V-230V	2	\N	\N
228	\N	106	TRANSFORMATOR 21V-230V	2	\N	\N
230	\N	107	TRANSFORMATOR 21V-230V	2	\N	\N
231	\N	108	Redukce BNC / F	22	\N	\N
233	\N	109	TRANSFORMATOR 21V-230V	2	\N	\N
235	\N	110	TRANSFORMATOR 21V-230V	2	\N	\N
237	\N	111	TRANSFORMATOR 21V-230V	2	\N	\N
240	\N	113	TRANSFORMATOR 21V-230V	2	\N	\N
242	\N	114	transformator 21v-230v	2	\N	\N
244	\N	115	TRANSFORMATOR 21V-230V	2	\N	\N
246	\N	116	TRANSFORMATOR 21V-230V	2	\N	\N
247	\N	117	Velké kleštiny	3	\N	\N
249	\N	118	TRANSFORMATOR 21V-230V	2	\N	\N
250	\N	119	TRANSFORMATOR 21V-230V	2	\N	\N
252	\N	120	TRANSFORMATOR 21V-230V	2	\N	\N
254	\N	121	TRANSFORMATOR 21V-230V	2	\N	\N
256	\N	122	TRANSFORMATOR 21V-230V	2	\N	\N
257	\N	123	Prazdna antena 65 vcetne radomu a vicka	2	\N	\N
259	\N	125	útlumový článek DST 7212 - širokopásmový	2	\N	\N
262	\N	127	TRANSFORMATOR 21V-230V	2	\N	\N
264	\N	128	TRANSFORMATOR 21V-230V	2	\N	\N
265	\N	129	malé kleštiny	1	\N	\N
267	\N	130	TRANSFORMATOR 21V-230V	2	\N	\N
269	\N	131	transformator 21v-230v	2	\N	\N
271	\N	132	TRANSFORMATOR 21V-230V	2	\N	\N
273	\N	133	TRANSFORMATOR 21V-230V	2	\N	\N
274	\N	134	Kabelaz k antene prumer 120cm	2	\N	\N
275	\N	134	Mechanika anteny prumer 120cm	2	\N	\N
276	\N	134	Radom prumer 120cm	2	\N	\N
277	\N	135	Radom prumer 65cm	1	\N	\N
279	\N	136	TRANSFORMATOR 21V-230V	2	\N	\N
280	\N	137	anténa 35, levá	1	\N	\N
281	\N	137	kabeláž 35	1	\N	\N
282	\N	137	radom 35	1	\N	\N
284	\N	138	TRANSFORMATOR 21V-230V	2	\N	\N
286	\N	140	kabeláž 120 cm	1	\N	\N
287	\N	140	kabeláž 60	1	\N	\N
288	\N	140	radom 120 cm	2	\N	\N
289	\N	140	TRANSFORMATOR 21V-230V	2	\N	\N
290	\N	141	TRANSFORMATOR 21V-230V	2	\N	\N
292	\N	142	transformator 21v-230v	2	\N	\N
359	\N	175	TRANSFORMATOR 21V-230V	2	\N	\N
360	\N	176	Stínící přípravek	2	\N	\N
369	\N	179	Stínicí přípravek	3	\N	\N
381	\N	182	nefakturovat!!!!	1	\N	\N
401	\N	198	Spektrální analyzér Unaohm RB23U	1	\N	\N
43	2760	32	\N	1	\N	1
405	\N	200	vráceny starší MJ č.: 3VC31U-2686, 3VC2JF-2213	1	\N	\N
413	\N	202	vráceny starší MJ č.: 1455, 1456	1	\N	\N
423	\N	205	RMA1056 PO ZARUCE, ZAPLATIT JEN 5000 KC	1	\N	\N
425	\N	206	Modem ITX32M 471 31.5. 230	2	\N	\N
435	\N	209	Nefakturovat!!! 	1	\N	\N
444	\N	213	nefakturovat!!! viz dodak c.:214	1	\N	\N
455	\N	215	viz RMA1117	1	\N	\N
457	\N	216	viz RMA1118	1	\N	\N
459	\N	217	viz zap. protokol c.107	1	\N	\N
460	\N	217	viz zap. protokol 108	1	\N	\N
726	\N	318	stinici pripravek	2	\N	\N
470	\N	220	Transformator 24V	2	\N	\N
481	\N	227	nefakturovat!!! viz dodak c.:228	1	\N	\N
497	\N	232	nefakturovat!!! viz. dodak c.:233	1	\N	\N
509	\N	234	nefakturovat!!! viz dodak c.:233	1	\N	\N
513	\N	237	Stinici prvek	2	\N	\N
524	\N	242	fakturovat!!! neopravněná reklamace č.:1125,1127	1	\N	\N
525	\N	242	náhradní díly 600 Kč	1	\N	\N
538	\N	247	stinici krabice	2	\N	\N
539	\N	247	UTLUMOVY CLANEK	2	\N	\N
540	\N	248	Utlumovy clanek DST7212	5	\N	\N
541	\N	249	nefakturovat !!!!	1	\N	\N
554	\N	254	vracena pouzita 65, vymenena za pouzitou 120	1	\N	\N
566	\N	257	nefakturovat!!!!	1	\N	\N
568	\N	258	fakturovat!!!! pozarucni oprava	1	\N	\N
569	\N	258	mikrovlnna jednotka - paroh	1	\N	\N
583	\N	261	stinici pripravek	1	\N	\N
584	\N	262	nefakturovat!!! viz. dodak c. 263	1	\N	\N
591	\N	264	nefakturovat!!! viz dodak c: 265	1	\N	\N
597	\N	266	nefakturovat !!! viz dodak c: 267	1	\N	\N
619	\N	275	STINICI PRIPRAVEK	2	\N	\N
623	\N	277	nefakturovat, jedna se o vyrizeni reklamace	1	\N	\N
730	\N	320	3XB000	2	\N	\N
647	\N	286	viz rma1238 - neopravnena reklamace	1	\N	\N
650	\N	287	viz rma 1241 - neopravnena reklamace	1	\N	\N
687	\N	302	Kompletni antena 65	1	\N	\N
736	\N	325	Nefakturovat viz. dodaci list 326	1	\N	\N
738	\N	326	viz predavaci protokol 325	1	\N	\N
702	\N	304	Stinici prvek	1	\N	\N
752	\N	328	vyroba 50 pristroju Dynablot 300909,50 Kc	1	\N	\N
753	\N	328	zaplacana zaloha 100000Kc	1	\N	\N
767	\N	332	osazeni dle objednavky c. 1101-1103 - 15800 Kc	1	\N	\N
768	\N	333	celkem za osazeni 130942 Kc	1	\N	\N
769	\N	333	cena po sleve 119038 Kc	1	\N	\N
770	\N	333	osazeni dle obj. 096-1103, 117-1103, 120-1103	1	\N	\N
771	\N	333	sleva za opakovani vyroby 10%	1	\N	\N
774	\N	336	Pájení  2000 Kč	1	\N	\N
775	\N	337	viz rma 80070	1	\N	\N
779	\N	338	obj.c: 12021751-1, obj.c: 12021751-2\n 19600Kc	1	\N	\N
780	\N	339	stinici pripravek 	3	\N	\N
782	\N	341	viz predavaci protokol c.:340	1	\N	\N
788	\N	342	fakturovat dle obj. c.: 2008075  4350Kc	1	\N	\N
793	\N	345	pouzita	1	\N	\N
799	\N	349	osazeni 106 ks desek  a 35 Kc	1	\N	\N
800	\N	350	celkem za osazovani leden + duben53675 Kc	1	\N	\N
801	\N	351	nefakturovat viz dodaci list c.:352	1	\N	\N
803	\N	352	viz predavaci protokol 351	1	\N	\N
809	\N	353	oprava radiove casti 3000Kc viz RMA80092	1	\N	\N
810	\N	354	osazeni dle objednavky 262-1103 - 37518Kc	1	\N	\N
1172	\N	369	priprava dat pro vyrobu plosneho spoje 12000Kc\n	1	\N	\N
1173	\N	370	za osazeni dle obj. c.: 304-1103  9000 Kc	1	\N	\N
1184	\N	377	BT 4100-0004 - 192,64Kc	100	\N	\N
1185	\N	377	BT 4100-0005 - 156,00Kc	100	\N	\N
1186	\N	378	propojovaci drat 5.8311.511 - 3Kc	1000	\N	\N
1187	\N	378	propojovaci drat 5.8311.509/01 - 1,8Kc	2000	\N	\N
1188	\N	379	viz RMA 80108 a 80100 - celkem 1000Kc	1	\N	\N
1189	\N	380	viz RMA 80114 celkem 1000Kc	1	\N	\N
1190	\N	381	fakturujeme Vam dle obj.c.:2008158 - 7067Kc	1	\N	\N
1191	\N	382	dodali 4 ks starych mikrovln na vynemu	1	\N	\N
1208	\N	387	repasovany vysilac, nastaveni spoje 9000 Kc	1	\N	\N
1209	\N	388	 \tfakturujeme Vam dle obj.c.:2008164 - 5231,50 Kc	1	\N	\N
1211	\N	390	47Kg tištěných spojů - 1410Kč	1	\N	\N
1219	\N	395	za 60 setů Dynablot vám fakturujeme 306 427 Kč	1	\N	\N
1220	\N	396	za 10 ks Dyna-mic 42309 Kč	1	\N	\N
1226	\N	400	do spojů byly doplněny 2ks datové desky	1	\N	\N
1229	\N	400	provedena výměna kabeláže	2	\N	\N
1230	\N	400	provedena výměna bnc/f konektoru	4	\N	\N
1231	\N	401	Neop. reklamace RMA80195	1	\N	\N
1233	\N	401	Nahradni dily	300	\N	\N
1237	\N	403	Radom o prumeru 120cm vcetne sroubu a matic	1	\N	\N
1249	\N	409	5.8311.509.02   1,8 Kč	1000	\N	\N
1250	\N	409	5.8311.509.03   1,8 Kč	1000	\N	\N
1251	\N	409	5.8311.509.04   2,0 Kč	1000	\N	\N
1252	\N	409	5.8311.509.05   2,0Kč	1000	\N	\N
1253	\N	410	Dle objednavky c.:612 Vam fakturujeme 5330 Kc	1	\N	\N
1259	\N	413	Celkem 52 377,60 Kč	1	\N	\N
1260	\N	413	Navýšení - nesouměrné rozměry 10%	1	\N	\N
1261	\N	413	120 ks  BT 4100-0005 (156 Kč/ks)	1	\N	\N
1262	\N	413	150 ks  BT 4100-0004 (192,4 Kč/ks)	1	\N	\N
1265	\N	415	jukebox 290 Kč	200	\N	\N
1266	\N	415	záloha 30 000Kč	1	\N	\N
1267	\N	416	osazení dle objednávky 5800 Kč	1	\N	\N
1272	\N	419	přetavení 20 ks desek 1000 Kč	1	\N	\N
1273	\N	420	osazování TIK102 124 Kč	50	\N	\N
1274	\N	420	osazování TPS704 170 Kč	20	\N	\N
1275	\N	420	osazování TPS804 170 Kč	20	\N	\N
1276	\N	420	osazování TPS404 232 Kč	50	\N	\N
1277	\N	421	redukce pro 3977 slb-t 400Kč	6	\N	\N
1290	\N	426	1N4XXX	2	\N	\N
1291	\N	426	1N53xxx	2	\N	\N
1292	\N	426	2N6xxx	2	\N	\N
1295	\N	427	pájecí práce 0,5 h 200Kč	1	\N	\N
1296	\N	428	návrh designu plošného spoje 10000 Kč	1	\N	\N
1301	\N	430	Stinici krabice	1	\N	\N
1302	\N	431	osazení 45 sad Dynablot - 228742 Kč	1	\N	\N
1328	\N	439	Dioda 1N5343B	20	\N	\N
1336	\N	443	záloha na nákup součástek 250000 Kč	1	\N	\N
1352	\N	449	osazení GPRS-Com 140Kč/ks	20	\N	\N
1353	\N	450	Osazování 55 setů Dynawash  278804 Kč	1	\N	\N
1354	\N	451	záloha na vývoj svítidel 40 000Kč	1	\N	\N
1355	\N	438	Osazení AT05H20 85Kč/ks	370	85	\N
1358	\N	454	Programátor T51prog2	1	6320	\N
1359	\N	454	vývoj programu pro Atmel AT89S52 dle zadání	1	35000	\N
1364	\N	90001	Dynawash (návrh, výroba, osazování, součástky)	42	519	\N
1380	\N	90008	přetavení desek	1	1000	\N
1381	\N	90009	Stinici prvek	2	\N	\N
1382	\N	90010	pájení vlnou	1	6000	\N
1383	\N	90011	BT 4100-0004\n	100	192	\N
1384	\N	90011	BT 4100-00045	100	156	\N
1393	\N	90018	Interface	50	593	\N
1394	\N	90018	návrh plošného spoje	2	5000	\N
1395	\N	90018	příplatek za druhou vrstvu plošného spoje	50	70	\N
1396	\N	90018	příprava výroby desky plošného spoje	2	1800	\N
1411	\N	90028	malá destička do BTS	150	2273	\N
1412	\N	90028	zálohová faktura 0443-M	1	-250000	\N
1415	\N	90029	Výměna vysílače 15000 kč	1	\N	\N
1426	\N	90035	Vývoj mechanického a elektronického řešení lampy Alkes Adela 11 IDK 905	1	20000	\N
1427	\N	90035	Vývoj mechanického a elektronického řešení lampy Alkes Adela 40 IDK 906	1	20000	\N
1428	\N	90034	Vývoj mechanického a elektronického řešení lampy Antares Adela 52 IDK 904	1	20000	\N
1429	\N	90034	Vývoj mechanického a elektronického řešení lampy Antares Regina 11 IDK 903	1	20000	\N
1430	\N	90034	Vývoj mechanického a elektronického řešení lampy Antares Regina 27 IDK 902	1	20000	\N
1431	\N	90034	Vývoj mechanického a elektronického řešení lampy Antares Roxana 150  IDK 901	1	20000	\N
1432	\N	90036	Vývoj mechanického a elektronického řešení lampy Aliot Adela 52 IDK 909	1	10000	\N
1433	\N	90036	Vývoj mechanického a elektronického řešení lampy Aliot Clara 52 IDK 914	1	10000	\N
1434	\N	90036	Vývoj mechanického a elektronického řešení lampy Aliot Diana 160 RGB IDK 907	1	20000	\N
1435	\N	90036	Vývoj mechanického a elektronického řešení lampy Aliot Diana 80 bílý IDK 908	1	20000	\N
1320	7488	437		2	\N	1
1436	\N	90036	Vývoj mechanického a elektronického řešení lampy Aliot Diana 80 RGB IDK 908	1	20000	\N
1437	\N	90037	Vývoj mechanického a elektronického řešení lampy Albireo Diana 150 RGB IDK 910	1	30000	\N
1438	\N	90037	Vývoj mechanického a elektronického řešení lampy Albireo Diana 80  bílý IDK 911	1	30000	\N
1439	\N	90038	Vývoj mechanického a elektronického řešení lampy Alnair Adela 15 IDK 913	1	20000	\N
1440	\N	90038	Vývoj mechanického a elektronického řešení lampy Alnair Adela 30 IDK 912	1	20000	\N
1444	\N	90039	Antares - Adela 52 - návrh DPS, zkouška mechanických vlastností, napájecí zdroj, návod k obsluze, vrtací šablona,	1	25000	\N
1445	\N	90039	Antares Regina 27 - návrh DPS, zkouška mechanických vlastností, napájecí zdroj, výběr šňůrového vypínače	1	15000	\N
1446	\N	90040	Alkes Adela 11 - návrh DPS, zkouška mechanických vlastností, napájecí zdroj, schémata zapojení	1	18000	\N
1447	\N	90041	Aliot Adela - zpracování výkresové dokumentace	1	10000	\N
1448	\N	90041	Aliot Clara - zpracování výkresové dokumentace	1	\N	\N
1449	\N	90042	Albireo Diana 150 - zpracování výkresové dokumentace, zkouška mechanické funkce	1	15000	\N
1450	\N	90042	Albireo Diana 80 - zpracování výkresové dokumentace	1	1000	\N
1451	\N	90043	návrh a vývoj dálkového ovladače	1	40000	\N
1452	\N	90044	návrh a vývoj stabilizátoru	1	20000	\N
1461	\N	90047	výroba prototypů	12	300	\N
1462	\N	90048	Výměna vysílače mikrovlny viz. RMA90029	1	\N	\N
1463	\N	90049	návrh schématu desky CIM	1	19200	\N
1464	\N	90049	Osazení desky CIM	39	469	\N
1466	\N	90049	set součástek na 99 desek CIM	1	85000	\N
1467	\N	90049	výroba lisovacího přípravku pro konektor TYCO	1	4000	\N
1476	\N	90053	popužitá anténa 120 cm cena 13683 Kc	1	\N	\N
1477	\N	90054	osazení setu Dynablot včetně součástek	70	5070	\N
1478	\N	90055	poštovné 200 Kč	1	\N	\N
1479	\N	90055	přechodky "F"/"BNC" 100 Kč	4	\N	\N
1480	\N	90055	řešení neoprávněné reklamace 2000 Kč	1	\N	\N
1486	\N	90057	Kompletní modem 15000 Kč	1	\N	\N
1497	\N	90061	Dyna vývěva výroba, výroba kabeláže	30	715	\N
1498	\N	90061	Dynawash úprava desky, výroba kabeláže	33	945	\N
1499	\N	90061	kabeláž Dynablot	30	1475	\N
1500	\N	90061	kabeláž Dynablot (dodáno přednostně)	20	1475	\N
1502	\N	90063	kabeláž Dynablot	50	1475	\N
1503	\N	90064	Osazení EVI3, včetně součástek 	100	39	\N
1505	\N	90065	Transformátor 230/21V	2	\N	\N
1508	\N	90066	víčko bez štítku ČTÚ!	1	\N	\N
1517	\N	90071	AT05H20	400	85	\N
1518	\N	90072	a	1	1	\N
1525	\N	90077	Kabeláž pro Dynablot	39	1475	\N
1526	\N	90077	osazení setu Dynablot včetně součástek	50	5070	\N
1529	\N	90080	osazování desky ATP-100M	200	32	\N
1530	\N	90081	úprava elektronického zámku výtahu	1	420	\N
1534	\N	90083	Osazení desky CIM	60	469	\N
1540	\N	90086	osazení VFG-2Rx-R	147	24	\N
1541	\N	90086	osazení VFG-2Tx-R	150	18	\N
1542	\N	90086	osazovací program VFG-2Rx-R	1	2400	\N
1543	\N	90086	osazovací program VFG-2Tx-R	1	1600	\N
1544	\N	90087	detekce závady (hodinová sazba)	3	336	\N
1547	\N	90089	INT_FE2-25-12, radiová deska 5000 Kč	1	\N	\N
1563	\N	90092	výměna vysílače 15000Kč	1	\N	\N
1572	\N	90094	výměna konvertoru 10000Kč	1	\N	\N
1579	\N	90101	osazení RV-4AR	150	28	\N
1580	\N	90101	osazení ZV4	150	35	\N
1581	\N	90101	osazovací program RV-4AR	1	1200	\N
1582	\N	90101	osazovací program ZV4	1	1600	\N
1585	\N	90103	osazeni PS 3021	108	18	\N
1586	\N	90103	osazeni PWRS-3	108	28	\N
1587	\N	90103	osazovací program PS3021	1	1200	\N
1588	\N	90103	osazovací program PWRS-3	1	1600	\N
1598	\N	90060	SX52BD 450Kč	1	\N	\N
1599	\N	90100	Atenuátor 198Kč/ks	2	\N	\N
1600	\N	90106	44444	1	12	\N
1601	\N	90107	pájení na vlně dle objednávky 2009138	1	7250	\N
1604	\N	90109	vývoj a výroba napájecího řadiče LED diody	1	3200	\N
1605	\N	90110	DPS Antares Roxana	1	10000	\N
1606	\N	90108	Celková cena opravy 1000Kč.	1	\N	\N
45	2763	33	\N	1	\N	1
1607	\N	90108	Oprava ETH desky 3UY12V - viz RMA90172	1	\N	\N
1608	\N	90111	Router TN1DN80100540127 9900kČ	1	\N	\N
1612	\N	90113	kabeláž pro Dynablot	7	1475	\N
1613	\N	90113	osazení setu Dynablot včetně součástek	80	5070	\N
1624	\N	90116	PS1521	120	17	\N
1625	\N	90116	VFG-2TxM-R	100	20	\N
1626	\N	90117	osazení prototypu Cmodul V1.2	2	600	\N
1627	\N	90117	vymena IO	4	300	\N
1631	\N	90118	F-konektor    á 45,-	2	\N	\N
1634	\N	90119	Výměna vysílače á 15 000,-	1	\N	\N
1635	\N	90120	5.8311.509.02	1000	1	\N
1636	\N	90120	5.8311.509.03	1000	1	\N
1637	\N	90120	5.8311.509.04	1000	2	\N
1638	\N	90120	5.8311.509.05	1000	2	\N
1639	\N	90120	5.8311.509.08	1000	2	\N
1640	\N	90120	5.8311.511	1000	3	\N
1641	\N	90121	display verze 1.0	10	150	\N
1642	\N	90122	osazení desky Cmodul 1.3	2	2100	\N
1647	\N	90124	prototypová výroba - multipanel (redukce konektoru, prototyp periferie)	1	5870	\N
1648	\N	100001	záloha na nákup součástek pro osazení EXIO AVR a EXIO REL	1	10000	\N
1650	\N	100003	Kompletní osazování SGA01-1	250	240	\N
1651	\N	100003	osazovací program pro SGA01-1	1	4000	\N
1667	\N	100014	osazeni RVA 102LB	79	23	\N
1668	\N	100014	program RVA 102LB	1	1200	\N
1670	\N	100007	osazení BT 6000-0002	100	156	\N
1671	\N	100006	osazení BT-6000-0004	67	192	\N
1672	\N	100016	osazení BT-6000-0004	33	192	\N
1673	\N	100017	osazení MLD02-6	103	43	\N
1674	\N	100017	osazovací program MLD02-6	1	3200	\N
1679	\N	100020	Osazeni DZV - 102	108	16	\N
1680	\N	100020	program DZV- 102	1	600	\N
1681	\N	100020	příprava pece	1	1000	\N
1682	\N	100021	ruční vzorkové osazování EXIO AVR - 5 kusu	1	4800	\N
1683	\N	100021	ruční vzorkové osazování EXIO REL - 2 kusy	1	1000	\N
1684	\N	100021	součástky pro zakázku	1	8246	\N
1688	\N	100025	konvertor číslo 495	1	\N	\N
1689	\N	100025	Manipulační poplatek za vyřízení neoprávněné reklamace 150Kč	1	\N	\N
1669	\N	100015	oprava Ladítka MW vysílačů, dle objednávky ze dne 9.2. 2010 10000Kč	1	10000	\N
1652	\N	100004	Oprava a naladění spoje (parohy)	1	4800	\N
1649	\N	100002	RMA100001 oprava radiové desky po připojení na 230V	1	2400	\N
1691	\N	100026	Dynablot v3	9	5070	\N
1692	\N	100026	Dynablot v4	40	5100	\N
1693	\N	100026	Příprava nových filmů pro Dynablot V4	1	3200	\N
1694	\N	100026	úprava dat pro Dynablot V4	1	2400	\N
105	2865	57	\N	1	\N	1
1695	\N	100026	úprava šablony pro Dynablot V4	1	500	\N
1696	\N	100027	Osazení EVI3, včetně součástek 	98	39	\N
1697	\N	100028	propojovací drát 5.8311.509.08	1000	2	\N
1698	\N	100029	BT 4100-004	200	192	\N
1699	\N	100029	BT 4100-005	200	156	\N
1643	7850	90123		1	\N	1
1700	\N	100030	oprava mikrovlnného vysílače číslo 1152 , výměna elektroniky konvertoru číslo 850;  7000 Kč	1	\N	\N
1703	\N	100031	Stínící přípravek	4	\N	\N
1705	\N	100032	oprava ethernetové karty 3UY662, RMA 10-0024; 6800 Kč 	1	\N	\N
1706	\N	100033	osazovací program pro PWS-73-N	1	800	\N
1707	\N	100033	osazování PWS-73-N	300	18	\N
1708	\N	100034	oprava mikrovlnné jednotky 3VC2DX, viz RMA 10-0016; 5000 Kč	1	\N	\N
1713	\N	100036	útlumový článek 280Kč	3	\N	\N
1714	\N	100037	stínící krabička 300Kč	2	\N	\N
1715	\N	100038	Magnetický senzor A	20	118	\N
1716	\N	100038	Magnetický senzor B	20	118	\N
1717	\N	100038	Magnetický senzor C	20	118	\N
1718	\N	100038	Návrh desek 	1	1600	\N
1719	\N	100039	osazení desky display	1	1600	\N
1726	\N	100043	osazení vzorků dynablot 7 pump včetně součástek	12	530	\N
1727	\N	100044	osazení desky uniprog V1.0	1	1200	\N
1728	\N	100045	frézování	1	600	\N
1729	\N	100045	LED pásek	123	54	\N
1730	\N	100045	návrh desky	1	2400	\N
1731	\N	100045	osazovací program	1	1500	\N
1732	\N	100045	příprava filmů	1	1500	\N
1733	\N	100045	šablona pro nanášení pasty	1	2800	\N
1734	\N	100046	5.8311.509.01	2000	1	\N
1735	\N	100046	5.8311.509.03	1000	1	\N
1742	\N	100049	jukebox	60	325	\N
718	6378	313	\N	60	\N	1
719	6379	313	\N	60	\N	1
150	3375	76	\N	1	\N	1
721	6380	315	\N	1	\N	1
722	6381	315	\N	1	\N	1
2	910	4	\N	1	\N	1
3	911	5	\N	1	\N	1
4	912	6	\N	1	\N	1
5	913	7	\N	1	\N	1
6	914	8	\N	1	\N	1
7	396	8	\N	1	\N	1
8	394	8	\N	1	\N	1
9	419	8	\N	1	\N	1
10	729	8	\N	1	\N	1
11	917	11	\N	1	\N	1
12	918	10	\N	1	\N	1
13	919	9	\N	1	\N	1
14	920	12	\N	1	\N	1
15	389	12	\N	1	\N	1
16	390	12	\N	1	\N	1
17	383	12	\N	1	\N	1
18	384	12	\N	1	\N	1
19	915	2	\N	1	\N	1
20	916	3	\N	1	\N	1
21	393	13	\N	1	\N	1
22	438	13	\N	1	\N	1
23	2224	14	\N	1	\N	1
24	2226	15	\N	1	\N	1
25	1420	16	\N	1	\N	1
26	2229	17	\N	1	\N	1
27	2227	18	\N	1	\N	1
28	2225	19	\N	1	\N	1
29	2230	21	\N	1	\N	1
30	2228	20	\N	1	\N	1
31	2259	27	\N	1	\N	1
32	2258	26	\N	1	\N	1
33	1424	25	\N	1	\N	1
34	2261	23	\N	1	\N	1
35	2262	24	\N	1	\N	1
36	2263	22	\N	1	\N	1
37	2690	28	\N	1	\N	1
38	2718	29	\N	1	\N	1
39	2717	30	\N	1	\N	1
41	2710	31	\N	1	\N	1
47	2768	34	\N	1	\N	1
49	2779	35	\N	1	\N	1
51	2803	36	\N	1	\N	1
52	2778	36	\N	1	\N	1
53	2803	37	\N	1	\N	1
54	2778	37	\N	1	\N	1
55	2802	38	\N	1	\N	1
56	2804	39	\N	1	\N	1
57	2805	39	\N	1	\N	1
58	2816	40	\N	1	\N	1
60	996	41	\N	1	\N	1
61	2704	41	\N	1	\N	1
62	2705	41	\N	1	\N	1
63	2706	41	\N	1	\N	1
64	2707	41	\N	1	\N	1
65	2708	41	\N	1	\N	1
66	996	42	\N	1	\N	1
67	2704	42	\N	1	\N	1
68	2705	42	\N	1	\N	1
69	2706	42	\N	1	\N	1
70	2707	42	\N	1	\N	1
71	2708	42	\N	1	\N	1
74	2806	44	\N	1	\N	1
75	433	45	\N	1	\N	1
76	1410	45	\N	1	\N	1
77	2698	45	\N	1	\N	1
78	2699	45	\N	1	\N	1
79	2700	45	\N	1	\N	1
80	2701	45	\N	1	\N	1
81	2702	45	\N	1	\N	1
82	2703	45	\N	1	\N	1
83	860	46	\N	1	\N	1
84	2757	46	\N	1	\N	1
89	2838	51	\N	1	\N	1
90	2856	52	\N	1	\N	1
91	2857	52	\N	1	\N	1
96	2860	54	\N	1	\N	1
98	3046	55	\N	1	\N	1
100	2879	57	\N	1	\N	1
101	2885	57	\N	1	\N	1
102	555	57	\N	1	\N	1
103	1256	57	\N	1	\N	1
104	2839	57	\N	1	\N	1
106	3050	58	\N	1	\N	1
108	3101	59	\N	1	\N	1
109	3102	59	\N	1	\N	1
110	3099	59	\N	1	\N	1
111	3100	59	\N	1	\N	1
112	2797	59	\N	1	\N	1
113	2913	59	\N	1	\N	1
114	3101	60	\N	1	\N	1
115	3102	60	\N	1	\N	1
116	3099	60	\N	1	\N	1
117	3100	60	\N	1	\N	1
118	2797	60	\N	1	\N	1
119	2913	60	\N	1	\N	1
122	3106	61	\N	1	\N	1
123	3107	62	\N	1	\N	1
125	3112	63	\N	1	\N	1
128	3113	64	\N	1	\N	1
129	3043	65	\N	1	\N	1
130	503	66	\N	1	\N	1
136	3193	68	\N	1	\N	1
137	3196	69	\N	1	\N	1
139	3229	70	\N	1	\N	1
156	3389	77	\N	1	\N	1
158	3388	78	\N	1	\N	1
159	3390	78	\N	1	\N	1
163	3408	80	\N	1	\N	1
168	3393	83	\N	1	\N	1
174	3392	85	\N	1	\N	1
176	3498	86	\N	1	\N	1
181	3521	88	\N	1	\N	1
183	3520	89	\N	1	\N	1
186	3522	90	\N	1	\N	1
188	3524	91	\N	1	\N	1
190	3525	92	\N	1	\N	1
192	3523	93	\N	1	\N	1
193	3526	94	\N	1	\N	1
195	3527	95	\N	1	\N	1
201	3528	96	\N	1	\N	1
202	3529	97	\N	1	\N	1
204	3578	98	\N	1	\N	1
206	3579	99	\N	1	\N	1
208	3580	100	\N	1	\N	1
210	3581	101	\N	1	\N	1
215	3421	103	\N	1	\N	1
216	3512	103	\N	1	\N	1
217	696	103	\N	1	\N	1
218	726	103	\N	1	\N	1
219	2962	103	\N	1	\N	1
220	2963	103	\N	1	\N	1
222	3419	104	\N	1	\N	1
223	3420	104	\N	1	\N	1
224	3509	104	\N	1	\N	1
225	3723	105	\N	1	\N	1
227	3374	106	\N	1	\N	1
229	3724	107	\N	1	\N	1
232	3725	109	\N	1	\N	1
234	3867	110	\N	1	\N	1
236	3870	111	\N	1	\N	1
238	3726	112	\N	1	\N	1
239	3871	113	\N	1	\N	1
241	3872	114	\N	1	\N	1
243	3873	115	\N	1	\N	1
245	3874	116	\N	1	\N	1
248	3921	118	\N	1	\N	1
251	3875	120	\N	1	\N	1
253	3943	121	\N	1	\N	1
255	3876	122	\N	1	\N	1
258	3946	124	\N	1	\N	1
260	3945	126	\N	1	\N	1
261	3877	127	\N	1	\N	1
263	3878	128	\N	1	\N	1
266	4126	130	\N	1	\N	1
268	4127	131	\N	1	\N	1
270	3879	132	\N	1	\N	1
272	3880	133	\N	1	\N	1
278	4128	136	\N	1	\N	1
283	4129	138	\N	1	\N	1
291	4130	142	\N	1	\N	1
301	2282	146	\N	1	\N	1
302	4169	146	\N	1	\N	1
303	4195	147	\N	1	\N	1
304	4131	148	\N	1	\N	1
305	4207	149	\N	1	\N	1
306	4208	150	\N	1	\N	1
723	4976	316	\N	1	\N	1
308	3748	152	\N	1	\N	1
309	4271	153	\N	1	\N	1
310	4270	154	\N	1	\N	1
311	4272	154	\N	1	\N	1
312	4273	154	\N	1	\N	1
313	4283	155	\N	1	\N	1
314	4290	156	\N	1	\N	1
315	4291	157	\N	1	\N	1
316	4292	158	\N	1	\N	1
317	4293	158	\N	1	\N	1
318	4294	158	\N	1	\N	1
319	4295	158	\N	1	\N	1
320	4269	159	\N	1	\N	1
321	4296	159	\N	1	\N	1
322	4297	159	\N	1	\N	1
323	4215	160	\N	1	\N	1
324	4299	161	\N	1	\N	1
325	4300	161	\N	1	\N	1
326	4301	161	\N	1	\N	1
327	4302	161	\N	1	\N	1
328	4303	161	\N	1	\N	1
329	4304	161	\N	1	\N	1
330	4305	161	\N	1	\N	1
724	6382	316	\N	2	\N	1
331	4267	162	\N	\N	\N	1
332	4349	162	\N	\N	\N	1
333	4350	162	\N	\N	\N	1
334	4351	162	\N	\N	\N	1
335	3944	163	\N	\N	\N	1
307	4214	151	\N	2	\N	1
336	4262	164	\N	\N	\N	1
337	4355	164	\N	\N	\N	1
338	4266	165	\N	\N	\N	1
339	4356	165	\N	\N	\N	1
340	4357	165	\N	\N	\N	1
341	4358	166	\N	\N	\N	1
725	6383	317	\N	1	\N	1
342	4362	167	\N	2	\N	1
343	4263	168	\N	\N	\N	1
344	4363	168	\N	\N	\N	1
345	4364	168	\N	\N	\N	1
346	4373	169	\N	1	\N	1
347	4374	169	\N	1	\N	1
348	4375	169	\N	1	\N	1
349	4376	169	\N	1	\N	1
350	4264	170	\N	1	\N	1
351	4378	170	\N	2	\N	1
352	4383	171	\N	1	\N	1
353	4390	172	\N	1	\N	1
354	4265	173	\N	1	\N	1
355	4397	173	\N	1	\N	1
356	4398	173	\N	1	\N	1
358	4132	175	\N	1	\N	1
367	4408	179	\N	1	\N	1
368	4409	179	\N	1	\N	1
370	4413	180	\N	2	\N	1
371	4414	180	\N	2	\N	1
372	4415	180	\N	2	\N	1
373	4416	180	\N	2	\N	1
374	4452	181	\N	1	\N	1
375	4453	181	\N	1	\N	1
376	4454	181	\N	1	\N	1
377	4455	181	\N	2	\N	1
378	4456	181	\N	2	\N	1
379	4457	181	\N	210	\N	1
380	4458	181	\N	30	\N	1
382	4133	182	\N	1	\N	1
385	4462	185	\N	1	\N	1
386	4463	185	\N	2	\N	1
387	4370	186	\N	1	\N	1
388	4464	186	\N	2	\N	1
389	4465	187	\N	2	\N	1
390	4473	188	\N	2	\N	1
391	4474	189	\N	550	\N	1
392	4475	190	\N	460	\N	1
393	4222	191	\N	1	\N	1
394	4478	192	\N	2	\N	1
395	4500	193	\N	1	\N	1
396	4510	193	\N	2	\N	1
397	4521	194	\N	240	\N	1
398	4528	195	\N	120	\N	1
399	4534	196	\N	180	\N	1
400	4535	197	\N	1	\N	1
402	4216	198	\N	1	\N	1
403	4466	199	\N	1	\N	1
404	4511	199	\N	1	\N	1
406	3620	200	\N	1	\N	1
407	3621	200	\N	1	\N	1
408	4543	200	\N	1	\N	1
409	4544	200	\N	1	\N	1
410	4545	200	\N	2	\N	1
411	4546	200	\N	60	\N	1
412	4504	201	\N	1	\N	1
414	3623	202	\N	1	\N	1
415	3624	202	\N	1	\N	1
416	4547	202	\N	1	\N	1
417	4548	202	\N	1	\N	1
418	4549	202	\N	1	\N	1
419	4550	202	\N	1	\N	1
420	4551	202	\N	60	\N	1
421	4552	203	\N	1	\N	1
422	4553	204	\N	1	\N	1
424	3892	205	\N	1	\N	1
427	4605	208	\N	1	\N	1
428	4606	208	\N	1	\N	1
429	4607	208	\N	1	\N	1
430	4608	208	\N	2	\N	1
431	4609	208	\N	2	\N	1
432	4610	208	\N	2	\N	1
433	4611	208	\N	2	\N	1
434	4612	208	\N	2	\N	1
436	4509	209	\N	1	\N	1
437	4630	210	\N	1	\N	1
438	4658	211	\N	1	\N	1
439	4659	211	\N	1	\N	1
440	4660	211	\N	1	\N	1
441	4661	212	\N	1	\N	1
442	4662	212	\N	1	\N	1
443	4663	212	\N	1	\N	1
445	4505	213	\N	1	\N	1
446	4664	214	\N	1	\N	1
447	4665	214	\N	1	\N	1
448	4666	214	\N	2	\N	1
449	4667	214	\N	2	\N	1
450	4668	214	\N	2	\N	1
451	4669	214	\N	2	\N	1
452	4670	214	\N	2	\N	1
453	4671	214	\N	2	\N	1
454	4672	214	\N	2	\N	1
456	4673	215	\N	180	\N	1
458	4674	216	\N	180	\N	1
461	4719	217	\N	2	\N	1
462	4720	217	\N	4	\N	1
463	4721	217	\N	4	\N	1
464	4722	217	\N	4	\N	1
469	4773	220	\N	1	\N	1
471	4803	221	\N	1	\N	1
472	4804	222	\N	1	\N	1
473	4809	223	\N	2	\N	1
474	4810	223	\N	4	\N	1
475	4811	223	\N	4	\N	1
476	4812	223	\N	4	\N	1
477	4813	224	\N	2	\N	1
478	4814	224	\N	2	\N	1
480	4826	226	\N	5	\N	1
482	4956	227	\N	1	\N	1
483	5151	228	\N	2	\N	1
484	5152	228	\N	2	\N	1
485	5153	228	\N	2	\N	1
486	5154	228	\N	2	\N	1
487	5155	228	\N	20	\N	1
488	4957	229	\N	1	\N	1
489	5156	229	\N	2	\N	1
490	4959	230	\N	1	\N	1
491	5157	230	\N	2	\N	1
492	4960	231	\N	1	\N	1
493	5158	231	\N	2	\N	1
494	5159	231	\N	2	\N	1
495	5160	231	\N	2	\N	1
496	5161	231	\N	2	\N	1
498	4962	232	\N	1	\N	1
499	5162	233	\N	1	\N	1
500	5163	233	\N	1	\N	1
501	5164	233	\N	1	\N	1
502	5165	233	\N	1	\N	1
503	5166	233	\N	2	\N	1
504	5167	233	\N	2	\N	1
505	5168	233	\N	240	\N	1
506	5169	233	\N	3	\N	1
507	5170	233	\N	4	\N	1
508	5171	233	\N	4	\N	1
510	4961	234	\N	1	\N	1
511	4762	235	\N	1	\N	1
512	5172	236	\N	6	\N	1
514	5173	238	\N	1	\N	1
515	4965	239	\N	1	\N	1
516	5174	239	\N	1	\N	1
517	5175	239	\N	2	\N	1
518	5176	239	\N	2	\N	1
519	5177	239	\N	2	\N	1
520	5178	239	\N	2	\N	1
521	5179	240	\N	200	\N	1
522	5180	240	\N	3900	\N	1
523	5181	241	\N	2	\N	1
526	5182	242	\N	1	\N	1
527	5183	242	\N	1	\N	1
528	5184	242	\N	120	\N	1
529	5185	242	\N	240	\N	1
530	4964	243	\N	1	\N	1
531	5186	243	\N	2	\N	1
532	5187	244	\N	4	\N	1
533	5188	245	\N	1	\N	1
534	4966	246	\N	1	\N	1
535	5189	246	\N	2	\N	1
536	5190	247	\N	1	\N	1
537	5191	247	\N	1	\N	1
542	3700	249	\N	1	\N	1
543	5241	250	\N	1	\N	1
544	4968	251	\N	1	\N	1
545	5287	251	\N	2	\N	1
546	4970	252	\N	1	\N	1
547	5293	252	\N	2	\N	1
548	5294	252	\N	2	\N	1
549	5295	252	\N	4	\N	1
550	5296	252	\N	4	\N	1
551	5297	252	\N	4	\N	1
552	4971	253	\N	1	\N	1
553	5298	253	\N	2	\N	1
555	5299	254	\N	1	\N	1
556	5300	254	\N	1	\N	1
557	5301	254	\N	1	\N	1
558	4972	255	\N	1	\N	1
559	5302	255	\N	2	\N	1
560	5303	255	\N	2	\N	1
561	5304	255	\N	4	\N	1
562	5305	255	\N	4	\N	1
563	5306	255	\N	4	\N	1
564	4982	256	\N	1	\N	1
565	5307	256	\N	2	\N	1
567	4969	257	\N	1	\N	1
570	5308	258	\N	1	\N	1
571	5309	258	\N	1	\N	1
572	5310	258	\N	2	\N	1
573	5311	258	\N	2	\N	1
574	5312	258	\N	2	\N	1
575	5313	258	\N	2	\N	1
576	5314	258	\N	2	\N	1
577	5315	258	\N	2	\N	1
578	5316	259	\N	1	\N	1
579	5317	259	\N	1	\N	1
580	5318	259	\N	1	\N	1
581	5319	259	\N	1	\N	1
582	5320	260	\N	1	\N	1
585	4983	262	\N	1	\N	1
586	5321	263	\N	1	\N	1
587	5322	263	\N	1	\N	1
588	5323	263	\N	1	\N	1
589	5324	263	\N	2	\N	1
590	5325	263	\N	60	\N	1
592	4990	264	\N	1	\N	1
593	5326	265	\N	1	\N	1
594	5327	265	\N	1	\N	1
595	5328	265	\N	1	\N	1
596	5329	265	\N	2	\N	1
598	5207	266	\N	1	\N	1
599	5273	266	\N	1	\N	1
600	4767	266	\N	1	\N	1
601	4768	266	\N	1	\N	1
602	3698	266	\N	1	\N	1
603	3699	266	\N	1	\N	1
604	5330	267	\N	1	\N	1
605	5331	267	\N	1	\N	1
606	5332	267	\N	60	\N	1
607	5333	268	\N	3	\N	1
608	4973	269	\N	1	\N	1
609	5334	269	\N	2	\N	1
610	4991	270	\N	1	\N	1
611	5335	270	\N	2	\N	1
612	4992	271	\N	1	\N	1
613	5336	271	\N	2	\N	1
614	6059	272	\N	1	\N	1
615	6060	272	\N	2	\N	1
616	6061	273	\N	1	\N	1
617	6062	274	\N	2	\N	1
618	6063	274	\N	4	\N	1
620	4519	276	\N	1	\N	1
621	4558	276	\N	1	\N	1
622	3664	276	\N	3	\N	1
624	4974	277	\N	1	\N	1
625	4993	278	\N	1	\N	1
626	6074	279	\N	2	\N	1
627	6075	280	\N	1	\N	1
628	4975	281	\N	1	\N	1
629	6076	281	\N	2	\N	1
727	6343	319	\N	1	\N	1
728	6384	319	\N	1	\N	1
729	5012	320	\N	1	\N	1
731	4977	321	\N	1	\N	1
732	6385	321	\N	2	\N	1
630	4285	282	\N	1	\N	1
631	5279	282	\N	1	\N	1
632	4288	282	\N	1	\N	1
633	4764	282	\N	1	\N	1
634	3717	283	\N	1	\N	1
635	3953	283	\N	1	\N	1
636	6077	283	\N	1	\N	1
637	6078	283	\N	1	\N	1
638	6079	283	\N	1	\N	1
639	6080	283	\N	120	\N	1
640	6093	284	\N	1	\N	1
641	6094	284	\N	1	\N	1
642	6095	284	\N	30	\N	1
643	6096	284	\N	600	\N	1
644	6097	285	\N	1	\N	1
645	6098	285	\N	1	\N	1
646	6099	285	\N	1	\N	1
648	6100	286	\N	1	\N	1
649	6101	286	\N	180	\N	1
651	6102	287	\N	1	\N	1
652	4012	288	\N	1	\N	1
653	4013	288	\N	1	\N	1
654	6103	289	\N	1	\N	1
655	6104	289	\N	1	\N	1
656	4984	290	\N	1	\N	1
657	6135	290	\N	2	\N	1
658	6136	291	\N	1	\N	1
659	6137	291	\N	1	\N	1
660	6138	291	\N	2	\N	1
733	6388	322	\N	5	\N	1
661	6142	292	\N	1	\N	1
662	6143	292	\N	1	\N	1
663	6144	292	\N	1	\N	1
664	4985	293	\N	1	\N	1
665	6145	293	\N	2	\N	1
666	4524	294	\N	1	\N	1
667	6290	294	\N	1	\N	1
668	6291	294	\N	1	\N	1
669	6292	295	\N	1	\N	1
670	6293	295	\N	1	\N	1
671	6294	295	\N	2	\N	1
672	5010	296	\N	1	\N	1
673	6295	296	\N	2	\N	1
674	6296	297	\N	1	\N	1
675	5011	298	\N	1	\N	1
676	6297	298	\N	2	\N	1
677	4389	299	\N	1	\N	1
678	5218	299	\N	1	\N	1
679	4850	299	\N	1	\N	1
680	4855	299	\N	1	\N	1
681	2907	299	\N	1	\N	1
682	2916	299	\N	1	\N	1
683	6298	299	\N	1	\N	1
684	6299	300	\N	6	\N	1
685	6300	301	\N	1	\N	1
686	6301	301	\N	3	\N	1
688	854	302	\N	1	\N	1
689	5197	302	\N	1	\N	1
690	588	302	\N	1	\N	1
691	590	302	\N	1	\N	1
692	3718	302	\N	1	\N	1
693	3719	302	\N	1	\N	1
734	6389	323	\N	5	\N	1
735	6390	324	\N	5	\N	1
737	5013	325	\N	1	\N	1
739	6391	326	\N	1	\N	1
740	6392	326	\N	2	\N	1
741	6393	326	\N	2	\N	1
694	6310	303	\N	1	\N	1
695	6311	303	\N	1	\N	1
696	6312	303	\N	1	\N	1
697	6313	303	\N	1	\N	1
698	6314	303	\N	2	\N	1
699	6315	303	\N	2	\N	1
700	6316	303	\N	2	\N	1
701	6317	303	\N	2	\N	1
703	4988	305	\N	1	\N	1
704	6318	305	\N	2	\N	1
705	6339	306	\N	2	\N	1
706	6340	307	\N	2	\N	1
707	4994	308	\N	1	\N	1
708	6341	308	\N	2	\N	1
709	3709	309	\N	1	\N	1
710	6350	310	\N	1	\N	1
711	6351	310	\N	1	\N	1
712	6342	311	\N	1	\N	1
713	6352	311	\N	1	\N	1
714	4227	312	\N	1	\N	1
715	5242	312	\N	1	\N	1
716	6354	312	\N	1	\N	1
742	6394	326	\N	2	\N	1
743	6395	326	\N	2	\N	1
744	6396	326	\N	2	\N	1
745	6397	326	\N	60	\N	1
746	6370	327	\N	1	\N	1
747	6371	327	\N	1	\N	1
748	5214	327	\N	1	\N	1
749	4771	327	\N	1	\N	1
750	4841	327	\N	1	\N	1
751	4830	327	\N	1	\N	1
760	6424	330	\N	400	\N	1
761	6425	331	\N	1	\N	1
762	6426	331	\N	1	\N	1
763	6427	331	\N	1	\N	1
764	6428	331	\N	2	\N	1
765	6429	331	\N	2	\N	1
766	6430	331	\N	60	\N	1
772	5015	334	\N	1	\N	1
773	6431	335	\N	1	\N	1
776	6435	337	\N	2	\N	1
777	6436	337	\N	2	\N	1
778	6437	337	\N	60	\N	1
781	4987	340	\N	1	\N	1
783	6572	341	\N	1	\N	1
784	6573	341	\N	1	\N	1
785	6574	341	\N	2	\N	1
786	6575	341	\N	2	\N	1
787	6576	341	\N	2	\N	1
789	4981	343	\N	1	\N	1
790	6577	343	\N	2	\N	1
791	4980	344	\N	1	\N	1
792	6578	344	\N	2	\N	1
794	6579	345	\N	1	\N	1
795	5017	346	\N	1	\N	1
796	6581	346	\N	2	\N	1
797	6582	347	\N	1	\N	1
798	5018	348	\N	1	\N	1
802	4995	351	\N	1	\N	1
804	6613	352	\N	1	\N	1
805	6614	352	\N	1	\N	1
806	6615	352	\N	2	\N	1
807	6616	352	\N	2	\N	1
808	6617	352	\N	60	\N	1
811	6620	355	\N	1	\N	1
812	6622	356	\N	1	\N	1
813	6624	356	\N	1	\N	1
814	6623	356	\N	1	\N	1
815	6683	356	\N	1	\N	1
816	6742	356	\N	1	\N	1
817	6743	356	\N	1	\N	1
818	6802	356	\N	1	\N	1
819	6803	356	\N	1	\N	1
820	6862	356	\N	1	\N	1
821	6863	356	\N	1	\N	1
822	6943	356	\N	1	\N	1
823	6944	356	\N	1	\N	1
824	6625	357	\N	1	\N	1
825	6626	357	\N	1	\N	1
826	6684	357	\N	1	\N	1
827	6685	357	\N	1	\N	1
828	6744	357	\N	1	\N	1
829	6745	357	\N	1	\N	1
830	6804	357	\N	1	\N	1
831	6805	357	\N	1	\N	1
832	6864	357	\N	1	\N	1
833	6865	357	\N	1	\N	1
834	6922	357	\N	1	\N	1
835	6945	357	\N	1	\N	1
836	6627	358	\N	1	\N	1
837	6628	358	\N	1	\N	1
838	6686	358	\N	1	\N	1
839	6687	358	\N	1	\N	1
840	6746	358	\N	1	\N	1
841	6747	358	\N	1	\N	1
842	6806	358	\N	1	\N	1
843	6807	358	\N	1	\N	1
844	6866	358	\N	1	\N	1
845	6867	358	\N	1	\N	1
846	6923	358	\N	1	\N	1
847	6924	358	\N	1	\N	1
848	6629	359	\N	1	\N	1
849	6630	359	\N	1	\N	1
850	6688	359	\N	1	\N	1
851	6689	359	\N	1	\N	1
852	6748	359	\N	1	\N	1
853	6749	359	\N	1	\N	1
854	6808	359	\N	1	\N	1
855	6809	359	\N	1	\N	1
856	6868	359	\N	1	\N	1
857	6869	359	\N	1	\N	1
858	6925	359	\N	1	\N	1
859	6946	359	\N	1	\N	1
860	6631	360	\N	1	\N	1
861	6632	360	\N	1	\N	1
862	6690	360	\N	1	\N	1
863	6691	360	\N	1	\N	1
864	6750	360	\N	1	\N	1
865	6751	360	\N	1	\N	1
866	6810	360	\N	1	\N	1
867	6811	360	\N	1	\N	1
868	6870	360	\N	1	\N	1
869	6871	360	\N	1	\N	1
870	6947	360	\N	1	\N	1
871	6948	360	\N	1	\N	1
872	6634	361	\N	1	\N	1
873	6633	361	\N	1	\N	1
874	6692	361	\N	1	\N	1
875	6693	361	\N	1	\N	1
876	6752	361	\N	1	\N	1
877	6753	361	\N	1	\N	1
878	6812	361	\N	1	\N	1
879	6813	361	\N	1	\N	1
880	6872	361	\N	1	\N	1
881	6873	361	\N	1	\N	1
882	6926	361	\N	1	\N	1
883	6982	361	\N	1	\N	1
884	6635	362	\N	1	\N	1
885	6636	362	\N	1	\N	1
886	6694	362	\N	1	\N	1
887	6695	362	\N	1	\N	1
888	6754	362	\N	1	\N	1
889	6755	362	\N	1	\N	1
890	6814	362	\N	1	\N	1
891	6815	362	\N	1	\N	1
892	6874	362	\N	1	\N	1
893	6875	362	\N	1	\N	1
894	6927	362	\N	1	\N	1
895	6983	362	\N	1	\N	1
896	6637	363	\N	1	\N	1
897	6638	363	\N	1	\N	1
898	6639	363	\N	1	\N	1
899	6640	363	\N	1	\N	1
900	6641	363	\N	1	\N	1
901	6642	363	\N	1	\N	1
902	6643	363	\N	1	\N	1
903	6644	363	\N	1	\N	1
904	6645	363	\N	1	\N	1
905	6646	363	\N	1	\N	1
906	6647	363	\N	1	\N	1
907	6648	363	\N	1	\N	1
908	6649	363	\N	1	\N	1
909	6650	363	\N	1	\N	1
910	6706	363	\N	1	\N	1
911	6707	363	\N	1	\N	1
912	6708	363	\N	1	\N	1
913	6709	363	\N	1	\N	1
914	6696	363	\N	1	\N	1
915	6697	363	\N	1	\N	1
916	6698	363	\N	1	\N	1
917	6699	363	\N	1	\N	1
918	6700	363	\N	1	\N	1
919	6701	363	\N	1	\N	1
920	6702	363	\N	1	\N	1
921	6703	363	\N	1	\N	1
922	6704	363	\N	1	\N	1
923	6705	363	\N	1	\N	1
924	6756	363	\N	1	\N	1
925	6757	363	\N	1	\N	1
926	6758	363	\N	1	\N	1
927	6759	363	\N	1	\N	1
928	6760	363	\N	1	\N	1
929	6761	363	\N	1	\N	1
930	6762	363	\N	1	\N	1
931	6763	363	\N	1	\N	1
932	6764	363	\N	1	\N	1
933	6765	363	\N	1	\N	1
934	6766	363	\N	1	\N	1
935	6767	363	\N	1	\N	1
936	6768	363	\N	1	\N	1
937	6769	363	\N	1	\N	1
938	6816	363	\N	1	\N	1
939	6817	363	\N	1	\N	1
940	6818	363	\N	1	\N	1
941	6819	363	\N	1	\N	1
942	6820	363	\N	1	\N	1
943	6821	363	\N	1	\N	1
944	6822	363	\N	1	\N	1
945	6823	363	\N	1	\N	1
946	6824	363	\N	1	\N	1
947	6825	363	\N	1	\N	1
948	6826	363	\N	1	\N	1
949	6827	363	\N	1	\N	1
950	6828	363	\N	1	\N	1
951	6829	363	\N	1	\N	1
952	6886	363	\N	1	\N	1
953	6887	363	\N	1	\N	1
954	6888	363	\N	1	\N	1
955	6889	363	\N	1	\N	1
956	6876	363	\N	1	\N	1
957	6877	363	\N	1	\N	1
958	6878	363	\N	1	\N	1
959	6879	363	\N	1	\N	1
960	6880	363	\N	1	\N	1
961	6881	363	\N	1	\N	1
962	6882	363	\N	1	\N	1
963	6883	363	\N	1	\N	1
964	6884	363	\N	1	\N	1
965	6885	363	\N	1	\N	1
966	6958	363	\N	1	\N	1
967	6959	363	\N	1	\N	1
968	6960	363	\N	1	\N	1
969	6961	363	\N	1	\N	1
970	6962	363	\N	1	\N	1
971	6949	363	\N	1	\N	1
972	6950	363	\N	1	\N	1
973	6951	363	\N	1	\N	1
974	6952	363	\N	1	\N	1
975	6953	363	\N	1	\N	1
976	6954	363	\N	1	\N	1
977	6955	363	\N	1	\N	1
978	6956	363	\N	1	\N	1
979	6957	363	\N	1	\N	1
980	6655	364	\N	1	\N	1
981	6656	364	\N	1	\N	1
982	6657	364	\N	1	\N	1
983	6658	364	\N	1	\N	1
984	6659	364	\N	1	\N	1
985	6660	364	\N	1	\N	1
986	6661	364	\N	1	\N	1
987	6662	364	\N	1	\N	1
988	6663	364	\N	1	\N	1
989	6664	364	\N	1	\N	1
990	6713	364	\N	1	\N	1
991	6714	364	\N	1	\N	1
992	6715	364	\N	1	\N	1
993	6716	364	\N	1	\N	1
994	6717	364	\N	1	\N	1
995	6718	364	\N	1	\N	1
996	6719	364	\N	1	\N	1
997	6720	364	\N	1	\N	1
998	6721	364	\N	1	\N	1
999	6722	364	\N	1	\N	1
1000	6778	364	\N	1	\N	1
1001	6779	364	\N	1	\N	1
1002	6780	364	\N	1	\N	1
1003	6781	364	\N	1	\N	1
1004	6782	364	\N	1	\N	1
1005	6783	364	\N	1	\N	1
1006	6774	364	\N	1	\N	1
1007	6775	364	\N	1	\N	1
1008	6776	364	\N	1	\N	1
1009	6777	364	\N	1	\N	1
1010	6840	364	\N	1	\N	1
1011	6841	364	\N	1	\N	1
1012	6842	364	\N	1	\N	1
1013	6843	364	\N	1	\N	1
1014	6834	364	\N	1	\N	1
1015	6835	364	\N	1	\N	1
1016	6836	364	\N	1	\N	1
1017	6837	364	\N	1	\N	1
1018	6838	364	\N	1	\N	1
1019	6839	364	\N	1	\N	1
1020	6893	364	\N	1	\N	1
1021	6894	364	\N	1	\N	1
1022	6895	364	\N	1	\N	1
1023	6896	364	\N	1	\N	1
1024	6897	364	\N	1	\N	1
1025	6898	364	\N	1	\N	1
1026	6899	364	\N	1	\N	1
1027	6900	364	\N	1	\N	1
1028	6901	364	\N	1	\N	1
1029	6902	364	\N	1	\N	1
1030	6928	364	\N	1	\N	1
1031	6930	364	\N	1	\N	1
1032	6931	364	\N	1	\N	1
1033	6932	364	\N	1	\N	1
1034	6929	364	\N	1	\N	1
1035	6963	364	\N	1	\N	1
1036	6964	364	\N	1	\N	1
1037	6965	364	\N	1	\N	1
1038	6966	364	\N	1	\N	1
1039	6967	364	\N	1	\N	1
1040	6670	365	\N	1	\N	1
1041	6671	365	\N	1	\N	1
1042	6672	365	\N	1	\N	1
1043	6665	365	\N	1	\N	1
1044	6666	365	\N	1	\N	1
1045	6667	365	\N	1	\N	1
1046	6668	365	\N	1	\N	1
1047	6669	365	\N	1	\N	1
1048	6723	365	\N	1	\N	1
1049	6724	365	\N	1	\N	1
1050	6725	365	\N	1	\N	1
1051	6726	365	\N	1	\N	1
1052	6727	365	\N	1	\N	1
1053	6728	365	\N	1	\N	1
1054	6729	365	\N	1	\N	1
1055	6730	365	\N	1	\N	1
1056	6784	365	\N	1	\N	1
1057	6785	365	\N	1	\N	1
1058	6786	365	\N	1	\N	1
1059	6787	365	\N	1	\N	1
1060	6788	365	\N	1	\N	1
1061	6789	365	\N	1	\N	1
1062	6790	365	\N	1	\N	1
1063	6791	365	\N	1	\N	1
1064	6850	365	\N	1	\N	1
1065	6851	365	\N	1	\N	1
1066	6844	365	\N	1	\N	1
1067	6845	365	\N	1	\N	1
1068	6846	365	\N	1	\N	1
1069	6847	365	\N	1	\N	1
1070	6848	365	\N	1	\N	1
1071	6849	365	\N	1	\N	1
1072	6903	365	\N	1	\N	1
1073	6904	365	\N	1	\N	1
1074	6905	365	\N	1	\N	1
1075	6906	365	\N	1	\N	1
1076	6907	365	\N	1	\N	1
1077	6908	365	\N	1	\N	1
1078	6909	365	\N	1	\N	1
1079	6910	365	\N	1	\N	1
1080	6933	365	\N	1	\N	1
1081	6934	365	\N	1	\N	1
1082	6935	365	\N	1	\N	1
1083	6936	365	\N	1	\N	1
1084	6937	365	\N	1	\N	1
1085	6938	365	\N	1	\N	1
1086	6939	365	\N	1	\N	1
1087	6940	365	\N	1	\N	1
1088	6651	366	\N	1	\N	1
1089	6652	366	\N	1	\N	1
1090	6653	366	\N	1	\N	1
1091	6654	366	\N	1	\N	1
1092	6710	366	\N	1	\N	1
1093	6711	366	\N	1	\N	1
1094	6712	366	\N	1	\N	1
1095	6731	366	\N	1	\N	1
1096	6770	366	\N	1	\N	1
1097	6771	366	\N	1	\N	1
1098	6772	366	\N	1	\N	1
1099	6773	366	\N	1	\N	1
1100	6830	366	\N	1	\N	1
1101	6831	366	\N	1	\N	1
1102	6832	366	\N	1	\N	1
1103	6833	366	\N	1	\N	1
1104	6890	366	\N	1	\N	1
1105	6891	366	\N	1	\N	1
1106	6892	366	\N	1	\N	1
1107	6911	366	\N	1	\N	1
1108	6941	366	\N	1	\N	1
1109	6942	366	\N	1	\N	1
1110	6984	366	\N	1	\N	1
1111	6985	366	\N	1	\N	1
1112	6673	367	\N	1	\N	1
1113	6674	367	\N	1	\N	1
1114	6675	367	\N	1	\N	1
1115	6676	367	\N	1	\N	1
1116	6677	367	\N	1	\N	1
1117	6678	367	\N	1	\N	1
1118	6732	367	\N	1	\N	1
1119	6733	367	\N	1	\N	1
1120	6734	367	\N	1	\N	1
1121	6735	367	\N	1	\N	1
1122	6736	367	\N	1	\N	1
1123	6737	367	\N	1	\N	1
1124	6792	367	\N	1	\N	1
1125	6793	367	\N	1	\N	1
1126	6794	367	\N	1	\N	1
1127	6795	367	\N	1	\N	1
1128	6796	367	\N	1	\N	1
1129	6797	367	\N	1	\N	1
1130	6852	367	\N	1	\N	1
1131	6853	367	\N	1	\N	1
1132	6854	367	\N	1	\N	1
1133	6855	367	\N	1	\N	1
1134	6856	367	\N	1	\N	1
1135	6857	367	\N	1	\N	1
1136	6912	367	\N	1	\N	1
1137	6913	367	\N	1	\N	1
1138	6914	367	\N	1	\N	1
1139	6915	367	\N	1	\N	1
1140	6916	367	\N	1	\N	1
1141	6917	367	\N	1	\N	1
1142	6986	367	\N	1	\N	1
1143	6987	367	\N	1	\N	1
1144	6988	367	\N	1	\N	1
1145	6989	367	\N	1	\N	1
1146	6990	367	\N	1	\N	1
1147	6991	367	\N	1	\N	1
1148	6679	368	\N	1	\N	1
1149	6680	368	\N	1	\N	1
1150	6681	368	\N	1	\N	1
1151	6682	368	\N	1	\N	1
1152	6738	368	\N	1	\N	1
1153	6739	368	\N	1	\N	1
1154	6740	368	\N	1	\N	1
1155	6741	368	\N	1	\N	1
1156	6798	368	\N	1	\N	1
1157	6799	368	\N	1	\N	1
1158	6800	368	\N	1	\N	1
1159	6801	368	\N	1	\N	1
1160	6858	368	\N	1	\N	1
1161	6859	368	\N	1	\N	1
1162	6860	368	\N	1	\N	1
1163	6861	368	\N	1	\N	1
1164	6918	368	\N	1	\N	1
1165	6919	368	\N	1	\N	1
1166	6920	368	\N	1	\N	1
1167	6921	368	\N	1	\N	1
1168	6968	368	\N	1	\N	1
1169	6969	368	\N	1	\N	1
1170	6992	368	\N	1	\N	1
1171	6993	368	\N	1	\N	1
1174	6994	371	\N	1	\N	1
1175	7000	372	\N	5	\N	1
1176	7011	373	\N	1	\N	1
1177	7012	373	\N	1	\N	1
1178	7013	373	\N	2	\N	1
1179	7014	374	\N	1	\N	1
1180	4525	375	\N	1	\N	1
1181	7019	375	\N	1	\N	1
1182	7020	375	\N	1	\N	1
1183	7021	376	\N	1	\N	1
1192	3664	382	\N	1	\N	1
1193	3673	382	\N	1	\N	1
1194	3674	382	\N	1	\N	1
1195	3948	382	\N	1	\N	1
1196	4997	383	\N	1	\N	1
1197	7202	383	\N	2	\N	1
1198	457	384	\N	1	\N	1
1199	5199	384	\N	1	\N	1
1200	492	384	\N	1	\N	1
1201	2290	384	\N	1	\N	1
1202	5133	384	\N	1	\N	1
1203	5134	384	\N	1	\N	1
1204	7203	385	\N	1	\N	1
1205	7204	385	\N	1	\N	1
1206	7205	385	\N	2	\N	1
1207	7206	386	\N	30	\N	1
1210	4998	389	\N	1	\N	1
1213	7209	392	\N	2	\N	1
1214	5001	393	\N	1	\N	1
1215	2698	394	\N	1	\N	1
1216	7250	394	\N	1	\N	1
1217	7251	394	\N	1	\N	1
1218	7252	394	\N	2	\N	1
1221	5000	397	\N	1	\N	1
1222	7253	397	\N	2	\N	1
1223	7254	398	\N	240	\N	1
1224	5007	399	\N	1	\N	1
1225	7255	399	\N	2	\N	1
1227	4999	400	\N	1	\N	1
1228	5002	400	\N	1	\N	1
1232	7256	401	\N	240	\N	1
1238	7264	404	\N	1	\N	1
1239	7265	404	\N	3	\N	1
1240	7266	404	\N	3	\N	1
1241	7267	404	\N	3	\N	1
1242	7268	405	\N	2	\N	1
1243	6563	406	\N	1	\N	1
1244	6564	406	\N	1	\N	1
1245	6565	406	\N	1	\N	1
1246	6566	406	\N	1	\N	1
1247	4797	407	\N	1	\N	1
1248	7033	408	\N	1	\N	1
1254	5004	411	\N	1	\N	1
1255	7269	412	\N	1	\N	1
1256	7270	412	\N	2	\N	1
1257	7271	412	\N	2	\N	1
1258	7272	412	\N	60	\N	1
1263	7273	414	\N	1	\N	1
1264	7274	414	\N	1	\N	1
1268	7005	417	\N	1	\N	1
1269	7275	418	\N	1	\N	1
1270	7276	418	\N	1	\N	1
1271	7277	418	\N	1	\N	1
1278	4489	422	\N	1	\N	1
1279	7316	422	\N	1	\N	1
1280	7310	422	\N	1	\N	1
1281	7312	422	\N	1	\N	1
1282	5135	422	\N	1	\N	1
1283	5136	422	\N	1	\N	1
1284	7319	423	\N	1	\N	1
1285	7320	423	\N	1	\N	1
1286	7321	423	\N	60	\N	1
1287	7015	424	\N	1	\N	1
1288	7322	424	\N	1	\N	1
1289	7323	425		1	\N	1
1293	7324	426	\N	45	\N	1
1294	7325	426	\N	60	\N	1
1297	7333	429	\N	1	\N	1
1298	7334	429	\N	1	\N	1
1299	7335	429	\N	1	\N	1
1300	7336	429	\N	1	\N	1
1303	7337	432		1	\N	1
1304	7338	432		2	\N	1
1305	7339	432		2	\N	1
1306	7340	432		180	\N	1
1314	7487	435		1	\N	1
1315	7484	435		1	\N	1
1316	7485	435		1	\N	1
1317	7486	435		1	\N	1
1318	5005	436	\N	1	\N	1
1319	7318	437		1	\N	1
1321	7489	437		1	\N	1
1322	2840	437		1	\N	1
1323	6539	437		1	\N	1
1324	4313	437		1	\N	1
1325	7490	437		90	\N	1
1326	7491	437		2	\N	1
1329	7492	440	\N	1	\N	1
1330	7493	440	\N	1	\N	1
1331	7494	440	\N	1	\N	1
1332	5006	441	\N	1	\N	1
1333	7495	441	\N	2	\N	1
1334	7353	442	\N	1	\N	1
1335	6536	442	\N	1	\N	1
1337	7496	444	\N	1	\N	1
1338	7497	444	\N	1	\N	1
1339	7498	444	\N	1	\N	1
1340	7499	445		1	\N	1
1341	7500	445		1	\N	1
1342	7501	445		2	\N	1
1343	7502	445		2	\N	1
1344	7503	445		2	\N	1
1345	7504	445		1	\N	1
1346	7505	445		2	\N	1
1347	5003	446	\N	1	\N	1
1348	7510	447	\N	1	\N	1
1349	7511	448		1	\N	1
1350	7512	448		2	\N	1
1351	7513	448		2	\N	1
1356	7514	452	\N	2	\N	1
1357	7516	453		1	\N	1
1360	7517	455	\N	2	\N	1
1361	7518	456	\N	2	\N	1
1362	7519	456	\N	2	\N	1
1363	7520	456	\N	2	\N	1
1365	7509	90002	\N	1	\N	1
1366	7521	90003	\N	1	\N	1
1367	7522	90003	\N	1	\N	1
1368	7523	90003	\N	1	\N	1
1369	7524	90003	\N	2	\N	1
1370	7525	90003	\N	2	\N	1
1371	7526	90003	\N	2	\N	1
1372	7527	90003	\N	60	\N	1
1373	3723	90004	\N	1	\N	1
1374	3874	90005	\N	1	\N	1
1375	7538	90006	\N	1	\N	1
1376	7539	90006	\N	1	\N	1
1377	7540	90006	\N	120	\N	1
1378	7541	90006	\N	4	\N	1
1379	7542	90007	\N	3	\N	1
1385	7553	90012	\N	1	\N	1
1386	7554	90012	\N	1	\N	1
1387	7555	90012	\N	1	\N	1
1388	7508	90013	\N	1	\N	1
1389	7507	90014	\N	1	\N	1
1390	7556	90015	\N	2	\N	1
1391	7557	90016	\N	3	\N	1
1392	7558	90017	\N	6	\N	1
1397	7559	90019	\N	2	\N	1
1398	7560	90019	\N	2	\N	1
1399	7561	90019	\N	2	\N	1
1400	7018	90020		1	\N	1
1401	7562	90020		1	\N	1
1402	7563	90020		1	\N	1
1403	7284	90021	\N	1	\N	1
1404	995	90021	\N	3	\N	1
1407	7585	90024	\N	2	\N	1
1413	7379	90029		1	\N	1
1414	7586	90029		1	\N	1
1418	7589	90031		1	\N	1
1419	7590	90031		2	\N	1
1420	7591	90031		2	\N	1
1421	7592	90031		120	\N	1
1422	5019	90032		1	\N	1
1423	7595	90033	\N	1	\N	1
1424	7596	90033	\N	1	\N	1
1457	725	90046		1	\N	1
1458	725	90045		1	\N	1
1459	7601	90045		12	\N	1
1460	7602	90045		12	\N	1
1465	7615	90049		1	\N	1
1468	7617	90050		1	\N	1
1469	7616	90050		1	\N	1
1470	7620	90050		2	\N	1
1471	7618	90050		1	\N	1
1472	7619	90050		1	\N	1
1473	7622	90051		1	\N	1
1474	5020	90052		1	\N	1
1475	7623	90052		2	\N	1
1481	7633	90056		1440	\N	1
1482	7630	90056		60	\N	1
1483	7631	90056		2	\N	1
1484	7632	90056		2	\N	1
1485	7548	90057		1	\N	1
1487	4514	90057		1	\N	1
1488	7634	90057		1	\N	1
1489	7635	90058	\N	120	\N	1
1490	7636	90058	\N	2	\N	1
1491	7637	90058	\N	60	\N	1
1492	7506	90059		1	\N	1
1501	5284	90062		1	\N	1
1504	7664	90065		1	\N	1
1506	7742	90066		1	\N	1
1507	7743	90066		1	\N	1
1509	7744	90067		1	\N	1
1510	7663	90068		1	\N	1
1511	7745	90068		2	\N	1
1512	4450	90069		1	\N	1
1513	4581	90069		1	\N	1
1514	4888	90069		1	\N	1
1515	7746	90069		1	\N	1
1519	7828	90073		1	\N	1
1520	7829	90073		10	\N	1
1521	7830	90074		1	\N	1
1522	7831	90075		1	\N	1
1523	1009	90076		1	\N	1
1524	4163	90076		1	\N	1
1527	7852	90078		1	\N	1
1528	7853	90079		1	\N	1
1531	7854	90082		1	\N	1
1532	7855	90082		10	\N	1
1533	7856	90082		4	\N	1
1535	7857	90083	"	1	\N	1
1536	7035	90084		1	\N	1
1537	7858	90085		1	\N	1
1538	7859	90085		1	\N	1
1539	7860	90085		1	\N	1
1545	7861	90088		90	\N	1
1546	7862	90088		246	\N	1
1548	7894	90090		1	\N	1
1549	7895	90090		2	\N	1
1550	7896	90090		1	\N	1
1551	7897	90090		2	\N	1
1552	7898	90090		2	\N	1
1553	7899	90091		1	\N	1
1554	7900	90091		1	\N	1
1555	7901	90091		1	\N	1
1556	7902	90091		2	\N	1
1557	7903	90091		2	\N	1
1558	7904	90092		1	\N	1
1559	7905	90092		2	\N	1
1560	7906	90092		1	\N	1
1561	7907	90092		2	\N	1
1562	7908	90092		2	\N	1
1564	7909	90093		1	\N	1
1565	7910	90093		1	\N	1
1566	7911	90093		2	\N	1
1567	7912	90093		2	\N	1
1568	7913	90094		1	\N	1
1569	7914	90094		1	\N	1
1570	7915	90094		2	\N	1
1571	7916	90094		2	\N	1
1573	7661	90095		1	\N	1
1574	7882	90096		1	\N	1
1575	7662	90097		1	\N	1
1576	7874	90098		1	\N	1
1577	7883	90099		1	\N	1
1583	7917	90102		1	\N	1
1584	7918	90102		2	\N	1
1589	7919	90104		2	\N	1
1590	7920	90104		1	\N	1
1591	7921	90105		1	\N	1
1592	7922	90105		1	\N	1
1594	7923	90070		4000	\N	1
1595	7638	90060		70	\N	1
1596	7639	90060		60	\N	1
1597	7640	90060		1	\N	1
1609	7924	90112		60	\N	1
1610	7925	90112		1	\N	1
1611	7926	90112		60	\N	1
1617	7931	90115		90	\N	1
1618	7932	90115		1	\N	1
1619	7933	90115		1	\N	1
1620	7934	90115		60	\N	1
1621	7935	90114		30	\N	1
1622	7928	90114		1	\N	1
1623	7929	90114		60	\N	1
1628	7939	90118		20	\N	1
1629	7937	90118		1	\N	1
1630	7938	90118		2	\N	1
1632	7940	90119		1	\N	1
1633	7941	90119		1	\N	1
1644	7942	90123		1	\N	1
1645	7943	90123		2	\N	1
1646	7944	90123		2	\N	1
1653	7875	100005		1	\N	1
1656	7880	100008		1	\N	1
1657	7881	100009		1	\N	1
1658	7959	100010		1	\N	1
1659	7960	100010		2	\N	1
1660	7961	100010		60	\N	1
1661	7962	100010		2	\N	1
1662	7963	100011		1	\N	1
1663	7964	100011		60	\N	1
1664	7965	100011		2	\N	1
1665	7506	100012		1	\N	1
1666	7966	100013		1	\N	1
1675	7879	100018		1	\N	1
1676	7970	100019		1	\N	1
1677	7971	100019		1	\N	1
1678	7972	100019		60	\N	1
1685	7973	100022		2	\N	1
1686	7986	100023		2	\N	1
1687	6559	100024		1	\N	1
1690	7997	100025		1	\N	1
1701	7998	100031		1	\N	1
1702	7999	100031		1	\N	1
1704	8000	100031		1	\N	1
1709	8001	100035		1	\N	1
1710	8002	100035		1	\N	1
1711	8004	100035		1	\N	1
1712	7983	100036		1	\N	1
1721	8016	100041		1	\N	1
1722	8017	100041		1	\N	1
1723	8018	100042		1	\N	1
1724	8019	100042		1	\N	1
1725	8020	100042		1	\N	1
1720	8015	100040		1	\N	1
1745	\N	100048	gogo	4	222	0
1746	8043	100048	\N	3	7440	1
1747	\N	100048	pokus	2	22	0
1748	\N	100048	polozka1	1	11	0
1749	\N	100047	Osazení desky MRT31DIU včetně dodaných součástek	196	45	0
1750	\N	100047	Osazovací program pro MRT31DIU	1	1600	0
1751	1481	100050	\N	6	40	0
1752	\N	100050	pol1	3	\N	0
1753	\N	100050	pol2	1	\N	0
1754	\N	100050	pol3	2	\N	0
1755	\N	100051	osazení RVA-102E	250	35	\N
1756	\N	100051	osazovací program pro RVA-102E	1	3200	\N
1757	8048	100052		1	\N	\N
1758	8049	100052		1	\N	\N
1759	8050	100052		1	\N	\N
1760	8051	100053		1	\N	\N
1761	\N	100053	Za vyřízení reklamací 10-0053, 10-0054, 10-0055 učtujeme  1000,- 	1	\N	\N
1762	8052	100054		2	\N	\N
1763	8063	100055		2	\N	\N
1764	\N	100056	osazení ZBI	84	21	\N
1765	\N	100056	osazovací program ZBI 	1	800	\N
1766	\N	100056	set součástek	1	1465	\N
1768	\N	100058	ruční osazení prototypů	2	3300	\N
1769	\N	100059	slučovač 24 V (návrh, výroba, osazení)	5	1480	\N
1771	\N	100057	slučovač 24 V (návrh, výroba, osazení) 	5	1480	\N
1772	\N	100061	expresní příplatek 40%	1	22286	\N
1774	\N	100061	osazení ATP 101 EBOR	130	55	\N
1775	\N	100061	osazení ATP 101 LBR 	504	41	\N
1776	\N	100061	osazení GIV	250	38	\N
1777	\N	100061	osazovací program pro ATP 101 EBOR	1	8600	\N
1778	\N	100061	osazovací program pro ATP 101 LBR 	1	5000	\N
1779	\N	100061	osazovací program pro GIV	1	4800	\N
1773	\N	100061	Kondenzátor 100n 603	3000	0.699999999999999956	\N
1780	7625	100062		1	\N	\N
1781	\N	100063	osazení AT05H20	429	85	\N
1782	\N	100064	propojovací drát 5.8311.509/02	1000	1	\N
1783	\N	100064	propojovací drát 5.8311.509/04	1000	2	\N
1784	\N	100064	propojovací drát 5.8311.509/05	1000	2	\N
1785	\N	100064	propojovací drát 5.8311.510/01	1000	3	\N
1786	\N	100065	Dynablot v4.1	63	5591	\N
1787	\N	100066	propojovací drát 5.8311.509/06 	1000	2	\N
1788	\N	100066	propojovací drát 5.8311.509/08	1000	2	\N
1789	7878	100067		1	\N	\N
1793	\N	100069	Osazení PS ED0372A 	120	30	\N
1794	\N	100069	osazovací program  ED0372A 	1	800	\N
1795	5263	100068		1	\N	\N
1796	8065	100068		1	\N	\N
1797	\N	100070	osazovací program pro SGB01	1	5600	\N
1798	\N	100070	Osazování SGB01	198	253	\N
1799	\N	100070	pájení vlnou SVB01 80 ks, LGREG01 100 ks	1	4000	\N
1800	\N	100070	příprava vlny	1	800	\N
1801	\N	100071	osazovací program pro ATP-101 LBT	1	3200	\N
1802	\N	100071	SMD osazování ATP-101 LBT	252	37	\N
1805	\N	100073	chybějící součástky	56	1	\N
1806	\N	100073	laser_rx_05 	4	260	\N
1807	\N	100073	laser_tx_05 	8	240	\N
1808	\N	100074	osazení PWS-73N	224	18	\N
1809	\N	100074	příprava výroby	1	400	\N
1810	\N	100072	osazení 4100-009	60	344	\N
1811	\N	100072	příprava výroby	3	400	\N
1812	\N	100075	osazení GIV2/2	150	39	\N
1813	\N	100075	připrava výroby GIV2/2	1	1600	\N
1814	\N	100075	úprava programu GIV2/2 	1	1200	\N
1815	\N	100076	osazení ZRM01-3	125	19	\N
1816	\N	100076	osazovací program pro ZRM01-3	1	1200	\N
1817	\N	100076	příprava výroby	1	400	\N
1818	8066	100077		1	\N	\N
1819	\N	100078	osazení vzorků dynablot 7 pump včetně součástek 	36	590	\N
1820	\N	100079	stínící přípravek 600 Kč	3	\N	\N
1821	\N	100080	osazení PWRS-3	96	28	\N
1822	\N	100080	příprava výroby	1	1200	\N
1823	8067	100081		1	\N	\N
1824	\N	100082	propojka 5.8311.509/7 	1800	2	\N
1825	\N	100083	stínící přípravek 600 Kč 	3	\N	\N
1826	\N	100084	GPRS COM	20	70	\N
1827	\N	100084	set součástek na 20 ks	1	3009	\N
1828	8081	110001		1	\N	\N
1830	\N	100085	příprava výroby	1	1600	\N
1829	\N	100085	osazení RVA - 102E	200	35	\N
1831	\N	110002	Dynablot v4.1 	60	5591	\N
1833	\N	100060	slučovač 24 V (návrh, výroba, osazení) 	1	0.100000000000000006	\N
1834	8083	110004		1	\N	\N
1835	8082	110004		3	\N	\N
1836	\N	110005	Dyna vývěva včetně kabeláže	20	525	\N
1837	\N	110005	Dynawash otočný přepínač s kabeláží	20	106	\N
1838	\N	110005	Dynawash úprava desky, nové filmy	1	2000	\N
1839	\N	110005	Dynawash včetně kabeláže	20	972	\N
1840	\N	110003	výroba plošných spojů pro ochranné moduly OMS 1.9-1.16 A OMJ 1.17	1	7400	\N
1841	8084	110006		20	\N	\N
1842	\N	110006	výměna 2x tranzistor 2x zenerka 1x transil 2x usmernovaci dioda 	1	\N	\N
1843	\N	110007	osazování ATP-101LBT	192	37	\N
1844	\N	110007	příprava výroby ATP-101LBT 	1	1600	\N
1845	\N	110008	 pájení vzorků	14	150	\N
1847	\N	110010	osazení RZV01	36	63	\N
1848	\N	110010	osazovací program RZV01	1	1200	\N
1849	\N	110010	příprava výroby 	1	800	\N
1853	\N	110012	osazení BT 4100-0004	55	192	\N
1854	\N	110012	příprava výroby 	1	1500	\N
1855	\N	110013	pájení IO (3 desky DCPS306)	30	70	\N
1856	\N	110014	SMD osazení Robofood (0,084EU)	3000	2	\N
1857	\N	110009	SMT osazení PC100 (0,77EU)	1165	18.5599999999999987	\N
1858	\N	110011	příprava výroby	1	1500	\N
1859	\N	110011	R 4k7 0805 	560	\N	\N
1860	\N	110011	SMT osazení PC100 (0,77EU)	335	18.5599999999999987	\N
\.


--
-- Data for Name: productstatus; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY productstatus (id, statusname) FROM stdin;
1	Produkt vyrobený, otestovaný, plně funkční
2	Produkt vadný neopravitelný
3	Produkt vadný opravitelný
4	Produkt vrácen dodavateli
5	Produkt předán zákazníkovi
6	Fiktivní čarový kód
18	Produkt přijat k reklamaci
19	Produkt vadný neopravitelný
7	Produkt vyrobený neotestovaný
21	Produkt zamontován jako součást jiného produktu
23	Produkt v zápůjčce
25	Produkt v opravě
0	Neznami stav
26	Motážní práce
\.


--
-- Data for Name: producttypes; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY producttypes (id, name, serialnumber, version, productcomment, barcodetitle, price, inevidence, quantity, units, unit, authorized, status, deleted, servicegroupid, service, quantity2, price2, productcategoryid, pricelist, author) FROM stdin;
1004	test gigo	\N	1.1.5	test	TEST	\N	1	1	\N	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
1006	pokus2	\N	1.1.1			\N	1	1	\N	1	\N	\N	t	\N	f	\N	4000	\N	t	\N
1002	pokus	\N	2.2.2			\N	1	1	\N	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
1001	ahoj2	\N	1.1.1			\N	1	1	\N	1	\N	\N	t	\N	f	\N	\N	\N	t	\N
119	Neznámý10	\N	1.0.0	\N	NEZ10	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
118	Neznámý09	\N	1.0.0	\N	NEZ09	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
114	Blok datového rozhraní 100Mb/s DA-SDM10-DE	\N	1.0.0	\N	DA-SDM10-DE/100	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
113	Blok radiomodemu RD-SDM10-DE/100Mbps	\N	1.0.0	\N	RD-SDM10-DE/100	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
117	Anténa 100Mbps 120 cm	\N	1.0.0	\N	ANt 120/100	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
115	Anténa 100Mbps 35 cm	\N	1.0.0	\N	Ant 35/100	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
116	Anténa 100Mbps 65 cm	\N	1.0.0	\N	Ant 65/100	\N	1	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
42	Analýza závady u neoprávněné reklamace	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	t	4	t	\N	\N	\N	t	\N
40	Balení antény (pouze čas)	\N	1.0.0		\N	60	0	15	x	4	\N	\N	t	7	t	15	\N	\N	t	\N
102	Další služby	\N	1.0.0	\N	\N	0	0	0	min	3	\N	\N	t	4	t	\N	\N	\N	t	\N
101	Doprava	\N	1.0.0	\N	\N	12	0	0	km	2	\N	\N	t	2	t	\N	\N	\N	t	\N
41	Expresní služby odeslání a doprava – dle tarifu kilometrovné	\N	1.0.0	\N	\N	5	0	0	km	3	\N	\N	t	3	t	\N	\N	\N	t	\N
49	Hodina testování bez klimatiky	\N	1.0.0	\N	\N	2	0	0	min	3	\N	\N	t	5	t	\N	\N	\N	t	\N
50	Hodina testování v klimatické komoře	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	t	8	t	\N	\N	\N	t	\N
28	Kompletace spoje 	\N	1.0.0	\N	\N	400	0	40	x	4	\N	\N	t	10	t	40	\N	\N	t	\N
43	Konzultace	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	t	8	t	\N	\N	\N	t	\N
48	Manipulační práce (balení, rozebírání, čištění, …)	\N	1.0.0	\N	\N	5	0	0	min	3	\N	\N	t	6	t	\N	\N	\N	t	\N
25	Měření spoje	\N	1.0.0	\N	\N	1200	0	60	x	4	\N	\N	t	8	t	60	\N	\N	t	\N
44	Montáž / demontáž radomu	\N	1.0.0	\N	\N	75	0	15	x	4	\N	\N	t	6	t	15	\N	\N	t	\N
37	Montáž / demontáž elektroniky z antény	\N	1.0.0	\N	\N	100	0	20	x	4	\N	\N	t	6	t	20	\N	\N	t	\N
47	Montážní práce	\N	1.0.0	\N	\N	10	0	0	min	3	\N	\N	t	10	t	\N	\N	\N	t	\N
39	Oprava antény (pouze čas)	\N	1.0.0	\N	\N	10	0	0	min	3	\N	\N	t	10	t	\N	\N	\N	t	\N
38	Příjem a čištění antény	\N	1.0.0	\N	\N	225	0	45	x	4	\N	\N	t	6	t	45	\N	\N	t	\N
31	Přeladění mikrovlny na vývojovém pracovišti 	\N	1.0.0	\N	\N	200	0	10	x	3	\N	\N	t	11	t	\N	\N	\N	t	\N
29	Přezkoušení spoje technikem	\N	1.0.0	\N	\N	1000	0	50	x	4	\N	\N	t	8	t	50	\N	\N	t	\N
30	Přeladění mikrovlny komunikátorem	\N	1.0.0	\N	\N	200	0	10	x	4	\N	\N	t	8	t	10	\N	\N	t	\N
46	Práce na měřícím pracovišti	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	t	8	t	\N	\N	\N	t	\N
45	Práce na mikrovlnné soupravě 	\N	1.0.0	\N	\N	50	0	0	min	3	\N	\N	t	11	t	\N	\N	\N	t	\N
26	Testování spoje (zahoření)	\N	1.0.0	\N	\N	2	0	0	min	3	\N	\N	t	5	t	\N	\N	\N	t	\N
27	Testování spoje (zahoření) v klimatické komoře	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	t	8	t	\N	\N	\N	t	\N
36	Výměna antény	\N	1.0.0	sloučit s řádkem 51	\N	560	0	0	x	\N	\N	\N	t	\N	t	\N	\N	\N	t	\N
51	Výměna antény	\N	1.0.0	\N	\N	560	0	0	x	4	\N	\N	t	1	t	35	\N	\N	t	\N
33	Změna firmwaru v deskách (včetně přezkoušení)	\N	1.0.0	\N	\N	1200	0	60	x	4	\N	\N	t	8	t	60	\N	\N	t	\N
32	Změna firmwaru v mikrovlně	\N	1.0.0	\N	\N	3000	0	60	x	4	\N	\N	t	11	t	60	\N	\N	t	\N
34	Změna polarizace spoje – 2 antény (demontáž radomu, otočení, přezkoušení, instalace radomu)	\N	1.0.0	\N	\N	600	0	60	x	4	\N	\N	t	10	t	60	\N	\N	t	\N
24	Výroba antény 	\N	1.0.0	\N	\N	300	0	30	x	4	\N	\N	t	10	t	30	\N	\N	t	\N
35	Výměna kleštin	\N	1.0.0	\N	\N	100	0	20	x	4	\N	\N	t	10	t	10	\N	\N	t	\N
58	Box pro elektroniku	\N	1.0.0	sloučit s 68	\N	399.95	0	0	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
81	Box pro elektroniku	\N	1.0.0	sloučit s 68	\N	400	0	0	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
68	Box pro elektroniku	\N	1.0.0		\N	400	0	0	ks	1	\N	\N	t	\N	f	\N	400	\N	t	\N
93	Datová deska	\N	1.0.0	\N	\N	10000	0	0	ks	1	\N	\N	t	\N	f	\N	10000	\N	t	\N
86	Kleštiny malé	\N	1.0.0	\N	\N	652	0	0	ks	1	\N	\N	t	\N	f	\N	652	\N	t	\N
85	Kleštiny velké	\N	1.0.0	\N	\N	1052	0	0	ks	1	\N	\N	t	\N	f	\N	1052	\N	t	\N
84	Kompletní anténa 120	\N	1.0.0	\N	\N	18244.03	0	0	ks	1	\N	\N	t	\N	f	\N	18244	\N	t	\N
71	Kompletní anténa 35	\N	1.0.0	\N	\N	7440.03	0	0	ks	1	\N	\N	t	\N	f	\N	7440	\N	t	\N
61	Kompletní anténa 65	\N	1.0.0	\N	\N	9659.03	0	0	ks	1	\N	\N	t	\N	f	\N	9660	\N	t	\N
92	Ladící deska	\N	1.0.0	\N	\N	7500	0	0	ks	1	\N	\N	t	\N	f	\N	7500	\N	t	\N
6	Mechanika antény 120	\N	1.0.0	z ceníku odstranit	MECHANTENA120	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
4	Mechanika antény 35	\N	1.0.0	z ceníku odstranit	MECHANTENA35	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
5	Mechanika antény 60	\N	1.0.0	z ceníku odstranit	MECHANTENA60	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
64	Mechanika paraboly 35	\N	1.0.0	\N	\N	3038	0	0	ks	1	\N	\N	t	\N	f	\N	3038	\N	t	\N
74	Mechanika paraboly 120	\N	1.0.0	\N	\N	4129	0	0	ks	1	\N	\N	t	\N	f	\N	4130	\N	t	\N
54	Mechanika paraboly 65	\N	1.0.0	\N	\N	2884	0	0	ks	1	\N	\N	t	\N	f	\N	2884	\N	t	\N
94	Měrná přijímací jednotka	\N	1.0.0	\N	\N	0	0	0	ks	1	\N	\N	t	\N	f	\N	15000	\N	t	\N
90	Mikrovlnná jednotka	\N	1.0.0	\N	\N	80000	0	0	ks	1	\N	\N	t	\N	f	\N	80000	\N	t	\N
66	Objímka ozařovače	\N	1.0.0	sloučit s řádkem 79	\N	213	0	0	ks	0	\N	\N	t	\N	f	\N	\N	\N	t	\N
56	Objímka ozařovače	\N	1.0.0	sloučit s řádkem 79	\N	213	0	0	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
79	Objímka ozařovače	\N	1.0.0	\N	\N	213	0	0	ks	1	\N	\N	t	\N	f	\N	213	\N	t	\N
73	Parabola 120	\N	1.0.0	\N	\N	4809	0	0	ks	1	\N	\N	t	\N	f	\N	4810	\N	t	\N
63	Parabola 35	\N	1.0.0	\N	\N	1312	0	0	ks	1	\N	\N	t	\N	f	\N	1312	\N	t	\N
53	Parabola 65	\N	1.0.0	\N	\N	2676	0	0	ks	1	\N	\N	t	\N	f	\N	2676	\N	t	\N
99	Poštovné ČP profibalík	\N	1.0.0	\N	\N	300	0	0	x	1	\N	\N	t	\N	f	\N	200	\N	t	\N
88	Přechodka F/BNC	\N	1.0.0	\N	\N	80	0	0	ks	1	\N	\N	t	\N	f	\N	80	\N	t	\N
70	Propojovací kabely	\N	1.0.0	sloučit s řádkem 60	\N	250	0	0	ks	1	\N	\N	t	\N	f	\N	\N	\N	t	\N
83	Propojovací kabely	\N	1.0.0	sloučit s řádkem 60	\N	250	0	0	ks	1	\N	\N	t	\N	f	\N	\N	\N	t	\N
60	Propojovací kabely	\N	1.0.0	\N	\N	250	0	0	ks	1	\N	\N	t	\N	f	\N	250	\N	t	\N
69	Průchodky pg21	\N	1.0.0	odstranit z ceníku	\N	24	0	0	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
82	Průchodky pg21	\N	1.0.0	odstranit z ceníku	\N	24	0	0	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
59	Průchodka pg21 + matka	\N	1.0.0	\N	\N	27.08	0	0	ks	1	\N	\N	t	\N	f	\N	28	\N	t	\N
91	Radiová deska	\N	1.0.0	\N	\N	5000	0	0	ks	1	\N	\N	t	\N	f	\N	5000	\N	t	\N
76	Radom 120	\N	1.0.0	\N	\N	6868	0	0	ks	1	\N	\N	t	\N	f	\N	6868	\N	t	\N
19	Radom 120cm	\N	1.0.0	odstranit z ceníku	RADOM 120	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
67	Radom 35	\N	1.0.0	\N	\N	1507	0	0	ks	1	\N	\N	t	\N	f	\N	1510	\N	t	\N
57	Radom 65	\N	1.0.0	\N	\N	2532	0	0	ks	1	\N	\N	t	\N	f	\N	2530	\N	t	\N
9	Sada pro konvertor	\N	1.0.0	odstranit z ceníku	\N	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
12	Sada pro mikrovlnou jednotku	\N	1.0.0	odstranit z ceníku	\N	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
11	Sada pro mechaniku	\N	1.0.0	odstranit z ceníku	\N	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
7	Sada pro skříň	\N	1.0.0	odstranit z ceníku	\N	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
8	Sada pro mikrovlny filtr	\N	1.0.0	odstranit z ceníku	\N	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
10	Sada pro vysílač	\N	1.0.0	odstranit z ceníku	\N	\N	0	1	ks	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
98	Spojovací materiál	\N	1.0.0	\N	\N	40	0	0	ks	1	\N	\N	t	\N	f	\N	40	\N	t	\N
75	Stavěcí tyčky objímky ozařovače 120	\N	1.0.0	\N	\N	264	0	0	ks	1	\N	\N	t	\N	f	\N	264	\N	t	\N
65	Stavěcí tyčky objímky ozařovače 35	\N	1.0.0	\N	\N	198	0	0	ks	1	\N	\N	t	\N	f	\N	198	\N	t	\N
55	Stavěcí tyčky objímky ozařovače 65	\N	1.0.0	\N	\N	239	0	0	ks	1	\N	\N	t	\N	f	\N	240	\N	t	\N
87	Transformátor 230V/21V	\N	1.0.0	\N	\N	793.2	0	0	ks	1	\N	\N	t	\N	f	\N	794	\N	t	\N
95	Transportní obal na anténu 35	\N	1.0.0	\N	\N	646	0	0	ks	1	\N	\N	t	\N	f	\N	646	\N	t	\N
96	Transportní obal na anténu 65	\N	1.0.0	\N	\N	690	0	0	ks	1	\N	\N	t	\N	f	\N	690	\N	t	\N
97	Transportní obal na transformátory	\N	1.0.0	\N	\N	0	0	0	ks	1	\N	\N	t	\N	f	\N	20	\N	t	\N
100	Transportní obal ostatní	\N	1.0.0		\N	70	0	0	x	1	\N	\N	t	0	f	\N	50	\N	t	\N
78	Plastové podložky	\N	1.0.0	\N	\N	66	0	0	ks	1	\N	\N	t	\N	f	\N	66	\N	t	\N
77	Vzpěry paraboly 120 3ks	\N	1.0.0	\N	\N	600	0	0	ks	1	\N	\N	t	\N	f	\N	600	\N	t	\N
80	Uchycení objímky ozařovače	\N	1.0.0	\N	\N	316	0	0	ks	1	\N	\N	t	\N	f	\N	316	\N	t	\N
52	Upgrade z 25 Mbit na 50 Mbit	\N	1.0.0	:-)))	\N	0	0	0	x	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
1000	ahoj	\N	1.0.1			\N	1	1	\N	\N	\N	\N	t	\N	f	\N	\N	\N	t	\N
1509	Změna polarizace spoje – 2 antény (demontáž radomu, otočení, přezkoušení, instalace radomu)	\N	1.0.0	\N	\N	600	0	60	x	4	\N	\N	f	10	t	60	\N	5	t	\N
1508	Změna firmwaru v mikrovlně	\N	1.0.0	\N	\N	3000	0	60	x	4	\N	\N	f	11	t	60	\N	5	t	\N
1507	Změna firmwaru v deskách (včetně přezkoušení)	\N	1.0.0	\N	\N	1200	0	60	x	4	\N	\N	f	8	t	60	\N	5	t	\N
1500	Přezkoušení spoje technikem	\N	1.0.0	\N	\N	1000	0	50	x	4	\N	\N	f	8	t	50	\N	5	t	\N
1481	Analýza závady u neoprávněné reklamace	\N	1.0.0	\N	\N	10	0	0	min	3	\N	\N	f	7	t	\N	\N	5	t	\N
1482	Balení antény (pouze čas)	\N	1.0.0	\N	\N	60	0	15	x	4	\N	\N	f	7	t	15	\N	5	t	\N
1483	Další služby	\N	1.0.0	\N	\N	0	0	0	min	3	\N	\N	f	4	t	\N	\N	5	t	\N
1484	Doprava	\N	1.0.0	\N	\N	12	0	0	km	2	\N	\N	f	2	t	\N	\N	5	t	\N
1485	Expresní služby odeslání a doprava – dle tarifu kilometrovné	\N	1.0.0	\N	\N	5	0	0	km	3	\N	\N	f	3	t	\N	\N	5	t	\N
1486	Testování bez klimatiky	\N	1.0.0	\N	\N	2	0	0	min	3	\N	\N	f	5	t	\N	\N	5	t	\N
1487	Testování v klimatické komoře	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	f	8	t	\N	\N	5	t	\N
1488	Kompletace spoje 	\N	1.0.0	\N	\N	400	0	40	x	4	\N	\N	f	10	t	40	\N	5	t	\N
1489	Konzultace	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	f	8	t	\N	\N	5	t	\N
1490	Manipulační práce (balení, rozebírání, čištění, …)	\N	1.0.0	\N	\N	5	0	0	min	3	\N	\N	f	6	t	\N	\N	5	t	\N
1491	Měření spoje	\N	1.0.0	\N	\N	1200	0	60	x	4	\N	\N	f	8	t	60	\N	5	t	\N
1492	Montáž / demontáž elektroniky z antény	\N	1.0.0	\N	\N	100	0	20	x	4	\N	\N	f	6	t	20	\N	5	t	\N
1493	Montáž / demontáž radomu	\N	1.0.0	\N	\N	75	0	15	x	4	\N	\N	f	6	t	15	\N	5	t	\N
1494	Montážní práce	\N	1.0.0	\N	\N	10	0	0	min	3	\N	\N	f	10	t	\N	\N	5	t	\N
1495	Oprava antény (pouze čas)	\N	1.0.0	\N	\N	10	0	0	min	3	\N	\N	f	10	t	\N	\N	5	t	\N
1496	Práce na měřícím pracovišti	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	f	8	t	\N	\N	5	t	\N
1497	Práce na mikrovlnné soupravě 	\N	1.0.0	\N	\N	50	0	0	min	3	\N	\N	f	11	t	\N	\N	5	t	\N
1498	Přeladění mikrovlny komunikátorem	\N	1.0.0	\N	\N	200	0	10	x	4	\N	\N	f	8	t	10	\N	5	t	\N
1499	Přeladění mikrovlny na vývojovém pracovišti 	\N	1.0.0	\N	\N	200	0	10	x	3	\N	\N	f	11	t	\N	\N	5	t	\N
1501	Příjem a čištění antény	\N	1.0.0	\N	\N	225	0	45	x	4	\N	\N	f	6	t	45	\N	5	t	\N
1502	Testování spoje (zahoření)	\N	1.0.0	\N	\N	2	0	0	min	3	\N	\N	f	5	t	\N	\N	5	t	\N
1503	Testování spoje (zahoření) v klimatické komoře	\N	1.0.0	\N	\N	20	0	0	min	3	\N	\N	f	8	t	\N	\N	5	t	\N
1504	Výměna antény	\N	1.0.0	\N	\N	560	0	0	x	4	\N	\N	f	1	t	35	\N	5	t	\N
1505	Výměna kleštin	\N	1.0.0	\N	\N	100	0	20	x	4	\N	\N	f	10	t	10	\N	5	t	\N
1506	Výroba antény 	\N	1.0.0	\N	\N	300	0	30	x	4	\N	\N	f	10	t	30	\N	5	t	\N
1621	Transportní obal na anténu 65	\N	1.0.0	\N	\N	690	0	0	ks	1	\N	\N	f	\N	f	\N	690	2	t	\N
1618	Stavěcí tyčky objímky ozařovače 65	\N	1.0.0	\N	\N	239	0	0	ks	1	\N	\N	f	\N	f	\N	240	2	t	\N
1614	Radom 65	\N	1.0.0	\N	\N	2532	0	0	ks	1	\N	\N	f	\N	f	\N	2530	2	t	\N
1605	Parabola 65	\N	1.0.0	\N	\N	2676	0	0	ks	1	\N	\N	f	\N	f	\N	2676	2	t	\N
1599	Mechanika paraboly 65	\N	1.0.0	\N	\N	2884	0	0	ks	1	\N	\N	f	\N	f	\N	2884	2	t	\N
1595	Kompletní anténa 65	\N	1.0.0	\N	\N	9659.03	0	0	ks	1	\N	\N	f	\N	f	\N	9660	2	t	\N
1616	Stavěcí tyčky objímky ozařovače 120	\N	1.0.0	\N	\N	264	0	0	ks	1	\N	\N	f	\N	f	\N	264	3	t	\N
1625	Vzpěry paraboly 120 3ks	\N	1.0.0	\N	\N	600	0	0	ks	1	\N	\N	f	\N	f	\N	600	3	t	\N
1612	Radom 120	\N	1.0.0	\N	\N	6868	0	0	ks	1	\N	\N	f	\N	f	\N	6868	3	t	\N
1603	Parabola 120	\N	1.0.0	\N	\N	4809	0	0	ks	1	\N	\N	f	\N	f	\N	4810	3	t	\N
1597	Mechanika paraboly 120	\N	1.0.0	\N	\N	4129	0	0	ks	1	\N	\N	f	\N	f	\N	4130	3	t	\N
1593	Kompletní anténa 120	\N	1.0.0	\N	\N	18244.03	0	0	ks	1	\N	\N	f	\N	f	\N	18244	3	t	\N
1620	Transportní obal na anténu 35	\N	1.0.0	\N	\N	646	0	0	ks	1	\N	\N	f	\N	f	\N	646	1	t	\N
1617	Stavěcí tyčky objímky ozařovače 35	\N	1.0.0	\N	\N	198	0	0	ks	1	\N	\N	f	\N	f	\N	198	1	t	\N
1613	Radom 35	\N	1.0.0	\N	\N	1507	0	0	ks	1	\N	\N	f	\N	f	\N	1510	1	t	\N
1604	Parabola 35	\N	1.0.0	\N	\N	1312	0	0	ks	1	\N	\N	f	\N	f	\N	1312	1	t	\N
1598	Mechanika paraboly 35	\N	1.0.0	\N	\N	3038	0	0	ks	1	\N	\N	f	\N	f	\N	3038	1	t	\N
1594	Kompletní anténa 35	\N	1.0.0	\N	\N	7440.03	0	0	ks	1	\N	\N	f	\N	f	\N	7440	1	t	\N
125	FPD malá deska	\N	1.0.0	Kusovnik Micro, zrušit v ceníku	FPD_MALA_DESKA	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
1622	Transportní obal na transformátory	\N	1.0.0	\N	\N	0	0	0	ks	1	\N	\N	f	\N	f	\N	20	4	t	\N
1623	Transportní obal ostatní	\N	1.0.0	\N	\N	70	0	0	x	1	\N	\N	f	0	f	\N	50	4	t	\N
121	Jukebox	\N	1.0.0	Kusovnik Micro	JUKEBOX	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
112	Mikrovlnný konvertor RX-SDM10-DE/100Mbps	\N	1.0.0	smazat pepik?	RX-SDM10-DE/100	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
111	Mikrovlnný vysílač TX-SDM10-DE/100Mbps 	\N	1.0.0	smazat pepik?	TX-SDM10-DE/100	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
110	Mikrovlnný sdružovač MS-SDM10-DE/100Mbps	\N	1.0.0	smazat pepik?	MD-SDM10-DE/100	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
109	GIGO	\N	1.0.0	zrušit v ceníku	GIGO	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
108	Dynablot	\N	1.0.0	Kusovnik Micro	DYNABLOT	\N	1	1	ks	1	\N	\N	f	\N	f	\N	5070	\N	f	\N
107	HE100RXTX-8	\N	1.0.0	smazat pepik?	HE100RXTX-8	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
106	HE100TX-8	\N	1.0.0	smazat pepik?	HE100TX-8	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
105	HE100RX-8	\N	1.0.0	smazat pepik?	HE100RX-8	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
104	Vysílač 12NF (verze 2)	\N	2.0.0	kusovnik	VYSILAC12NF (V2)	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
1	Konvertor	\N	3	doplnit cenu	KONVERTOR3	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
2	MINIFE95, datová deska	\N	9.5	\N	MINIFE95	10000	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
3	INT_FE2-25-12, radiová deska	\N	20	\N	INTFE20	5000	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
14	Vysílač 12NF	\N	1.0.0	kusovnik	VYSILAC12NF	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
13	Vysílač 11VF	\N	1.0.0	kusovnik	VYSILAC11VF	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
17	Stanice spoje	\N	\N	struktura	STANICE	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
15	Mikrovlnný vysílač	\N	1.0.0	\N	\N	15000	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
16	Mikrovlnná jednotka	\N	1.0.0	\N	MICROWAVES MW UNIT	80000	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
20	HE-100-7	\N	1.7	smazat Pepik?	HE100-7	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
21	Ladicí radiová deska	\N	1.0.0	\N	LRD-100	7500	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
18	Mikrovlnný spoj	\N	\N	doplnit cenu	MIKROVLNNY SPOJ	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
123	IKT-UCM-00B	\N	1.0.0	Kusovnik Micro	IKT-UCM-00B	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
23	Mikrovlnný filtr	\N	1.0.0	smazat pepik?	FILTR	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
22	Měrná přijímací jednotka 10GHz	\N	1.0.0	\N	MER PRIJIMAC 10GHZ	15000	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
126	FPD CIM\n	\N	1.0.0	Kusovnik Micro, cena bez součástek	FPD CIM\n	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	469	\N	f	\N
120	Gigo SC1-SVM_HE2-03A-04	\N	1.0.0	zrušit v ceníku	GIGO HE2-03A-04	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
103	Vysílač 11VF (verze 2) 	\N	2.0.0	kusovnik	VYSILAC11VF (V2)	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
122	XC103-202 CAMERA LINK interface BOM C2	\N	1.0.0	Kusovnik Micro	XC103-202	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
124	Dynawash	\N	1.0.0	Kusovnik Micro, zrušit v ceníku	DYNAWASH	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
127	Dyva - vývěva	\N	1.0.0	Kusovnik Micro, zrušit v ceníku	DYVA-VYVEVA	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
999	Pokus	\N	1.0.0	\N	POKUS	\N	1	1	ks	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
1003	Gigo V6 vyskladnit	\N	1.1.6	leden 2010 yru		\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
1005	Radiovka_2009	\N	1.0.0	listopad 2009		\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	f	\N
1008	pokus radiovka 2010 V10	\N	1.0.0	vyroba prototypu		\N	1	1	\N	2	\N	\N	f	3	f	55	1	\N	f	\N
1009	Dynex magnetický senzor	\N	1.0.0	Kusovnik Micro,Dynex magnetický senzor		\N	1	1	\N	0	\N	\N	f	0	f	0	\N	\N	f	\N
1600	Měrná přijímací jednotka (bez seriového čísla)	\N	1.0.0	\N	\N	0	0	0	ks	1	\N	\N	f	\N	f	\N	15000	4	t	\N
1601	Mikrovlnná jednotka (bez seriového čísla)	\N	1.0.0	\N	\N	80000	0	0	ks	1	\N	\N	f	\N	f	\N	80000	4	t	\N
1602	Objímka ozařovače	\N	1.0.0	\N	\N	213	0	0	ks	1	\N	\N	f	\N	f	\N	213	4	t	\N
1589	Box pro elektroniku	\N	1.0.0		\N	400	0	0	ks	1	\N	\N	f	\N	f	\N	400	4	t	\N
1590	Datová deska (bez seriového čísla)	\N	1.0.0	\N	\N	10000	0	0	ks	1	\N	\N	f	\N	f	\N	10000	4	t	\N
1591	Kleštiny malé	\N	1.0.0	\N	\N	652	0	0	ks	1	\N	\N	f	\N	f	\N	652	4	t	\N
1592	Kleštiny velké	\N	1.0.0	\N	\N	1052	0	0	ks	1	\N	\N	f	\N	f	\N	1052	4	t	\N
1596	Ladící deska (bez seriového čísla)	\N	1.0.0	\N	\N	7500	0	0	ks	1	\N	\N	f	\N	f	\N	7500	4	t	\N
1606	Plastové podložky	\N	1.0.0	\N	\N	66	0	0	ks	1	\N	\N	f	\N	f	\N	66	4	t	\N
1607	Poštovné ČP profibalík	\N	1.0.0	\N	\N	300	0	0	x	1	\N	\N	f	\N	f	\N	200	4	t	\N
1608	Přechodka F/BNC	\N	1.0.0	\N	\N	80	0	0	ks	1	\N	\N	f	\N	f	\N	80	4	t	\N
1609	Propojovací kabely	\N	1.0.0	\N	\N	250	0	0	ks	1	\N	\N	f	\N	f	\N	250	4	t	\N
1610	Průchodka pg21 + matka	\N	1.0.0	\N	\N	27.08	0	0	ks	1	\N	\N	f	\N	f	\N	28	4	t	\N
1611	Radiová deska (bez seriového čísla)	\N	1.0.0	\N	\N	5000	0	0	ks	1	\N	\N	f	\N	f	\N	5000	4	t	\N
1619	Transformátor 230V/21V	\N	1.0.0	\N	\N	793.2	0	0	ks	1	\N	\N	f	\N	f	\N	794	4	t	\N
1615	Spojovací materiál	\N	1.0.0	\N	\N	40	0	0	ks	1	\N	\N	f	\N	f	\N	40	4	t	\N
1624	Uchycení objímky ozařovače	\N	1.0.0	\N	\N	316	0	0	ks	1	\N	\N	f	\N	f	\N	316	4	t	\N
1629	test	\N	1.1.1		aaa	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	99999
1628	Vysílač VF - TX_MIX_V1 - 29.9.2010	\N	1.0.0	směšovaný vysílač 10GHz, mikrovlnná část, první verze		\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	f	5
1627	Radiovka 4.8.2010	\N	1.0.0	Radiovka 4.8.2010	Radiovka 4.8.2010	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	f	1
1626	Gigo V7	\N	1.0.0	29.7.2010	Gigo V7	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	f	14
1631	Radiovka_V15 25.10.2010	\N	1.0.0			\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	14
1630	Vysílač NF - TX_12NFmix1_V2 - 29.9.2010	\N	1.0.0		vysilac NF 29.9.2010	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	1
1010	Dynex periferie 7 pump	\N	1.0.0	Kusovnik Micro, zrušit v ceníku	periferie 7 pump	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	f	1
1632	Radiovka_V17 19.11.2010 	\N	1.0.0	doladit	Radiovka_V17	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	14
1633	Dynawash	\N	1.0.2		DYNAWASH	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	99999
1007	Dynablot v4	\N	4.0.0	Kusovnik Micro, unor 2010	Dynablot v4	\N	1	1	\N	\N	\N	\N	f	\N	f	0	5100	\N	f	1
1634	Radiovka_V19	\N	1.0.0			\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	14
1635	testik	\N	1.0.0	\N	\N	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	14
1636	testik2	\N	1.0.0	\N	\N	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	14
1637	Dyna vývěva	\N	1.0.0	24.1.2011	Dyna vyveva	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	1
1639	Dynawash kabeláž	\N	1.0.0	24.1.2011	Dynawash kabelaz	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	1
1638	Dyna vývěva kabeláž	\N	1.0.0	24.1.2011	Dyna vyveva kabelaz	\N	1	1	\N	\N	\N	\N	f	\N	f	\N	\N	\N	t	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: svm
--

COPY users (id, name, surname, username, userpassword, email, deleted, role, companyid) FROM stdin;
9	Jiří 		georg	\N	\N	f	customer	\N
10000	root		root	59606609c6f2b0f4ac81167fe123c3f1	jiri.chludil@svm.cz\r\n	f	customer	\N
18	Eva		eva	\N	\N	f	customer	\N
17	Antonín		tonda	\N	\N	t	customer	\N
8	Ladislav		lada	69b01e21587f4b537a567d0c0025618a	\N	f	customer	\N
10	Jiří2		jyrgen	e59fb86f7694aae502e6706497b8895d	\N	f	customer	\N
15	Barbora		bara	\N	\N	f	customer	\N
14	Tomáš		tomas	f6ef80827f3d8d7aae66f804fae171ca	\N	f	customer	\N
16	Michal		michal	\N	\N	t	customer	\N
19	Jiří		sdvds	0cc175b9c0f1b6a831c399e269772661	jiri.chludil@svm.cz\r\n	f	customer	\N
99999	Admin		xx	\N	jiri.chludil@svm.cz\r\n	f	customer	\N
11	Vladimír 		vlada	\N	\N	f	customer	\N
6	senior		tata	49d02d55ad10973b7b9d0dc9eba7fdf0	\N	f	customer	\N
13	Radka		radka	cb0447e9c75448bbaee0ecd300e47ea8	\N	t	customer	\N
1	Jan		hanz	9b014050156005036f00d84c3ba507b1	\N	f	customer	\N
5	Josef		pepik	8827d52c113d97159c79be8c745f42b0	\N	f	customer	\N
100000	supdanie	supdanie	supdanie	$2y$10$3aFbOHUTa4dH36FIfmA.juiCNFVtkl9nhh4C.bXquT.tyvlI140ni	supdaniel@seznam.cz	f	customer	\N
\.


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: deliverynotes_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_pkey PRIMARY KEY (id);


--
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: productcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY productcategory
    ADD CONSTRAINT productcategory_pkey PRIMARY KEY (id);


--
-- Name: productorder_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT productorder_pkey PRIMARY KEY (id);


--
-- Name: products_pkey1; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey1 PRIMARY KEY (id);


--
-- Name: productsondeliverynote_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY productsondeliverynote
    ADD CONSTRAINT productsondeliverynote_pkey PRIMARY KEY (id);


--
-- Name: productstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY productstatus
    ADD CONSTRAINT productstatus_pkey PRIMARY KEY (id);


--
-- Name: producttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY producttypes
    ADD CONSTRAINT producttypes_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: svm; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: deliverynotes_approveduserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_approveduserid_fkey FOREIGN KEY (approveduserid) REFERENCES users(id);


--
-- Name: deliverynotes_companyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_companyid_fkey FOREIGN KEY (companyid) REFERENCES companies(id);


--
-- Name: deliverynotes_companyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_companyid_fkey FOREIGN KEY (companyid) REFERENCES companies(id);


--
-- Name: deliverynotes_expeditionuserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_expeditionuserid_fkey FOREIGN KEY (expeditionuserid) REFERENCES users(id);


--
-- Name: deliverynotes_invoiceuserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_invoiceuserid_fkey FOREIGN KEY (invoiceuserid) REFERENCES users(id);


--
-- Name: deliverynotes_paiduserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_paiduserid_fkey FOREIGN KEY (paiduserid) REFERENCES users(id);


--
-- Name: deliverynotes_paiduserid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_paiduserid_fkey FOREIGN KEY (paiduserid) REFERENCES users(id);


--
-- Name: deliverynotes_productorderid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorderitem
    ADD CONSTRAINT deliverynotes_productorderid_fkey FOREIGN KEY (productorderid) REFERENCES productorders(id);


--
-- Name: deliverynotes_recieverid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorders
    ADD CONSTRAINT deliverynotes_recieverid_fkey FOREIGN KEY (recieverid) REFERENCES users(id);


--
-- Name: deliverynotes_userid2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_userid2_fkey FOREIGN KEY (userid2) REFERENCES users(id);


--
-- Name: deliverynotes_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY deliverynotes
    ADD CONSTRAINT deliverynotes_userid_fkey FOREIGN KEY (userid) REFERENCES users(id);


--
-- Name: deliverynotesproductid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productorderitem
    ADD CONSTRAINT deliverynotesproductid_fkey FOREIGN KEY (productid) REFERENCES products(id);


--
-- Name: products_barcoderecipientid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_barcoderecipientid_fkey FOREIGN KEY (barcoderecipientid) REFERENCES users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: products_locationid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_locationid_fkey FOREIGN KEY (locationid) REFERENCES locations(id);


--
-- Name: products_ownerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_ownerid_fkey FOREIGN KEY (ownerid) REFERENCES users(id);


--
-- Name: products_producttypeid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_producttypeid_fkey FOREIGN KEY (producttypeid) REFERENCES producttypes(id);


--
-- Name: products_statusid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_statusid_fkey FOREIGN KEY (statusid) REFERENCES productstatus(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: productsondeliverynote_deliverynoteid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productsondeliverynote
    ADD CONSTRAINT productsondeliverynote_deliverynoteid_fkey FOREIGN KEY (deliverynoteid) REFERENCES deliverynotes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: productsondeliverynote_productid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY productsondeliverynote
    ADD CONSTRAINT productsondeliverynote_productid_fkey FOREIGN KEY (productid) REFERENCES products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: producttypes_productcategoryid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY producttypes
    ADD CONSTRAINT producttypes_productcategoryid_fkey FOREIGN KEY (productcategoryid) REFERENCES productcategory(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: users_companyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: svm
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_companyid_fkey FOREIGN KEY (companyid) REFERENCES companies(id);


--
-- Name: companies; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE companies FROM PUBLIC;
REVOKE ALL ON TABLE companies FROM svm;
GRANT ALL ON TABLE companies TO svm;
GRANT ALL ON TABLE companies TO postgres;


--
-- Name: deliverynotes; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE deliverynotes FROM PUBLIC;
REVOKE ALL ON TABLE deliverynotes FROM svm;
GRANT ALL ON TABLE deliverynotes TO svm;
GRANT ALL ON TABLE deliverynotes TO postgres;


--
-- Name: locations; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE locations FROM PUBLIC;
REVOKE ALL ON TABLE locations FROM svm;
GRANT ALL ON TABLE locations TO svm;
GRANT ALL ON TABLE locations TO postgres;


--
-- Name: products; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE products FROM PUBLIC;
REVOKE ALL ON TABLE products FROM svm;
GRANT ALL ON TABLE products TO svm;
GRANT ALL ON TABLE products TO postgres;


--
-- Name: productsondeliverynote; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE productsondeliverynote FROM PUBLIC;
REVOKE ALL ON TABLE productsondeliverynote FROM svm;
GRANT ALL ON TABLE productsondeliverynote TO svm;
GRANT ALL ON TABLE productsondeliverynote TO postgres;


--
-- Name: productstatus; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE productstatus FROM PUBLIC;
REVOKE ALL ON TABLE productstatus FROM svm;
GRANT ALL ON TABLE productstatus TO svm;
GRANT ALL ON TABLE productstatus TO postgres;


--
-- Name: producttypes; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE producttypes FROM PUBLIC;
REVOKE ALL ON TABLE producttypes FROM svm;
GRANT ALL ON TABLE producttypes TO svm;
GRANT ALL ON TABLE producttypes TO postgres;


--
-- Name: users; Type: ACL; Schema: public; Owner: svm
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM svm;
GRANT ALL ON TABLE users TO svm;
GRANT ALL ON TABLE users TO postgres;


--
-- PostgreSQL database dump complete
--

